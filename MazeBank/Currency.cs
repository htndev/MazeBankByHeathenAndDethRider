﻿using System;
using System.Drawing;
using System.Windows.Forms;
using CurrencyLibrary;

namespace MazeBank
{
    public partial class Currency : Form
    {
        public Currency()
        {
            InitializeComponent();
            currencyToConvert.SelectedIndex = 0;
        }

        #region Переменные

        CurrencyExg exg = new CurrencyExg();

        MainMenu mainMenu = new MainMenu();

        public long login;

        public bool theme;

        #endregion Переменные

        private void Currency_Load(object sender, EventArgs e)
        {
            if (System.Net.NetworkInformation.NetworkInterface.GetIsNetworkAvailable())
            {
                today.Text = DateTime.Now.ToShortDateString();
                buyUSD.Text = exg.toBuyUsd.ToString();
                sellUsd.Text = exg.toSellUsd.ToString();
                buyEur.Text = exg.toBuyEur.ToString();
                sellEur.Text = exg.toSellEur.ToString();
                buyGbp.Text = exg.toBuyGbp.ToString();
                sellGbp.Text = exg.toSellGbp.ToString();
                if (theme)
                {
                    foreach (Control control in this.Controls)
                    {
                        if (control is Label)
                        {
                            ((Label)control).ForeColor = Color.White;
                        }
                    }
                    exgStripes.Load(@"C:\Users\aleks\source\repos\MazeBank\Pics\exgWhite.png");
                    exitBut.Load(@"C:\Users\aleks\source\repos\MazeBank\ExitPics\exitBlackTheme.png");
                    backBut.Load(@"C:\Users\aleks\source\repos\MazeBank\Pics\backWhite.png");
                    this.BackColor = Color.FromArgb(56, 56, 56);
                    convertorBox.BackColor = Color.FromArgb(80, 80, 80);
                    convertorBox.ForeColor = Color.White;
                    convertationResult.BackColor = Color.FromArgb(80, 80, 80);
                    convertationResult.ForeColor = Color.White;
                }
            }
            else
            {
                MessageBox.Show("Мы не можем подключиться к источнику. Попробуйте позже.", "Ошибка соединения", MessageBoxButtons.OK, MessageBoxIcon.Error);
                this.Close();
                mainMenu.login = login;
                mainMenu.Show();
                return;
            }
        }

        private void exitBut_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void backBut_Click(object sender, EventArgs e)
        {
            Currency currency = new Currency();
            Close();
            mainMenu.login = login;
            mainMenu.Show();
        }

        private void convertorBox_TextChanged(object sender, EventArgs e)
        {
            decimal value;
            try
            {
                if (currencyToConvert.SelectedIndex == 0)
                {
                    value = Math.Round(decimal.Parse(convertorBox.Text.Replace(".", ",")));
                    convertationResult.Text = (value * (decimal)exg.toBuyUsd).ToString("F2");
                }
                else if (currencyToConvert.SelectedIndex == 1)
                {
                    value = Math.Round(decimal.Parse(convertorBox.Text.Replace(".", ",")));
                    convertationResult.Text = (value * (decimal)exg.toBuyEur).ToString("F2");
                }
                else if (currencyToConvert.SelectedIndex == 2)
                {
                    value = Math.Round(decimal.Parse(convertorBox.Text.Replace(".", ",")));
                    convertationResult.Text = (value * (decimal)exg.toBuyGbp).ToString("F2");
                }
            }
            catch (FormatException)
            {
                convertationResult.Text = "0";
            }
        }

        private void Currency_KeyDown(object sender, KeyEventArgs e)
        {
            if(e.KeyCode == Keys.Escape)
            {
                Currency currency = new Currency();
                Close();
                mainMenu.login = login;
                mainMenu.Show();
            }
        }
    }
}