﻿namespace MazeBank
{
    partial class MainMenu
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainMenu));
            this.exitBut = new System.Windows.Forms.PictureBox();
            this.yourCard = new System.Windows.Forms.Label();
            this.UserCard = new System.Windows.Forms.Label();
            this.CheckHistory = new System.Windows.Forms.Button();
            this.user = new System.Windows.Forms.Label();
            this.sendMoney = new System.Windows.Forms.PictureBox();
            this.rechargePhone = new System.Windows.Forms.PictureBox();
            this.Help = new System.Windows.Forms.ToolTip(this.components);
            this.settings = new System.Windows.Forms.PictureBox();
            this.callCenter = new System.Windows.Forms.PictureBox();
            this.currency = new System.Windows.Forms.PictureBox();
            this.utilites = new System.Windows.Forms.PictureBox();
            this.internetAndTV = new System.Windows.Forms.PictureBox();
            this.onlinePayments = new System.Windows.Forms.PictureBox();
            this.tickets = new System.Windows.Forms.PictureBox();
            this.gamesPayments = new System.Windows.Forms.PictureBox();
            this.mazeFine = new System.Windows.Forms.PictureBox();
            this.panel = new System.Windows.Forms.PictureBox();
            this.moneyPanel = new System.Windows.Forms.Panel();
            this.balanceOfUser = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.exitBut)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sendMoney)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.rechargePhone)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.settings)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.callCenter)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.currency)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.utilites)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.internetAndTV)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.onlinePayments)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tickets)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gamesPayments)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.mazeFine)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panel)).BeginInit();
            this.moneyPanel.SuspendLayout();
            this.SuspendLayout();
            // 
            // exitBut
            // 
            this.exitBut.Cursor = System.Windows.Forms.Cursors.Hand;
            this.exitBut.Image = ((System.Drawing.Image)(resources.GetObject("exitBut.Image")));
            this.exitBut.Location = new System.Drawing.Point(358, 12);
            this.exitBut.Name = "exitBut";
            this.exitBut.Size = new System.Drawing.Size(34, 34);
            this.exitBut.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.exitBut.TabIndex = 11;
            this.exitBut.TabStop = false;
            this.exitBut.Click += new System.EventHandler(this.exitBut_Click);
            // 
            // yourCard
            // 
            this.yourCard.AutoSize = true;
            this.yourCard.Font = new System.Drawing.Font("Verdana", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.yourCard.ForeColor = System.Drawing.Color.DarkRed;
            this.yourCard.Location = new System.Drawing.Point(134, 88);
            this.yourCard.Name = "yourCard";
            this.yourCard.Size = new System.Drawing.Size(152, 25);
            this.yourCard.TabIndex = 12;
            this.yourCard.Text = "Ваша карта";
            // 
            // UserCard
            // 
            this.UserCard.AutoSize = true;
            this.UserCard.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.UserCard.ForeColor = System.Drawing.Color.Red;
            this.UserCard.Location = new System.Drawing.Point(43, 140);
            this.UserCard.Name = "UserCard";
            this.UserCard.Size = new System.Drawing.Size(101, 18);
            this.UserCard.TabIndex = 13;
            this.UserCard.Text = "**** 9999";
            // 
            // CheckHistory
            // 
            this.CheckHistory.FlatAppearance.BorderSize = 0;
            this.CheckHistory.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.CheckHistory.Font = new System.Drawing.Font("Verdana", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.CheckHistory.Location = new System.Drawing.Point(68, 174);
            this.CheckHistory.Name = "CheckHistory";
            this.CheckHistory.Size = new System.Drawing.Size(275, 40);
            this.CheckHistory.TabIndex = 15;
            this.CheckHistory.Text = "История";
            this.CheckHistory.UseVisualStyleBackColor = true;
            this.CheckHistory.Click += new System.EventHandler(this.CheckHistory_Click);
            // 
            // user
            // 
            this.user.AutoSize = true;
            this.user.Font = new System.Drawing.Font("Verdana", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.user.Location = new System.Drawing.Point(107, 57);
            this.user.Name = "user";
            this.user.Size = new System.Drawing.Size(0, 23);
            this.user.TabIndex = 16;
            this.user.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // sendMoney
            // 
            this.sendMoney.Cursor = System.Windows.Forms.Cursors.Hand;
            this.sendMoney.Image = ((System.Drawing.Image)(resources.GetObject("sendMoney.Image")));
            this.sendMoney.Location = new System.Drawing.Point(20, 246);
            this.sendMoney.Name = "sendMoney";
            this.sendMoney.Size = new System.Drawing.Size(80, 80);
            this.sendMoney.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.sendMoney.TabIndex = 17;
            this.sendMoney.TabStop = false;
            this.Help.SetToolTip(this.sendMoney, "Перевести деньги");
            this.sendMoney.Click += new System.EventHandler(this.sendMoney_Click);
            // 
            // rechargePhone
            // 
            this.rechargePhone.Cursor = System.Windows.Forms.Cursors.Hand;
            this.rechargePhone.Image = ((System.Drawing.Image)(resources.GetObject("rechargePhone.Image")));
            this.rechargePhone.Location = new System.Drawing.Point(121, 246);
            this.rechargePhone.Name = "rechargePhone";
            this.rechargePhone.Size = new System.Drawing.Size(80, 80);
            this.rechargePhone.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.rechargePhone.TabIndex = 18;
            this.rechargePhone.TabStop = false;
            this.Help.SetToolTip(this.rechargePhone, "Пополнить мобильный");
            this.rechargePhone.Click += new System.EventHandler(this.rechargePhone_Click);
            // 
            // settings
            // 
            this.settings.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.settings.Cursor = System.Windows.Forms.Cursors.Hand;
            this.settings.Image = ((System.Drawing.Image)(resources.GetObject("settings.Image")));
            this.settings.Location = new System.Drawing.Point(351, 515);
            this.settings.Name = "settings";
            this.settings.Size = new System.Drawing.Size(40, 40);
            this.settings.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.settings.TabIndex = 20;
            this.settings.TabStop = false;
            this.Help.SetToolTip(this.settings, "Настройки");
            this.settings.Click += new System.EventHandler(this.settings_Click);
            // 
            // callCenter
            // 
            this.callCenter.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.callCenter.Cursor = System.Windows.Forms.Cursors.Hand;
            this.callCenter.Image = ((System.Drawing.Image)(resources.GetObject("callCenter.Image")));
            this.callCenter.Location = new System.Drawing.Point(184, 513);
            this.callCenter.Name = "callCenter";
            this.callCenter.Size = new System.Drawing.Size(45, 44);
            this.callCenter.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.callCenter.TabIndex = 21;
            this.callCenter.TabStop = false;
            this.Help.SetToolTip(this.callCenter, "Звонок техподдержке");
            this.callCenter.Click += new System.EventHandler(this.callCenter_Click);
            // 
            // currency
            // 
            this.currency.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.currency.Cursor = System.Windows.Forms.Cursors.Hand;
            this.currency.Image = ((System.Drawing.Image)(resources.GetObject("currency.Image")));
            this.currency.Location = new System.Drawing.Point(12, 513);
            this.currency.Name = "currency";
            this.currency.Size = new System.Drawing.Size(45, 44);
            this.currency.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.currency.TabIndex = 22;
            this.currency.TabStop = false;
            this.Help.SetToolTip(this.currency, "Курс валют");
            this.currency.Click += new System.EventHandler(this.currency_Click);
            // 
            // utilites
            // 
            this.utilites.Cursor = System.Windows.Forms.Cursors.Hand;
            this.utilites.Image = ((System.Drawing.Image)(resources.GetObject("utilites.Image")));
            this.utilites.Location = new System.Drawing.Point(217, 246);
            this.utilites.Name = "utilites";
            this.utilites.Size = new System.Drawing.Size(80, 80);
            this.utilites.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.utilites.TabIndex = 24;
            this.utilites.TabStop = false;
            this.Help.SetToolTip(this.utilites, "Оплатить услуги ЖКХ");
            this.utilites.Click += new System.EventHandler(this.utilites_Click);
            // 
            // internetAndTV
            // 
            this.internetAndTV.Cursor = System.Windows.Forms.Cursors.Hand;
            this.internetAndTV.Image = ((System.Drawing.Image)(resources.GetObject("internetAndTV.Image")));
            this.internetAndTV.Location = new System.Drawing.Point(311, 246);
            this.internetAndTV.Name = "internetAndTV";
            this.internetAndTV.Size = new System.Drawing.Size(80, 80);
            this.internetAndTV.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.internetAndTV.TabIndex = 25;
            this.internetAndTV.TabStop = false;
            this.Help.SetToolTip(this.internetAndTV, "Оплатить Интернет и Телевиденье");
            this.internetAndTV.Click += new System.EventHandler(this.internetAndTV_Click);
            // 
            // onlinePayments
            // 
            this.onlinePayments.Cursor = System.Windows.Forms.Cursors.Hand;
            this.onlinePayments.Image = ((System.Drawing.Image)(resources.GetObject("onlinePayments.Image")));
            this.onlinePayments.Location = new System.Drawing.Point(20, 366);
            this.onlinePayments.Name = "onlinePayments";
            this.onlinePayments.Size = new System.Drawing.Size(80, 80);
            this.onlinePayments.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.onlinePayments.TabIndex = 26;
            this.onlinePayments.TabStop = false;
            this.Help.SetToolTip(this.onlinePayments, "Пополнение онлайн кошельков");
            this.onlinePayments.Click += new System.EventHandler(this.onlinePayments_Click);
            // 
            // tickets
            // 
            this.tickets.Cursor = System.Windows.Forms.Cursors.Hand;
            this.tickets.Image = ((System.Drawing.Image)(resources.GetObject("tickets.Image")));
            this.tickets.Location = new System.Drawing.Point(121, 366);
            this.tickets.Name = "tickets";
            this.tickets.Size = new System.Drawing.Size(80, 80);
            this.tickets.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.tickets.TabIndex = 27;
            this.tickets.TabStop = false;
            this.Help.SetToolTip(this.tickets, "Покупка билетов через MazeTickets");
            this.tickets.Click += new System.EventHandler(this.tickets_Click);
            // 
            // gamesPayments
            // 
            this.gamesPayments.Cursor = System.Windows.Forms.Cursors.Hand;
            this.gamesPayments.Image = ((System.Drawing.Image)(resources.GetObject("gamesPayments.Image")));
            this.gamesPayments.Location = new System.Drawing.Point(217, 366);
            this.gamesPayments.Name = "gamesPayments";
            this.gamesPayments.Size = new System.Drawing.Size(80, 80);
            this.gamesPayments.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.gamesPayments.TabIndex = 28;
            this.gamesPayments.TabStop = false;
            this.Help.SetToolTip(this.gamesPayments, "Пополнение игрового баланса");
            this.gamesPayments.Click += new System.EventHandler(this.gamesPayments_Click);
            // 
            // mazeFine
            // 
            this.mazeFine.Cursor = System.Windows.Forms.Cursors.Hand;
            this.mazeFine.Image = ((System.Drawing.Image)(resources.GetObject("mazeFine.Image")));
            this.mazeFine.Location = new System.Drawing.Point(312, 366);
            this.mazeFine.Name = "mazeFine";
            this.mazeFine.Size = new System.Drawing.Size(80, 80);
            this.mazeFine.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.mazeFine.TabIndex = 29;
            this.mazeFine.TabStop = false;
            this.Help.SetToolTip(this.mazeFine, "Оплата штрафов");
            this.mazeFine.Click += new System.EventHandler(this.mazeTaxi_Click);
            // 
            // panel
            // 
            this.panel.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.panel.Location = new System.Drawing.Point(-6, 507);
            this.panel.Name = "panel";
            this.panel.Size = new System.Drawing.Size(417, 62);
            this.panel.TabIndex = 19;
            this.panel.TabStop = false;
            // 
            // moneyPanel
            // 
            this.moneyPanel.Controls.Add(this.balanceOfUser);
            this.moneyPanel.Location = new System.Drawing.Point(151, 140);
            this.moneyPanel.Name = "moneyPanel";
            this.moneyPanel.Size = new System.Drawing.Size(241, 31);
            this.moneyPanel.TabIndex = 30;
            // 
            // balanceOfUser
            // 
            this.balanceOfUser.AutoSize = true;
            this.balanceOfUser.Dock = System.Windows.Forms.DockStyle.Right;
            this.balanceOfUser.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.balanceOfUser.Location = new System.Drawing.Point(112, 0);
            this.balanceOfUser.Name = "balanceOfUser";
            this.balanceOfUser.Size = new System.Drawing.Size(129, 18);
            this.balanceOfUser.TabIndex = 0;
            this.balanceOfUser.Text = "9999999.99 ₴";
            // 
            // MainMenu
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(404, 561);
            this.ControlBox = false;
            this.Controls.Add(this.moneyPanel);
            this.Controls.Add(this.mazeFine);
            this.Controls.Add(this.gamesPayments);
            this.Controls.Add(this.tickets);
            this.Controls.Add(this.onlinePayments);
            this.Controls.Add(this.internetAndTV);
            this.Controls.Add(this.utilites);
            this.Controls.Add(this.currency);
            this.Controls.Add(this.callCenter);
            this.Controls.Add(this.settings);
            this.Controls.Add(this.panel);
            this.Controls.Add(this.rechargePhone);
            this.Controls.Add(this.sendMoney);
            this.Controls.Add(this.user);
            this.Controls.Add(this.CheckHistory);
            this.Controls.Add(this.UserCard);
            this.Controls.Add(this.yourCard);
            this.Controls.Add(this.exitBut);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.KeyPreview = true;
            this.Name = "MainMenu";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Главное меню";
            this.Load += new System.EventHandler(this.MainMenu_Load);
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.MainMenu_KeyDown);
            ((System.ComponentModel.ISupportInitialize)(this.exitBut)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sendMoney)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.rechargePhone)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.settings)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.callCenter)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.currency)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.utilites)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.internetAndTV)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.onlinePayments)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tickets)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gamesPayments)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.mazeFine)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.panel)).EndInit();
            this.moneyPanel.ResumeLayout(false);
            this.moneyPanel.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.PictureBox exitBut;
        private System.Windows.Forms.Label yourCard;
        private System.Windows.Forms.Label UserCard;
        private System.Windows.Forms.Button CheckHistory;
        private System.Windows.Forms.Label user;
        private System.Windows.Forms.PictureBox sendMoney;
        private System.Windows.Forms.ToolTip Help;
        private System.Windows.Forms.PictureBox rechargePhone;
        private System.Windows.Forms.PictureBox panel;
        private System.Windows.Forms.PictureBox settings;
        private System.Windows.Forms.PictureBox callCenter;
        private System.Windows.Forms.PictureBox currency;
        private System.Windows.Forms.PictureBox utilites;
        private System.Windows.Forms.PictureBox internetAndTV;
        private System.Windows.Forms.PictureBox onlinePayments;
        private System.Windows.Forms.PictureBox tickets;
        private System.Windows.Forms.PictureBox gamesPayments;
        private System.Windows.Forms.PictureBox mazeFine;
        private System.Windows.Forms.Panel moneyPanel;
        private System.Windows.Forms.Label balanceOfUser;
    }
}