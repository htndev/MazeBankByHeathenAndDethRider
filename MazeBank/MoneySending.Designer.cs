﻿namespace MazeBank
{
    partial class MoneySending
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MoneySending));
            this.backBut = new System.Windows.Forms.PictureBox();
            this.exitBut = new System.Windows.Forms.PictureBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.underRecipient = new System.Windows.Forms.PictureBox();
            this.label3 = new System.Windows.Forms.Label();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.sendMoney = new System.Windows.Forms.Button();
            this.MaxAmount = new System.Windows.Forms.ToolTip(this.components);
            this.recipient_textBox = new System.Windows.Forms.TextBox();
            this.sendingAmount = new System.Windows.Forms.TextBox();
            this.billToPrint = new System.Windows.Forms.PrintDialog();
            ((System.ComponentModel.ISupportInitialize)(this.backBut)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.exitBut)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.underRecipient)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // backBut
            // 
            this.backBut.Cursor = System.Windows.Forms.Cursors.Hand;
            this.backBut.Image = ((System.Drawing.Image)(resources.GetObject("backBut.Image")));
            this.backBut.Location = new System.Drawing.Point(12, 12);
            this.backBut.Name = "backBut";
            this.backBut.Size = new System.Drawing.Size(34, 34);
            this.backBut.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.backBut.TabIndex = 14;
            this.backBut.TabStop = false;
            this.backBut.Click += new System.EventHandler(this.backBut_Click);
            // 
            // exitBut
            // 
            this.exitBut.Cursor = System.Windows.Forms.Cursors.Hand;
            this.exitBut.Image = ((System.Drawing.Image)(resources.GetObject("exitBut.Image")));
            this.exitBut.Location = new System.Drawing.Point(358, 12);
            this.exitBut.Name = "exitBut";
            this.exitBut.Size = new System.Drawing.Size(34, 34);
            this.exitBut.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.exitBut.TabIndex = 15;
            this.exitBut.TabStop = false;
            this.exitBut.Click += new System.EventHandler(this.exitBut_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Verdana", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label1.Location = new System.Drawing.Point(101, 49);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(218, 25);
            this.label1.TabIndex = 16;
            this.label1.Text = "Перевед средств";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label2.Location = new System.Drawing.Point(42, 92);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(336, 18);
            this.label2.TabIndex = 17;
            this.label2.Text = "Введите карту, кому хотите перевести:";
            // 
            // underRecipient
            // 
            this.underRecipient.BackColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.underRecipient.Location = new System.Drawing.Point(63, 162);
            this.underRecipient.Name = "underRecipient";
            this.underRecipient.Size = new System.Drawing.Size(294, 2);
            this.underRecipient.TabIndex = 44;
            this.underRecipient.TabStop = false;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Verdana", 12.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label3.Location = new System.Drawing.Point(31, 233);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(175, 20);
            this.label3.TabIndex = 45;
            this.label3.Text = "Сумма к отправке:";
            // 
            // pictureBox1
            // 
            this.pictureBox1.BackColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.pictureBox1.Location = new System.Drawing.Point(210, 256);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(147, 2);
            this.pictureBox1.TabIndex = 47;
            this.pictureBox1.TabStop = false;
            // 
            // sendMoney
            // 
            this.sendMoney.BackColor = System.Drawing.Color.Transparent;
            this.sendMoney.FlatAppearance.BorderSize = 0;
            this.sendMoney.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.sendMoney.Font = new System.Drawing.Font("Verdana", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.sendMoney.ForeColor = System.Drawing.Color.Red;
            this.sendMoney.Location = new System.Drawing.Point(131, 295);
            this.sendMoney.Name = "sendMoney";
            this.sendMoney.Size = new System.Drawing.Size(142, 61);
            this.sendMoney.TabIndex = 48;
            this.sendMoney.TabStop = false;
            this.sendMoney.Text = "Первести";
            this.sendMoney.UseVisualStyleBackColor = false;
            this.sendMoney.Click += new System.EventHandler(this.sendMoney_Click);
            // 
            // MaxAmount
            // 
            this.MaxAmount.Tag = "ОГРАНИЧЕНИЯ!";
            // 
            // recipient_textBox
            // 
            this.recipient_textBox.BackColor = System.Drawing.SystemColors.Control;
            this.recipient_textBox.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.recipient_textBox.Font = new System.Drawing.Font("Verdana", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.recipient_textBox.ForeColor = System.Drawing.SystemColors.WindowFrame;
            this.recipient_textBox.Location = new System.Drawing.Point(64, 131);
            this.recipient_textBox.Name = "recipient_textBox";
            this.recipient_textBox.Size = new System.Drawing.Size(290, 30);
            this.recipient_textBox.TabIndex = 49;
            this.recipient_textBox.TabStop = false;
            this.recipient_textBox.Text = "Карта получателя";
            this.recipient_textBox.Click += new System.EventHandler(this.recipient_textBox_Click);
            this.recipient_textBox.TextChanged += new System.EventHandler(this.recipient_textBox_TextChanged);
            this.recipient_textBox.KeyDown += new System.Windows.Forms.KeyEventHandler(this.recipient_textBox_KeyDown);
            this.recipient_textBox.Leave += new System.EventHandler(this.recipient_textBox_Leave);
            // 
            // sendingAmount
            // 
            this.sendingAmount.BackColor = System.Drawing.SystemColors.Control;
            this.sendingAmount.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.sendingAmount.Font = new System.Drawing.Font("Verdana", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.sendingAmount.ForeColor = System.Drawing.SystemColors.WindowFrame;
            this.sendingAmount.Location = new System.Drawing.Point(212, 228);
            this.sendingAmount.Name = "sendingAmount";
            this.sendingAmount.Size = new System.Drawing.Size(144, 26);
            this.sendingAmount.TabIndex = 50;
            this.sendingAmount.TabStop = false;
            this.sendingAmount.Text = "Сумма";
            this.sendingAmount.Click += new System.EventHandler(this.sendingAmount_Click);
            this.sendingAmount.TextChanged += new System.EventHandler(this.sendingAmount_TextChanged);
            this.sendingAmount.KeyDown += new System.Windows.Forms.KeyEventHandler(this.sendingAmount_KeyDown);
            this.sendingAmount.Leave += new System.EventHandler(this.sendingAmount_Leave);
            // 
            // billToPrint
            // 
            this.billToPrint.UseEXDialog = true;
            // 
            // MoneySending
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(404, 561);
            this.Controls.Add(this.sendingAmount);
            this.Controls.Add(this.recipient_textBox);
            this.Controls.Add(this.sendMoney);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.underRecipient);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.exitBut);
            this.Controls.Add(this.backBut);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.KeyPreview = true;
            this.Name = "MoneySending";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Отправка денежных средств";
            this.Load += new System.EventHandler(this.MoneySending_Load);
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.MoneySending_KeyDown);
            ((System.ComponentModel.ISupportInitialize)(this.backBut)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.exitBut)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.underRecipient)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.PictureBox backBut;
        private System.Windows.Forms.PictureBox exitBut;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.PictureBox underRecipient;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Button sendMoney;
        private System.Windows.Forms.ToolTip MaxAmount;
        private System.Windows.Forms.TextBox recipient_textBox;
        private System.Windows.Forms.TextBox sendingAmount;
        private System.Windows.Forms.PrintDialog billToPrint;
    }
}