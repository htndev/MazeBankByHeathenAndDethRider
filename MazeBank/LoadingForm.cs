﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MazeBank
{
    public partial class LoadingForm : Form
    {
        public LoadingForm()
        {
            InitializeComponent();
        }

        private void LoadingForm_Load(object sender, EventArgs e)
        {
            this.FormBorderStyle = FormBorderStyle.None;
            
            for(int progress = 1; progress <= 100; progress++)
            {
                System.Threading.Thread.Sleep(5);
                progressBar.Value = progress;
                progressBar.Update();
            }
            this.Close();
        }
    }
}
