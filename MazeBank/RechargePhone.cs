﻿using System;
using System.Windows.Forms;
using AuthClass;
using WinNotification;
using BillPrinting;
using System.Drawing;
using System.Text.RegularExpressions;
using System.Media;
using HistoryClass;
using MailNotify;

namespace MazeBank
{
    public partial class RechargePhone : Form
    {
        public RechargePhone()
        {
            InitializeComponent();
        }

        #region Переменные
        public long login;

        const double percent = 1.5;

        public decimal amount;

        int clicker = 0;

        int changed = 0;

        int errCount = 0;

        int errPhn = 0;

        MoneyTransactions user = new MoneyTransactions();

        ConfirmationForm confirmation = new ConfirmationForm();

        LoadingForm loading = new LoadingForm();

        MainMenu mainMenu = new MainMenu();

        SoundPlayer sound = new SoundPlayer(@"C:\Users\aleks\source\repos\MazeBank\sound.wav");

        #endregion Переменные
        private void exitBut_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void backBut_Click(object sender, EventArgs e)
        {
            RechargePhone rechargePhone = new RechargePhone();
            Close();
            mainMenu.login = login;
            mainMenu.Show();
        }

        private void RechargePhone_Load(object sender, EventArgs e)
        {
            phone.Text = login.ToString();
            user.UserConnection(login);
            if (user.theme)
            {
                this.BackColor = Color.FromArgb(56, 56, 56);
                foreach(Control control in this.Controls)
                {
                    if(control is Label)
                    {
                        ((Label)control).ForeColor = Color.White;
                    }
                }
                pictureBox1.BackColor = Color.White;
                pictureBox3.BackColor = Color.White;
                rechargingAmount.BackColor = Color.FromArgb(56, 56, 56);
                phone.BackColor = Color.FromArgb(56, 56, 56);
                phone.ForeColor = Color.White;
                exitBut.Load(@"C:\Users\aleks\source\repos\MazeBank\ExitPics\exitBlackTheme.png");
                backBut.Load(@"C:\Users\aleks\source\repos\MazeBank\Pics\backWhite.png");
            }
        }

        private void rechargingAmount_TextChanged(object sender, EventArgs e)
        {
            rechargingAmount.MaxLength = 8;
        }

        private void rechargingAmount_Leave(object sender, EventArgs e)
        {
            changed = 0;
            if (rechargingAmount.Text == "")
            {
                changed++;
                rechargingAmount.Text = "Сумма";
                rechargingAmount.ForeColor = Color.Gray;
            }
            if (!Regex.IsMatch(rechargingAmount.Text, @"^\d{0,5}(?:(?:(\.)|(\,))\d{0,2})?$", RegexOptions.Compiled) && errCount <= 4)
            {
                errCount++;
                rechargingAmount.Clear();
                rechargingAmount.Focus();
                MessageBox.Show("Введенаня сумма не соответствует формату! Проверьте или введите еще раз.");
                if (user.theme)
                {
                    rechargingAmount.ForeColor = Color.Black;
                }
                if (user.theme)
                {
                    rechargingAmount.ForeColor = Color.White;
                }
                return;
            }
            if(errCount > 4)
            {
                rechargingAmount.Clear();
                rechargingAmount.Focus();
                rechargingAmount.ForeColor = Color.Black;
                WinNotify.ShowWinNotify("Ошибка ввода суммы", $"{user.userName}, Вы вводите неправильный формат суммы.\nФормат должен быть таким: 12.34 или 15", 10000);
            }
        }

        private void rechargingAmount_Click(object sender, EventArgs e)
        {
            if (user.theme)
            {
                if (rechargingAmount.Text != null && clicker == 0)
                {
                    rechargingAmount.Clear();
                    rechargingAmount.ForeColor = Color.White;
                    clicker++;
                }
                else if (rechargingAmount.Text != null && changed != 0)
                {
                    rechargingAmount.Clear();
                    rechargingAmount.ForeColor = Color.White;
                }
            }
            else if(!user.theme)
            {
                if (rechargingAmount.Text != null && clicker == 0)
                {
                    rechargingAmount.Clear();
                    rechargingAmount.ForeColor = Color.Black;
                    clicker++;
                }
                else if (rechargingAmount.Text != null && changed != 0)
                {
                    rechargingAmount.Clear();
                    rechargingAmount.ForeColor = Color.Black;
                }
            }
        }

        private void RechargePhone_KeyDown(object sender, KeyEventArgs e)
        {
            if(e.KeyCode == Keys.Enter)
            {
                pay.PerformClick();
            }
            if(e.KeyCode == Keys.Down)
            {
                rechargingAmount.Focus();
            }
            if(e.KeyCode == Keys.Up)
            {
                phone.Focus();
            }
            if(e.KeyCode == Keys.Escape)
            {
                DialogResult dialog = MessageBox.Show("Вы уверены, что хотите вернуться в главное меню?", "Выход из меню", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                if(dialog == DialogResult.Yes)
                {
                    this.Close();
                    mainMenu.login = login;
                    mainMenu.Show();
                }
            }
        }

        private void phone_Leave(object sender, EventArgs e)
        {
            if (!Regex.IsMatch(phone.Text, @"^380(3|[5-6]|9)[0-9]\d{3}\d{2}\d{2}", RegexOptions.Compiled) && errPhn <= 3)
            {
                errPhn++;
                MessageBox.Show("Неверно введен номер телефона! Проверьте еще раз!");
                if (user.theme)
                {
                    phone.ForeColor = Color.Black;
                }
                if (!user.theme)
                {
                    phone.ForeColor = Color.White;
                }
                return;
            }
            if(errPhn > 3)
            {
                WinNotify.ShowWinNotify("Ошибка ввода телефона", $"{user.userName}, Вы вводите неверный формат номера телеофна.\nПравильный формат: 380966020340 / 380636343223", 10000);
            }
        }

        private void phone_TextChanged(object sender, EventArgs e)
        {
            phone.MaxLength = 12;
            if (phone.Text == "" || phone.Text == "3" || phone.Text == "38")
            {
                phone.Text = "380";
                phone.SelectionStart = phone.TextLength;
                phone.ScrollToCaret();
            }
        }

        private void pay_Click(object sender, EventArgs e)
        {
            if (Regex.IsMatch(rechargingAmount.Text, @"^\d{0,5}(?:(?:(\.)|(\,))\d{0,2})?$", RegexOptions.Compiled) && Regex.IsMatch(phone.Text, @"^380(3|[5-6]|9)[0-9]\d{3}\d{2}\d{2}", RegexOptions.Compiled))
            {
                try
                {
                    string phoneNum = phone.Text;
                    amount = decimal.Parse(rechargingAmount.Text.Replace(".", ","));
                    decimal committee = MoneyTransactions.CalculateCommittee(amount, percent);
                    amount += committee;
                    if(user.userBalance >= amount)
                    {
                        confirmation.userName = user.userName;
                        confirmation.userPassword = user.userPassword;
                        confirmation.amount = amount;
                        this.Opacity = 0.2;
                        confirmation.ShowDialog();
                        if (!confirmation.ok)
                        {
                            user.userBalance -= amount;
                            this.Opacity = 1;
                            user.UserTransfer(login, user.userBalance);
                            Close();
                            loading.ShowDialog();
                            DialogResult askPrintBill = MessageBox.Show("Вы желаете распечатать чек?", "Печать чека", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                            if (askPrintBill == DialogResult.Yes)
                            {
                                History.AddToHistory(user.userCard, $"-{amount} – Пополнение мобильного телефона {phoneNum}");
                                MailDelivery.NotifyByMail(user.userEmail, user.userName, user.userSurname, $"пополнения мобильного телефона {phoneNum}", amount, user.userBalance);
                                PrintBill.PrintingBill(user.userName, user.userSurname, user.userPatronymic, amount - committee, committee, phoneNum);
                                WinNotify.ShowWinNotify("Пополнение мобильного счета", $"Мобильный счет {phoneNum} был успешно пополнен на сумму в размере {amount - committee}₴\nВаш поточный баланс: {user.userBalance:0.##}₴", 5000);
                                sound.Play();
                                mainMenu.login = login;
                                mainMenu.Show();
                            }
                            else
                            {
                                History.AddToHistory(user.userCard, $"-{amount} – Пополнение мобильного телефона {phoneNum}");
                                MailDelivery.NotifyByMail(user.userEmail, user.userName, user.userSurname, $"пополнения мобильного телефона {phoneNum}", amount, user.userBalance);
                                PrintBill.CreatingBill(user.userName, user.userSurname, user.userPatronymic, amount - committee, committee, phoneNum);
                                WinNotify.ShowWinNotify("Пополнение мобильного счета", $"Мобильный счет {phoneNum} был успешно пополнен на сумму в размере {amount - committee}₴\nВаш поточный баланс: {user.userBalance:0.##}₴", 5000);
                                sound.Play();
                                mainMenu.login = login;
                                mainMenu.Show();
                            }
                        }
                        else if (confirmation.ok)
                        {
                            MessageBox.Show($"Извините, {user.userName}, Вы не можете произвести операцию без подтверждения.", "Ошибка перевода", MessageBoxButtons.OK, MessageBoxIcon.Error);
                            pay.Focus();
                            this.Opacity = 1;
                            return;
                        }
                        else
                        {
                            this.Opacity = 1;
                        }
                    }
                    else
                    {
                        MessageBox.Show($"У вас недостаточно средств для осуществления пополнения. Введите сумму меньше или равную {amount - (MoneyTransactions.CalculateCommittee(amount, percent))}.", "Ошибка размера суммы", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        rechargingAmount.Focus();
                        this.Opacity = 1;
                        return;
                    }
                }
                catch(FormatException fe)
                {
                    MessageBox.Show(fe.Message);
                }
            }
            else
            {
                MessageBox.Show("Извините, Вы ввели некорректные данные.", "Ошибка формата", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
        }
    }
}
