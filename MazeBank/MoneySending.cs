﻿using System;
using System.Drawing;
using System.Windows.Forms;
using System.Text.RegularExpressions;
using AuthClass;
using BillPrinting;
using WinNotification;
using System.Media;
using MailNotify;
using HistoryClass;

namespace MazeBank
{
    public partial class MoneySending : Form
    {
        public MoneySending()
        {
            InitializeComponent();
        }

        #region Переменные

        MoneyTransactions userTransaction = new MoneyTransactions();

        MoneyTransactions recipientTransaction = new MoneyTransactions();

        SoundPlayer sound = new SoundPlayer(@"C:\Users\aleks\source\repos\MazeBank\sound.wav");

        public long login;              // Номер телефона отправителя

        public string passReciever;     // Пароль для сравнения с паролем из подтверждения

        string recipientCard;             // Карта получателя

        public decimal amountForUser;    // Сумма к переводу для отправителя

        decimal amountForRecipient;      // Сумма к переводу для получателя

        MainMenu mainMenu = new MainMenu();

        int recipientClicked = 0;

        int recipientChanged = 0;

        int sendingAmountClicked = 0;

        int sendingAmountChanged = 0;

        int errCountRec = 0;

        int errCountAmount = 0;

        const int percent = 5; // Процент от перевода

        #endregion
        private void MoneySending_Load(object sender, EventArgs e)
        {
            this.KeyPreview = true;
            sendMoney.Left = (this.Width - sendMoney.Width) / 2;
            userTransaction.SenderLoading(login);
            if(userTransaction.theme)
            {
                sendingAmount.BackColor = Color.FromArgb(56, 56, 56);
                recipient_textBox.BackColor = Color.FromArgb(56, 56, 56);
                sendingAmount.ForeColor = Color.White;
                recipient_textBox.ForeColor = Color.White;
                this.BackColor = Color.FromArgb(56, 56, 56);
                exitBut.Load(@"C:\Users\aleks\source\repos\MazeBank\ExitPics\exitBlackTheme.png");
                backBut.Load(@"C:\Users\aleks\source\repos\MazeBank\Pics\backWhite.png");
                pictureBox1.BackColor = Color.White;
                underRecipient.BackColor = Color.White;
                foreach (Control control in this.Controls)
                {
                    if (control is Label)
                    {
                        ((Label)control).ForeColor = Color.White;
                    }
                }
            }
        }

        private void sendMoney_Click(object sender, EventArgs e)
        {
            if(Regex.IsMatch(recipient_textBox.Text, @"^[0-9]{15}", RegexOptions.Compiled) && Regex.IsMatch(sendingAmount.Text, @"^\d{0,5}(?:(?:(\.)|(\,))\d{0,2})?$", RegexOptions.Compiled))
            {
                try
                {
                    amountForUser = decimal.Parse(sendingAmount.Text.Replace('.', ','));
                    amountForRecipient = amountForUser;
                    decimal committee = MoneyTransactions.CalculateCommittee(amountForUser, percent);
                    amountForUser += committee;
                    this.Visible = false;
                    LoadingForm loading = new LoadingForm();
                    loading.ShowDialog();
                    if (userTransaction.userBalance >= amountForUser)
                    {
                        this.Visible = true;
                        try
                        {
                            try
                            {
                                recipientCard = recipient_textBox.Text;
                                recipientTransaction.RecipientLoading(recipientCard); // Подгрузка получателя
                            }
                            catch (Exception ex)
                            {
                                MessageBox.Show(ex.Message + "_____1");
                                return;
                            }
                            if (recipientTransaction.operationSuccsessful == 0 || recipientCard == userTransaction.userCard)
                            {
                                recipient_textBox.Focus();
                                MessageBox.Show("Вы не можете перевести деньги на этот счет.\nПроверьте введенные данные", "Ошибка перевеода", MessageBoxButtons.OK, MessageBoxIcon.Error);
                                return;
                            }
                            else if (recipientTransaction.operationSuccsessful != 0 && recipientCard != userTransaction.userCard)
                            {
                                ConfirmationForm confirmation = new ConfirmationForm();
                                userTransaction.userBalance -= amountForUser;
                                recipientTransaction.recipientBalance += amountForRecipient;
                                this.Opacity = 0.2;
                                confirmation.userName = userTransaction.userName;
                                confirmation.userPassword = userTransaction.userPassword;
                                confirmation.amount = amountForUser;
                                confirmation.ShowDialog();
                                if (!confirmation.ok)
                                {
                                    this.Opacity = 1;
                                    try
                                    {                                    
                                        userTransaction.UserTransfer(login, userTransaction.userBalance);
                                        recipientTransaction.RecipientTransfer(recipientCard, recipientTransaction.recipientBalance);
                                        this.Close();
                                        LoadingForm loadingForm = new LoadingForm();
                                        loadingForm.Show();
                                        DialogResult askPrintBill = MessageBox.Show("Вы желаете распечатать чек?", "Печать чека", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                                        if (askPrintBill == DialogResult.Yes)
                                        {
                                            History.AddToHistory(userTransaction.userCard, $"-{amountForUser} – Перевод сресдств для {recipientTransaction.recipientName} {recipientTransaction.recipientPatronymic} {recipientTransaction.recipientSurname}");
                                            MailDelivery.NotifyByMail(userTransaction.userEmail, userTransaction.userName, userTransaction.userSurname, userTransaction.userPatronymic, recipientTransaction.recipientName, recipientTransaction.recipientSurname, recipientTransaction.recipientPatronymic, amountForUser, userTransaction.userBalance);
                                            MailDelivery.NotifyByMailRecep(amountForRecipient, recipientTransaction.recipientBalance, recipientTransaction.recipientName, recipientTransaction.recipientSurname, recipientTransaction.recipientEmail, userTransaction.userSurname, userTransaction.userPatronymic, userTransaction.userName);
                                            History.AddToHistory(recipientCard, $"+{amountForRecipient} – Перевод сресдств от {userTransaction.userName} {userTransaction.userPatronymic} {userTransaction.userSurname}");
                                            PrintBill.PrintingBill(userTransaction.userName, userTransaction.userSurname, userTransaction.userPatronymic, amountForRecipient, userTransaction.userCard, recipientCard, recipientTransaction.recipientName, committee);
                                            WinNotify.ShowWinNotify("Перевод средств", $"Сумма в размере {amountForRecipient}₴ были успешно переведены {recipientTransaction.recipientSurname} {recipientTransaction.recipientName} {recipientTransaction.recipientPatronymic}\nВаш поточный баланс: {userTransaction.userBalance:0.##}₴", 5000);
                                            sound.Play();
                                            mainMenu.login = login;
                                            mainMenu.Show();
                                        }
                                        else 
                                        {
                                            History.AddToHistory(userTransaction.userCard, $"-{amountForUser} – Перевод сресдств для {recipientTransaction.recipientName} {recipientTransaction.recipientPatronymic} {recipientTransaction.recipientSurname}");
                                            MailDelivery.NotifyByMail(userTransaction.userEmail, userTransaction.userName, userTransaction.userSurname, userTransaction.userPatronymic, recipientTransaction.recipientName, recipientTransaction.recipientSurname, recipientTransaction.recipientPatronymic, amountForUser, userTransaction.userBalance);
                                            MailDelivery.NotifyByMailRecep(amountForRecipient, recipientTransaction.recipientBalance, recipientTransaction.recipientName, recipientTransaction.recipientSurname, recipientTransaction.recipientEmail, userTransaction.userSurname, userTransaction.userPatronymic, userTransaction.userName);
                                            History.AddToHistory(recipientCard, $"+{amountForRecipient} – Перевод сресдств от {userTransaction.userName} {userTransaction.userPatronymic} {userTransaction.userSurname}");                                          
                                            PrintBill.CreatingBill(userTransaction.userName, userTransaction.userSurname, userTransaction.userPatronymic, amountForRecipient, userTransaction.userCard, recipientCard, recipientTransaction.recipientName, committee);
                                            WinNotify.ShowWinNotify("Перевод средств", $"Сумма в размере {amountForRecipient}₴ были успешно переведены {recipientTransaction.recipientSurname} {recipientTransaction.recipientName} {recipientTransaction.recipientPatronymic}\nВаш поточный баланс: {userTransaction.userBalance:0.##}₴", 5000);
                                            sound.Play();
                                            mainMenu.login = login;
                                            mainMenu.Show();
                                        }
                                    }
                                    catch (Exception ex)
                                    {
                                        MessageBox.Show(ex.Message + "_____2");
                                        return;
                                    }
                                }
                                else if (!confirmation.ok)
                                {
                                    this.Opacity = 1;
                                    MessageBox.Show($"Извините, {userTransaction.userName}, Вы не можете отправить деньги без подтверждения.", "Ошибка перевода", MessageBoxButtons.OK, MessageBoxIcon.Error);
                                    sendMoney.Focus();
                                    return;
                                }
                                else
                                {
                                    this.Opacity = 1;
                                }
                            }
                        }
                        catch (FormatException fe)
                        {
                            MessageBox.Show(fe.Message + "_____3");
                        }
                    }
                    else
                    {
                        this.Visible = true;
                        MessageBox.Show($"У вас не достаточно сресдтв для перевода\nВведите сумму меньше или равную {amountForUser}", "Ошибка перевеода", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        return;
                    }
                }
                catch(FormatException)
                {
                    MessageBox.Show("Вы ввели некорректную сумму! Введите заново!", "Ошибка ввода", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    sendingAmount.Clear();
                    return;
                }
            }
            else
            {
                MessageBox.Show("Вы ввели неверные данные.", "Ошибка формата", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
        }

        private void exitBut_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void backBut_Click(object sender, EventArgs e)
        {
            MoneySending moneySending = new MoneySending();
            mainMenu.login = login;
            this.Close();
            mainMenu.Show();
        }

        private void recipient_textBox_TextChanged(object sender, EventArgs e)
        {
            recipient_textBox.MaxLength = 16;
        }

        private void recipient_textBox_Click(object sender, EventArgs e)
        {
            if(userTransaction.theme)
            {
                if (recipient_textBox.Text != null && recipientClicked == 0)
                {
                    recipient_textBox.Clear();
                    recipient_textBox.ForeColor = Color.White;
                    recipientClicked++;
                }

                else if (recipient_textBox.Text != null && recipientChanged != 0)
                {
                    recipient_textBox.Clear();
                    recipient_textBox.ForeColor = Color.White;
                }
            }
            if(!userTransaction.theme)
            {
                if (recipient_textBox.Text != null && recipientClicked == 0)
                {
                    recipient_textBox.Clear();
                    recipient_textBox.ForeColor = Color.Black;
                    recipientClicked++;
                }

                else if (recipient_textBox.Text != null && recipientChanged != 0)
                {
                    recipient_textBox.Clear();
                    recipient_textBox.ForeColor = Color.Black;
                }
            }
        }

        private void sendingAmount_Click(object sender, EventArgs e)
        {
            if(userTransaction.theme)
            {
                if (sendingAmount.Text != null && sendingAmountClicked == 0)
                {
                    sendingAmount.Clear();
                    sendingAmount.ForeColor = Color.White;
                    sendingAmountClicked++;
                }

                else if (sendingAmount.Text != null && sendingAmountChanged != 0)
                {
                    sendingAmount.Clear();
                    sendingAmount.ForeColor = Color.White;
                }
            }
            if(!userTransaction.theme)
            {
                if (sendingAmount.Text != null && sendingAmountClicked == 0)
                {
                    sendingAmount.Clear();
                    sendingAmount.ForeColor = Color.Black;
                    sendingAmountClicked++;
                }

                else if (sendingAmount.Text != null && sendingAmountChanged != 0)
                {
                    sendingAmount.Clear();
                    sendingAmount.ForeColor = Color.Black;
                }
            }
        }

        private void sendingAmount_TextChanged(object sender, EventArgs e)
        {
            sendingAmount.MaxLength = 8;
        }

        private void recipient_textBox_Leave(object sender, EventArgs e)
        {
            recipientChanged = 0;
            if (!Regex.IsMatch(recipient_textBox.Text, @"^[0-9]{15}", RegexOptions.Compiled) && errCountRec <= 3)
            {
                errCountRec++;
                MessageBox.Show("Неверно введен номер карты! Проверьте еще раз!");
                recipient_textBox.ForeColor = Color.Black;
                return;
            }

            else if (errCountRec > 3)
            {
                WinNotify.ShowWinNotify("Ошибка ввода карты", $"{userTransaction.userName}, Вы вводите неправильный формат карты.\nФормат карты содержит 16 символов: 1234567812345678", 10000);
            }
            if (recipient_textBox.Text == "")
            {
                recipientChanged++;
                recipient_textBox.Text = "Карта получателя";
                recipient_textBox.ForeColor = Color.Gray;
            }
        }

        private void sendingAmount_Leave(object sender, EventArgs e)
        {
            sendingAmountChanged = 0;
            if (!Regex.IsMatch(sendingAmount.Text, @"^\d{0,5}(?:(?:(\.)|(\,))\d{0,2})?$", RegexOptions.Compiled) && errCountAmount <= 4)
            {
                errCountAmount++;
                sendingAmount.Clear();
                sendingAmount.Focus();
                MessageBox.Show("Введенаня сумма не соответствует формату! Проверьте или введите еще раз.");
                sendingAmount.ForeColor = Color.Black;
                return;
            }

            else if (errCountAmount > 3)
            {
                sendingAmount.Clear();
                sendingAmount.Focus();
                WinNotify.ShowWinNotify("Ошибка ввода суммы", $"{userTransaction.userName}, Вы вводите неправильный формат суммы.\nФормат должен быть таким: 12.34 или 15", 10000);
            }

            if (sendingAmount.Text == null)
            {
                sendingAmountChanged++;
                sendingAmount.Text = "Сумма";
                sendingAmount.ForeColor = Color.Gray;
            }
        }

        private void recipient_textBox_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Down)
            {
                sendingAmount.Focus();
                if (sendingAmount != null && sendingAmountClicked == 0)
                {
                    sendingAmount.Clear();
                    sendingAmount.ForeColor = Color.Black;
                    sendingAmountClicked++;
                }

                else if (sendingAmount.Text != null && sendingAmountChanged != 0)
                {
                    sendingAmount.Clear();
                    sendingAmount.ForeColor = Color.Black;
                }
            }
        }

        private void sendingAmount_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Up)
            {
                recipient_textBox.Focus();
                if (recipient_textBox.Text != null && recipientClicked == 0)
                {
                    recipient_textBox.Clear();
                    recipient_textBox.ForeColor = Color.Black;
                    recipientClicked++;
                }

                else if (recipient_textBox.Text != null && recipientChanged != 0)
                {
                    recipient_textBox.Clear();
                    recipient_textBox.ForeColor = Color.Black;
                }
            }
        }

        private void MoneySending_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                sendMoney.PerformClick();
            }
        }
    }
}