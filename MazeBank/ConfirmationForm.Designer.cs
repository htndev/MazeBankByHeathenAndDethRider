﻿namespace MazeBank
{
    partial class ConfirmationForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ConfirmationForm));
            this.confPass = new System.Windows.Forms.Button();
            this.passVisibility = new System.Windows.Forms.PictureBox();
            this.pictureBox3 = new System.Windows.Forms.PictureBox();
            this.passwordBox = new System.Windows.Forms.TextBox();
            this.toPay = new System.Windows.Forms.Label();
            this.confirmStr = new System.Windows.Forms.Label();
            this.exitBut = new System.Windows.Forms.PictureBox();
            ((System.ComponentModel.ISupportInitialize)(this.passVisibility)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.exitBut)).BeginInit();
            this.SuspendLayout();
            // 
            // confPass
            // 
            this.confPass.FlatAppearance.BorderSize = 0;
            this.confPass.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.confPass.Font = new System.Drawing.Font("Verdana", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.confPass.ForeColor = System.Drawing.Color.Red;
            this.confPass.Location = new System.Drawing.Point(103, 156);
            this.confPass.Name = "confPass";
            this.confPass.Size = new System.Drawing.Size(146, 39);
            this.confPass.TabIndex = 32;
            this.confPass.Text = "Подтвердить";
            this.confPass.UseVisualStyleBackColor = true;
            this.confPass.Click += new System.EventHandler(this.confPass_Click);
            // 
            // passVisibility
            // 
            this.passVisibility.Cursor = System.Windows.Forms.Cursors.Hand;
            this.passVisibility.Image = ((System.Drawing.Image)(resources.GetObject("passVisibility.Image")));
            this.passVisibility.Location = new System.Drawing.Point(270, 105);
            this.passVisibility.Name = "passVisibility";
            this.passVisibility.Size = new System.Drawing.Size(50, 50);
            this.passVisibility.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.passVisibility.TabIndex = 31;
            this.passVisibility.TabStop = false;
            this.passVisibility.Click += new System.EventHandler(this.passVisibility_Click);
            // 
            // pictureBox3
            // 
            this.pictureBox3.BackColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.pictureBox3.Location = new System.Drawing.Point(83, 141);
            this.pictureBox3.Name = "pictureBox3";
            this.pictureBox3.Size = new System.Drawing.Size(184, 2);
            this.pictureBox3.TabIndex = 30;
            this.pictureBox3.TabStop = false;
            // 
            // passwordBox
            // 
            this.passwordBox.BackColor = System.Drawing.SystemColors.Control;
            this.passwordBox.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.passwordBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.passwordBox.ForeColor = System.Drawing.SystemColors.WindowFrame;
            this.passwordBox.Location = new System.Drawing.Point(85, 117);
            this.passwordBox.Name = "passwordBox";
            this.passwordBox.Size = new System.Drawing.Size(180, 22);
            this.passwordBox.TabIndex = 29;
            this.passwordBox.TabStop = false;
            this.passwordBox.Text = "Введите пароль";
            this.passwordBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.passwordBox.Click += new System.EventHandler(this.passwordBox_Click);
            this.passwordBox.Leave += new System.EventHandler(this.passwordBox_Leave);
            // 
            // toPay
            // 
            this.toPay.AutoSize = true;
            this.toPay.Font = new System.Drawing.Font("Verdana", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.toPay.Location = new System.Drawing.Point(48, 69);
            this.toPay.Name = "toPay";
            this.toPay.Size = new System.Drawing.Size(260, 18);
            this.toPay.TabIndex = 28;
            this.toPay.Text = "Сумма к переводу: 99999999";
            // 
            // confirmStr
            // 
            this.confirmStr.AutoSize = true;
            this.confirmStr.Font = new System.Drawing.Font("Verdana", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.confirmStr.ForeColor = System.Drawing.Color.Black;
            this.confirmStr.Location = new System.Drawing.Point(21, 33);
            this.confirmStr.Name = "confirmStr";
            this.confirmStr.Size = new System.Drawing.Size(319, 18);
            this.confirmStr.TabIndex = 26;
            this.confirmStr.Text = "Пользователь, подтвердите перевод";
            // 
            // exitBut
            // 
            this.exitBut.Cursor = System.Windows.Forms.Cursors.Hand;
            this.exitBut.Image = ((System.Drawing.Image)(resources.GetObject("exitBut.Image")));
            this.exitBut.Location = new System.Drawing.Point(324, 5);
            this.exitBut.Name = "exitBut";
            this.exitBut.Size = new System.Drawing.Size(25, 25);
            this.exitBut.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.exitBut.TabIndex = 33;
            this.exitBut.TabStop = false;
            this.exitBut.Click += new System.EventHandler(this.exitBut_Click);
            // 
            // ConfirmationForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(356, 207);
            this.Controls.Add(this.exitBut);
            this.Controls.Add(this.confPass);
            this.Controls.Add(this.passVisibility);
            this.Controls.Add(this.pictureBox3);
            this.Controls.Add(this.passwordBox);
            this.Controls.Add(this.toPay);
            this.Controls.Add(this.confirmStr);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.KeyPreview = true;
            this.Name = "ConfirmationForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "ConfirmationForm";
            this.Load += new System.EventHandler(this.ConfirmationForm_Load);
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.ConfirmationForm_KeyDown);
            ((System.ComponentModel.ISupportInitialize)(this.passVisibility)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.exitBut)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.PictureBox passVisibility;
        private System.Windows.Forms.PictureBox pictureBox3;
        public System.Windows.Forms.TextBox passwordBox;
        private System.Windows.Forms.Label toPay;
        private System.Windows.Forms.Label confirmStr;
        private System.Windows.Forms.PictureBox exitBut;
        public System.Windows.Forms.Button confPass;
    }
}