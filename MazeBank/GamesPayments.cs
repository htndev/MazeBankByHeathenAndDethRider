﻿using AuthClass;
using System;
using System.Drawing;
using System.Text.RegularExpressions;
using System.Windows.Forms;
using WinNotification;
using BillPrinting;
using System.Media;
using MailNotify;
using HistoryClass;

namespace MazeBank
{
    public partial class GamesPayments : Form
    {
        public GamesPayments()
        {
            InitializeComponent();
        }

        #region Переменные 

        public long login;

        MainMenu mainMenu = new MainMenu();

        MoneyTransactions user = new MoneyTransactions();

        ConfirmationForm confirmation = new ConfirmationForm();

        LoadingForm loading = new LoadingForm();

        SoundPlayer sound = new SoundPlayer(@"C:\Users\aleks\source\repos\MazeBank\sound.wav");

        int idClick = 0;

        int idChanged = 0;

        int amountClick = 0;

        int amountChanged = 0;

        int errCountAmount = 0;

        int errCountId = 0;

        decimal amount = 0;

        int stCmt = 4;

        int wgCmt = 8;

        double bnCmt = 3.2;

        #endregion Переменные

        private void GamesPayments_Load(object sender, EventArgs e)
        {
            user.UserConnection(login);
            steam.Hide();
            wargaming.Hide();
            battlenet.Hide();
            if(user.theme)
            {
                this.BackColor = Color.FromArgb(56, 56, 56);
                exitBtn.Load(@"C:\Users\aleks\source\repos\MazeBank\ExitPics\exitBlackTheme.png");
                backBtn.Load(@"C:\Users\aleks\source\repos\MazeBank\Pics\backWhite.png");
                steamExit.Load(@"C:\Users\aleks\source\repos\MazeBank\ExitPics\exitBlackTheme.png");
                steamBack.Load(@"C:\Users\aleks\source\repos\MazeBank\Pics\backWhite.png");
                wgExit.Load(@"C:\Users\aleks\source\repos\MazeBank\ExitPics\exitBlackTheme.png");
                wgBack.Load(@"C:\Users\aleks\source\repos\MazeBank\Pics\backWhite.png");
                bnExit.Load(@"C:\Users\aleks\source\repos\MazeBank\ExitPics\exitBlackTheme.png");
                bnBack.Load(@"C:\Users\aleks\source\repos\MazeBank\Pics\backWhite.png");
                pictureBox1.BackColor = Color.White;
                pictureBox3.BackColor = Color.White;
                pictureBox5.BackColor = Color.White;
                pictureBox6.BackColor = Color.White;
                pictureBox9.BackColor = Color.White;
                pictureBox8.BackColor = Color.White;
                label.ForeColor = Color.White;
                foreach(Control c in steam.Controls)
                {
                    if(c is Label)
                    {
                        ((Label)c).ForeColor = Color.White;
                    }
                    if(c is TextBox)
                    {
                        ((TextBox)c).BackColor = Color.FromArgb(56, 56, 56);
                    }
                }

                foreach (Control c in battlenet.Controls)
                {
                    if (c is Label)
                    {
                        ((Label)c).ForeColor = Color.White;
                    }
                    if (c is TextBox)
                    {
                        ((TextBox)c).BackColor = Color.FromArgb(56, 56, 56);
                    }
                }

                foreach (Control c in wargaming.Controls)
                {
                    if (c is Label)
                    {
                        ((Label)c).ForeColor = Color.White;
                    }
                    if (c is TextBox)
                    {
                        ((TextBox)c).BackColor = Color.FromArgb(56, 56, 56);
                    }
                }
            }
        }

        private void backBtn_Click(object sender, EventArgs e)
        {
            Close();
            mainMenu.login = login;
            mainMenu.Show();
        }

        private void exitBtn_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void steamPic_Click(object sender, EventArgs e)
        {
            steam.Show();
        }

        private void battlenetPic_Click(object sender, EventArgs e)
        {
            battlenet.Show();
        }

        private void wargamingPic_Click(object sender, EventArgs e)
        {
            wargaming.Show();
        }

        private void steamExit_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void steamBack_Click(object sender, EventArgs e)
        {
            amountClick = 0;
            amountChanged = 0;
            idChanged = 0;
            idClick = 0;
            amount = 0;
            errCountAmount = 0;
            errCountId = 0;
            steam.Hide();
        }

        private void idText_Click(object sender, EventArgs e)
        {
            if (user.theme)
            {
                if (idText.Text != null && idClick == 0)
                {
                    idText.Clear();
                    idText.ForeColor = Color.White;
                    idClick++;
                }
                else if (idText.Text != null && idChanged != 0)
                {
                    idText.Clear();
                    idText.ForeColor = Color.White;
                }
            }
            else if (!user.theme)
            {
                if (idText.Text != null && idClick == 0)
                {
                    idText.Clear();
                    idText.ForeColor = Color.Black;
                    idClick++;
                }
                else if (idText.Text != null && idChanged != 0)
                {
                    idText.Clear();
                    idText.ForeColor = Color.Black;
                }
            }
        }

        private void idText_Leave(object sender, EventArgs e)
        {
            idChanged = 0;
            if (idText.Text == "")
            {
                idChanged++;
                idText.Text = "Ваш ID";
                idText.ForeColor = Color.Gray;
            }

            if (!Regex.IsMatch(idText.Text, @"^[\d\w_.]+$", RegexOptions.Compiled) && errCountId <= 4)
            {
                errCountId++;
                idText.Clear();
                idText.Focus();
                MessageBox.Show("Введенаня никнейм не соответствует формату! Проверьте или введите еще раз.");
                if (user.theme)
                {
                    idText.ForeColor = Color.White;
                }
                else if (!user.theme)
                {
                    idText.ForeColor = Color.Black;
                }
                return;
            }

            if (errCountId > 4)
            {
                idText.Clear();
                idText.Focus();
                WinNotify.ShowWinNotify("Ошибка ввода никнейма", $"{user.userName}, Вы вводите неправильный формат никнейма.\nФормат должен быть таким: MrTwister_2018 (Доступные символы: . _).", 10000);
            }
        }

        private void amountText_Leave(object sender, EventArgs e)
        {
            amountChanged = 0;
            if (amountText.Text == "")
            {
                amountChanged++;
                amountText.Text = "Сумма";
                amountText.ForeColor = Color.Gray;
            }
            if (!Regex.IsMatch(amountText.Text, @"^\d{0,5}(?:(?:(\.)|(\,))\d{0,2})?$", RegexOptions.Compiled) && errCountAmount <= 4)
            {
                errCountAmount++;
                amountText.Clear();
                amountText.Focus();
                MessageBox.Show("Введенаня сумма не соответствует формату! Проверьте или введите еще раз.");
                if (user.theme)
                {
                    amountText.ForeColor = Color.White;
                }
                else if (!user.theme)
                {
                    amountText.ForeColor = Color.Black;
                }
                return;
            }
            if (errCountAmount > 4)
            {
                amountText.Clear();
                amountText.Focus();
                WinNotify.ShowWinNotify("Ошибка ввода суммы", $"{user.userName}, Вы вводите неправильный формат суммы.\nФормат должен быть таким: 12.34 или 15", 10000);
            }
        }

        private void amountText_Click(object sender, EventArgs e)
        {
            if (user.theme)
            {
                if (amountText.Text != null && amountClick == 0)
                {
                    amountText.Clear();
                    amountText.ForeColor = Color.White;
                    amountClick++;
                }
                else if (amountText.Text != null && amountChanged != 0)
                {
                    amountText.Clear();
                    amountText.ForeColor = Color.White;
                }
            }
            else if (!user.theme)
            {
                if (amountText.Text != null && amountClick == 0)
                {
                    amountText.Clear();
                    amountText.ForeColor = Color.Black;
                    amountClick++;
                }
                else if (amountText.Text != null && amountChanged != 0)
                {
                    amountText.Clear();
                    amountText.ForeColor = Color.Black;
                }
            }
        }

        private void amountText_TextChanged(object sender, EventArgs e)
        {
            amountText.MaxLength = 8;
        }

        private void idText_TextChanged(object sender, EventArgs e)
        {
            idText.MaxLength = 20;
        }

        private void paySteam_Click(object sender, EventArgs e)
        {
            if (Regex.IsMatch(amountText.Text, @"^\d{0,5}(?:(?:(\.)|(\,))\d{0,2})?$", RegexOptions.Compiled) && Regex.IsMatch(idText.Text, @"^[\d\w_.]+$", RegexOptions.Compiled))
            {
                try
                {
                    string id = idText.Text;
                    amount = decimal.Parse(amountText.Text.Replace(".", ","));
                    decimal committee = MoneyTransactions.CalculateCommittee(amount, stCmt);
                    amount += committee;
                    if (user.userBalance >= amount)
                    {
                        this.Opacity = 0.2;
                        confirmation.userName = user.userName;
                        confirmation.userPassword = user.userPassword;
                        confirmation.amount = amount;
                        confirmation.ShowDialog();
                        if (!confirmation.ok)
                        {
                            user.userBalance -= amount;
                            this.Opacity = 1;
                            user.UserTransfer(login, user.userBalance);
                            Close();
                            loading.ShowDialog();
                            DialogResult askPrintBill = MessageBox.Show("Вы желаете распечатать чек?", "Печать чека", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                            if (askPrintBill == DialogResult.Yes)
                            {
                                History.AddToHistory(user.userCard, $"-{amount} – Пополнение игрового аккаунта Steam {id}");
                                PrintBill.PrintingBill("Steam", user.userName, user.userSurname, user.userPatronymic, user.userCard, amount - committee, committee, id, "Valve Inc.");
                                WinNotify.ShowWinNotify("Пополнение баланса Steam", $"Вы успешно пополнили баланс Steam в размере {amount - committee}₴\nВаш поточный баланс: {user.userBalance:0.##}₴", 5000);
                                MailDelivery.NotifyByMail(user.userEmail, user.userName, user.userSurname, $"пополнение баланса Steam аккаунта: {id}", amount, user.userBalance);
                                sound.Play();
                                mainMenu.login = login;
                                mainMenu.Show();
                            }
                            else
                            {
                                History.AddToHistory(user.userCard, $"-{amount} – Пополнение игрового аккаунта Steam {id}");
                                PrintBill.CreatingBill("Steam", user.userName, user.userSurname, user.userPatronymic, user.userCard, amount - committee, committee, id, "Valce Inc.");
                                WinNotify.ShowWinNotify("Пополнение баланса Steam", $"Вы успешно пополнили баланс Steam в размере {amount - committee}₴\nВаш поточный баланс: {user.userBalance:0.##}₴", 5000);
                                MailDelivery.NotifyByMail(user.userEmail, user.userName, user.userSurname, $"пополнение баланса Steam аккаунта: {id}", amount, user.userBalance);
                                sound.Play();
                                mainMenu.login = login;
                                mainMenu.Show();
                            }
                        }
                        else if (confirmation.ok)
                        {
                            this.Opacity = 1;
                            MessageBox.Show($"Извините, {user.userName}, Вы не можете произвести операцию без подтверждения.", "Ошибка перевода", MessageBoxButtons.OK, MessageBoxIcon.Error);
                            paySteam.Focus();
                            return;
                        }
                        else
                        {
                            this.Opacity = 1;
                        }
                    }
                    else
                    {
                        MessageBox.Show($"{user.userName}, у Вас не хватает средств оплатить эти услуги.", "Недостаточно средств", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        amount = 0;
                        return;
                    }
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message);
                    return;
                }
            }
            else
            {
                MessageBox.Show("Введены неверные данные. Проверьте данные и попробуйте снова.", "Ошибка введеных данных", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
        }

        private void wgExit_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void wgBack_Click(object sender, EventArgs e)
        {
            wargaming.Hide();
            amount = 0;
            amountClick = 0;
            amountChanged = 0;
            idChanged = 0;
            idClick = 0;
            errCountAmount = 0;
            errCountId = 0;
        }

        private void idTextWG_TextChanged(object sender, EventArgs e)
        {
            idTextWG.MaxLength = 20;
        }

        private void idTextWG_Leave(object sender, EventArgs e)
        {
            idChanged = 0;
            if (idTextWG.Text == "")
            {
                idChanged++;
                idTextWG.Text = "Ваш ID";
                idTextWG.ForeColor = Color.Gray;
            }

            if (!Regex.IsMatch(idTextWG.Text, @"^[\d\w_.]+$", RegexOptions.Compiled) && errCountId <= 4)
            {
                errCountId++;
                idTextWG.Clear();
                idTextWG.Focus();
                MessageBox.Show("Введенаня никнейм не соответствует формату! Проверьте или введите еще раз.");
                if (user.theme)
                {
                    idTextWG.ForeColor = Color.White;
                }
                else if (!user.theme)
                {
                    idTextWG.ForeColor = Color.Black;
                }
                return;
            }

            if (errCountId > 4)
            {
                idTextWG.Clear();
                idTextWG.Focus();
                WinNotify.ShowWinNotify("Ошибка ввода никнейма", $"{user.userName}, Вы вводите неправильный формат никнейма.\nФормат должен быть таким: MrTwister_2018 (Доступные символы: . _).", 10000);
            }
        }

        private void idTextWG_Click(object sender, EventArgs e)
        {
            if (user.theme)
            {
                if (idTextWG.Text != null && idClick == 0)
                {
                    idTextWG.Clear();
                    idTextWG.ForeColor = Color.White;
                    idClick++;
                }
                else if (idText.Text != null && idChanged != 0)
                {
                    idTextWG.Clear();
                    idTextWG.ForeColor = Color.White;
                }
            }
            else if (!user.theme)
            {
                if (idTextWG.Text != null && idClick == 0)
                {
                    idTextWG.Clear();
                    idTextWG.ForeColor = Color.Black;
                    idClick++;
                }
                else if (idTextWG.Text != null && idChanged != 0)
                {
                    idTextWG.Clear();
                    idTextWG.ForeColor = Color.Black;
                }
            }
        }

        private void amountTextWG_TextChanged(object sender, EventArgs e)
        {
            amountTextWG.MaxLength = 8;
        }

        private void amountTextWG_Leave(object sender, EventArgs e)
        {
            amountChanged = 0;
            if (amountTextWG.Text == "")
            {
                amountChanged++;
                amountTextWG.Text = "Сумма";
                amountTextWG.ForeColor = Color.Gray;
            }
            if (!Regex.IsMatch(amountTextWG.Text, @"^\d{0,5}(?:(?:(\.)|(\,))\d{0,2})?$", RegexOptions.Compiled) && errCountAmount <= 4)
            {
                errCountAmount++;
                amountTextWG.Clear();
                amountTextWG.Focus();
                MessageBox.Show("Введенаня сумма не соответствует формату! Проверьте или введите еще раз.");
                if(user.theme)
                {
                    amountTextWG.ForeColor = Color.White;
                }
                else if(!user.theme)
                {
                    amountTextWG.ForeColor = Color.Black;
                }
                return;
            }
            if (errCountAmount > 4)
            {
                amountTextWG.Clear();
                amountTextWG.Focus();
                WinNotify.ShowWinNotify("Ошибка ввода суммы", $"{user.userName}, Вы вводите неправильный формат суммы.\nФормат должен быть таким: 12.34 или 15", 10000);
            }
        }

        private void amountTextWG_Click(object sender, EventArgs e)
        {
            if (user.theme)
            {
                if (amountTextWG.Text != null && amountClick == 0)
                {
                    amountTextWG.Clear();
                    amountTextWG.ForeColor = Color.White;
                    amountClick++;
                }
                else if (amountTextWG.Text != null && amountChanged != 0)
                {
                    amountTextWG.Clear();
                    amountTextWG.ForeColor = Color.White;
                }
            }
            else if (!user.theme)
            {
                if (amountTextWG.Text != null && amountClick == 0)
                {
                    amountTextWG.Clear();
                    amountTextWG.ForeColor = Color.Black;
                    amountClick++;
                }
                else if (amountTextWG.Text != null && amountChanged != 0)
                {
                    amountTextWG.Clear();
                    amountTextWG.ForeColor = Color.Black;
                }
            }
        }

        private void payWG_Click(object sender, EventArgs e)
        {
            if (Regex.IsMatch(amountTextWG.Text, @"^\d{0,5}(?:(?:(\.)|(\,))\d{0,2})?$", RegexOptions.Compiled) && Regex.IsMatch(idTextWG.Text, @"^[\d\w_.]+$", RegexOptions.Compiled))
            {
                try
                {
                    string id = idTextWG.Text;
                    amount = decimal.Parse(amountTextWG.Text.Replace(".", ","));
                    decimal committee = MoneyTransactions.CalculateCommittee(amount, wgCmt);
                    amount += committee;
                    if (user.userBalance >= amount)
                    {
                        this.Opacity = 0.2;
                        confirmation.userName = user.userName;
                        confirmation.userPassword = user.userPassword;
                        confirmation.amount = amount;
                        confirmation.ShowDialog();
                        if (!confirmation.ok)
                        {
                            user.userBalance -= amount;
                            this.Opacity = 1;
                            user.UserTransfer(login, user.userBalance);
                            Close();
                            loading.ShowDialog();
                            DialogResult askPrintBill = MessageBox.Show("Вы желаете распечатать чек?", "Печать чека", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                            if (askPrintBill == DialogResult.Yes)
                            {
                                History.AddToHistory(user.userCard, $"-{amount} – Пополнение игрового аккаунта WarGaming {id}");
                                PrintBill.PrintingBill("WarGaming", user.userName, user.userSurname, user.userPatronymic, user.userCard, amount - committee, committee, id, "WarGaming Public Co. Ltd.");
                                WinNotify.ShowWinNotify("Пополнение баланса WarGaming", $"Вы успешно пополнили баланс WarGaming в размере {amount - committee}₴\nВаш поточный баланс: {user.userBalance:0.##}₴", 5000);
                                MailDelivery.NotifyByMail(user.userEmail, user.userName, user.userSurname, $"пополнения баланса WarGaming аккаунта: {id}", amount, user.userBalance);
                                sound.Play();
                                mainMenu.login = login;
                                mainMenu.Show();
                            }
                            else
                            {
                                History.AddToHistory(user.userCard, $"-{amount} – Пополнение игрового аккаунта WarGaming {id}");
                                PrintBill.CreatingBill("WarGaming", user.userName, user.userSurname, user.userPatronymic, user.userCard, amount - committee, committee, id, "WarGaming Public Co. Ltd.");
                                WinNotify.ShowWinNotify("Пополнение баланса WarGaming", $"Вы успешно пополнили баланс WarGaming в размере {amount - committee}₴\nВаш поточный баланс: {user.userBalance:0.##}₴", 5000);
                                MailDelivery.NotifyByMail(user.userEmail, user.userName, user.userSurname, $"пополнения баланса WarGaming аккаунта: {id}", amount, user.userBalance);
                                sound.Play();
                                mainMenu.login = login;
                                mainMenu.Show();
                            }
                        }
                        else if (confirmation.ok)
                        {
                            this.Opacity = 1;
                            MessageBox.Show($"Извините, {user.userName}, Вы не можете произвести операцию без подтверждения.", "Ошибка перевода", MessageBoxButtons.OK, MessageBoxIcon.Error);
                            paySteam.Focus();
                            return;
                        }
                        else
                        {
                            this.Opacity = 1;
                        }
                    }
                    else
                    {
                        MessageBox.Show($"{user.userName}, у Вас не хватает средств оплатить эти услуги.", "Недостаточно средств", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        amount = 0;
                        return;
                    }
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message);
                    return;
                }
            }
            else
            {
                MessageBox.Show("Введены неверные данные. Проверьте данные и попробуйте снова.", "Ошибка введеных данных", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
        }

        private void bnBack_Click(object sender, EventArgs e)
        {
            battlenet.Hide();
            amount = 0;
            amountClick = 0;
            amountChanged = 0;
            idChanged = 0;
            idClick = 0;
            errCountAmount = 0;
            errCountId = 0;
        }

        private void bnExit_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void idTextBN_TextChanged(object sender, EventArgs e)
        {
            idTextBN.MaxLength = 20;
        }

        private void idTextBN_Leave(object sender, EventArgs e)
        {
            idChanged = 0;
            if (idTextBN.Text == "")
            {
                idChanged++;
                idTextBN.Text = "Ваш ID";
                idTextBN.ForeColor = Color.Gray;
            }
            if (!Regex.IsMatch(idTextBN.Text, @"^[\d\w_.]+$", RegexOptions.Compiled) && errCountId <= 4)
            {
                errCountId++;
                idTextBN.Clear();
                idTextBN.Focus();
                MessageBox.Show("Введенаня никнейм не соответствует формату! Проверьте или введите еще раз.");
                if (user.theme)
                {
                    idTextBN.ForeColor = Color.White;
                }
                else if (!user.theme)
                {
                    idTextBN.ForeColor = Color.Black;
                }
                return;
            }
            if (errCountId > 4)
            {
                idTextBN.Clear();
                idTextBN.Focus();
                WinNotify.ShowWinNotify("Ошибка ввода никнейма", $"{user.userName}, Вы вводите неправильный формат никнейма.\nФормат должен быть таким: MrTwister_2018 (Доступные символы: . _).", 10000);
            }
        }

        private void idTextBN_Click(object sender, EventArgs e)
        {
            if (user.theme)
            {
                if (idTextBN.Text != null && idClick == 0)
                {
                    idTextBN.Clear();
                    idTextBN.ForeColor = Color.White;
                    idClick++;
                }
                else if (idText.Text != null && idChanged != 0)
                {
                    idTextBN.Clear();
                    idTextBN.ForeColor = Color.White;
                }
            }
            else if (!user.theme)
            {
                if (idTextBN.Text != null && idClick == 0)
                {
                    idTextBN.Clear();
                    idTextBN.ForeColor = Color.Black;
                    idClick++;
                }
                else if (idTextBN.Text != null && idChanged != 0)
                {
                    idTextBN.Clear();
                    idTextBN.ForeColor = Color.Black;
                }
            }
        }

        private void amountTextBN_TextChanged(object sender, EventArgs e)
        {
            amountTextBN.MaxLength = 8;
        }

        private void amountTextBN_Leave(object sender, EventArgs e)
        {
            amountChanged = 0;
            if (amountTextBN.Text == "")
            {
                amountChanged++;
                amountTextBN.Text = "Сумма";
                amountTextBN.ForeColor = Color.Gray;
            }
            if (!Regex.IsMatch(amountTextBN.Text, @"^\d{0,5}(?:(?:(\.)|(\,))\d{0,2})?$", RegexOptions.Compiled) && errCountAmount <= 4)
            {
                errCountAmount++;
                amountTextBN.Clear();
                amountTextBN.Focus();
                MessageBox.Show("Введенаня сумма не соответствует формату! Проверьте или введите еще раз.");
                if (user.theme)
                {
                    amountTextBN.ForeColor = Color.White;
                }
                else if (!user.theme)
                {
                    amountTextBN.ForeColor = Color.Black;
                }
                return;
            }
            if (errCountAmount > 4)
            {
                amountTextBN.Clear();
                amountTextBN.Focus();
                WinNotify.ShowWinNotify("Ошибка ввода суммы", $"{user.userName}, Вы вводите неправильный формат суммы.\nФормат должен быть таким: 12.34 или 15", 10000);
            }
        }

        private void amountTextBN_Click(object sender, EventArgs e)
        {
            if (user.theme)
            {
                if (amountTextBN.Text != null && amountClick == 0)
                {
                    amountTextBN.Clear();
                    amountTextBN.ForeColor = Color.White;
                    amountClick++;
                }
                else if (amountTextBN.Text != null && amountChanged != 0)
                {
                    amountTextBN.Clear();
                    amountTextBN.ForeColor = Color.White;
                }
            }
            else if (!user.theme)
            {
                if (amountTextBN.Text != null && amountClick == 0)
                {
                    amountTextBN.Clear();
                    amountTextBN.ForeColor = Color.Black;
                    amountClick++;
                }
                else if (amountTextBN.Text != null && amountChanged != 0)
                {
                    amountTextBN.Clear();
                    amountTextBN.ForeColor = Color.Black;
                }
            }
        }

        private void payBN_Click(object sender, EventArgs e)
        {
            if (Regex.IsMatch(amountTextBN.Text, @"^\d{0,5}(?:(?:(\.)|(\,))\d{0,2})?$", RegexOptions.Compiled) && Regex.IsMatch(idTextBN.Text, @"^[\d\w_.]+$", RegexOptions.Compiled))
            {
                try
                {
                    string id = idTextBN.Text;
                    amount = decimal.Parse(amountTextBN.Text.Replace(".", ","));
                    decimal committee = MoneyTransactions.CalculateCommittee(amount, bnCmt);
                    amount += committee;
                    if (user.userBalance >= amount)
                    {
                        this.Opacity = 0.2;
                        confirmation.userName = user.userName;
                        confirmation.userPassword = user.userPassword;
                        confirmation.amount = amount;
                        confirmation.ShowDialog();
                        if (!confirmation.ok)
                        {
                            user.userBalance -= amount;
                            this.Opacity = 1;
                            user.UserTransfer(login, user.userBalance);
                            Close();
                            loading.ShowDialog();
                            DialogResult askPrintBill = MessageBox.Show("Вы желаете распечатать чек?", "Печать чека", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                            if (askPrintBill == DialogResult.Yes)
                            {
                                History.AddToHistory(user.userCard, $"-{amount} – Пополнение игрового аккаунта Battlenet {id}");
                                PrintBill.PrintingBill("Battle.net", user.userName, user.userSurname, user.userPatronymic, user.userCard, amount - committee, committee, id, "Blizzard Entertainment.");
                                WinNotify.ShowWinNotify("Пополнение баланса Battle.net", $"Вы успешно пополнили баланс Battle.net в размере {amount - committee}₴\nВаш поточный баланс: {user.userBalance:0.##}₴", 5000);
                                MailDelivery.NotifyByMail(user.userEmail, user.userName, user.userSurname, $"пополнение баланса Battle.net аккаунта: {id}", amount, user.userBalance);
                                sound.Play();
                                mainMenu.login = login;
                                mainMenu.Show();
                            }
                            else
                            {
                                History.AddToHistory(user.userCard, $"-{amount} – Пополнение игрового аккаунта Battlenet {id}");
                                PrintBill.CreatingBill("Battle.net", user.userName, user.userSurname, user.userPatronymic, user.userCard, amount - committee, committee, id, "Blizzard Entertainment.");
                                WinNotify.ShowWinNotify("Пополнение баланса Battle.net", $"Вы успешно пополнили баланс Battle.net в размере {amount - committee}₴\nВаш поточный баланс: {user.userBalance:0.##}₴", 5000);
                                MailDelivery.NotifyByMail(user.userEmail, user.userName, user.userSurname, $"пополнение баланса Battle.net аккаунта: {id}", amount, user.userBalance);
                                sound.Play();
                                mainMenu.login = login;
                                mainMenu.Show();
                            }
                        }
                        else if (confirmation.ok)
                        {
                            this.Opacity = 1;
                            MessageBox.Show($"Извините, {user.userName}, Вы не можете произвести операцию без подтверждения.", "Ошибка перевода", MessageBoxButtons.OK, MessageBoxIcon.Error);
                            paySteam.Focus();
                            return;
                        }
                        else
                        {
                            this.Opacity = 1;
                        }
                    }
                    else
                    {
                        MessageBox.Show($"{user.userName}, у Вас не хватает средств оплатить эти услуги.", "Недостаточно средств", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        amount = 0;
                        return;
                    }
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message);
                    return;
                }
            }
            else
            {
                MessageBox.Show("Введены неверные данные. Проверьте данные и попробуйте снова.", "Ошибка введеных данных", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
        }
    }
}
