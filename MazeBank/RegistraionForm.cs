﻿using System;
using System.Text.RegularExpressions;
using System.Windows.Forms;
using System.IO;
using AuthClass;
using WinNotification;
using MailNotify;

namespace MazeBank
{
    public partial class RegistrationForm : Form
    {
        public RegistrationForm()
        {
            InitializeComponent();

            newPassword.UseSystemPasswordChar = true;
        }
        #region Переменные

        int vis = 1;

        Auth auth = new Auth();

        UserEntering newUser = new UserEntering();

        UserEntering existUser = new UserEntering();

        #endregion Перменные

        private void newPhone_TextChanged(object sender, EventArgs e)
        {
            if(newPhone.Text == "" || newPhone.Text == "3" || newPhone.Text == "38")
            {
                newPhone.Text = "380";
                newPhone.SelectionStart = newPhone.TextLength;
                newPhone.ScrollToCaret();
            }
            if (newPhone.Text == "380" || !Regex.IsMatch(newPhone.Text, @"^380(3|[5-6]|9)[0-9]\d{3}\d{2}\d{2}$", RegexOptions.Compiled))
            {
                newPhoneCheck.Load(@"C:\Users\aleks\source\repos\MazeBank\Pics\wrong.png");
            }
            else if (newPhone.Text != "" || Regex.IsMatch(newPhone.Text, @"^380(3|[5-6]|9)[0-9]\d{3}\d{2}\d{2}$", RegexOptions.Compiled))
            {
                newPhoneCheck.Load(@"C:\Users\aleks\source\repos\MazeBank\Pics\correct.png");
            }
        }

        private void newCard_TextChanged(object sender, EventArgs e)
        {
            if (newCard.Text == "" || !Regex.IsMatch(newCard.Text, @"^(5168|4149)\d{12}$", RegexOptions.Compiled))
            {
                newCardCheck.Load(@"C:\Users\aleks\source\repos\MazeBank\Pics\wrong.png");
            }
            else if (newCard.Text != "" || Regex.IsMatch(newCard.Text, @"^(5168|4149)\d{12}$", RegexOptions.Compiled))
            {
                newCardCheck.Load(@"C:\Users\aleks\source\repos\MazeBank\Pics\correct.png");
            }
        }

        private void newEmail_TextChanged(object sender, EventArgs e)
        {
            if (newEmail.Text == "" || !Regex.IsMatch(newEmail.Text, @"^[A-Za-z]+[\.A-Za-z\d_-]*[A-Za-z\d]+@[A-Za-z]+\.[A-Za-z]{2,6}$", RegexOptions.Compiled))
            {
                newEmailCheck.Load(@"C:\Users\aleks\source\repos\MazeBank\Pics\wrong.png");
            }
            else if (newEmail.Text != "" || Regex.IsMatch(newEmail.Text, @"^[A-Za-z]+[\.A-Za-z\d_-]*[A-Za-z\d]+@[A-Za-z]+\.[A-Za-z]{2,6}$", RegexOptions.Compiled))
            {
                newEmailCheck.Load(@"C:\Users\aleks\source\repos\MazeBank\Pics\correct.png");
            }
        }

        private void newPassword_TextChanged(object sender, EventArgs e)
        {
            if (newPassword.Text == "" || !Regex.IsMatch(newPassword.Text, @"^(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{5,20}$", RegexOptions.Compiled))
            {
                newPassCheck.Load(@"C:\Users\aleks\source\repos\MazeBank\Pics\wrong.png");
            }
            else if (newPassword.Text != "" && Regex.IsMatch(newPassword.Text, @"^(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{5,20}$", RegexOptions.Compiled))
            {
                newPassCheck.Load(@"C:\Users\aleks\source\repos\MazeBank\Pics\correct.png");
            }
        }

        private void newSurname_TextChanged(object sender, EventArgs e)
        {
            if (newSurname.Text == "" || !Regex.IsMatch(newSurname.Text, @"^[А-Я][а-я]+(\-[А-Я][а-я]+)?$", RegexOptions.Compiled))
            {
                newSurnameCheck.Load(@"C:\Users\aleks\source\repos\MazeBank\Pics\wrong.png");
            }
            else if (newSurname.Text != "" && Regex.IsMatch(newSurname.Text, @"^[А-Я][а-я]+(\-[А-Я][а-я]+)?$", RegexOptions.Compiled))
            {
                newSurnameCheck.Load(@"C:\Users\aleks\source\repos\MazeBank\Pics\correct.png");
            }
        }

        private void newName_TextChanged(object sender, EventArgs e)
        {
            if (newName.Text == "" || !Regex.IsMatch(newName.Text, @"^[А-Я][а-я]+(\-[А-Я][а-я]+)?$", RegexOptions.Compiled))
            {
                newNameCheck.Load(@"C:\Users\aleks\source\repos\MazeBank\Pics\wrong.png");
            }
            else if (newName.Text != "" && Regex.IsMatch(newName.Text, @"^[А-Я][а-я]+(\-[А-Я][а-я]+)?$", RegexOptions.Compiled))
            {
                newNameCheck.Load(@"C:\Users\aleks\source\repos\MazeBank\Pics\correct.png");
            }
        }

        private void newPatronymic_TextChanged(object sender, EventArgs e)
        {
            if (newPatronymic.Text == "" || !Regex.IsMatch(newPatronymic.Text, @"^[А-Я][а-я]+?$", RegexOptions.Compiled))
            {
                newPatronymicCheck.Load(@"C:\Users\aleks\source\repos\MazeBank\Pics\wrong.png");
            }
            else if (newPatronymic.Text != "" && Regex.IsMatch(newPatronymic.Text, @"^[А-Я][а-я]+(\-[А-Я][а-я]+)?$", RegexOptions.Compiled))
            {
                newPatronymicCheck.Load(@"C:\Users\aleks\source\repos\MazeBank\Pics\correct.png");
            }
        }

        private void newAdress_TextChanged(object sender, EventArgs e)
        {
            if (newAdress.Text == "" || !Regex.IsMatch(newAdress.Text, @"^(ул.|проул.|просп.)\s[А-ЯІЄЇ][а-яєїі]+((\s([А-ЯІЄЇ][а-яєїі]+)+|\-[А-ЯІЄЇ][а-яєїі]+)+)?\s\d+(\/\d+| [А-Яа-яІЄЇєїі]+|[А-Яа-яєїі]+)?$", RegexOptions.Compiled))
            {
                newAddressCheck.Load(@"C:\Users\aleks\source\repos\MazeBank\Pics\wrong.png");
            }
            else if (newAdress.Text != "" && Regex.IsMatch(newAdress.Text, @"^(ул.|проул.|просп.)\s[А-ЯІЄЇ][а-яєїі]+((\s([А-ЯІЄЇ][а-яєїі]+)+|\-[А-ЯІЄЇ][а-яєїі]+)+)?\s\d+(\/\d+| [А-Яа-яІЄЇєїі]+|[А-Яа-яєїі]+)?$", RegexOptions.Compiled))
            {
                newAddressCheck.Load(@"C:\Users\aleks\source\repos\MazeBank\Pics\correct.png");
            }
        }

        private void backToSettingsPhone_Click(object sender, EventArgs e)
        {
            Close();
            auth.Show();
        }

        private void createAccount_Click(object sender, EventArgs e)
        {
            if(Regex.IsMatch(newAdress.Text, @"^(ул.|проул.|просп.)\s[А-ЯІЄЇ][а-яєїі]+((\s([А-ЯІЄЇ][а-яєїі]+)+|\-[А-ЯІЄЇ][а-яєїі]+)+)?\s\d+(\/\d+| [А-Яа-яІЄЇєїі]+|[А-Яа-яєїі]+)?$", RegexOptions.Compiled) && Regex.IsMatch(newPatronymic.Text, @"^[А-Я][а-я]+(\-[А-Я][а-я]+)?$", RegexOptions.Compiled) && Regex.IsMatch(newName.Text, @"^[А-Я][а-я]+(\-[А-Я][а-я]+)?$", RegexOptions.Compiled) && Regex.IsMatch(newSurname.Text, @"^[А-Я][а-я]+(\-[А-Я][а-я]+)?$", RegexOptions.Compiled) && Regex.IsMatch(newPassword.Text, @"^(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{5,20}$", RegexOptions.Compiled) && Regex.IsMatch(newEmail.Text, @"^[A-Za-z]+[\.A-Za-z\d_-]*[A-Za-z\d]+@[A-Za-z]+\.[A-Za-z]{2,6}$", RegexOptions.Compiled) && Regex.IsMatch(newCard.Text, @"^(5168|4149)\d{12}$", RegexOptions.Compiled) && Regex.IsMatch(newPhone.Text, @"^380(3|[5-6]|9)[0-9]\d{3}\d{2}\d{2}$", RegexOptions.Compiled))
            {
                existUser.UserExist(long.Parse(newPhone.Text));
                existUser.UserExist(newCard.Text);
                if (existUser.counter != 0 && existUser.cardCounter != 0)
                {
                    Random rnd = new Random();
                    int pin = rnd.Next(1000, 9999);
                    newUser.NewUser(newName.Text, newSurname.Text, newPatronymic.Text, newCard.Text, newEmail.Text, long.Parse(newPhone.Text), newAdress.Text, newPassword.Text, pin);
                    string path = $@"C:\Users\aleks\source\repos\MazeBank\Histories\history_of_user#" + newCard.Text + ".txt";
                    File.Create(path);
                    MailDelivery.WelcomeMail(newEmail.Text, newName.Text, newSurname.Text, newCard.Text, pin.ToString());
                    MessageBox.Show($"{newName.Text}, Ваш ПИН-код: {pin}.\nНикому его не говорите!\nТак же мы отправили его Вам на почту.\nСменить его Вы сможете в банкомате или приложении MazeBank.", "Получение ПИН-кода", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    WinNotify.ShowWinNotify("Новый аккаунт", $"{newName.Text} {newPatronymic.Text}, поздравляем с созданием аккаунта!\nДобро пожаловать в MazeBank!", 7000);
                    Close();
                    auth.Show();
                }
                else
                {
                    MessageBox.Show($"{newName.Text}, извините, но такой аккаунт уже существует.", "Такой пользователь существует", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return;
                }
            }
            else
            {
                MessageBox.Show("Извините, Вы не можете создать аккаунт, пока у вас введены неверные данные.", "Ошибка создания аккаунта", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
        }

        private void exit_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void passVis_Click(object sender, EventArgs e)
        {
            if (vis % 2 == 0)
            {
                passVis.Load(@"C:\Users\aleks\source\repos\MazeBank\Eyes\openedEye.png");
                newPassword.UseSystemPasswordChar = false;
                vis++;
            }

            else if (vis % 2 == 1)
            {
                passVis.Load(@"C:\Users\aleks\source\repos\MazeBank\Eyes\closedEye.png");
                newPassword.UseSystemPasswordChar = true;
                vis--;
            }
        }
    }
}
