﻿using System;
using System.Drawing;
using System.Windows.Forms;
using WinNotification;
using BillPrinting;
using AuthClass;
using System.Media;
using HistoryClass;
using MailNotify;

namespace MazeBank
{
    public partial class Tickets : Form
    {
        public Tickets()
        {
            InitializeComponent();
            departAP.SelectedIndex = 0;
        }

        #region Переменные

        public long login;

        MainMenu mainMenu = new MainMenu();

        MoneyTransactions user = new MoneyTransactions();

        ConfirmationForm confirmation = new ConfirmationForm();

        LoadingForm loading = new LoadingForm();

        SoundPlayer sound = new SoundPlayer(@"C:\Users\aleks\source\repos\MazeBank\sound.wav");

        FolderBrowserDialog openDialog = new FolderBrowserDialog();

        string path = null;

        decimal amount = 0;

        int apBonus = 7;

        int tnBonus = 5;

        double bsBonus = 3;

        double cnBonus = 1;

        decimal sale;

        #endregion Переменные

        private void GeneratePlace(int amount, ListBox listBox)
        {
            string[] letters = { "A", "B", "C", "D", "E", "F" };
            Random rnd = new Random();
            for (int i = 0; i < amount; i++)
            {
                listBox.Items.Add(rnd.Next(1, 130) + letters[rnd.Next(0, 5)]);
            }
        }

        private int DropDownWidth(ComboBox comboBox)
        {
            int maxWidth = 0;
            int tmp = 0;
            Label label = new Label();
            foreach (var obj in comboBox.Items)
            {
                label.Text = obj.ToString();
                tmp = label.PreferredWidth;
                if (tmp > maxWidth)
                {
                    maxWidth = tmp;
                }
            }
            label.Dispose();
            return maxWidth;
        }

        private void GenerateCinemaPlace(int amount, ListBox listBox)
        {
            Random rnd = new Random();
            for (int i = 0; i < amount; i++)
            {
                listBox.Items.Add("Место: " + rnd.Next(1, 20) + " Ряд: " + rnd.Next(0, 15));
            }
        }

        private void Tickets_Load(object sender, EventArgs e)
        {
            user.UserConnection(login);
            arrivalAP.DropDownWidth = DropDownWidth(arrivalAP) + 24;
            if(user.theme)
            {
                this.BackColor = Color.FromArgb(56, 56, 56);
                backToAP.ForeColor = Color.White;
                foreach (Control c in this.Controls)
                {
                    if(c is Label)
                    {
                        ((Label)c).ForeColor = Color.White;
                    }
                    if(c is PictureBox)
                    {
                        ((PictureBox)c).BackColor = Color.FromArgb(56, 56, 56);
                    }
                }
                foreach (Control c in dtPanelAP.Controls)
                {
                    if (c is NumericUpDown)
                    {
                        ((NumericUpDown)c).BackColor = Color.FromArgb(80, 80, 80);
                        ((NumericUpDown)c).ForeColor = Color.White;
                    }
                    if (c is DateTimePicker)
                    {
                        ((DateTimePicker)c).BackColor = Color.FromArgb(80, 80, 80);
                        ((DateTimePicker)c).ForeColor = Color.White;
                    }
                    if (c is Label)
                    {
                        ((Label)c).ForeColor = Color.White;
                    }
                }
                foreach (Control c in dtTimeTR.Controls)
                {
                    if (c is NumericUpDown)
                    {
                        ((NumericUpDown)c).BackColor = Color.FromArgb(80, 80, 80);
                        ((NumericUpDown)c).ForeColor = Color.White;
                    }
                    if (c is DateTimePicker)
                    {
                        ((DateTimePicker)c).BackColor = Color.FromArgb(80, 80, 80);
                        ((DateTimePicker)c).ForeColor = Color.White;
                    }
                    if (c is Label)
                    {
                        ((Label)c).ForeColor = Color.White;
                    }
                }
                foreach (Control c in airplane.Controls)
                {
                    if (c is Label)
                    {
                        ((Label)c).ForeColor = Color.White;
                    }
                    if (c is PictureBox)
                    {
                        ((PictureBox)c).BackColor = Color.FromArgb(56, 56, 56);
                    }
                    if(c is NumericUpDown)
                    {
                        ((NumericUpDown)c).BackColor = Color.FromArgb(80, 80, 80);
                        ((NumericUpDown)c).ForeColor = Color.White;
                    }
                    if (c is DateTimePicker)
                    {
                        ((DateTimePicker)c).BackColor = Color.FromArgb(80, 80, 80);
                        ((DateTimePicker)c).ForeColor = Color.White;
                    }
                    if (c is CheckBox)
                    {
                        ((CheckBox)c).ForeColor = Color.White;
                    }
                }
                foreach (Control c in issueAP.Controls)
                {
                    if (c is Label)
                    {
                        ((Label)c).ForeColor = Color.White;
                    }
                }
                foreach (Control c in issueTR.Controls)
                {
                    if (c is Label)
                    {
                        ((Label)c).ForeColor = Color.White;
                    }
                }
                foreach (Control c in issueBS.Controls)
                {
                    if (c is Label)
                    {
                        ((Label)c).ForeColor = Color.White;
                    }
                }
                foreach (Control c in issueCinema.Controls)
                {
                    if (c is Label)
                    {
                        ((Label)c).ForeColor = Color.White;
                    }
                }
                foreach (Control c in train.Controls)
                {
                    if (c is Label)
                    {
                        ((Label)c).ForeColor = Color.White;
                    }
                    if (c is PictureBox)
                    {
                        ((PictureBox)c).BackColor = Color.FromArgb(56, 56, 56);
                    }
                    if (c is NumericUpDown)
                    {
                        ((NumericUpDown)c).BackColor = Color.FromArgb(80, 80, 80);
                        ((NumericUpDown)c).ForeColor = Color.White;
                    }
                    if (c is DateTimePicker)
                    {
                        ((DateTimePicker)c).BackColor = Color.FromArgb(80, 80, 80);
                        ((DateTimePicker)c).ForeColor = Color.White;
                    }
                    if (c is CheckBox)
                    {
                        ((CheckBox)c).ForeColor = Color.White;
                    }
                }
                foreach (Control c in cinema.Controls)
                {
                    if (c is Label)
                    {
                        ((Label)c).ForeColor = Color.White;
                    }
                    if (c is PictureBox)
                    {
                        ((PictureBox)c).BackColor = Color.FromArgb(56, 56, 56);
                    }
                    if (c is NumericUpDown)
                    {
                        ((NumericUpDown)c).BackColor = Color.FromArgb(80, 80, 80);
                        ((NumericUpDown)c).ForeColor = Color.White;
                    }
                    if (c is DateTimePicker)
                    {
                        ((DateTimePicker)c).BackColor = Color.FromArgb(80, 80, 80);
                        ((DateTimePicker)c).ForeColor = Color.White;
                    }
                    if (c is CheckBox)
                    {
                        ((CheckBox)c).ForeColor = Color.White;
                    }
                }
                foreach (Control c in bus.Controls)
                {
                    if (c is Label)
                    {
                        ((Label)c).ForeColor = Color.White;
                    }
                    if (c is PictureBox)
                    {
                        ((PictureBox)c).BackColor = Color.FromArgb(56, 56, 56);
                    }
                    if (c is NumericUpDown)
                    {
                        ((NumericUpDown)c).BackColor = Color.FromArgb(80, 80, 80);
                        ((NumericUpDown)c).ForeColor = Color.White;
                    }
                    if (c is DateTimePicker)
                    {
                        ((DateTimePicker)c).BackColor = Color.FromArgb(80, 80, 80);
                        ((DateTimePicker)c).ForeColor = Color.White;
                    }
                    if (c is CheckBox)
                    {
                        ((CheckBox)c).ForeColor = Color.White;
                    }
                }
                foreach (Control c in arrivalPanelBS.Controls)
                {
                    if (c is NumericUpDown)
                    {
                        ((NumericUpDown)c).BackColor = Color.FromArgb(80, 80, 80);
                        ((NumericUpDown)c).ForeColor = Color.White;
                    }
                    if (c is DateTimePicker)
                    {
                        ((DateTimePicker)c).BackColor = Color.FromArgb(80, 80, 80);
                        ((DateTimePicker)c).ForeColor = Color.White;
                    }
                    if (c is Label)
                    {
                        ((Label)c).ForeColor = Color.White;
                    }
                }
                exitBut.Load(@"C:\Users\aleks\source\repos\MazeBank\ExitPics\exitBlackTheme.png");
                backBut.Load(@"C:\Users\aleks\source\repos\MazeBank\Pics\backWhite.png");
                trExit.Load(@"C:\Users\aleks\source\repos\MazeBank\ExitPics\exitBlackTheme.png");
                trBack.Load(@"C:\Users\aleks\source\repos\MazeBank\Pics\backWhite.png");
                issueTRExit.Load(@"C:\Users\aleks\source\repos\MazeBank\ExitPics\exitBlackTheme.png");
                issueTRBack.Load(@"C:\Users\aleks\source\repos\MazeBank\Pics\backWhite.png");
                apExit.Load(@"C:\Users\aleks\source\repos\MazeBank\ExitPics\exitBlackTheme.png");
                apBack.Load(@"C:\Users\aleks\source\repos\MazeBank\Pics\backWhite.png");
                apIssueExit.Load(@"C:\Users\aleks\source\repos\MazeBank\ExitPics\exitBlackTheme.png");
                apIssueBack.Load(@"C:\Users\aleks\source\repos\MazeBank\Pics\backWhite.png");
                backBS.Load(@"C:\Users\aleks\source\repos\MazeBank\Pics\backWhite.png");
                exitBS.Load(@"C:\Users\aleks\source\repos\MazeBank\ExitPics\exitBlackTheme.png");
                issueBSBack.Load(@"C:\Users\aleks\source\repos\MazeBank\Pics\backWhite.png");
                issueBsExit.Load(@"C:\Users\aleks\source\repos\MazeBank\ExitPics\exitBlackTheme.png");
                cinemaBack.Load(@"C:\Users\aleks\source\repos\MazeBank\Pics\backWhite.png");
                cinemaExit.Load(@"C:\Users\aleks\source\repos\MazeBank\ExitPics\exitBlackTheme.png");
                cinemaIssueBack.Load(@"C:\Users\aleks\source\repos\MazeBank\Pics\backWhite.png");
                cinemaIssueExit.Load(@"C:\Users\aleks\source\repos\MazeBank\ExitPics\exitBlackTheme.png");
            }
            dtTimeTR.Visible = false;
            arrivalPanelBS.Visible = false;
            issueCinema.Hide();
            cinema.Hide();
            bus.Hide();
            issueTR.Hide();
            issueAP.Hide();
            issueBS.Hide();
            airplane.Hide();
            train.Hide();
            backToAP.Visible = false;
            backToTR.Visible = false;
            backToBS.Visible = false;
            departDateAP.Value = DateTime.Today;
            departDateAP.MinDate = DateTime.Today;
            arrivalDateAP.MinDate = DateTime.Today;
            departDateTR.MinDate = DateTime.Today;
            arrivalDateTR.MinDate = DateTime.Today;
            departDateTR.Value = DateTime.Today;
            departDateBS.MinDate = DateTime.Today;
            arrivalDateBS.MinDate = DateTime.Today;
            departDateBS.Value = DateTime.Today;
            moviesSchedule.MinDate = DateTime.Today;
            moviesSchedule.Value = DateTime.Today;
            WinNotify.ShowWinNotify("Экономьте с MazeBank'ом!", "Покупайте билеты через MazeBank и получите скидку! Авиабилеты – 7%, билеты на поезд – 5%, билеты на автобусы – 3%, билеты в кино – 1%", 10000);
        }

        private void exitBut_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void backBut_Click(object sender, EventArgs e)
        {
            this.Close();
            mainMenu.login = login;
            mainMenu.Show();
        }

        private void airplanePic_Click(object sender, EventArgs e)
        {
            airplane.Show();
        }

        private void apExit_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void apBack_Click(object sender, EventArgs e)
        {
            amount = 0;
            airplane.Hide();
        }

        private void issueBtnAP_Click(object sender, EventArgs e)
        {
            if(oneWay.Checked || twoWays.Checked)
            {
                if (!string.IsNullOrEmpty(departAP.Text) && !string.IsNullOrEmpty(arrivalAP.Text) && oneWay.Checked)
                {
                    Random rnd = new Random();
                    amount = rnd.Next(3323, 8539) * amountOfTicketsAP.Value;
                    issueAP.Show();
                    amountAP.Text = $"Сумма к оплате: {amount}";
                    amountAP.Left = (this.Width - amountAP.Width) / 2;
                    departPlaceAP.Text = "Вылет из: " + departAP.Items[departAP.SelectedIndex].ToString();
                    departPlaceAP.Left = (this.Width - departPlaceAP.Width) / 2;
                    placeAP.Left = (this.Width - placeAP.Width) / 2;
                    GeneratePlace(Convert.ToInt32(amountOfTicketsAP.Value), placesAP);
                    departTimeAP.Text = departDateAP.Value.Date.ToShortDateString() + " " + departHourAI.Value + ":" + departMinAP.Value;
                    departTimeAP.Left = (this.Width - departTimeAP.Width) / 2;
                    arrivalPlaceAP.Text = "Прибытие в: " + arrivalAP.Items[arrivalAP.SelectedIndex].ToString();
                    arrivalPlaceAP.Left = (this.Width - arrivalPlaceAP.Width) / 2;
                    backToAP.Visible = false;
                    arrivalTimeAP.Visible = false;
                }
                else if (!string.IsNullOrEmpty(departAP.Text) && !string.IsNullOrEmpty(arrivalAP.Text) && twoWays.Checked)
                {
                    if (departDateAP.Value.Day < arrivalDateAP.Value.Day || departDateAP.Value.Month < arrivalDateAP.Value.Month)
                    {
                        Random rnd = new Random();
                        amount = rnd.Next(3323, 8539) * 2 * amountOfTicketsAP.Value;
                        issueAP.Show();
                        amountAP.Text = $"Сумма к оплате: {amount}";
                        amountAP.Left = (this.Width - amountAP.Width) / 2;
                        departPlaceAP.Text = "Вылет из: " + departAP.Items[departAP.SelectedIndex].ToString();
                        departPlaceAP.Left = (this.Width - departPlaceAP.Width) / 2;
                        placeAP.Left = (this.Width - placeAP.Width) / 2;
                        GeneratePlace(Convert.ToInt32(amountOfTicketsAP.Value), placesAP);
                        departTimeAP.Text = departDateAP.Value.Date.ToShortDateString() + " " + departHourAI.Value + ":" + departMinAP.Value;
                        departTimeAP.Left = (this.Width - departTimeAP.Width) / 2;
                        arrivalPlaceAP.Text = "Прибытие в: " + arrivalAP.Items[arrivalAP.SelectedIndex].ToString();
                        arrivalPlaceAP.Left = (this.Width - arrivalPlaceAP.Width) / 2;
                        backToAP.Visible = true;
                        arrivalTimeAP.Visible = true;
                        arrivalTimeAP.Text = arrivalDateAP.Value.Date.ToShortDateString() + " " + arrivalHourAP.Value + ":" + ArrivalMinAP.Value;
                        arrivalTimeAP.Left = (this.Width - arrivalTimeAP.Width) / 2;
                    }
                    else
                    {
                        MessageBox.Show("Вы ошиблись выбором даты.", "Ошибка выбора даты.", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        return;
                    }
                }
                else
                {
                    MessageBox.Show("Вы ввели некорректные данные!\nПроверьте их и попробуйте еще раз.", "Ошибка данных", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return;
                }
            }
            else
            {
                MessageBox.Show($"{user.userName}, Вы должны выбрать какой-либо пункт, он не должен оставаться невыбранынм.", "Ошибка выбора пункта", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
        }

        private void oneWay_CheckedChanged(object sender, EventArgs e)
        {
            if (oneWay.Checked)
            {
                dtPanelAP.Visible = false;
                twoWays.Checked = false;
            }
        }

        private void twoWays_CheckedChanged(object sender, EventArgs e)
        {
            if (twoWays.Checked)
            {
                dtPanelAP.Visible = true;
                oneWay.Checked = false;
            }
        }

        private void apIssueBack_Click(object sender, EventArgs e)
        {
            issueAP.Hide();
            amount = 0;
            placesAP.Items.Clear();
        }

        private void apIssueExit_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void apPay_Click(object sender, EventArgs e)
        {
            sale = MoneyTransactions.CalculateCommittee(amount, apBonus);
            amount -= sale;
            if (user.userBalance >= amount)
            {
                this.Visible = false;
                confirmation.userName = user.userName;
                confirmation.userPassword = user.userPassword;
                confirmation.amount = amount;
                confirmation.ShowDialog();
                if (!confirmation.ok)
                {
                    user.userBalance -= amount;
                    this.Visible = true;
                    user.UserTransfer(login, user.userBalance);
                    Close();
                    loading.ShowDialog();
                    if (oneWay.Checked)
                    {
                        do
                        {
                            if (openDialog.ShowDialog() == DialogResult.OK)
                            {
                                path = openDialog.SelectedPath;
                                break;
                            }
                            else
                            {
                                MessageBox.Show("Вы ничего не выбрали!", "Ошибка места сохранения", MessageBoxButtons.OK, MessageBoxIcon.Warning, MessageBoxDefaultButton.Button1, MessageBoxOptions.ServiceNotification);
                            }
                        } while (openDialog.SelectedPath != null);
                        PrintBill.PrintingBill(path, "АВИАБИЛЕТ", user.userName, user.userSurname, user.userPatronymic, (amount / amountOfTicketsAP.Value), (sale / amountOfTicketsAP.Value), departAP.Items[departAP.SelectedIndex].ToString(), arrivalAP.Items[arrivalAP.SelectedIndex].ToString(), departDateAP.Value.ToShortDateString() + " " + departHourAI.Value + ":" + ArrivalMinAP.Value, placesAP, "аиабилетов", "🛫", user.userEmail);
                        History.AddToHistory(user.userCard, $"-{amount} – Покупка авиабилетов");
                        WinNotify.ShowWinNotify("Покупка авиабилетов", $"{user.userName}, Вы приобрели {amountOfTicketsAP.Value} билет(ов) через приложение MazeBank. Сумма к оплате: {amount - sale}₴. Благодарим, что пользуетесь нашим сервисом.\nВаш поточный баланс: {user.userBalance}₴", 10000);
                        sound.Play();
                        System.Diagnostics.Process.Start("explorer", path);
                        mainMenu.login = login;
                        Close();
                        mainMenu.Show();
                    }
                    else if (twoWays.Checked)
                    {
                        do
                        {
                            if (openDialog.ShowDialog() == DialogResult.OK)
                            {
                                path = openDialog.SelectedPath;
                                break;
                            }
                            else
                            {
                                MessageBox.Show("Вы ничего не выбрали!", "Ошибка места сохранения", MessageBoxButtons.OK, MessageBoxIcon.Warning, MessageBoxDefaultButton.Button1, MessageBoxOptions.ServiceNotification);
                            }
                        } while (openDialog.SelectedPath != null);
                        path = openDialog.SelectedPath;
                        openDialog.Dispose();
                        History.AddToHistory(user.userCard, $"-{amount} – Покупка авиабилетов");
                        PrintBill.PrintingBill(path, "АВИАБИЛЕТ", "авиабилетов", user.userName, user.userSurname, user.userPatronymic, (amount / amountOfTicketsAP.Value), (sale / amountOfTicketsAP.Value), departAP.Items[departAP.SelectedIndex].ToString(), arrivalAP.Items[arrivalAP.SelectedIndex].ToString(), departDateAP.Value.ToShortDateString() + " " + departHourAI.Value + ":" + ArrivalMinAP.Value, arrivalDateAP.Value.ToShortDateString() + " " + arrivalHourAP.Value + ":" + ArrivalMinAP.Value, placesAP, user.userEmail, " 🛫");
                        WinNotify.ShowWinNotify("Покупка авиабилетов", $"{user.userName}, Вы приобрели {amountOfTicketsAP.Value} билет(ов) через приложение MazeBank. Сумма к оплате: {amount - sale}₴. Благодарим, что пользуетесь нашим сервисом.\nВаш поточный баланс: {user.userBalance}₴", 10000);
                        System.Diagnostics.Process.Start("explorer", path);
                        sound.Play();
                        mainMenu.login = login;
                        Close();
                        mainMenu.Show();
                    }
                }
                else if (confirmation.ok)
                {
                    this.Visible = true;
                    MessageBox.Show($"{user.userName}, Вы не можете провести операцию без подтверждения.", "Ошибка подтверждения", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return;
                }
                else
                {
                    this.Visible = true;
                }
            }
            else
            {
                MessageBox.Show($"{user.userName}, у Вас недостаточно средств для проведения операции.", "Недостаточно средств", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
        }

        private void trainPic_Click(object sender, EventArgs e)
        {
            train.Show();
        }

        private void trExit_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void trBack_Click(object sender, EventArgs e)
        {
            train.Hide();
            amount = 0;
        }

        private void oneWayTr_CheckedChanged(object sender, EventArgs e)
        {
            if (oneWayTr.Checked)
            {
                dtTimeTR.Visible = false;
                twoWaysTr.Checked = false;
            }
        }

        private void twoWaysTr_CheckedChanged(object sender, EventArgs e)
        {
            if (twoWaysTr.Checked)
            {
                oneWayTr.Checked = false;
                dtTimeTR.Visible = true;
            }
        }

        private void issueBtnTR_Click(object sender, EventArgs e)
        {
            if(twoWaysTr.Checked || oneWayTr.Checked)
            {
                if (!string.IsNullOrEmpty(departTR.Text) && !string.IsNullOrEmpty(arrivalTR.Text) && arrivalTR.Text != departTR.Text && oneWayTr.Checked)
                {
                    issueTR.Show();
                    Random rnd = new Random();
                    amount = rnd.Next(1350, 4325) * amountOfTicketsTR.Value;
                    toPayTR.Left = (this.Width - toPayTR.Width) / 2;
                    backToTR.Visible = false;
                    departFromTR.Text = "Отправка из: " + departTR.Items[departTR.SelectedIndex].ToString();
                    departFromTR.Left = (this.Width - departFromTR.Width) / 2;
                    departDateLabelTR.Text = departDateAP.Value.ToShortDateString() + " " + departTimeHourTR.Value + ":" + departTimeMinTR.Value;
                    departDateLabelTR.Left = (this.Width - departDateLabelTR.Width) / 2;
                    arrivalToTR.Text = "Прибывает в: " + arrivalTR.Items[arrivalTR.SelectedIndex].ToString();
                    arrivalToTR.Left = (this.Width - arrivalToTR.Width) / 2;
                    toPayTR.Text = "Сумма к оплате: " + amount;
                    toPayTR.Left = (this.Width - toPayTR.Width) / 2;
                    GeneratePlace(Convert.ToInt32(amountOfTicketsTR.Value), placesTR);
                }
                else if (!string.IsNullOrEmpty(departTR.Text) && !string.IsNullOrEmpty(arrivalTR.Text) && arrivalTR.Text != departTR.Text && twoWaysTr.Checked)
                {
                    if (departDateTR.Value.Day < arrivalDateTR.Value.Day || departDateTR.Value.Month < arrivalDateTR.Value.Month)
                    {
                        issueTR.Show();
                        Random rnd = new Random();
                        amount = rnd.Next(1350, 4325) * amountOfTicketsTR.Value * 2;
                        toPayTR.Text = "Сумма к оплате: " + amount;
                        toPayTR.Left = (this.Width - toPayTR.Width) / 2;
                        backToTR.Visible = true;
                        departFromTR.Text = "Отправка из: " + departTR.Items[departTR.SelectedIndex].ToString();
                        departFromTR.Left = (this.Width - departFromTR.Width) / 2;
                        departDateLabelTR.Text = departDateTR.Value.ToShortDateString() + " " + departTimeHourTR.Value + ":" + departTimeMinTR.Value;
                        departDateLabelTR.Left = (this.Width - departDateLabelTR.Width) / 2;
                        arrivalToTR.Text = "Прибывает в: " + arrivalTR.Items[arrivalTR.SelectedIndex].ToString();
                        arrivalToTR.Left = (this.Width - arrivalToTR.Width) / 2;
                        arrivalDateLabelTR.Text = arrivalDateTR.Value.ToShortDateString() + " " + arrivalTimeHourTR.Value + ":" + arrivalTimeMinTR.Value;
                        toPayTR.Text = "Сумма к оплате: " + amount;
                        toPayTR.Left = (this.Width - toPayTR.Width) / 2;
                        GeneratePlace(Convert.ToInt32(amountOfTicketsTR.Value), placesTR);
                    }
                    else
                    {
                        MessageBox.Show("Вы ввели некорректную дату. Проверьте данные.", "Ошибка даты", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        return;
                    }
                }
                else
                {
                    MessageBox.Show("Вы ошибились направление поездки! Проверьте данные.", "Ошибка направления", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return;
                }
            }
            else
            {
                MessageBox.Show("Вы не выбрали направление поездки.", "Ошибка направления", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
        }

        private void issueTRExit_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void issueTRBack_Click(object sender, EventArgs e)
        {
            issueTR.Hide();
            amount = 0;
            placesTR.Items.Clear();
        }

        private void payTR_Click(object sender, EventArgs e)
        {
            sale = MoneyTransactions.CalculateCommittee(amount, tnBonus);
            amount -= sale;
            if (user.userBalance >= amount)
            {
                this.Visible = false;
                confirmation.userName = user.userName;
                confirmation.userPassword = user.userPassword;
                confirmation.amount = amount;
                confirmation.ShowDialog();
                if (!confirmation.ok)
                {
                    user.userBalance -= amount;
                    this.Visible = true;
                    user.UserTransfer(login, user.userBalance);
                    Close();
                    loading.ShowDialog();
                    if (oneWayTr.Checked)
                    {
                        do
                        {
                            if (openDialog.ShowDialog() == DialogResult.OK)
                            {
                                path = openDialog.SelectedPath;
                                break;
                            }
                            else
                            {
                                MessageBox.Show("Вы ничего не выбрали!", "Ошибка места сохранения", MessageBoxButtons.OK, MessageBoxIcon.Warning, MessageBoxDefaultButton.Button1, MessageBoxOptions.ServiceNotification);
                            }
                        } while (openDialog.SelectedPath != null);
                        path = openDialog.SelectedPath;
                        History.AddToHistory(user.userCard, $"-{amount} – Покупка билетов на поезд");
                        PrintBill.PrintingBill(path, "БИЛЕТЫ НА ПОЕЗД", user.userName, user.userSurname, user.userPatronymic, (amount / amountOfTicketsTR.Value), (sale / amountOfTicketsTR.Value), departTR.Items[departTR.SelectedIndex].ToString(), arrivalTR.Items[arrivalTR.SelectedIndex].ToString(), departDateTR.Value.ToShortDateString() + " " + departTimeHourTR.Value + ":" + arrivalTimeMinTR.Value, placesTR, "билеты на поезд", "🚝", user.userEmail);
                        WinNotify.ShowWinNotify("Покупка билетов на поезд", $"{user.userName}, Вы приобрели {amountOfTicketsTR.Value} билет(ов) через приложение MazeBank. Сумма к оплате: {amount - sale}₴. Благодарим, что пользуетесь нашим сервисом.\nВаш поточный баланс: {user.userBalance}₴", 10000);
                        sound.Play();
                        System.Diagnostics.Process.Start("explorer", path);
                        mainMenu.login = login;
                        Close();
                        mainMenu.Show();
                    }
                    else if (twoWaysTr.Checked)
                    {
                        do
                        {
                            if (openDialog.ShowDialog() == DialogResult.OK)
                            {
                                path = openDialog.SelectedPath;
                                break;
                            }
                            else
                            {
                                MessageBox.Show("Вы ничего не выбрали!", "Ошибка места сохранения", MessageBoxButtons.OK, MessageBoxIcon.Warning, MessageBoxDefaultButton.Button1, MessageBoxOptions.ServiceNotification);
                            }
                        } while (openDialog.SelectedPath != null);
                        History.AddToHistory(user.userCard, $"-{amount} – Покупка билетов на поезд");
                        PrintBill.PrintingBill(path, "БИЛЕТ НА ПОЕЗД", "билеты на поезд", user.userName, user.userSurname, user.userPatronymic, (amount / amountOfTicketsTR.Value), (sale / amountOfTicketsTR.Value), departTR.Items[departTR.SelectedIndex].ToString(), arrivalTR.Items[arrivalTR.SelectedIndex].ToString(), departDateTR.Value.ToShortDateString() + " " + departTimeHourTR.Value + ":" + arrivalTimeMinTR.Value, arrivalDateTR.Value.ToShortDateString() + " " + arrivalTimeHourTR.Value + ":" + arrivalTimeMinTR.Value, placesTR, user.userEmail, " 🚝");
                        WinNotify.ShowWinNotify("Покупка билетов на поезд", $"{user.userName}, Вы приобрели {amountOfTicketsTR.Value} билет(ов) через приложение MazeBank. Сумма к оплате: {amount - sale}₴. Благодарим, что пользуетесь нашим сервисом.\nВаш поточный баланс: {user.userBalance}₴", 10000);
                        sound.Play();
                        System.Diagnostics.Process.Start("explorer", path);
                        mainMenu.login = login;
                        Close();
                        mainMenu.Show();
                    }
                }
                else if (confirmation.ok)
                {
                    this.Visible = true;
                    MessageBox.Show($"{user.userName}, Вы не можете провести операцию без подтверждения.", "Ошибка подтверждения", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return;
                }
                else
                {
                    this.Visible = true;
                }
            }
            else
            {
                MessageBox.Show($"{user.userName}, у Вас недостаточно средств для проведения операции.", "Недостаточно средств", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
        }

        private void backBS_Click(object sender, EventArgs e)
        {
            bus.Hide();
        }

        private void exitBS_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void busPic_Click(object sender, EventArgs e)
        {
            bus.Show();
        }

        private void oneWayBS_CheckedChanged(object sender, EventArgs e)
        {
            if (oneWayBS.Checked)
            {
                oneWayBS.Checked = true;
                twoWaysBS.Checked = false;
                arrivalPanelBS.Visible = false;
            }
        }

        private void twoWaysBS_CheckedChanged(object sender, EventArgs e)
        {
            if (twoWaysBS.Checked)
            {
                oneWayBS.Checked = false;
                arrivalPanelBS.Visible = true;
            }
        }

        private void issueBtnBS_Click(object sender, EventArgs e)
        {
            if (twoWaysBS.Checked || oneWayBS.Checked)
            {
                if (!string.IsNullOrEmpty(departBS.Text) && !string.IsNullOrEmpty(arrivalBS.Text) && arrivalBS.Text != departBS.Text && oneWayBS.Checked)
                {
                    issueBS.Show();
                    backToBS.Visible = false;
                    arrivalTimeLblBS.Visible = false;
                    Random rnd = new Random();
                    amount = rnd.Next(450, 2351) * amountOfTicketsBS.Value;
                    toPayBS.Text = "Сумма к оплате: " + amount;
                    toPayBS.Left = (this.Width - toPayBS.Width) / 2;
                    departFromBS.Text = "Отправка из: " + departBS.Items[departBS.SelectedIndex].ToString();
                    departFromBS.Left = (this.Width - departFromBS.Width) / 2;
                    departTimeLblBS.Text = departDateBS.Value.ToShortDateString() + " " + departTimeHourBS.Value + ":" + departTimeMinBS.Value;
                    departTimeLblBS.Left = (this.Width - departTimeLblBS.Width) / 2;
                    arrivalToLblBS.Text = "Прибытие в: " + arrivalBS.Items[arrivalBS.SelectedIndex].ToString();
                    arrivalToLblBS.Left = (this.Width - arrivalToLblBS.Width) / 2;
                    GeneratePlace(Convert.ToInt32(amountOfTicketsBS.Value), placesBS);
                }
                else if (!string.IsNullOrEmpty(departBS.Text) && !string.IsNullOrEmpty(arrivalBS.Text) && arrivalBS.Text != departBS.Text && twoWaysBS.Checked)
                {
                    if (departDateBS.Value.Day < arrivalDateBS.Value.Day || departDateBS.Value.Month < arrivalDateBS.Value.Month)
                    {
                        issueBS.Show();
                        backToBS.Visible = true;
                        Random rnd = new Random();
                        arrivalTimeLblBS.Visible = true;
                        amount = rnd.Next(450, 2351) * amountOfTicketsBS.Value;
                        toPayBS.Text = "Сумма к оплате: " + amount;
                        toPayBS.Left = (this.Width - toPayBS.Width) / 2;
                        departFromBS.Text = "Отправка из: " + departBS.Items[departBS.SelectedIndex].ToString();
                        departFromBS.Left = (this.Width - departFromBS.Width) / 2;
                        departTimeLblBS.Text = departDateBS.Value.ToShortDateString() + " " + departTimeHourBS.Value + ":" + departTimeMinBS.Value;
                        departTimeLblBS.Left = (this.Width - departTimeLblBS.Width) / 2;
                        arrivalToLblBS.Text = "Прибытие в: " + arrivalBS.Items[arrivalBS.SelectedIndex].ToString();
                        arrivalToLblBS.Left = (this.Width - arrivalToLblBS.Width) / 2;
                        arrivalTimeLblBS.Text = arrivalDateBS.Value.ToShortDateString() + " " + arrivalTimeHourBS.Value + ":" + arrivalTimeMinBS.Value;
                        arrivalTimeLblBS.Left = (this.Width - arrivalTimeLblBS.Width) / 2;
                        GeneratePlace(Convert.ToInt32(amountOfTicketsBS.Value), placesBS);
                    }
                    else
                    {
                        MessageBox.Show("Вы ввели некорректную дату. Проверьте данные.", "Ошибка даты", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        return;
                    }
                }
                else
                {
                    MessageBox.Show("Вы ошибились направление поездки! Проверьте данные.", "Ошибка направления", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return;
                }
            }
            else
            {
                MessageBox.Show("Вы не выбрали направление поездки.", "Ошибка направления", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
        }

        private void issueBSBack_Click(object sender, EventArgs e)
        {
            issueBS.Hide();
            placesBS.Items.Clear();
        }

        private void issueBsExit_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void payBS_Click(object sender, EventArgs e)
        {
            sale = MoneyTransactions.CalculateCommittee(amount, bsBonus);
            amount -= sale;
            if (user.userBalance >= amount)
            {
                this.Visible = false;
                confirmation.userName = user.userName;
                confirmation.userPassword = user.userPassword;
                confirmation.amount = amount;
                confirmation.ShowDialog();
                if (!confirmation.ok)
                {
                    user.userBalance -= amount;
                    this.Visible = true;
                    user.UserTransfer(login, user.userBalance);
                    Close();
                    loading.ShowDialog();
                    if (oneWayBS.Checked)
                    {
                        do
                        {
                            if (openDialog.ShowDialog() == DialogResult.OK)
                            {
                                path = openDialog.SelectedPath;
                                break;
                            }
                            else
                            {
                                MessageBox.Show("Вы ничего не выбрали!", "Ошибка места сохранения", MessageBoxButtons.OK, MessageBoxIcon.Warning, MessageBoxDefaultButton.Button1, MessageBoxOptions.ServiceNotification);
                            }
                        } while (openDialog.SelectedPath != null);
                        PrintBill.PrintingBill(path, "БИЛЕТ НА АВТОБУС", user.userName, user.userSurname, user.userPatronymic, (amount / amountOfTicketsBS.Value), Math.Round((sale / amountOfTicketsBS.Value), 2), departBS.Items[departBS.SelectedIndex].ToString(), arrivalBS.Items[arrivalBS.SelectedIndex].ToString(), departDateBS.Value.ToShortDateString() + " " + departTimeHourBS.Value + ":" + departTimeMinBS.Value, placesBS, "билеты на автобус", " 🚌", user.userEmail);
                        History.AddToHistory(user.userCard, $"-{amount} – Покупка билетов на автобус");
                        WinNotify.ShowWinNotify("Покупка билетов на автобус", $"{user.userName}, Вы приобрели {amountOfTicketsBS.Value} билет(ов) через приложение MazeBank. Сумма к оплате: {amount - sale}₴. Благодарим, что пользуетесь нашим сервисом.\nВаш поточный баланс: {user.userBalance}₴", 10000);
                        sound.Play();
                        System.Diagnostics.Process.Start("explorer", path);
                        mainMenu.login = login;
                        Close();
                        mainMenu.Show();
                    }
                    else if (twoWaysBS.Checked)
                    {
                        do
                        {
                            if (openDialog.ShowDialog() == DialogResult.OK)
                            {
                                path = openDialog.SelectedPath;
                                break;
                            }
                            else
                            {
                                MessageBox.Show("Вы ничего не выбрали!", "Ошибка места сохранения", MessageBoxButtons.OK, MessageBoxIcon.Warning, MessageBoxDefaultButton.Button1, MessageBoxOptions.ServiceNotification);
                            }
                        } while (openDialog.SelectedPath != null);
                        PrintBill.PrintingBill(path, "БИЛЕТ НА АВТОБУС", "билеты на автобус", user.userName, user.userSurname, user.userPatronymic, (amount / amountOfTicketsBS.Value), Math.Round((sale / amountOfTicketsBS.Value), 2), departBS.Items[departBS.SelectedIndex].ToString(), arrivalBS.Items[arrivalBS.SelectedIndex].ToString(), departDateBS.Value.ToShortDateString() + " " + departTimeHourBS.Value + ":" + arrivalTimeMinBS.Value, arrivalDateBS.Value.ToShortDateString() + " " + arrivalTimeHourBS.Value + ":" + arrivalTimeMinBS.Value, placesBS, user.userEmail, " 🚌");
                        History.AddToHistory(user.userCard, $"-{amount} – Покупка билетов на автобус");
                        WinNotify.ShowWinNotify("Покупка билетов на автобус", $"{user.userName}, Вы приобрели {amountOfTicketsBS.Value} билет(ов) через приложение MazeBank. Сумма к оплате: {amount - sale}₴. Благодарим, что пользуетесь нашим сервисом.\nВаш поточный баланс: {user.userBalance}₴", 10000);
                        System.Diagnostics.Process.Start("explorer", path);
                        mainMenu.login = login;
                        Close();
                        mainMenu.Show();
                    }
                }
                else if (confirmation.ok)
                {
                    this.Visible = true;
                    MessageBox.Show($"{user.userName}, Вы не можете провести операцию без подтверждения.", "Ошибка подтверждения", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return;
                }
                else
                {
                    this.Visible = true;
                }
            }
            else
            {
                MessageBox.Show($"{user.userName}, у Вас недостаточно средств для проведения операции.", "Недостаточно средств", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
        }

        private void cinemaExit_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void cinemaPic_Click(object sender, EventArgs e)
        {
            cinema.Show();
        }

        private void issueMovie_Click(object sender, EventArgs e)
        {
            if(movies.Text != "")
            {
                Random rnd = new Random();
                issueCinema.Show();
                amount = rnd.Next(65, 195) * amountOfTickets.Value;
                movieTitle.Text = movies.Items[movies.SelectedIndex].ToString();
                movieTitle.Left = (this.Width - movieTitle.Width) / 2;
                movieDate.Text = moviesSchedule.Value.ToLongDateString() + " " + cinemaHour.Value + ":" + cinemaMin.Value;
                movieDate.Left = (this.Width - movieDate.Width) / 2;
                GenerateCinemaPlace(Convert.ToInt32(amountOfTickets.Value), cinemaTickets);
                toPayCinema.Text = "Сумма к оплате: " + amount;
                toPayCinema.Left = (this.Width - toPayCinema.Width) / 2;
            }
            else
            {
                MessageBox.Show($"{user.userName}, выберите фильм.", "Ошибка выбора фильма", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
        }

        private void cinemaIssueBack_Click(object sender, EventArgs e)
        {
            issueCinema.Hide();
            amount = 0;
            cinemaTickets.Items.Clear();
        }

        private void cinemaIssueExit_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void cinemaBack_Click(object sender, EventArgs e)
        {
            cinema.Hide();
        }

        private void payCinema_Click(object sender, EventArgs e)
        {
            sale = MoneyTransactions.CalculateCommittee(amount, cnBonus);
            amount -= sale;
            if (user.userBalance >= amount)
            {
                this.Visible = false;
                confirmation.userName = user.userName;
                confirmation.userPassword = user.userPassword;
                confirmation.amount = amount;
                confirmation.ShowDialog();
                if (!confirmation.ok)
                {
                    user.userBalance -= amount;
                    this.Visible = true;
                    user.UserTransfer(login, user.userBalance);
                    Close();
                    loading.ShowDialog();
                    do
                    {
                        if (openDialog.ShowDialog() == DialogResult.OK)
                        {
                            path = openDialog.SelectedPath;
                            break;
                        }
                        else
                        {
                            MessageBox.Show("Вы ничего не выбрали!", "Ошибка места сохранения", MessageBoxButtons.OK, MessageBoxIcon.Warning, MessageBoxDefaultButton.Button1, MessageBoxOptions.ServiceNotification);
                        }
                    } while (openDialog.SelectedPath != null);
                    PrintBill.PrintingBill(path, user.userName, user.userSurname, user.userPatronymic, amount / amountOfTickets.Value, sale / amountOfTickets.Value, cinemaTickets, movies.Items[movies.SelectedIndex].ToString(), moviesSchedule.Value.ToLongDateString() + " " + cinemaHour.Value + ":" + cinemaMin.Value, user.userEmail);
                    History.AddToHistory(user.userCard, $"-{amount} – Покупка билетов в кино");
                    WinNotify.ShowWinNotify("Покупка билетов в кино", $"{user.userName}, Вы приобрели {amountOfTicketsBS.Value} билет(ов) через приложение MazeBank. Сумма к оплате: {amount - sale}₴. Благодарим, что пользуетесь нашим сервисом.\nВаш поточный баланс: {user.userBalance}₴", 10000);
                    sound.Play();
                    System.Diagnostics.Process.Start("explorer", path);
                    mainMenu.login = login;
                    Close();
                    mainMenu.Show();
                }
                else if (confirmation.ok)
                {
                    this.Visible = true;
                    MessageBox.Show($"{user.userName}, Вы не можете провести операцию без подтверждения.", "Ошибка подтверждения", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return;
                }
                else
                {
                    this.Visible = true;
                }
            }
            else
            {
                MessageBox.Show($"{user.userName}, у Вас недостаточно средств для проведения операции.", "Недостаточно средств", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
        }
    }
}