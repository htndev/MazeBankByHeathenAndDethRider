﻿using System;
using System.Drawing;
using System.Media;
using System.Text.RegularExpressions;
using System.Windows.Forms;
using AuthClass;
using BillPrinting;
using HistoryClass;
using MailNotify;
using WinNotification;

namespace MazeBank
{
    public partial class OnlinePayments : Form
    {
        public OnlinePayments()
        {
            InitializeComponent();
        }

        #region Переменные

        public long login;

        SoundPlayer sound = new SoundPlayer(@"C:\Users\aleks\source\repos\MazeBank\sound.wav");

        MoneyTransactions user = new MoneyTransactions();

        MainMenu mainMenu = new MainMenu();

        ConfirmationForm confirmation = new ConfirmationForm();

        LoadingForm loading = new LoadingForm();

        int walletClicks = 0;

        int walletChanged = 0;

        int amountClicks = 0;

        int amountChanged = 0;

        int walletErr = 0;

        int amountErr = 0;

        decimal amount = 0;

        int wmPercent = 5;

        int qiwiPercent = 5;

        double ydPercent = 4.5;

        double skPercent = 4;

        #endregion Переменные

        private void OnlinePayments_Load(object sender, EventArgs e)
        {
            skrill.Hide();
            yandexMoney.Hide();
            webmoney.Hide();
            qiwi.Hide();
            user.UserConnection(login);
            if(user.theme)
            {
                exitBut.Load(@"C:\Users\aleks\source\repos\MazeBank\ExitPics\exitBlackTheme.png");
                backBut.Load(@"C:\Users\aleks\source\repos\MazeBank\Pics\backWhite.png");
                wmExit.Load(@"C:\Users\aleks\source\repos\MazeBank\ExitPics\exitBlackTheme.png");
                wmBack.Load(@"C:\Users\aleks\source\repos\MazeBank\Pics\backWhite.png");
                qiwiExit.Load(@"C:\Users\aleks\source\repos\MazeBank\ExitPics\exitBlackTheme.png");
                qiwiBack.Load(@"C:\Users\aleks\source\repos\MazeBank\Pics\backWhite.png");
                ydExit.Load(@"C:\Users\aleks\source\repos\MazeBank\ExitPics\exitBlackTheme.png");
                ydBack.Load(@"C:\Users\aleks\source\repos\MazeBank\Pics\backWhite.png");
                skExit.Load(@"C:\Users\aleks\source\repos\MazeBank\ExitPics\exitBlackTheme.png");
                skBack.Load(@"C:\Users\aleks\source\repos\MazeBank\Pics\backWhite.png");
                this.BackColor = Color.FromArgb(56, 56, 56);
                foreach (Control control in this.Controls)
                {
                    if(control is Label)
                    {
                        ((Label)control).ForeColor = Color.White;
                    }
                    if (control is PictureBox)
                    {
                        ((PictureBox)control).BackColor = Color.FromArgb(56, 56, 56);
                    }
                }
                pictureBox1.BackColor = Color.FromArgb(56, 56, 56);
                pictureBox2.BackColor = Color.White;
                pictureBox3.BackColor = Color.White;
                pictureBox6.BackColor = Color.White;
                pictureBox7.BackColor = Color.White;
                pictureBox10.BackColor = Color.White;
                pictureBox9.BackColor = Color.White;
                pictureBox12.BackColor = Color.White;
                pictureBox13.BackColor = Color.White;
                foreach (Control control in webmoney.Controls)
                {
                    if (control is Label)
                    {
                        ((Label)control).ForeColor = Color.White;
                    }
                    if (control is TextBox)
                    {
                        ((TextBox)control).BackColor = Color.FromArgb(56, 56, 56);
                    }
                }
                foreach (Control control in qiwi.Controls)
                {
                    if (control is Label)
                    {
                        ((Label)control).ForeColor = Color.White;
                    }
                    if (control is TextBox)
                    {
                        ((TextBox)control).BackColor = Color.FromArgb(56, 56, 56);
                    }
                }
                foreach (Control control in yandexMoney.Controls)
                {
                    if (control is Label)
                    {
                        ((Label)control).ForeColor = Color.White;
                    }
                    if (control is TextBox)
                    {
                        ((TextBox)control).BackColor = Color.FromArgb(56, 56, 56);
                    }
                }
                foreach (Control control in skrill.Controls)
                {
                    if (control is Label)
                    {
                        ((Label)control).ForeColor = Color.White;
                    }
                    if (control is TextBox)
                    {
                        ((TextBox)control).BackColor = Color.FromArgb(56, 56, 56);
                    }
                }
            }
        }

        private void backBut_Click(object sender, EventArgs e)
        {
            Close();
            mainMenu.login = login;
            mainMenu.Show();
        }

        private void exitBut_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void webmoneyPic_Click(object sender, EventArgs e)
        {
            webmoney.Show();
        }

        private void wmBack_Click(object sender, EventArgs e)
        {
            walletClicks = 0;

            walletChanged = 0;

            amountClicks = 0;

            amountChanged = 0;

            walletErr = 0;

            amountErr = 0;

            amount = 0;

            webmoney.Hide();
        }

        private void wmExit_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void wmWallet_TextChanged(object sender, EventArgs e)
        {
            wmWallet.MaxLength = 13;
            if(wmWallet.Text == "")
            {
                wmWallet.Text = "U";
                wmWallet.SelectionStart = wmWallet.TextLength;
                wmWallet.ScrollToCaret();
            }
        }

        private void wmWallet_Click(object sender, EventArgs e)
        {
            if (user.theme)
            {
                if (wmWallet.Text != null && walletClicks == 0)
                {
                    wmWallet.Clear();
                    wmWallet.ForeColor = Color.White;
                    walletClicks++;
                }

                else if (wmWallet.Text != null && walletChanged != 0)
                {
                    wmWallet.Clear();
                    wmWallet.ForeColor = Color.White;
                }
            }
            if (!user.theme)
            {
                if (wmWallet.Text != null && walletClicks == 0)
                {
                    wmWallet.Clear();
                    wmWallet.ForeColor = Color.Black;
                    walletClicks++;
                }

                else if (wmWallet.Text != null && walletChanged != 0)
                {
                    wmWallet.Clear();
                    wmWallet.ForeColor = Color.Black;
                }
            }
        }

        private void wmWallet_Leave(object sender, EventArgs e)
        {
            walletChanged = 0;
            if (!Regex.IsMatch(wmWallet.Text, @"^U\d{12}$", RegexOptions.Compiled) && walletErr <= 3)
            {
                walletErr++;
                MessageBox.Show("Неверно введен идентификатор получателя! Проверьте еще раз!");
                if (user.theme)
                {
                    wmAmount.ForeColor = Color.White;
                }
                else if (!user.theme)
                {
                    wmAmount.ForeColor = Color.Black;
                }
                return;
            }

            if (walletErr > 3)
            {
                WinNotify.ShowWinNotify("Ошибка ввода идентификатора", $"{user.userName}, Вы вводите неправильный формат идентификатора.\nФормат идентификатора содержит 12 символов: U123456789012", 10000);
            }

            if (wmWallet.Text == "")
            {
                amountErr++;
                wmWallet.Text = "";
                wmWallet.ForeColor = Color.Gray;
            }
        }

        private void wmAmount_TextChanged(object sender, EventArgs e)
        {
            wmAmount.MaxLength = 8;
        }

        private void wmAmount_Click(object sender, EventArgs e)
        {
            if (user.theme)
            {
                if (wmAmount.Text != null && amountClicks == 0)
                {
                    wmAmount.Clear();
                    wmAmount.ForeColor = Color.White;
                    amountClicks++;
                }

                else if (wmAmount.Text != null && amountChanged != 0)
                {
                    wmAmount.Clear();
                    wmAmount.ForeColor = Color.White;
                }
            }
            if (!user.theme)
            {
                if (wmAmount.Text != null && amountClicks == 0)
                {
                    wmAmount.Clear();
                    wmAmount.ForeColor = Color.Black;
                    amountClicks++;
                }

                else if (wmAmount.Text != null && amountChanged != 0)
                {
                    wmAmount.Clear();
                    wmAmount.ForeColor = Color.Black;
                }
            }
        }

        private void wmAmount_Leave(object sender, EventArgs e)
        {
            amountChanged = 0;
            if (!Regex.IsMatch(wmAmount.Text, @"^(?:([^A-Za-z\s][0-9]{0,4}\.[^A-Za-z][0-9]{0,1}$)|([^A-Za-z\s][0-9]{0,7}$))", RegexOptions.Compiled) && amountErr <= 4)
            {
                amountErr++;
                wmAmount.Clear();
                wmAmount.Focus();
                MessageBox.Show("Введенаня сумма не соответствует формату! Проверьте или введите еще раз.");
                if(user.theme)
                {
                    wmAmount.ForeColor = Color.White;
                }
                else if(!user.theme)
                {
                    wmAmount.ForeColor = Color.Black;
                }
                return;
            }

            else if (amountErr > 3)
            {
                wmAmount.Clear();
                wmAmount.Focus();
                WinNotify.ShowWinNotify("Ошибка ввода суммы", $"{user.userName}, Вы вводите неправильный формат суммы.\nФормат должен быть таким: 12.34 или 15", 10000);
            }

            if (wmAmount.Text == null)
            {
                amountChanged++;
                wmAmount.Text = "Сумма";
                wmAmount.ForeColor = Color.Gray;
            }
        }

        private void wmPay_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                wmPay.PerformClick();
            }
        }

        private void wmPay_Click(object sender, EventArgs e)
        {
            if(Regex.IsMatch(wmAmount.Text, @"^(?:([^A-Za-z\s][0-9]{0,4}\.[^A-Za-z][0-9]{0,1}$)|([^A-Za-z\s][0-9]{0,7}$))", RegexOptions.Compiled) && Regex.IsMatch(wmWallet.Text, @"^U\d{12}$", RegexOptions.Compiled))
            {
                string wmId = wmWallet.Text;
                amount = decimal.Parse(wmAmount.Text.Replace(".", ","));
                amount += MoneyTransactions.CalculateCommittee(amount, wmPercent);
                if (user.userBalance >= amount)
                {
                    this.Visible = false;
                    webmoney.Hide();
                    confirmation.userName = user.userName;
                    confirmation.userPassword = user.userPassword;
                    confirmation.amount = amount;
                    confirmation.ShowDialog();
                    if (!confirmation.ok)
                    {
                        user.userBalance -= amount;
                        this.Opacity = 1;
                        user.UserTransfer(login, user.userBalance);
                        Close();
                        loading.ShowDialog();
                        DialogResult askPrintBill = MessageBox.Show("Вы желаете распечатать чек?", "Печать чека", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                        if (askPrintBill == DialogResult.Yes)
                        {

                            PrintBill.PrintingBill("Webmoney", user.userName, user.userSurname, user.recipientPatronymic, user.userCard, amount - (MoneyTransactions.CalculateCommittee(amount, wmPercent)), MoneyTransactions.CalculateCommittee(amount, wmPercent), wmId);
                            WinNotify.ShowWinNotify("Оплата пополнение счета Webmoney", $"Вы успешно пополнили счет Webmoney {wmId} в размере {amount - (MoneyTransactions.CalculateCommittee(amount, wmPercent))}₴\nВаш поточный баланс: {user.userBalance:0.##}₴", 5000);
                            sound.Play();
                            mainMenu.login = login;
                            mainMenu.Show();
                        }
                        else
                        {

                            PrintBill.CreatingBill("Webmoney", user.userName, user.userSurname, user.recipientPatronymic, user.userCard, amount - (MoneyTransactions.CalculateCommittee(amount, wmPercent)), MoneyTransactions.CalculateCommittee(amount, wmPercent), wmId);
                            WinNotify.ShowWinNotify("Оплата пополнение счета Webmoney", $"Вы успешно пополнили счет Webmoney {wmId} в размере {amount - (MoneyTransactions.CalculateCommittee(amount, wmPercent))}₴\nВаш поточный баланс: {user.userBalance:0.##}₴", 5000);
                            sound.Play();
                            mainMenu.login = login;
                            mainMenu.Show();
                        }
                    }
                    else if (confirmation.ok)
                    {
                        this.Visible = true;
                        webmoney.Show();
                        MessageBox.Show($"Извините, {user.userName}, Вы не можете произвести операцию без подтверждения.", "Ошибка перевода", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        wmPay.Focus();
                        return;
                    }
                    else
                    {
                        this.Visible = true;
                        webmoney.Show();
                    }
                }
                else
                {
                    MessageBox.Show($"{user.userName}, у Вас не хватает средств оплатить эти услуги.", "Недостаточно средств", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    amount = 0;
                    return;
                }
            }
            else
            {
                MessageBox.Show("Вы ввели некорректные данные.\nПерепроверьте и попробуйте снова.", "Ошибка формата", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
        }

        private void qiwiPic_Click(object sender, EventArgs e)
        {
            qiwi.Show();
        }

        private void qiwiBack_Click(object sender, EventArgs e)
        {
            walletClicks = 0;

            walletChanged = 0;

            amountClicks = 0;

            amountChanged = 0;

            walletErr = 0;

            amountErr = 0;

            amount = 0;

            qiwi.Hide();
        }

        private void qiwiExit_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void qiwiWallet_Click(object sender, EventArgs e)
        {
            if (user.theme)
            {
                if (qiwiWallet.Text != null && walletClicks == 0)
                {
                    qiwiWallet.Clear();
                    qiwiWallet.ForeColor = Color.White;
                    walletClicks++;
                }

                else if (wmAmount.Text != null && walletChanged != 0)
                {
                    qiwiWallet.Clear();
                    wmAmount.ForeColor = Color.White;
                }
            }
            if (!user.theme)
            {
                if (wmAmount.Text != null && walletClicks == 0)
                {
                    qiwiWallet.Clear();
                    qiwiWallet.ForeColor = Color.Black;
                    walletClicks++;
                }

                else if (wmAmount.Text != null && walletChanged != 0)
                {
                    qiwiWallet.Clear();
                    qiwiWallet.ForeColor = Color.Black;
                }
            }
        }

        private void qiwiWallet_Leave(object sender, EventArgs e)
        {
            walletChanged = 0;
            if (!Regex.IsMatch(qiwiWallet.Text, @"^\d{12}$", RegexOptions.Compiled) && walletErr <= 3)
            {
                walletErr++;
                MessageBox.Show("Неверно введен идентификатор получателя! Проверьте еще раз!");
                if (user.theme)
                {
                    qiwiWallet.ForeColor = Color.White;
                }
                else if (!user.theme)
                {
                    qiwiWallet.ForeColor = Color.Black;
                }
                return;
            }

            if (walletErr > 3)
            {
                WinNotify.ShowWinNotify("Ошибка ввода идентификатора", $"{user.userName}, Вы вводите неправильный формат идентификатора.\nФормат идентификатора содержит 12 символов: 380953237634", 10000);
            }

            if (wmWallet.Text == "")
            {
                amountErr++;
                wmWallet.Text = "ID";
                wmWallet.ForeColor = Color.Gray;
            }
        }

        private void qiwiWallet_TextChanged(object sender, EventArgs e)
        {
            qiwiWallet.MaxLength = 12;
        }

        private void qiwiAmount_Leave(object sender, EventArgs e)
        {
            amountChanged = 0;
            if (!Regex.IsMatch(qiwiAmount.Text, @"^(?:([^A-Za-z\s][0-9]{0,4}\.[^A-Za-z][0-9]{0,1}$)|([^A-Za-z\s][0-9]{0,7}$))", RegexOptions.Compiled) && amountErr <= 4)
            {
                amountErr++;
                qiwiAmount.Clear();
                qiwiAmount.Focus();
                MessageBox.Show("Введенаня сумма не соответствует формату! Проверьте или введите еще раз.");
                if (user.theme)
                {
                    qiwiAmount.ForeColor = Color.White;
                }
                else if (!user.theme)
                {
                    qiwiAmount.ForeColor = Color.Black;
                }
                return;
            }

            else if (amountErr > 3)
            {
                qiwiAmount.Clear();
                qiwiAmount.Focus();
                WinNotify.ShowWinNotify("Ошибка ввода суммы", $"{user.userName}, Вы вводите неправильный формат суммы.\nФормат должен быть таким: 12.34 или 15", 10000);
            }

            if (wmAmount.Text == null)
            {
                amountChanged++;
                qiwiAmount.Text = "Сумма";
                qiwiAmount.ForeColor = Color.Gray;
            }
        }

        private void qiwiAmount_TextChanged(object sender, EventArgs e)
        {
            qiwiAmount.MaxLength = 8;
        }

        private void qiwiAmount_Click(object sender, EventArgs e)
        {
            if (user.theme)
            {
                if (qiwiAmount.Text != null && amountClicks == 0)
                {
                    qiwiAmount.Clear();
                    qiwiAmount.ForeColor = Color.White;
                    amountClicks++;
                }

                else if (wmAmount.Text != null && amountChanged != 0)
                {
                    qiwiAmount.Clear();
                    qiwiAmount.ForeColor = Color.White;
                }
            }

            if (!user.theme)
            {
                if (qiwiAmount.Text != null && amountClicks == 0)
                {
                    qiwiAmount.Clear();
                    qiwiAmount.ForeColor = Color.Black;
                    amountClicks++;
                }

                else if (qiwiAmount.Text != null && amountChanged != 0)
                {
                    qiwiAmount.Clear();
                    qiwiAmount.ForeColor = Color.Black;
                }
            }
        }

        private void qiwiPay_Click(object sender, EventArgs e)
        {
            if (Regex.IsMatch(qiwiAmount.Text, @"^(?:([^A-Za-z\s][0-9]{0,4}\.[^A-Za-z][0-9]{0,1}$)|([^A-Za-z\s][0-9]{0,7}$))", RegexOptions.Compiled) && Regex.IsMatch(qiwiWallet.Text, @"^\d{12}$", RegexOptions.Compiled))
            {
                string qiwiId = qiwiWallet.Text;
                amount = decimal.Parse(qiwiAmount.Text.Replace(".", ","));
                amount += MoneyTransactions.CalculateCommittee(amount, qiwiPercent);
                if (user.userBalance >= amount)
                {
                    this.Visible = false;
                    qiwi.Hide();
                    confirmation.userName = user.userName;
                    confirmation.userPassword = user.userPassword;
                    confirmation.amount = amount;
                    confirmation.ShowDialog();
                    if (!confirmation.ok)
                    {
                        user.userBalance -= amount;
                        this.Opacity = 1;
                        user.UserTransfer(login, user.userBalance);
                        Close();
                        loading.ShowDialog();
                        DialogResult askPrintBill = MessageBox.Show("Вы желаете распечатать чек?", "Печать чека", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                        if (askPrintBill == DialogResult.Yes)
                        {

                            PrintBill.PrintingBill("QIWI", user.userName, user.userSurname, user.recipientPatronymic, user.userCard, amount - (MoneyTransactions.CalculateCommittee(amount, qiwiPercent)), MoneyTransactions.CalculateCommittee(amount, qiwiPercent), qiwiId);
                            WinNotify.ShowWinNotify("Оплата пополнение счета QIWI", $"Вы успешно пополнили счет QIWI {qiwiId} в размере {amount - (MoneyTransactions.CalculateCommittee(amount, qiwiPercent))}₴\nВаш поточный баланс: {user.userBalance:0.##}₴", 5000);
                            sound.Play();
                            mainMenu.login = login;
                            mainMenu.Show();
                        }
                        else
                        {

                            PrintBill.CreatingBill("QIWI", user.userName, user.userSurname, user.recipientPatronymic, user.userCard, amount - (MoneyTransactions.CalculateCommittee(amount, qiwiPercent)), MoneyTransactions.CalculateCommittee(amount, qiwiPercent), qiwiId);
                            WinNotify.ShowWinNotify("Оплата пополнение счета QIWI", $"Вы успешно пополнили счет QIWI {qiwiId} в размере {amount - (MoneyTransactions.CalculateCommittee(amount, qiwiPercent))}₴\nВаш поточный баланс: {user.userBalance:0.##}₴", 5000);
                            sound.Play();
                            mainMenu.login = login;
                            mainMenu.Show();
                        }
                    }
                    else if (confirmation.ok)
                    {
                        this.Visible = true;
                        qiwi.Show();
                        MessageBox.Show($"Извините, {user.userName}, Вы не можете произвести операцию без подтверждения.", "Ошибка перевода", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        qiwiPay.Focus();
                        return;
                    }
                    else
                    {
                        this.Visible = true;
                        qiwi.Show();
                    }
                }
                else
                {
                    MessageBox.Show($"{user.userName}, у Вас не хватает средств оплатить эти услуги.", "Недостаточно средств", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    amount = 0;
                    return;
                }
            }
            else
            {
                MessageBox.Show("Вы ввели некорректные данные.\nПерепроверьте и попробуйте снова.", "Ошибка формата", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
        }

        private void ydBack_Click(object sender, EventArgs e)
        {
            walletClicks = 0;

            walletChanged = 0;

            amountClicks = 0;

            amountChanged = 0;

            walletErr = 0;

            amountErr = 0;

            amount = 0;

            yandexMoney.Hide();
        }

        private void ydExit_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void yandexmoneyPic_Click(object sender, EventArgs e)
        {
            yandexMoney.Show();
        }

        private void ydWallet_Click(object sender, EventArgs e)
        {
            if (user.theme)
            {
                if (ydWallet.Text != null && walletClicks == 0)
                {
                    ydWallet.Clear();
                    ydWallet.ForeColor = Color.White;
                    walletClicks++;
                }

                else if (ydWallet.Text != null && walletChanged != 0)
                {
                    ydWallet.Clear();
                    ydWallet.ForeColor = Color.White;
                }
            }
            if (!user.theme)
            {
                if (ydWallet.Text != null && walletClicks == 0)
                {
                    ydWallet.Clear();
                    ydWallet.ForeColor = Color.Black;
                    walletClicks++;
                }

                else if (ydWallet.Text != null && walletChanged != 0)
                {
                    ydWallet.Clear();
                    ydWallet.ForeColor = Color.Black;
                }
            }
        }

        private void ydWallet_Leave(object sender, EventArgs e)
        {
            walletChanged = 0;
            if (!Regex.IsMatch(ydWallet.Text, @"^\d{12}$", RegexOptions.Compiled) && walletErr <= 3)
            {
                walletErr++;
                MessageBox.Show("Неверно введен идентификатор получателя! Проверьте еще раз!");
                if (user.theme)
                {
                    ydWallet.ForeColor = Color.White;
                }
                else if (!user.theme)
                {
                    ydWallet.ForeColor = Color.Black;
                }
                return;
            }

            if (walletErr > 3)
            {
                WinNotify.ShowWinNotify("Ошибка ввода идентификатора", $"{user.userName}, Вы вводите неправильный формат идентификатора.\nФормат идентификатора содержит 12 символов: 380953237634", 10000);
            }

            if (ydWallet.Text == "")
            {
                amountErr++;
                ydWallet.Text = "ID";
                ydWallet.ForeColor = Color.Gray;
            }
        }

        private void ydWallet_TextChanged(object sender, EventArgs e)
        {
            ydWallet.MaxLength = 12;
        }

        private void ydAmount_TextChanged(object sender, EventArgs e)
        {
            ydAmount.MaxLength = 8;
        }

        private void ydAmount_Leave(object sender, EventArgs e)
        {
            amountChanged = 0;
            if (!Regex.IsMatch(ydAmount.Text, @"^(?:([^A-Za-z\s][0-9]{0,4}\.[^A-Za-z][0-9]{0,1}$)|([^A-Za-z\s][0-9]{0,7}$))", RegexOptions.Compiled) && amountErr <= 4)
            {
                amountErr++;
                ydAmount.Clear();
                ydAmount.Focus();
                MessageBox.Show("Введенаня сумма не соответствует формату! Проверьте или введите еще раз.");
                if (user.theme)
                {
                    ydAmount.ForeColor = Color.White;
                }
                else if (!user.theme)
                {
                    ydAmount.ForeColor = Color.Black;
                }
                return;
            }

            else if (amountErr > 3)
            {
                ydAmount.Clear();
                ydAmount.Focus();
                WinNotify.ShowWinNotify("Ошибка ввода суммы", $"{user.userName}, Вы вводите неправильный формат суммы.\nФормат должен быть таким: 12.34 или 15", 10000);
            }

            if (ydAmount.Text == null)
            {
                amountChanged++;
                ydAmount.Text = "Сумма";
                ydAmount.ForeColor = Color.Gray;
            }
        }

        private void ydAmount_Click(object sender, EventArgs e)
        {
            if (user.theme)
            {
                if (ydAmount.Text != null && amountClicks == 0)
                {
                    ydAmount.Clear();
                    ydAmount.ForeColor = Color.White;
                    amountClicks++;
                }

                else if (ydAmount.Text != null && amountChanged != 0)
                {
                    ydAmount.Clear();
                    ydAmount.ForeColor = Color.White;
                }
            }

            if (!user.theme)
            {
                if (ydAmount.Text != null && amountClicks == 0)
                {
                    ydAmount.Clear();
                    ydAmount.ForeColor = Color.Black;
                    amountClicks++;
                }

                else if (ydAmount.Text != null && amountChanged != 0)
                {
                    ydAmount.Clear();
                    ydAmount.ForeColor = Color.Black;
                }
            }
        }

        private void payYD_Click(object sender, EventArgs e)
        {
            if (Regex.IsMatch(ydAmount.Text, @"^(?:([^A-Za-z\s][0-9]{0,4}\.[^A-Za-z][0-9]{0,1}$)|([^A-Za-z\s][0-9]{0,7}$))", RegexOptions.Compiled) && Regex.IsMatch(ydWallet.Text, @"^\d{12}$", RegexOptions.Compiled))
            {
                string ydId = ydWallet.Text;
                amount = decimal.Parse(ydAmount.Text.Replace(".", ","));
                amount += MoneyTransactions.CalculateCommittee(amount, ydPercent);
                if (user.userBalance >= amount)
                {
                    this.Visible = false;
                    yandexMoney.Hide();
                    confirmation.userName = user.userName;
                    confirmation.userPassword = user.userPassword;
                    confirmation.amount = amount;
                    confirmation.ShowDialog();
                    if (!confirmation.ok)
                    {
                        user.userBalance -= amount;
                        this.Opacity = 1;
                        user.UserTransfer(login, user.userBalance);
                        Close();
                        loading.ShowDialog();
                        DialogResult askPrintBill = MessageBox.Show("Вы желаете распечатать чек?", "Печать чека", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                        if (askPrintBill == DialogResult.Yes)
                        {
                            History.AddToHistory(user.userCard, $"-{amount} – Поплнение кошелька Яндекс Деньги {ydId}");
                            MailDelivery.NotifyByMail(user.userEmail, user.userName, user.userSurname, $"поплнение кошелька Яндекс Деньги {ydId}", amount, user.userBalance);
                            PrintBill.PrintingBill("Yandex Money", user.userName, user.userSurname, user.userPatronymic, user.userCard, amount - (MoneyTransactions.CalculateCommittee(amount, ydPercent)), MoneyTransactions.CalculateCommittee(amount, ydPercent), ydId);
                            WinNotify.ShowWinNotify("Оплата пополнение счета Yandex Money", $"Вы успешно пополнили счет Yandex Money {ydId} в размере {amount - (MoneyTransactions.CalculateCommittee(amount, ydPercent))}₴\nВаш поточный баланс: {user.userBalance:0.##}₴", 5000);
                            sound.Play();
                            mainMenu.login = login;
                            mainMenu.Show();
                        }
                        else
                        {
                            History.AddToHistory(user.userCard, $"-{amount} – Поплнение кошелька Яндекс Деньги {ydId}");
                            MailDelivery.NotifyByMail(user.userEmail, user.userName, user.userSurname, $"поплнение кошелька Яндекс Деньги {ydId}", amount, user.userBalance);
                            PrintBill.CreatingBill("Yandex Money", user.userName, user.userSurname, user.userPatronymic, user.userCard, amount - (MoneyTransactions.CalculateCommittee(amount, ydPercent)), MoneyTransactions.CalculateCommittee(amount, ydPercent), ydId);
                            WinNotify.ShowWinNotify("Оплата пополнение счета Yandex Money", $"Вы успешно пополнили счет Yandex Money {ydId} в размере {amount - (MoneyTransactions.CalculateCommittee(amount, ydPercent))}₴\nВаш поточный баланс: {user.userBalance:0.##}₴", 5000);
                            sound.Play();
                            mainMenu.login = login;
                            mainMenu.Show();
                        }
                    }
                    else if (confirmation.ok)
                    {
                        this.Visible = true;
                        yandexMoney.Show();
                        MessageBox.Show($"Извините, {user.userName}, Вы не можете произвести операцию без подтверждения.", "Ошибка перевода", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        payYD.Focus();
                        return;
                    }
                    else
                    {
                        this.Visible = true;
                        yandexMoney.Show();
                    }
                }
                else
                {
                    MessageBox.Show($"{user.userName}, у Вас не хватает средств оплатить эти услуги.", "Недостаточно средств", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    amount = 0;
                    return;
                }
            }
            else
            {
                MessageBox.Show("Вы ввели некорректные данные.\nПерепроверьте и попробуйте снова.", "Ошибка формата", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
        }

        private void skBack_Click(object sender, EventArgs e)
        {
            walletClicks = 0;

            walletChanged = 0;

            amountClicks = 0;

            amountChanged = 0;

            walletErr = 0;

            amountErr = 0;

            amount = 0;

            skrill.Hide();
        }

        private void skExit_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void skrillPic_Click(object sender, EventArgs e)
        {
            skrill.Show();
        }

        private void skWallet_Click(object sender, EventArgs e)
        {
            if (user.theme)
            {
                if (skWallet.Text != null && walletClicks == 0)
                {
                    skWallet.Clear();
                    skWallet.ForeColor = Color.White;
                    walletClicks++;
                }

                else if (skWallet.Text != null && walletChanged != 0)
                {
                    skWallet.Clear();
                    skWallet.ForeColor = Color.White;
                }
            }
            if (!user.theme)
            {
                if (skWallet.Text != null && walletClicks == 0)
                {
                    skWallet.Clear();
                    skWallet.ForeColor = Color.Black;
                    walletClicks++;
                }

                else if (skWallet.Text != null && walletChanged != 0)
                {
                    skWallet.Clear();
                    skWallet.ForeColor = Color.Black;
                }
            }
        }

        private void skWallet_TextChanged(object sender, EventArgs e)
        {
            skWallet.MaxLength = 9;
        }

        private void skWallet_Leave(object sender, EventArgs e)
        {
            walletChanged = 0;
            if (!Regex.IsMatch(skWallet.Text, @"^\d{9}$", RegexOptions.Compiled) && walletErr <= 3)
            {
                walletErr++;
                MessageBox.Show("Неверно введен идентификатор получателя! Проверьте еще раз!");
                if (user.theme)
                {
                    skWallet.ForeColor = Color.White;
                }
                else if (!user.theme)
                {
                    skWallet.ForeColor = Color.Black;
                }
                return;
            }

            if (walletErr > 3)
            {
                WinNotify.ShowWinNotify("Ошибка ввода идентификатора", $"{user.userName}, Вы вводите неправильный формат идентификатора.\nФормат идентификатора содержит 12 символов: 123456789", 10000);
            }

            if (skWallet.Text == "")
            {
                amountErr++;
                skWallet.Text = "ID";
                skWallet.ForeColor = Color.Gray;
            }
        }

        private void skAmount_Click(object sender, EventArgs e)
        {
            if (user.theme)
            {
                if (skAmount.Text != null && amountClicks == 0)
                {
                    skAmount.Clear();
                    skAmount.ForeColor = Color.White;
                    amountClicks++;
                }

                else if (skAmount.Text != null && amountChanged != 0)
                {
                    skAmount.Clear();
                    skAmount.ForeColor = Color.White;
                }
            }

            if (!user.theme)
            {
                if (skAmount.Text != null && amountClicks == 0)
                {
                    skAmount.Clear();
                    skAmount.ForeColor = Color.Black;
                    amountClicks++;
                }

                else if (skAmount.Text != null && amountChanged != 0)
                {
                    skAmount.Clear();
                    skAmount.ForeColor = Color.Black;
                }
            }
        }

        private void skAmount_Leave(object sender, EventArgs e)
        {
            amountChanged = 0;
            if (!Regex.IsMatch(skAmount.Text, @"^(?:([^A-Za-z\s][0-9]{0,4}\.[^A-Za-z][0-9]{0,1}$)|([^A-Za-z\s][0-9]{0,7}$))", RegexOptions.Compiled) && amountErr <= 4)
            {
                amountErr++;
                skAmount.Clear();
                skAmount.Focus();
                MessageBox.Show("Введенаня сумма не соответствует формату! Проверьте или введите еще раз.");
                if (user.theme)
                {
                    skAmount.ForeColor = Color.White;
                }
                else if (!user.theme)
                {
                    skAmount.ForeColor = Color.Black;
                }
                return;
            }

            else if (amountErr > 3)
            {
                skAmount.Clear();
                skAmount.Focus();
                WinNotify.ShowWinNotify("Ошибка ввода суммы", $"{user.userName}, Вы вводите неправильный формат суммы.\nФормат должен быть таким: 12.34 или 15", 10000);
            }

            if (skAmount.Text == null)
            {
                amountChanged++;
                skAmount.Text = "Сумма";
                skAmount.ForeColor = Color.Gray;
            }
        }

        private void skAmount_TextChanged(object sender, EventArgs e)
        {
            skAmount.MaxLength = 8;
        }

        private void skPay_Click(object sender, EventArgs e)
        {
            if (Regex.IsMatch(skAmount.Text, @"^(?:([^A-Za-z\s][0-9]{0,4}\.[^A-Za-z][0-9]{0,1}$)|([^A-Za-z\s][0-9]{0,7}$))", RegexOptions.Compiled) && Regex.IsMatch(skWallet.Text, @"^\d{9}$", RegexOptions.Compiled))
            {
                string skId = skWallet.Text;
                amount = decimal.Parse(skAmount.Text.Replace(".", ","));
                amount += MoneyTransactions.CalculateCommittee(amount, skPercent);
                if (user.userBalance >= amount)
                {
                    this.Visible = false;
                    skrill.Hide();
                    confirmation.userName = user.userName;
                    confirmation.userPassword = user.userPassword;
                    confirmation.amount = amount;
                    confirmation.ShowDialog();
                    if (!confirmation.ok)
                    {
                        user.userBalance -= amount;
                        this.Opacity = 1;
                        user.UserTransfer(login, user.userBalance);
                        Close();
                        loading.ShowDialog();
                        DialogResult askPrintBill = MessageBox.Show("Вы желаете распечатать чек?", "Печать чека", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                        if (askPrintBill == DialogResult.Yes)
                        {
                            History.AddToHistory(user.userCard, $"-{amount} – Поплнение кошелька Skrill {skId}");
                            MailDelivery.NotifyByMail(user.userEmail, user.userName, user.userSurname, $"поплнение кошелька Skrill {skId}", amount, user.userBalance);
                            PrintBill.PrintingBill("Skrill", user.userName, user.userSurname, user.userPatronymic, user.userCard, amount - (MoneyTransactions.CalculateCommittee(amount, skPercent)), MoneyTransactions.CalculateCommittee(amount, skPercent), skId);
                            WinNotify.ShowWinNotify("Оплата пополнение счета Skrill", $"Вы успешно пополнили счет Skrill {skId} в размере {amount - (MoneyTransactions.CalculateCommittee(amount, skPercent))}₴\nВаш поточный баланс: {user.userBalance:0.##}₴", 5000);
                            sound.Play();
                            mainMenu.login = login;
                            mainMenu.Show();
                        }
                        else
                        {
                            History.AddToHistory(user.userCard, $"-{amount} – Поплнение кошелька Skrill {skId}");
                            MailDelivery.NotifyByMail(user.userEmail, user.userName, user.userSurname, $"поплнение кошелька Skrill {skId}", amount, user.userBalance);
                            PrintBill.CreatingBill("Skrill", user.userName, user.userSurname, user.userPatronymic, user.userCard, amount - (MoneyTransactions.CalculateCommittee(amount, skPercent)), MoneyTransactions.CalculateCommittee(amount, skPercent), skId);
                            WinNotify.ShowWinNotify("Оплата пополнение счета Skrill", $"Вы успешно пополнили счет Skrill {skId} в размере {amount - (MoneyTransactions.CalculateCommittee(amount, skPercent))}₴\nВаш поточный баланс: {user.userBalance:0.##}₴", 5000);
                            sound.Play();
                            mainMenu.login = login;
                            mainMenu.Show();
                        }
                    }
                    else if (confirmation.ok)
                    {
                        this.Visible = true;
                        skrill.Show();
                        MessageBox.Show($"Извините, {user.userName}, Вы не можете произвести операцию без подтверждения.", "Ошибка перевода", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        skPay.Focus();
                        return;
                    }
                    else
                    {
                        this.Visible = true;
                        skrill.Show();
                    }
                }
                else
                {
                    MessageBox.Show($"{user.userName}, у Вас не хватает средств оплатить эти услуги.", "Недостаточно средств", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    amount = 0;
                    return;
                }
            }
            else
            {
                MessageBox.Show("Вы ввели некорректные данные.\nПерепроверьте и попробуйте снова.", "Ошибка формата", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
        }

    }
}