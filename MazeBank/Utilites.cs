﻿using System;
using System.Drawing;
using System.Windows.Forms;
using AuthClass;
using WinNotification;
using BillPrinting;
using System.Media;
using HistoryClass;
using MailNotify;

namespace MazeBank
{
    public partial class Utilites : Form
    {
        public Utilites()
        {
            InitializeComponent();
        }

        #region Переменные

        public long login;

        decimal amount;

        Random rnd = new Random();

        double gaz, water, electricity, trash, flatPay;

        MoneyTransactions user = new MoneyTransactions();

        ConfirmationForm confirmation = new ConfirmationForm();

        LoadingForm loading = new LoadingForm();

        MainMenu mainMenu = new MainMenu();

        SoundPlayer sound = new SoundPlayer(@"C:\Users\aleks\source\repos\MazeBank\sound.wav");

        int check = 0;

        const int percent = 3;

        #endregion Переменные
        private void pay_Click(object sender, EventArgs e)
        {
            if (gazCheck.Checked)
            {
                amount += (decimal)gaz;
            }
            if (waterCheck.Checked)
            {
                amount += (decimal)water;
            }
            if (electicityCheck.Checked)
            {
                amount += (decimal)electricity;
            }
            if (trashCheck.Checked)
            {
                amount += (decimal)trash;
            }
            if (flatPayCheck.Checked)
            {
                amount += (decimal)flatPay;
            }
            decimal committee = MoneyTransactions.CalculateCommittee(amount, percent);
            amount += committee;
            if(user.userBalance >= amount)
            {
                this.Opacity = 0.2;
                confirmation.userName = user.userName;
                confirmation.userPassword = user.userPassword;
                confirmation.amount = amount;
                confirmation.ShowDialog();
                if (!confirmation.ok)
                {
                    user.userBalance -= amount;
                    this.Opacity = 1;
                    user.UserTransfer(login, user.userBalance);
                    Close();
                    loading.ShowDialog();
                    DialogResult askPrintBill = MessageBox.Show("Вы желаете распечатать чек?", "Печать чека", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                    if (askPrintBill == DialogResult.Yes)
                    {
                        History.AddToHistory(user.userCard, $"-{amount} – Оплата услуг ЖКХ");
                        MailDelivery.NotifyByMail(user.userEmail, user.userName, user.userSurname, $"оплаты услуг ЖКХ", amount, user.userBalance);
                        PrintBill.PrintingBill(user.userName, user.userSurname, user.userPatronymic, amount - committee, committee, gaz, water, electricity, trash, flatPay);
                        WinNotify.ShowWinNotify("Оплата услуг ЖКХ", $"Вы успешно оплатили услуги ЖКХ в размере {amount - committee}₴\nВаш поточный баланс: {user.userBalance:0.##}₴", 5000);
                        sound.Play();
                        mainMenu.login = login;
                        mainMenu.Show();
                    }
                    else
                    {
                        History.AddToHistory(user.userCard, $"-{amount} – Оплата услуг ЖКХ");
                        MailDelivery.NotifyByMail(user.userEmail, user.userName, user.userSurname, $"оплаты услуг ЖКХ", amount, user.userBalance);
                        PrintBill.CreatingBill(user.userName, user.userSurname, user.userPatronymic, amount - committee, committee, gaz, water, electricity, trash, flatPay);
                        WinNotify.ShowWinNotify("Оплата услуг ЖКХ", $"Вы успешно оплатили услуги ЖКХ в размере {amount - committee}₴\nВаш поточный баланс: {user.userBalance:0.##}₴", 5000);
                        sound.Play();
                        mainMenu.login = login;
                        mainMenu.Show();
                    }
                }
                else if (confirmation.ok)
                {
                    this.Opacity = 1;
                    amount = 0;
                    MessageBox.Show($"Извините, {user.userName}, Вы не можете произвести операцию без подтверждения.", "Ошибка перевода", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    pay.Focus();
                    return;
                }
                else
                {
                    amount = 0;
                    this.Opacity = 1;
                }
            }
            else
            {
                MessageBox.Show($"{user.userName}, у Вас не хватает средств оплатить эти услуги.", "Недостаточно средств", MessageBoxButtons.OK, MessageBoxIcon.Error);
                amount = 0;
                return;
            }
            
        }

        private void all_Click(object sender, EventArgs e)
        {
            check++;
            if(check % 2 == 1)
            {
                all.Text = "Снять все";
                foreach (Control control in this.Controls)
                {
                    if (control is CheckBox)
                        ((CheckBox)control).Checked = true;
                }
            }
            else if(check % 2 == 0)
            {
                all.Text = "Выбрать все";
                foreach (Control control in this.Controls)
                {
                    if (control is CheckBox)
                        ((CheckBox)control).Checked = false;
                }
            }
            
        }

        private void Utilites_Load(object sender, EventArgs e)
        {
            double first = rnd.Next(3256, 68934);
            gaz = Math.Round((first / 100), 2);
            double second = rnd.Next(1853, 53486);
            water = Math.Round((second / 100), 2);
            double third = rnd.Next(4512, 245323);
            electricity = Math.Round((third / 100), 2);
            double fourth = rnd.Next(1464, 3262);
            trash = Math.Round((fourth / 100), 2);
            double fifth = rnd.Next(95678, 682315);
            flatPay = Math.Round((fifth / 100), 2);
            gazText.Text = "За газ: " + gaz;
            waterText.Text = "За воду: " + water;
            electricityText.Text = "За электроэнергию: " + electricity;
            flatPayText.Text = "Квартплата: " + flatPay;
            trashText.Text = "Вывоз муора: " + trash;
            user.UserConnection(login);
            addressLbl.Text = user.userAddress;
            addressLbl.Left = (this.Width - addressLbl.Width) / 2;
            if(user.theme)
            {
                label1.ForeColor = Color.White;
                addressLbl.ForeColor = Color.White;
                this.BackColor = Color.FromArgb(56, 56, 56);
                exitBut.Load(@"C:\Users\aleks\source\repos\MazeBank\ExitPics\exitBlackTheme.png");
                backBut.Load(@"C:\Users\aleks\source\repos\MazeBank\Pics\backWhite.png");
                all.ForeColor = Color.White;
                foreach(Control control in this.Controls)
                {
                    if(control is Label)
                    {
                        ((Label)control).ForeColor = Color.White;
                    }
                }
            }
        }

        private void exitBut_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void backBut_Click(object sender, EventArgs e)
        {
            this.Close();
            mainMenu.login = login;
            mainMenu.Show();
        }
    }
}
