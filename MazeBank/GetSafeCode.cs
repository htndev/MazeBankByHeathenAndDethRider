﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MazeBank
{
    public partial class GetSafeCode : Form
    {
        public GetSafeCode()
        {
            InitializeComponent();
        }

        public int verifyCode;

        private void GetSafeCode_Load(object sender, EventArgs e)
        {
            this.FormBorderStyle = FormBorderStyle.None;
            GetSafeCode getSafeCode = new GetSafeCode();
            ForgottenPassFromAuth forgottenPassFromAuth = new ForgottenPassFromAuth();
            Random rnd = new Random();
            System.Threading.Thread.Sleep(20);
            verifyCode = rnd.Next(1000, 9999);
            safeCode.Text = verifyCode.ToString();
        }
    }
}
