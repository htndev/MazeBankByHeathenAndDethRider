﻿namespace MazeBank
{
    partial class Settings
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Settings));
            this.exitBut = new System.Windows.Forms.PictureBox();
            this.backBut = new System.Windows.Forms.PictureBox();
            this.QuitProfile = new System.Windows.Forms.PictureBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.toggleTheme = new System.Windows.Forms.PictureBox();
            this.changePass = new System.Windows.Forms.Label();
            this.changePin = new System.Windows.Forms.Label();
            this.changeLogin = new System.Windows.Forms.Label();
            this.changePassPanel = new System.Windows.Forms.Panel();
            this.oldPassConfVis = new System.Windows.Forms.PictureBox();
            this.oldPassVis = new System.Windows.Forms.PictureBox();
            this.newPassVis = new System.Windows.Forms.PictureBox();
            this.oldPassConfValid = new System.Windows.Forms.PictureBox();
            this.oldPassValid = new System.Windows.Forms.PictureBox();
            this.newPassValid = new System.Windows.Forms.PictureBox();
            this.label6 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.ConfBtn = new System.Windows.Forms.Button();
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            this.oldPassConf = new System.Windows.Forms.TextBox();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.oldPass = new System.Windows.Forms.TextBox();
            this.pictureBox3 = new System.Windows.Forms.PictureBox();
            this.newPass = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.backToSettingsPass = new System.Windows.Forms.PictureBox();
            this.exitPass = new System.Windows.Forms.PictureBox();
            this.changeEmail = new System.Windows.Forms.Label();
            this.changeEmailPanel = new System.Windows.Forms.Panel();
            this.passConfVisEmail = new System.Windows.Forms.PictureBox();
            this.passVisEmail = new System.Windows.Forms.PictureBox();
            this.emailPassConfCheck = new System.Windows.Forms.PictureBox();
            this.emailPassCheck = new System.Windows.Forms.PictureBox();
            this.newEmailCheck = new System.Windows.Forms.PictureBox();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.saveEmail = new System.Windows.Forms.Button();
            this.pictureBox10 = new System.Windows.Forms.PictureBox();
            this.passConfEmail = new System.Windows.Forms.TextBox();
            this.pictureBox11 = new System.Windows.Forms.PictureBox();
            this.passEmail = new System.Windows.Forms.TextBox();
            this.pictureBox12 = new System.Windows.Forms.PictureBox();
            this.newEmail = new System.Windows.Forms.TextBox();
            this.label10 = new System.Windows.Forms.Label();
            this.backToSettingEmail = new System.Windows.Forms.PictureBox();
            this.emailExit = new System.Windows.Forms.PictureBox();
            this.changePhonePanel = new System.Windows.Forms.Panel();
            this.passPhoneConfVis = new System.Windows.Forms.PictureBox();
            this.passPhoneVis = new System.Windows.Forms.PictureBox();
            this.passPhoneConfCheck = new System.Windows.Forms.PictureBox();
            this.passPhoneCheck = new System.Windows.Forms.PictureBox();
            this.phoneCheck = new System.Windows.Forms.PictureBox();
            this.label11 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.savePhone = new System.Windows.Forms.Button();
            this.pictureBox9 = new System.Windows.Forms.PictureBox();
            this.passPhoneConf = new System.Windows.Forms.TextBox();
            this.pictureBox13 = new System.Windows.Forms.PictureBox();
            this.passPhone = new System.Windows.Forms.TextBox();
            this.pictureBox14 = new System.Windows.Forms.PictureBox();
            this.newPhone = new System.Windows.Forms.TextBox();
            this.label14 = new System.Windows.Forms.Label();
            this.backToSettingsPhone = new System.Windows.Forms.PictureBox();
            this.exitPhone = new System.Windows.Forms.PictureBox();
            this.changePinPanel = new System.Windows.Forms.Panel();
            this.pinVis = new System.Windows.Forms.PictureBox();
            this.passConfVisPin = new System.Windows.Forms.PictureBox();
            this.passVisPin = new System.Windows.Forms.PictureBox();
            this.passCheckConfPin = new System.Windows.Forms.PictureBox();
            this.passCheckPin = new System.Windows.Forms.PictureBox();
            this.pinCheck = new System.Windows.Forms.PictureBox();
            this.label15 = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.label17 = new System.Windows.Forms.Label();
            this.savePin = new System.Windows.Forms.Button();
            this.pictureBox15 = new System.Windows.Forms.PictureBox();
            this.passConfPin = new System.Windows.Forms.TextBox();
            this.pictureBox16 = new System.Windows.Forms.PictureBox();
            this.passPin = new System.Windows.Forms.TextBox();
            this.pictureBox17 = new System.Windows.Forms.PictureBox();
            this.newPin = new System.Windows.Forms.TextBox();
            this.label18 = new System.Windows.Forms.Label();
            this.backToSettingsPin = new System.Windows.Forms.PictureBox();
            this.exitPin = new System.Windows.Forms.PictureBox();
            ((System.ComponentModel.ISupportInitialize)(this.exitBut)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.backBut)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.QuitProfile)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.toggleTheme)).BeginInit();
            this.changePassPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.oldPassConfVis)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.oldPassVis)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.newPassVis)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.oldPassConfValid)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.oldPassValid)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.newPassValid)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.backToSettingsPass)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.exitPass)).BeginInit();
            this.changeEmailPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.passConfVisEmail)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.passVisEmail)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emailPassConfCheck)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emailPassCheck)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.newEmailCheck)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox11)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox12)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.backToSettingEmail)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emailExit)).BeginInit();
            this.changePhonePanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.passPhoneConfVis)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.passPhoneVis)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.passPhoneConfCheck)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.passPhoneCheck)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.phoneCheck)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox13)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox14)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.backToSettingsPhone)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.exitPhone)).BeginInit();
            this.changePinPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pinVis)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.passConfVisPin)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.passVisPin)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.passCheckConfPin)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.passCheckPin)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pinCheck)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox15)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox16)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox17)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.backToSettingsPin)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.exitPin)).BeginInit();
            this.SuspendLayout();
            // 
            // exitBut
            // 
            this.exitBut.Cursor = System.Windows.Forms.Cursors.Hand;
            this.exitBut.Image = ((System.Drawing.Image)(resources.GetObject("exitBut.Image")));
            this.exitBut.Location = new System.Drawing.Point(358, 12);
            this.exitBut.Name = "exitBut";
            this.exitBut.Size = new System.Drawing.Size(34, 34);
            this.exitBut.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.exitBut.TabIndex = 12;
            this.exitBut.TabStop = false;
            this.exitBut.Click += new System.EventHandler(this.exitBut_Click);
            // 
            // backBut
            // 
            this.backBut.Cursor = System.Windows.Forms.Cursors.Hand;
            this.backBut.Image = ((System.Drawing.Image)(resources.GetObject("backBut.Image")));
            this.backBut.Location = new System.Drawing.Point(12, 12);
            this.backBut.Name = "backBut";
            this.backBut.Size = new System.Drawing.Size(34, 34);
            this.backBut.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.backBut.TabIndex = 13;
            this.backBut.TabStop = false;
            this.backBut.Click += new System.EventHandler(this.backBut_Click);
            // 
            // QuitProfile
            // 
            this.QuitProfile.Cursor = System.Windows.Forms.Cursors.Hand;
            this.QuitProfile.Image = ((System.Drawing.Image)(resources.GetObject("QuitProfile.Image")));
            this.QuitProfile.Location = new System.Drawing.Point(104, 471);
            this.QuitProfile.Name = "QuitProfile";
            this.QuitProfile.Size = new System.Drawing.Size(196, 52);
            this.QuitProfile.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.QuitProfile.TabIndex = 88;
            this.QuitProfile.TabStop = false;
            this.QuitProfile.Click += new System.EventHandler(this.QuitProfile_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Verdana", 20.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label1.Location = new System.Drawing.Point(112, 43);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(180, 32);
            this.label1.TabIndex = 14;
            this.label1.Text = "Настройки";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Verdana", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label2.Location = new System.Drawing.Point(23, 138);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(151, 23);
            this.label2.TabIndex = 15;
            this.label2.Text = "Темная тема";
            // 
            // toggleTheme
            // 
            this.toggleTheme.Cursor = System.Windows.Forms.Cursors.Hand;
            this.toggleTheme.Image = ((System.Drawing.Image)(resources.GetObject("toggleTheme.Image")));
            this.toggleTheme.Location = new System.Drawing.Point(305, 109);
            this.toggleTheme.Name = "toggleTheme";
            this.toggleTheme.Size = new System.Drawing.Size(87, 87);
            this.toggleTheme.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.toggleTheme.TabIndex = 16;
            this.toggleTheme.TabStop = false;
            this.toggleTheme.Click += new System.EventHandler(this.toggleTheme_Click);
            // 
            // changePass
            // 
            this.changePass.AutoSize = true;
            this.changePass.Cursor = System.Windows.Forms.Cursors.Hand;
            this.changePass.Font = new System.Drawing.Font("Verdana", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.changePass.Location = new System.Drawing.Point(108, 216);
            this.changePass.Name = "changePass";
            this.changePass.Size = new System.Drawing.Size(188, 23);
            this.changePass.TabIndex = 17;
            this.changePass.Text = "Сменить пароль";
            this.changePass.Click += new System.EventHandler(this.changePass_Click);
            // 
            // changePin
            // 
            this.changePin.AutoSize = true;
            this.changePin.Cursor = System.Windows.Forms.Cursors.Hand;
            this.changePin.Font = new System.Drawing.Font("Verdana", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.changePin.Location = new System.Drawing.Point(125, 423);
            this.changePin.Name = "changePin";
            this.changePin.Size = new System.Drawing.Size(158, 23);
            this.changePin.TabIndex = 89;
            this.changePin.Text = "Сменить ПИН";
            this.changePin.Click += new System.EventHandler(this.changePin_Click);
            // 
            // changeLogin
            // 
            this.changeLogin.AutoSize = true;
            this.changeLogin.Cursor = System.Windows.Forms.Cursors.Hand;
            this.changeLogin.Font = new System.Drawing.Font("Verdana", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.changeLogin.Location = new System.Drawing.Point(115, 285);
            this.changeLogin.Name = "changeLogin";
            this.changeLogin.Size = new System.Drawing.Size(174, 23);
            this.changeLogin.TabIndex = 18;
            this.changeLogin.Text = "Сменить логин";
            this.changeLogin.Click += new System.EventHandler(this.changeLogin_Click);
            // 
            // changePassPanel
            // 
            this.changePassPanel.Controls.Add(this.oldPassConfVis);
            this.changePassPanel.Controls.Add(this.oldPassVis);
            this.changePassPanel.Controls.Add(this.newPassVis);
            this.changePassPanel.Controls.Add(this.oldPassConfValid);
            this.changePassPanel.Controls.Add(this.oldPassValid);
            this.changePassPanel.Controls.Add(this.newPassValid);
            this.changePassPanel.Controls.Add(this.label6);
            this.changePassPanel.Controls.Add(this.label5);
            this.changePassPanel.Controls.Add(this.label4);
            this.changePassPanel.Controls.Add(this.ConfBtn);
            this.changePassPanel.Controls.Add(this.pictureBox2);
            this.changePassPanel.Controls.Add(this.oldPassConf);
            this.changePassPanel.Controls.Add(this.pictureBox1);
            this.changePassPanel.Controls.Add(this.oldPass);
            this.changePassPanel.Controls.Add(this.pictureBox3);
            this.changePassPanel.Controls.Add(this.newPass);
            this.changePassPanel.Controls.Add(this.label3);
            this.changePassPanel.Controls.Add(this.backToSettingsPass);
            this.changePassPanel.Controls.Add(this.exitPass);
            this.changePassPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.changePassPanel.Location = new System.Drawing.Point(0, 0);
            this.changePassPanel.Name = "changePassPanel";
            this.changePassPanel.Size = new System.Drawing.Size(404, 561);
            this.changePassPanel.TabIndex = 19;
            // 
            // oldPassConfVis
            // 
            this.oldPassConfVis.Cursor = System.Windows.Forms.Cursors.Hand;
            this.oldPassConfVis.Image = ((System.Drawing.Image)(resources.GetObject("oldPassConfVis.Image")));
            this.oldPassConfVis.Location = new System.Drawing.Point(56, 301);
            this.oldPassConfVis.Name = "oldPassConfVis";
            this.oldPassConfVis.Size = new System.Drawing.Size(50, 50);
            this.oldPassConfVis.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.oldPassConfVis.TabIndex = 50;
            this.oldPassConfVis.TabStop = false;
            this.oldPassConfVis.Click += new System.EventHandler(this.oldPassConfVis_Click);
            // 
            // oldPassVis
            // 
            this.oldPassVis.Cursor = System.Windows.Forms.Cursors.Hand;
            this.oldPassVis.Image = ((System.Drawing.Image)(resources.GetObject("oldPassVis.Image")));
            this.oldPassVis.Location = new System.Drawing.Point(56, 209);
            this.oldPassVis.Name = "oldPassVis";
            this.oldPassVis.Size = new System.Drawing.Size(50, 50);
            this.oldPassVis.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.oldPassVis.TabIndex = 49;
            this.oldPassVis.TabStop = false;
            this.oldPassVis.Click += new System.EventHandler(this.oldPassVis_Click);
            // 
            // newPassVis
            // 
            this.newPassVis.Cursor = System.Windows.Forms.Cursors.Hand;
            this.newPassVis.Image = ((System.Drawing.Image)(resources.GetObject("newPassVis.Image")));
            this.newPassVis.Location = new System.Drawing.Point(56, 122);
            this.newPassVis.Name = "newPassVis";
            this.newPassVis.Size = new System.Drawing.Size(50, 50);
            this.newPassVis.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.newPassVis.TabIndex = 48;
            this.newPassVis.TabStop = false;
            this.newPassVis.Click += new System.EventHandler(this.newPassVis_Click);
            // 
            // oldPassConfValid
            // 
            this.oldPassConfValid.Location = new System.Drawing.Point(317, 312);
            this.oldPassConfValid.Name = "oldPassConfValid";
            this.oldPassConfValid.Size = new System.Drawing.Size(30, 30);
            this.oldPassConfValid.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.oldPassConfValid.TabIndex = 47;
            this.oldPassConfValid.TabStop = false;
            // 
            // oldPassValid
            // 
            this.oldPassValid.Location = new System.Drawing.Point(317, 218);
            this.oldPassValid.Name = "oldPassValid";
            this.oldPassValid.Size = new System.Drawing.Size(30, 30);
            this.oldPassValid.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.oldPassValid.TabIndex = 46;
            this.oldPassValid.TabStop = false;
            // 
            // newPassValid
            // 
            this.newPassValid.Location = new System.Drawing.Point(317, 135);
            this.newPassValid.Name = "newPassValid";
            this.newPassValid.Size = new System.Drawing.Size(30, 30);
            this.newPassValid.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.newPassValid.TabIndex = 45;
            this.newPassValid.TabStop = false;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Verdana", 14.25F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label6.Location = new System.Drawing.Point(81, 275);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(241, 23);
            this.label6.TabIndex = 44;
            this.label6.Text = "Подтверждение пароля";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Verdana", 14.25F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label5.Location = new System.Drawing.Point(122, 186);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(159, 23);
            this.label5.TabIndex = 43;
            this.label5.Text = "Старый пароль";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Verdana", 14.25F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label4.Location = new System.Drawing.Point(126, 93);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(151, 23);
            this.label4.TabIndex = 42;
            this.label4.Text = "Новый пароль";
            // 
            // ConfBtn
            // 
            this.ConfBtn.FlatAppearance.BorderSize = 0;
            this.ConfBtn.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.ConfBtn.Font = new System.Drawing.Font("Verdana", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.ConfBtn.ForeColor = System.Drawing.Color.Red;
            this.ConfBtn.Location = new System.Drawing.Point(121, 399);
            this.ConfBtn.Name = "ConfBtn";
            this.ConfBtn.Size = new System.Drawing.Size(161, 66);
            this.ConfBtn.TabIndex = 41;
            this.ConfBtn.Text = "Сохранить";
            this.ConfBtn.UseVisualStyleBackColor = true;
            this.ConfBtn.Click += new System.EventHandler(this.ConfBtn_Click);
            // 
            // pictureBox2
            // 
            this.pictureBox2.BackColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.pictureBox2.Location = new System.Drawing.Point(111, 344);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new System.Drawing.Size(185, 2);
            this.pictureBox2.TabIndex = 40;
            this.pictureBox2.TabStop = false;
            // 
            // oldPassConf
            // 
            this.oldPassConf.BackColor = System.Drawing.SystemColors.Control;
            this.oldPassConf.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.oldPassConf.Font = new System.Drawing.Font("Microsoft Sans Serif", 20.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.oldPassConf.ForeColor = System.Drawing.Color.Black;
            this.oldPassConf.Location = new System.Drawing.Point(112, 312);
            this.oldPassConf.Name = "oldPassConf";
            this.oldPassConf.Size = new System.Drawing.Size(183, 31);
            this.oldPassConf.TabIndex = 39;
            this.oldPassConf.TabStop = false;
            this.oldPassConf.TextChanged += new System.EventHandler(this.oldPassConf_TextChanged);
            // 
            // pictureBox1
            // 
            this.pictureBox1.BackColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.pictureBox1.Location = new System.Drawing.Point(111, 249);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(185, 2);
            this.pictureBox1.TabIndex = 38;
            this.pictureBox1.TabStop = false;
            // 
            // oldPass
            // 
            this.oldPass.BackColor = System.Drawing.SystemColors.Control;
            this.oldPass.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.oldPass.Font = new System.Drawing.Font("Microsoft Sans Serif", 20.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.oldPass.ForeColor = System.Drawing.Color.Black;
            this.oldPass.Location = new System.Drawing.Point(112, 217);
            this.oldPass.Name = "oldPass";
            this.oldPass.Size = new System.Drawing.Size(183, 31);
            this.oldPass.TabIndex = 37;
            this.oldPass.TabStop = false;
            this.oldPass.TextChanged += new System.EventHandler(this.oldPass_TextChanged);
            // 
            // pictureBox3
            // 
            this.pictureBox3.BackColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.pictureBox3.Location = new System.Drawing.Point(112, 163);
            this.pictureBox3.Name = "pictureBox3";
            this.pictureBox3.Size = new System.Drawing.Size(185, 2);
            this.pictureBox3.TabIndex = 36;
            this.pictureBox3.TabStop = false;
            // 
            // newPass
            // 
            this.newPass.BackColor = System.Drawing.SystemColors.Control;
            this.newPass.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.newPass.Font = new System.Drawing.Font("Microsoft Sans Serif", 20.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.newPass.ForeColor = System.Drawing.Color.Black;
            this.newPass.Location = new System.Drawing.Point(113, 131);
            this.newPass.Name = "newPass";
            this.newPass.Size = new System.Drawing.Size(183, 31);
            this.newPass.TabIndex = 35;
            this.newPass.TabStop = false;
            this.newPass.TextChanged += new System.EventHandler(this.newPass_TextChanged);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Verdana", 20.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label3.Location = new System.Drawing.Point(85, 43);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(233, 32);
            this.label3.TabIndex = 16;
            this.label3.Text = "Смена пароля";
            // 
            // backToSettingsPass
            // 
            this.backToSettingsPass.Cursor = System.Windows.Forms.Cursors.Hand;
            this.backToSettingsPass.Image = ((System.Drawing.Image)(resources.GetObject("backToSettingsPass.Image")));
            this.backToSettingsPass.Location = new System.Drawing.Point(12, 12);
            this.backToSettingsPass.Name = "backToSettingsPass";
            this.backToSettingsPass.Size = new System.Drawing.Size(34, 34);
            this.backToSettingsPass.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.backToSettingsPass.TabIndex = 15;
            this.backToSettingsPass.TabStop = false;
            this.backToSettingsPass.Click += new System.EventHandler(this.backToSettingsPass_Click);
            // 
            // exitPass
            // 
            this.exitPass.Cursor = System.Windows.Forms.Cursors.Hand;
            this.exitPass.Image = ((System.Drawing.Image)(resources.GetObject("exitPass.Image")));
            this.exitPass.Location = new System.Drawing.Point(358, 12);
            this.exitPass.Name = "exitPass";
            this.exitPass.Size = new System.Drawing.Size(34, 34);
            this.exitPass.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.exitPass.TabIndex = 14;
            this.exitPass.TabStop = false;
            this.exitPass.Click += new System.EventHandler(this.exitPass_Click);
            // 
            // changeEmail
            // 
            this.changeEmail.AutoSize = true;
            this.changeEmail.Cursor = System.Windows.Forms.Cursors.Hand;
            this.changeEmail.Font = new System.Drawing.Font("Verdana", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.changeEmail.Location = new System.Drawing.Point(116, 353);
            this.changeEmail.Name = "changeEmail";
            this.changeEmail.Size = new System.Drawing.Size(172, 23);
            this.changeEmail.TabIndex = 20;
            this.changeEmail.Text = "Сменить почту";
            this.changeEmail.Click += new System.EventHandler(this.changeEmail_Click);
            // 
            // changeEmailPanel
            // 
            this.changeEmailPanel.Controls.Add(this.passConfVisEmail);
            this.changeEmailPanel.Controls.Add(this.passVisEmail);
            this.changeEmailPanel.Controls.Add(this.emailPassConfCheck);
            this.changeEmailPanel.Controls.Add(this.emailPassCheck);
            this.changeEmailPanel.Controls.Add(this.newEmailCheck);
            this.changeEmailPanel.Controls.Add(this.label7);
            this.changeEmailPanel.Controls.Add(this.label8);
            this.changeEmailPanel.Controls.Add(this.label9);
            this.changeEmailPanel.Controls.Add(this.saveEmail);
            this.changeEmailPanel.Controls.Add(this.pictureBox10);
            this.changeEmailPanel.Controls.Add(this.passConfEmail);
            this.changeEmailPanel.Controls.Add(this.pictureBox11);
            this.changeEmailPanel.Controls.Add(this.passEmail);
            this.changeEmailPanel.Controls.Add(this.pictureBox12);
            this.changeEmailPanel.Controls.Add(this.newEmail);
            this.changeEmailPanel.Controls.Add(this.label10);
            this.changeEmailPanel.Controls.Add(this.backToSettingEmail);
            this.changeEmailPanel.Controls.Add(this.emailExit);
            this.changeEmailPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.changeEmailPanel.Location = new System.Drawing.Point(0, 0);
            this.changeEmailPanel.Name = "changeEmailPanel";
            this.changeEmailPanel.Size = new System.Drawing.Size(404, 561);
            this.changeEmailPanel.TabIndex = 51;
            // 
            // passConfVisEmail
            // 
            this.passConfVisEmail.Cursor = System.Windows.Forms.Cursors.Hand;
            this.passConfVisEmail.Image = ((System.Drawing.Image)(resources.GetObject("passConfVisEmail.Image")));
            this.passConfVisEmail.Location = new System.Drawing.Point(56, 301);
            this.passConfVisEmail.Name = "passConfVisEmail";
            this.passConfVisEmail.Size = new System.Drawing.Size(50, 50);
            this.passConfVisEmail.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.passConfVisEmail.TabIndex = 69;
            this.passConfVisEmail.TabStop = false;
            this.passConfVisEmail.Click += new System.EventHandler(this.passConfVisEmail_Click);
            // 
            // passVisEmail
            // 
            this.passVisEmail.Cursor = System.Windows.Forms.Cursors.Hand;
            this.passVisEmail.Image = ((System.Drawing.Image)(resources.GetObject("passVisEmail.Image")));
            this.passVisEmail.Location = new System.Drawing.Point(56, 209);
            this.passVisEmail.Name = "passVisEmail";
            this.passVisEmail.Size = new System.Drawing.Size(50, 50);
            this.passVisEmail.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.passVisEmail.TabIndex = 68;
            this.passVisEmail.TabStop = false;
            this.passVisEmail.Click += new System.EventHandler(this.passVisEmail_Click);
            // 
            // emailPassConfCheck
            // 
            this.emailPassConfCheck.Location = new System.Drawing.Point(317, 312);
            this.emailPassConfCheck.Name = "emailPassConfCheck";
            this.emailPassConfCheck.Size = new System.Drawing.Size(30, 30);
            this.emailPassConfCheck.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.emailPassConfCheck.TabIndex = 66;
            this.emailPassConfCheck.TabStop = false;
            // 
            // emailPassCheck
            // 
            this.emailPassCheck.Location = new System.Drawing.Point(317, 218);
            this.emailPassCheck.Name = "emailPassCheck";
            this.emailPassCheck.Size = new System.Drawing.Size(30, 30);
            this.emailPassCheck.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.emailPassCheck.TabIndex = 65;
            this.emailPassCheck.TabStop = false;
            // 
            // newEmailCheck
            // 
            this.newEmailCheck.Location = new System.Drawing.Point(332, 133);
            this.newEmailCheck.Name = "newEmailCheck";
            this.newEmailCheck.Size = new System.Drawing.Size(30, 30);
            this.newEmailCheck.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.newEmailCheck.TabIndex = 64;
            this.newEmailCheck.TabStop = false;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Verdana", 14.25F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label7.Location = new System.Drawing.Point(81, 275);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(241, 23);
            this.label7.TabIndex = 63;
            this.label7.Text = "Подтверждение пароля";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Verdana", 14.25F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label8.Location = new System.Drawing.Point(161, 186);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(82, 23);
            this.label8.TabIndex = 62;
            this.label8.Text = "Пароль";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Verdana", 14.25F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label9.Location = new System.Drawing.Point(126, 93);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(132, 23);
            this.label9.TabIndex = 61;
            this.label9.Text = "Новая почта";
            // 
            // saveEmail
            // 
            this.saveEmail.FlatAppearance.BorderSize = 0;
            this.saveEmail.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.saveEmail.Font = new System.Drawing.Font("Verdana", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.saveEmail.ForeColor = System.Drawing.Color.Red;
            this.saveEmail.Location = new System.Drawing.Point(121, 399);
            this.saveEmail.Name = "saveEmail";
            this.saveEmail.Size = new System.Drawing.Size(161, 66);
            this.saveEmail.TabIndex = 60;
            this.saveEmail.Text = "Сохранить";
            this.saveEmail.UseVisualStyleBackColor = true;
            this.saveEmail.Click += new System.EventHandler(this.saveEmail_Click);
            // 
            // pictureBox10
            // 
            this.pictureBox10.BackColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.pictureBox10.Location = new System.Drawing.Point(111, 344);
            this.pictureBox10.Name = "pictureBox10";
            this.pictureBox10.Size = new System.Drawing.Size(185, 2);
            this.pictureBox10.TabIndex = 59;
            this.pictureBox10.TabStop = false;
            // 
            // passConfEmail
            // 
            this.passConfEmail.BackColor = System.Drawing.SystemColors.Control;
            this.passConfEmail.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.passConfEmail.Font = new System.Drawing.Font("Microsoft Sans Serif", 20.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.passConfEmail.ForeColor = System.Drawing.Color.Black;
            this.passConfEmail.Location = new System.Drawing.Point(112, 312);
            this.passConfEmail.Name = "passConfEmail";
            this.passConfEmail.Size = new System.Drawing.Size(183, 31);
            this.passConfEmail.TabIndex = 58;
            this.passConfEmail.TabStop = false;
            this.passConfEmail.TextChanged += new System.EventHandler(this.passConfEmail_TextChanged);
            // 
            // pictureBox11
            // 
            this.pictureBox11.BackColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.pictureBox11.Location = new System.Drawing.Point(111, 249);
            this.pictureBox11.Name = "pictureBox11";
            this.pictureBox11.Size = new System.Drawing.Size(185, 2);
            this.pictureBox11.TabIndex = 57;
            this.pictureBox11.TabStop = false;
            // 
            // passEmail
            // 
            this.passEmail.BackColor = System.Drawing.SystemColors.Control;
            this.passEmail.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.passEmail.Font = new System.Drawing.Font("Microsoft Sans Serif", 20.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.passEmail.ForeColor = System.Drawing.Color.Black;
            this.passEmail.Location = new System.Drawing.Point(112, 217);
            this.passEmail.Name = "passEmail";
            this.passEmail.Size = new System.Drawing.Size(183, 31);
            this.passEmail.TabIndex = 56;
            this.passEmail.TabStop = false;
            this.passEmail.TextChanged += new System.EventHandler(this.passEmail_TextChanged);
            // 
            // pictureBox12
            // 
            this.pictureBox12.BackColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.pictureBox12.Location = new System.Drawing.Point(86, 163);
            this.pictureBox12.Name = "pictureBox12";
            this.pictureBox12.Size = new System.Drawing.Size(231, 2);
            this.pictureBox12.TabIndex = 55;
            this.pictureBox12.TabStop = false;
            // 
            // newEmail
            // 
            this.newEmail.BackColor = System.Drawing.SystemColors.Control;
            this.newEmail.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.newEmail.Font = new System.Drawing.Font("Microsoft Sans Serif", 20.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.newEmail.ForeColor = System.Drawing.Color.Black;
            this.newEmail.Location = new System.Drawing.Point(87, 131);
            this.newEmail.Name = "newEmail";
            this.newEmail.Size = new System.Drawing.Size(229, 31);
            this.newEmail.TabIndex = 54;
            this.newEmail.TabStop = false;
            this.newEmail.TextChanged += new System.EventHandler(this.newEmail_TextChanged);
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Verdana", 20.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label10.Location = new System.Drawing.Point(94, 43);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(216, 32);
            this.label10.TabIndex = 53;
            this.label10.Text = "Смена почты";
            // 
            // backToSettingEmail
            // 
            this.backToSettingEmail.Cursor = System.Windows.Forms.Cursors.Hand;
            this.backToSettingEmail.Image = ((System.Drawing.Image)(resources.GetObject("backToSettingEmail.Image")));
            this.backToSettingEmail.Location = new System.Drawing.Point(12, 12);
            this.backToSettingEmail.Name = "backToSettingEmail";
            this.backToSettingEmail.Size = new System.Drawing.Size(34, 34);
            this.backToSettingEmail.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.backToSettingEmail.TabIndex = 52;
            this.backToSettingEmail.TabStop = false;
            this.backToSettingEmail.Click += new System.EventHandler(this.backToSettingEmail_Click);
            // 
            // emailExit
            // 
            this.emailExit.Cursor = System.Windows.Forms.Cursors.Hand;
            this.emailExit.Image = ((System.Drawing.Image)(resources.GetObject("emailExit.Image")));
            this.emailExit.Location = new System.Drawing.Point(358, 12);
            this.emailExit.Name = "emailExit";
            this.emailExit.Size = new System.Drawing.Size(34, 34);
            this.emailExit.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.emailExit.TabIndex = 51;
            this.emailExit.TabStop = false;
            this.emailExit.Click += new System.EventHandler(this.emailExit_Click);
            // 
            // changePhonePanel
            // 
            this.changePhonePanel.Controls.Add(this.passPhoneConfVis);
            this.changePhonePanel.Controls.Add(this.passPhoneVis);
            this.changePhonePanel.Controls.Add(this.passPhoneConfCheck);
            this.changePhonePanel.Controls.Add(this.passPhoneCheck);
            this.changePhonePanel.Controls.Add(this.phoneCheck);
            this.changePhonePanel.Controls.Add(this.label11);
            this.changePhonePanel.Controls.Add(this.label12);
            this.changePhonePanel.Controls.Add(this.label13);
            this.changePhonePanel.Controls.Add(this.savePhone);
            this.changePhonePanel.Controls.Add(this.pictureBox9);
            this.changePhonePanel.Controls.Add(this.passPhoneConf);
            this.changePhonePanel.Controls.Add(this.pictureBox13);
            this.changePhonePanel.Controls.Add(this.passPhone);
            this.changePhonePanel.Controls.Add(this.pictureBox14);
            this.changePhonePanel.Controls.Add(this.newPhone);
            this.changePhonePanel.Controls.Add(this.label14);
            this.changePhonePanel.Controls.Add(this.backToSettingsPhone);
            this.changePhonePanel.Controls.Add(this.exitPhone);
            this.changePhonePanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.changePhonePanel.Location = new System.Drawing.Point(0, 0);
            this.changePhonePanel.Name = "changePhonePanel";
            this.changePhonePanel.Size = new System.Drawing.Size(404, 561);
            this.changePhonePanel.TabIndex = 70;
            // 
            // passPhoneConfVis
            // 
            this.passPhoneConfVis.Cursor = System.Windows.Forms.Cursors.Hand;
            this.passPhoneConfVis.Image = ((System.Drawing.Image)(resources.GetObject("passPhoneConfVis.Image")));
            this.passPhoneConfVis.Location = new System.Drawing.Point(56, 301);
            this.passPhoneConfVis.Name = "passPhoneConfVis";
            this.passPhoneConfVis.Size = new System.Drawing.Size(50, 50);
            this.passPhoneConfVis.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.passPhoneConfVis.TabIndex = 87;
            this.passPhoneConfVis.TabStop = false;
            this.passPhoneConfVis.Click += new System.EventHandler(this.passPhoneConfVis_Click);
            // 
            // passPhoneVis
            // 
            this.passPhoneVis.Cursor = System.Windows.Forms.Cursors.Hand;
            this.passPhoneVis.Image = ((System.Drawing.Image)(resources.GetObject("passPhoneVis.Image")));
            this.passPhoneVis.Location = new System.Drawing.Point(56, 209);
            this.passPhoneVis.Name = "passPhoneVis";
            this.passPhoneVis.Size = new System.Drawing.Size(50, 50);
            this.passPhoneVis.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.passPhoneVis.TabIndex = 86;
            this.passPhoneVis.TabStop = false;
            this.passPhoneVis.Click += new System.EventHandler(this.passPhoneVis_Click);
            // 
            // passPhoneConfCheck
            // 
            this.passPhoneConfCheck.Location = new System.Drawing.Point(317, 312);
            this.passPhoneConfCheck.Name = "passPhoneConfCheck";
            this.passPhoneConfCheck.Size = new System.Drawing.Size(30, 30);
            this.passPhoneConfCheck.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.passPhoneConfCheck.TabIndex = 85;
            this.passPhoneConfCheck.TabStop = false;
            // 
            // passPhoneCheck
            // 
            this.passPhoneCheck.Location = new System.Drawing.Point(317, 218);
            this.passPhoneCheck.Name = "passPhoneCheck";
            this.passPhoneCheck.Size = new System.Drawing.Size(30, 30);
            this.passPhoneCheck.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.passPhoneCheck.TabIndex = 84;
            this.passPhoneCheck.TabStop = false;
            // 
            // phoneCheck
            // 
            this.phoneCheck.Location = new System.Drawing.Point(332, 133);
            this.phoneCheck.Name = "phoneCheck";
            this.phoneCheck.Size = new System.Drawing.Size(30, 30);
            this.phoneCheck.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.phoneCheck.TabIndex = 83;
            this.phoneCheck.TabStop = false;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Verdana", 14.25F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label11.Location = new System.Drawing.Point(81, 275);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(241, 23);
            this.label11.TabIndex = 82;
            this.label11.Text = "Подтверждение пароля";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("Verdana", 14.25F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label12.Location = new System.Drawing.Point(161, 186);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(82, 23);
            this.label12.TabIndex = 81;
            this.label12.Text = "Пароль";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Font = new System.Drawing.Font("Verdana", 14.25F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label13.Location = new System.Drawing.Point(120, 93);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(164, 23);
            this.label13.TabIndex = 80;
            this.label13.Text = "Новый телефон";
            // 
            // savePhone
            // 
            this.savePhone.FlatAppearance.BorderSize = 0;
            this.savePhone.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.savePhone.Font = new System.Drawing.Font("Verdana", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.savePhone.ForeColor = System.Drawing.Color.Red;
            this.savePhone.Location = new System.Drawing.Point(121, 399);
            this.savePhone.Name = "savePhone";
            this.savePhone.Size = new System.Drawing.Size(161, 66);
            this.savePhone.TabIndex = 79;
            this.savePhone.Text = "Сохранить";
            this.savePhone.UseVisualStyleBackColor = true;
            this.savePhone.Click += new System.EventHandler(this.savePhone_Click);
            // 
            // pictureBox9
            // 
            this.pictureBox9.BackColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.pictureBox9.Location = new System.Drawing.Point(111, 344);
            this.pictureBox9.Name = "pictureBox9";
            this.pictureBox9.Size = new System.Drawing.Size(185, 2);
            this.pictureBox9.TabIndex = 78;
            this.pictureBox9.TabStop = false;
            // 
            // passPhoneConf
            // 
            this.passPhoneConf.BackColor = System.Drawing.SystemColors.Control;
            this.passPhoneConf.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.passPhoneConf.Font = new System.Drawing.Font("Microsoft Sans Serif", 20.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.passPhoneConf.ForeColor = System.Drawing.Color.Black;
            this.passPhoneConf.Location = new System.Drawing.Point(112, 312);
            this.passPhoneConf.Name = "passPhoneConf";
            this.passPhoneConf.Size = new System.Drawing.Size(183, 31);
            this.passPhoneConf.TabIndex = 77;
            this.passPhoneConf.TabStop = false;
            this.passPhoneConf.TextChanged += new System.EventHandler(this.passPhoneConf_TextChanged);
            // 
            // pictureBox13
            // 
            this.pictureBox13.BackColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.pictureBox13.Location = new System.Drawing.Point(111, 249);
            this.pictureBox13.Name = "pictureBox13";
            this.pictureBox13.Size = new System.Drawing.Size(185, 2);
            this.pictureBox13.TabIndex = 76;
            this.pictureBox13.TabStop = false;
            // 
            // passPhone
            // 
            this.passPhone.BackColor = System.Drawing.SystemColors.Control;
            this.passPhone.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.passPhone.Font = new System.Drawing.Font("Microsoft Sans Serif", 20.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.passPhone.ForeColor = System.Drawing.Color.Black;
            this.passPhone.Location = new System.Drawing.Point(112, 217);
            this.passPhone.Name = "passPhone";
            this.passPhone.Size = new System.Drawing.Size(183, 31);
            this.passPhone.TabIndex = 75;
            this.passPhone.TabStop = false;
            this.passPhone.TextChanged += new System.EventHandler(this.passPhone_TextChanged);
            // 
            // pictureBox14
            // 
            this.pictureBox14.BackColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.pictureBox14.Location = new System.Drawing.Point(86, 163);
            this.pictureBox14.Name = "pictureBox14";
            this.pictureBox14.Size = new System.Drawing.Size(231, 2);
            this.pictureBox14.TabIndex = 74;
            this.pictureBox14.TabStop = false;
            // 
            // newPhone
            // 
            this.newPhone.BackColor = System.Drawing.SystemColors.Control;
            this.newPhone.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.newPhone.Font = new System.Drawing.Font("Microsoft Sans Serif", 20.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.newPhone.ForeColor = System.Drawing.Color.Black;
            this.newPhone.Location = new System.Drawing.Point(87, 131);
            this.newPhone.Name = "newPhone";
            this.newPhone.Size = new System.Drawing.Size(229, 31);
            this.newPhone.TabIndex = 73;
            this.newPhone.TabStop = false;
            this.newPhone.TextChanged += new System.EventHandler(this.newPhone_TextChanged);
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Font = new System.Drawing.Font("Verdana", 20.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label14.Location = new System.Drawing.Point(87, 43);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(229, 32);
            this.label14.TabIndex = 72;
            this.label14.Text = "Смена логина";
            // 
            // backToSettingsPhone
            // 
            this.backToSettingsPhone.Cursor = System.Windows.Forms.Cursors.Hand;
            this.backToSettingsPhone.Image = ((System.Drawing.Image)(resources.GetObject("backToSettingsPhone.Image")));
            this.backToSettingsPhone.Location = new System.Drawing.Point(12, 12);
            this.backToSettingsPhone.Name = "backToSettingsPhone";
            this.backToSettingsPhone.Size = new System.Drawing.Size(34, 34);
            this.backToSettingsPhone.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.backToSettingsPhone.TabIndex = 71;
            this.backToSettingsPhone.TabStop = false;
            this.backToSettingsPhone.Click += new System.EventHandler(this.backToSettingsPhone_Click);
            // 
            // exitPhone
            // 
            this.exitPhone.Cursor = System.Windows.Forms.Cursors.Hand;
            this.exitPhone.Image = ((System.Drawing.Image)(resources.GetObject("exitPhone.Image")));
            this.exitPhone.Location = new System.Drawing.Point(358, 12);
            this.exitPhone.Name = "exitPhone";
            this.exitPhone.Size = new System.Drawing.Size(34, 34);
            this.exitPhone.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.exitPhone.TabIndex = 70;
            this.exitPhone.TabStop = false;
            this.exitPhone.Click += new System.EventHandler(this.exitPhone_Click);
            // 
            // changePinPanel
            // 
            this.changePinPanel.Controls.Add(this.pinVis);
            this.changePinPanel.Controls.Add(this.passConfVisPin);
            this.changePinPanel.Controls.Add(this.passVisPin);
            this.changePinPanel.Controls.Add(this.passCheckConfPin);
            this.changePinPanel.Controls.Add(this.passCheckPin);
            this.changePinPanel.Controls.Add(this.pinCheck);
            this.changePinPanel.Controls.Add(this.label15);
            this.changePinPanel.Controls.Add(this.label16);
            this.changePinPanel.Controls.Add(this.label17);
            this.changePinPanel.Controls.Add(this.savePin);
            this.changePinPanel.Controls.Add(this.pictureBox15);
            this.changePinPanel.Controls.Add(this.passConfPin);
            this.changePinPanel.Controls.Add(this.pictureBox16);
            this.changePinPanel.Controls.Add(this.passPin);
            this.changePinPanel.Controls.Add(this.pictureBox17);
            this.changePinPanel.Controls.Add(this.newPin);
            this.changePinPanel.Controls.Add(this.label18);
            this.changePinPanel.Controls.Add(this.backToSettingsPin);
            this.changePinPanel.Controls.Add(this.exitPin);
            this.changePinPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.changePinPanel.Location = new System.Drawing.Point(0, 0);
            this.changePinPanel.Name = "changePinPanel";
            this.changePinPanel.Size = new System.Drawing.Size(404, 561);
            this.changePinPanel.TabIndex = 88;
            // 
            // pinVis
            // 
            this.pinVis.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pinVis.Image = ((System.Drawing.Image)(resources.GetObject("pinVis.Image")));
            this.pinVis.Location = new System.Drawing.Point(87, 119);
            this.pinVis.Name = "pinVis";
            this.pinVis.Size = new System.Drawing.Size(50, 50);
            this.pinVis.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pinVis.TabIndex = 106;
            this.pinVis.TabStop = false;
            this.pinVis.Click += new System.EventHandler(this.pinVis_Click);
            // 
            // passConfVisPin
            // 
            this.passConfVisPin.Cursor = System.Windows.Forms.Cursors.Hand;
            this.passConfVisPin.Image = ((System.Drawing.Image)(resources.GetObject("passConfVisPin.Image")));
            this.passConfVisPin.Location = new System.Drawing.Point(56, 301);
            this.passConfVisPin.Name = "passConfVisPin";
            this.passConfVisPin.Size = new System.Drawing.Size(50, 50);
            this.passConfVisPin.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.passConfVisPin.TabIndex = 105;
            this.passConfVisPin.TabStop = false;
            this.passConfVisPin.Click += new System.EventHandler(this.passConfVisPin_Click);
            // 
            // passVisPin
            // 
            this.passVisPin.Cursor = System.Windows.Forms.Cursors.Hand;
            this.passVisPin.Image = ((System.Drawing.Image)(resources.GetObject("passVisPin.Image")));
            this.passVisPin.Location = new System.Drawing.Point(56, 209);
            this.passVisPin.Name = "passVisPin";
            this.passVisPin.Size = new System.Drawing.Size(50, 50);
            this.passVisPin.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.passVisPin.TabIndex = 104;
            this.passVisPin.TabStop = false;
            this.passVisPin.Click += new System.EventHandler(this.passVisPin_Click);
            // 
            // passCheckConfPin
            // 
            this.passCheckConfPin.Location = new System.Drawing.Point(317, 312);
            this.passCheckConfPin.Name = "passCheckConfPin";
            this.passCheckConfPin.Size = new System.Drawing.Size(30, 30);
            this.passCheckConfPin.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.passCheckConfPin.TabIndex = 103;
            this.passCheckConfPin.TabStop = false;
            // 
            // passCheckPin
            // 
            this.passCheckPin.Location = new System.Drawing.Point(317, 218);
            this.passCheckPin.Name = "passCheckPin";
            this.passCheckPin.Size = new System.Drawing.Size(30, 30);
            this.passCheckPin.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.passCheckPin.TabIndex = 102;
            this.passCheckPin.TabStop = false;
            // 
            // pinCheck
            // 
            this.pinCheck.Location = new System.Drawing.Point(280, 127);
            this.pinCheck.Name = "pinCheck";
            this.pinCheck.Size = new System.Drawing.Size(30, 30);
            this.pinCheck.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pinCheck.TabIndex = 101;
            this.pinCheck.TabStop = false;
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Font = new System.Drawing.Font("Verdana", 14.25F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label15.Location = new System.Drawing.Point(81, 275);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(241, 23);
            this.label15.TabIndex = 100;
            this.label15.Text = "Подтверждение пароля";
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Font = new System.Drawing.Font("Verdana", 14.25F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label16.Location = new System.Drawing.Point(161, 186);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(82, 23);
            this.label16.TabIndex = 99;
            this.label16.Text = "Пароль";
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Font = new System.Drawing.Font("Verdana", 14.25F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label17.Location = new System.Drawing.Point(140, 93);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(123, 23);
            this.label17.TabIndex = 98;
            this.label17.Text = "Новый ПИН";
            // 
            // savePin
            // 
            this.savePin.FlatAppearance.BorderSize = 0;
            this.savePin.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.savePin.Font = new System.Drawing.Font("Verdana", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.savePin.ForeColor = System.Drawing.Color.Red;
            this.savePin.Location = new System.Drawing.Point(121, 399);
            this.savePin.Name = "savePin";
            this.savePin.Size = new System.Drawing.Size(161, 66);
            this.savePin.TabIndex = 97;
            this.savePin.Text = "Сохранить";
            this.savePin.UseVisualStyleBackColor = true;
            this.savePin.Click += new System.EventHandler(this.savePin_Click);
            // 
            // pictureBox15
            // 
            this.pictureBox15.BackColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.pictureBox15.Location = new System.Drawing.Point(111, 344);
            this.pictureBox15.Name = "pictureBox15";
            this.pictureBox15.Size = new System.Drawing.Size(185, 2);
            this.pictureBox15.TabIndex = 96;
            this.pictureBox15.TabStop = false;
            // 
            // passConfPin
            // 
            this.passConfPin.BackColor = System.Drawing.SystemColors.Control;
            this.passConfPin.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.passConfPin.Font = new System.Drawing.Font("Microsoft Sans Serif", 20.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.passConfPin.ForeColor = System.Drawing.Color.Black;
            this.passConfPin.Location = new System.Drawing.Point(112, 312);
            this.passConfPin.Name = "passConfPin";
            this.passConfPin.Size = new System.Drawing.Size(183, 31);
            this.passConfPin.TabIndex = 95;
            this.passConfPin.TabStop = false;
            this.passConfPin.TextChanged += new System.EventHandler(this.passConfPin_TextChanged);
            // 
            // pictureBox16
            // 
            this.pictureBox16.BackColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.pictureBox16.Location = new System.Drawing.Point(111, 249);
            this.pictureBox16.Name = "pictureBox16";
            this.pictureBox16.Size = new System.Drawing.Size(185, 2);
            this.pictureBox16.TabIndex = 94;
            this.pictureBox16.TabStop = false;
            // 
            // passPin
            // 
            this.passPin.BackColor = System.Drawing.SystemColors.Control;
            this.passPin.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.passPin.Font = new System.Drawing.Font("Microsoft Sans Serif", 20.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.passPin.ForeColor = System.Drawing.Color.Black;
            this.passPin.Location = new System.Drawing.Point(112, 217);
            this.passPin.Name = "passPin";
            this.passPin.Size = new System.Drawing.Size(183, 31);
            this.passPin.TabIndex = 93;
            this.passPin.TabStop = false;
            this.passPin.TextChanged += new System.EventHandler(this.passPin_TextChanged);
            // 
            // pictureBox17
            // 
            this.pictureBox17.BackColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.pictureBox17.Location = new System.Drawing.Point(155, 158);
            this.pictureBox17.Name = "pictureBox17";
            this.pictureBox17.Size = new System.Drawing.Size(104, 2);
            this.pictureBox17.TabIndex = 92;
            this.pictureBox17.TabStop = false;
            // 
            // newPin
            // 
            this.newPin.BackColor = System.Drawing.SystemColors.Control;
            this.newPin.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.newPin.Font = new System.Drawing.Font("Microsoft Sans Serif", 20.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.newPin.ForeColor = System.Drawing.Color.Black;
            this.newPin.Location = new System.Drawing.Point(155, 126);
            this.newPin.Name = "newPin";
            this.newPin.Size = new System.Drawing.Size(102, 31);
            this.newPin.TabIndex = 91;
            this.newPin.TabStop = false;
            this.newPin.TextChanged += new System.EventHandler(this.newPin_TextChanged);
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Font = new System.Drawing.Font("Verdana", 20.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label18.Location = new System.Drawing.Point(107, 43);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(190, 32);
            this.label18.TabIndex = 90;
            this.label18.Text = "Смена ПИН";
            // 
            // backToSettingsPin
            // 
            this.backToSettingsPin.Cursor = System.Windows.Forms.Cursors.Hand;
            this.backToSettingsPin.Image = ((System.Drawing.Image)(resources.GetObject("backToSettingsPin.Image")));
            this.backToSettingsPin.Location = new System.Drawing.Point(12, 12);
            this.backToSettingsPin.Name = "backToSettingsPin";
            this.backToSettingsPin.Size = new System.Drawing.Size(34, 34);
            this.backToSettingsPin.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.backToSettingsPin.TabIndex = 89;
            this.backToSettingsPin.TabStop = false;
            this.backToSettingsPin.Click += new System.EventHandler(this.backToSettingsPin_Click);
            // 
            // exitPin
            // 
            this.exitPin.Cursor = System.Windows.Forms.Cursors.Hand;
            this.exitPin.Image = ((System.Drawing.Image)(resources.GetObject("exitPin.Image")));
            this.exitPin.Location = new System.Drawing.Point(358, 12);
            this.exitPin.Name = "exitPin";
            this.exitPin.Size = new System.Drawing.Size(34, 34);
            this.exitPin.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.exitPin.TabIndex = 88;
            this.exitPin.TabStop = false;
            this.exitPin.Click += new System.EventHandler(this.exitPin_Click);
            // 
            // Settings
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(404, 561);
            this.Controls.Add(this.changePinPanel);
            this.Controls.Add(this.changePhonePanel);
            this.Controls.Add(this.changeEmailPanel);
            this.Controls.Add(this.changePassPanel);
            this.Controls.Add(this.changePin);
            this.Controls.Add(this.changeEmail);
            this.Controls.Add(this.changeLogin);
            this.Controls.Add(this.changePass);
            this.Controls.Add(this.toggleTheme);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.backBut);
            this.Controls.Add(this.exitBut);
            this.Controls.Add(this.QuitProfile);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "Settings";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Настройки";
            this.Load += new System.EventHandler(this.Settings_Load);
            ((System.ComponentModel.ISupportInitialize)(this.exitBut)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.backBut)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.QuitProfile)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.toggleTheme)).EndInit();
            this.changePassPanel.ResumeLayout(false);
            this.changePassPanel.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.oldPassConfVis)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.oldPassVis)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.newPassVis)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.oldPassConfValid)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.oldPassValid)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.newPassValid)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.backToSettingsPass)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.exitPass)).EndInit();
            this.changeEmailPanel.ResumeLayout(false);
            this.changeEmailPanel.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.passConfVisEmail)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.passVisEmail)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emailPassConfCheck)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emailPassCheck)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.newEmailCheck)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox11)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox12)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.backToSettingEmail)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emailExit)).EndInit();
            this.changePhonePanel.ResumeLayout(false);
            this.changePhonePanel.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.passPhoneConfVis)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.passPhoneVis)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.passPhoneConfCheck)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.passPhoneCheck)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.phoneCheck)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox13)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox14)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.backToSettingsPhone)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.exitPhone)).EndInit();
            this.changePinPanel.ResumeLayout(false);
            this.changePinPanel.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pinVis)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.passConfVisPin)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.passVisPin)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.passCheckConfPin)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.passCheckPin)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pinCheck)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox15)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox16)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox17)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.backToSettingsPin)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.exitPin)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.PictureBox exitBut;
        private System.Windows.Forms.PictureBox backBut;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.PictureBox toggleTheme;
        private System.Windows.Forms.Label changePass;
        private System.Windows.Forms.Label changePin;
        private System.Windows.Forms.Label changeLogin;
        private System.Windows.Forms.Panel changePassPanel;
        private System.Windows.Forms.PictureBox backToSettingsPass;
        private System.Windows.Forms.PictureBox exitPass;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Button ConfBtn;
        private System.Windows.Forms.PictureBox pictureBox2;
        private System.Windows.Forms.TextBox oldPassConf;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.TextBox oldPass;
        private System.Windows.Forms.PictureBox pictureBox3;
        private System.Windows.Forms.TextBox newPass;
        private System.Windows.Forms.PictureBox oldPassConfValid;
        private System.Windows.Forms.PictureBox oldPassValid;
        private System.Windows.Forms.PictureBox newPassValid;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.PictureBox oldPassConfVis;
        private System.Windows.Forms.PictureBox oldPassVis;
        private System.Windows.Forms.PictureBox newPassVis;
        private System.Windows.Forms.Label changeEmail;
        private System.Windows.Forms.Panel changeEmailPanel;
        private System.Windows.Forms.PictureBox passConfVisEmail;
        private System.Windows.Forms.PictureBox passVisEmail;
        private System.Windows.Forms.PictureBox emailPassConfCheck;
        private System.Windows.Forms.PictureBox emailPassCheck;
        private System.Windows.Forms.PictureBox newEmailCheck;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Button saveEmail;
        private System.Windows.Forms.PictureBox pictureBox10;
        private System.Windows.Forms.TextBox passConfEmail;
        private System.Windows.Forms.PictureBox pictureBox11;
        private System.Windows.Forms.TextBox passEmail;
        private System.Windows.Forms.PictureBox pictureBox12;
        private System.Windows.Forms.TextBox newEmail;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.PictureBox backToSettingEmail;
        private System.Windows.Forms.PictureBox emailExit;
        private System.Windows.Forms.Panel changePhonePanel;
        private System.Windows.Forms.PictureBox passPhoneConfVis;
        private System.Windows.Forms.PictureBox passPhoneVis;
        private System.Windows.Forms.PictureBox passPhoneConfCheck;
        private System.Windows.Forms.PictureBox passPhoneCheck;
        private System.Windows.Forms.PictureBox phoneCheck;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Button savePhone;
        private System.Windows.Forms.PictureBox pictureBox9;
        private System.Windows.Forms.TextBox passPhoneConf;
        private System.Windows.Forms.PictureBox pictureBox13;
        private System.Windows.Forms.TextBox passPhone;
        private System.Windows.Forms.PictureBox pictureBox14;
        private System.Windows.Forms.TextBox newPhone;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.PictureBox backToSettingsPhone;
        private System.Windows.Forms.PictureBox exitPhone;
        private System.Windows.Forms.PictureBox QuitProfile;
        private System.Windows.Forms.Panel changePinPanel;
        private System.Windows.Forms.PictureBox pinVis;
        private System.Windows.Forms.PictureBox passConfVisPin;
        private System.Windows.Forms.PictureBox passVisPin;
        private System.Windows.Forms.PictureBox passCheckConfPin;
        private System.Windows.Forms.PictureBox passCheckPin;
        private System.Windows.Forms.PictureBox pinCheck;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Button savePin;
        private System.Windows.Forms.PictureBox pictureBox15;
        private System.Windows.Forms.TextBox passConfPin;
        private System.Windows.Forms.PictureBox pictureBox16;
        private System.Windows.Forms.TextBox passPin;
        private System.Windows.Forms.PictureBox pictureBox17;
        private System.Windows.Forms.TextBox newPin;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.PictureBox backToSettingsPin;
        private System.Windows.Forms.PictureBox exitPin;
    }
}