﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Windows.Forms;
using BillPrinting;

namespace MazeBank
{
    public partial class UserHistory : Form
    {
        public string card;

        public bool theme;

        public string name;

        public string surname;

        public string patronymic;

        public List<string> historyList = new List<string>();

        public UserHistory()
        {
            InitializeComponent();
        }

        private void closeHistory_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void UserHistory_Load(object sender, EventArgs e)
        {
            if(theme)
            {
                this.BackColor = Color.FromArgb(56, 56, 56);
                label.ForeColor = Color.White;
                closeHistory.Load(@"C:\Users\aleks\source\repos\MazeBank\Pics\closeWhite.png");
            }
            string path = $@"C:\Users\aleks\source\repos\MazeBank\Histories\history_of_user#{card}.txt";
            string[] operations = File.ReadAllLines(path, System.Text.Encoding.Default);
            foreach (string operation in operations)
            {
                historyOperation.Items.Add(operation);
            }
        }

        private void printHistory_Click(object sender, EventArgs e)
        {
            PrintBill.PrintingBill(name, surname, patronymic, card, historyOperation);
        }
    }
}
