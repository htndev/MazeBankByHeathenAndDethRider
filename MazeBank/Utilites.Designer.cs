﻿namespace MazeBank
{
    partial class Utilites
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Utilites));
            this.exitBut = new System.Windows.Forms.PictureBox();
            this.backBut = new System.Windows.Forms.PictureBox();
            this.label1 = new System.Windows.Forms.Label();
            this.addressLbl = new System.Windows.Forms.Label();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            this.pictureBox3 = new System.Windows.Forms.PictureBox();
            this.pictureBox4 = new System.Windows.Forms.PictureBox();
            this.pictureBox5 = new System.Windows.Forms.PictureBox();
            this.gazText = new System.Windows.Forms.Label();
            this.waterText = new System.Windows.Forms.Label();
            this.electricityText = new System.Windows.Forms.Label();
            this.trashText = new System.Windows.Forms.Label();
            this.flatPayText = new System.Windows.Forms.Label();
            this.gazCheck = new System.Windows.Forms.CheckBox();
            this.waterCheck = new System.Windows.Forms.CheckBox();
            this.electicityCheck = new System.Windows.Forms.CheckBox();
            this.trashCheck = new System.Windows.Forms.CheckBox();
            this.flatPayCheck = new System.Windows.Forms.CheckBox();
            this.all = new System.Windows.Forms.CheckBox();
            this.label2 = new System.Windows.Forms.Label();
            this.pay = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.exitBut)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.backBut)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox5)).BeginInit();
            this.SuspendLayout();
            // 
            // exitBut
            // 
            this.exitBut.Cursor = System.Windows.Forms.Cursors.Hand;
            this.exitBut.Image = ((System.Drawing.Image)(resources.GetObject("exitBut.Image")));
            this.exitBut.Location = new System.Drawing.Point(358, 12);
            this.exitBut.Name = "exitBut";
            this.exitBut.Size = new System.Drawing.Size(34, 34);
            this.exitBut.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.exitBut.TabIndex = 12;
            this.exitBut.TabStop = false;
            this.exitBut.Click += new System.EventHandler(this.exitBut_Click);
            // 
            // backBut
            // 
            this.backBut.Cursor = System.Windows.Forms.Cursors.Hand;
            this.backBut.Image = ((System.Drawing.Image)(resources.GetObject("backBut.Image")));
            this.backBut.Location = new System.Drawing.Point(12, 12);
            this.backBut.Name = "backBut";
            this.backBut.Size = new System.Drawing.Size(34, 34);
            this.backBut.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.backBut.TabIndex = 15;
            this.backBut.TabStop = false;
            this.backBut.Click += new System.EventHandler(this.backBut_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Verdana", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label1.Location = new System.Drawing.Point(46, 62);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(312, 23);
            this.label1.TabIndex = 16;
            this.label1.Text = "Оплата комунальных услуг";
            // 
            // addressLbl
            // 
            this.addressLbl.AutoSize = true;
            this.addressLbl.Font = new System.Drawing.Font("Verdana", 11.25F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.addressLbl.Location = new System.Drawing.Point(118, 96);
            this.addressLbl.Name = "addressLbl";
            this.addressLbl.Size = new System.Drawing.Size(153, 18);
            this.addressLbl.TabIndex = 17;
            this.addressLbl.Text = "ул. Киевская 21, 6";
            // 
            // pictureBox1
            // 
            this.pictureBox1.BackColor = System.Drawing.Color.Transparent;
            this.pictureBox1.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox1.Image")));
            this.pictureBox1.Location = new System.Drawing.Point(31, 154);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(50, 50);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox1.TabIndex = 18;
            this.pictureBox1.TabStop = false;
            // 
            // pictureBox2
            // 
            this.pictureBox2.BackColor = System.Drawing.Color.Transparent;
            this.pictureBox2.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox2.Image")));
            this.pictureBox2.Location = new System.Drawing.Point(31, 222);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new System.Drawing.Size(50, 50);
            this.pictureBox2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox2.TabIndex = 19;
            this.pictureBox2.TabStop = false;
            // 
            // pictureBox3
            // 
            this.pictureBox3.BackColor = System.Drawing.Color.Transparent;
            this.pictureBox3.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox3.Image")));
            this.pictureBox3.Location = new System.Drawing.Point(31, 289);
            this.pictureBox3.Name = "pictureBox3";
            this.pictureBox3.Size = new System.Drawing.Size(50, 50);
            this.pictureBox3.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox3.TabIndex = 20;
            this.pictureBox3.TabStop = false;
            // 
            // pictureBox4
            // 
            this.pictureBox4.BackColor = System.Drawing.Color.Transparent;
            this.pictureBox4.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox4.Image")));
            this.pictureBox4.Location = new System.Drawing.Point(31, 357);
            this.pictureBox4.Name = "pictureBox4";
            this.pictureBox4.Size = new System.Drawing.Size(50, 50);
            this.pictureBox4.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox4.TabIndex = 21;
            this.pictureBox4.TabStop = false;
            // 
            // pictureBox5
            // 
            this.pictureBox5.BackColor = System.Drawing.Color.Transparent;
            this.pictureBox5.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox5.Image")));
            this.pictureBox5.Location = new System.Drawing.Point(31, 424);
            this.pictureBox5.Name = "pictureBox5";
            this.pictureBox5.Size = new System.Drawing.Size(50, 50);
            this.pictureBox5.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox5.TabIndex = 22;
            this.pictureBox5.TabStop = false;
            // 
            // gazText
            // 
            this.gazText.AutoSize = true;
            this.gazText.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.gazText.Location = new System.Drawing.Point(87, 171);
            this.gazText.Name = "gazText";
            this.gazText.Size = new System.Drawing.Size(68, 18);
            this.gazText.TabIndex = 23;
            this.gazText.Text = "За газ:";
            // 
            // waterText
            // 
            this.waterText.AutoSize = true;
            this.waterText.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.waterText.Location = new System.Drawing.Point(87, 239);
            this.waterText.Name = "waterText";
            this.waterText.Size = new System.Drawing.Size(80, 18);
            this.waterText.TabIndex = 24;
            this.waterText.Text = "За воду:";
            // 
            // electricityText
            // 
            this.electricityText.AutoSize = true;
            this.electricityText.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.electricityText.Location = new System.Drawing.Point(87, 306);
            this.electricityText.Name = "electricityText";
            this.electricityText.Size = new System.Drawing.Size(176, 18);
            this.electricityText.TabIndex = 25;
            this.electricityText.Text = "За электроэнергию:";
            // 
            // trashText
            // 
            this.trashText.AutoSize = true;
            this.trashText.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.trashText.Location = new System.Drawing.Point(87, 374);
            this.trashText.Name = "trashText";
            this.trashText.Size = new System.Drawing.Size(132, 18);
            this.trashText.TabIndex = 26;
            this.trashText.Text = "Вывоз мусора:";
            // 
            // flatPayText
            // 
            this.flatPayText.AutoSize = true;
            this.flatPayText.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.flatPayText.Location = new System.Drawing.Point(87, 443);
            this.flatPayText.Name = "flatPayText";
            this.flatPayText.Size = new System.Drawing.Size(110, 18);
            this.flatPayText.TabIndex = 27;
            this.flatPayText.Text = "Квартплата:";
            // 
            // gazCheck
            // 
            this.gazCheck.AutoSize = true;
            this.gazCheck.Location = new System.Drawing.Point(347, 174);
            this.gazCheck.Name = "gazCheck";
            this.gazCheck.Size = new System.Drawing.Size(15, 14);
            this.gazCheck.TabIndex = 28;
            this.gazCheck.UseVisualStyleBackColor = true;
            // 
            // waterCheck
            // 
            this.waterCheck.AutoSize = true;
            this.waterCheck.Location = new System.Drawing.Point(347, 243);
            this.waterCheck.Name = "waterCheck";
            this.waterCheck.Size = new System.Drawing.Size(15, 14);
            this.waterCheck.TabIndex = 29;
            this.waterCheck.UseVisualStyleBackColor = true;
            // 
            // electicityCheck
            // 
            this.electicityCheck.AutoSize = true;
            this.electicityCheck.Location = new System.Drawing.Point(347, 310);
            this.electicityCheck.Name = "electicityCheck";
            this.electicityCheck.Size = new System.Drawing.Size(15, 14);
            this.electicityCheck.TabIndex = 30;
            this.electicityCheck.UseVisualStyleBackColor = true;
            // 
            // trashCheck
            // 
            this.trashCheck.AutoSize = true;
            this.trashCheck.Location = new System.Drawing.Point(347, 378);
            this.trashCheck.Name = "trashCheck";
            this.trashCheck.Size = new System.Drawing.Size(15, 14);
            this.trashCheck.TabIndex = 31;
            this.trashCheck.UseVisualStyleBackColor = true;
            // 
            // flatPayCheck
            // 
            this.flatPayCheck.AutoSize = true;
            this.flatPayCheck.Location = new System.Drawing.Point(347, 447);
            this.flatPayCheck.Name = "flatPayCheck";
            this.flatPayCheck.Size = new System.Drawing.Size(15, 14);
            this.flatPayCheck.TabIndex = 32;
            this.flatPayCheck.UseVisualStyleBackColor = true;
            // 
            // all
            // 
            this.all.AutoSize = true;
            this.all.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.all.Location = new System.Drawing.Point(302, 512);
            this.all.Name = "all";
            this.all.Size = new System.Drawing.Size(100, 17);
            this.all.TabIndex = 33;
            this.all.Text = "Выбрать все";
            this.all.UseVisualStyleBackColor = true;
            this.all.Click += new System.EventHandler(this.all_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Verdana", 11.25F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label2.Location = new System.Drawing.Point(312, 132);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(79, 18);
            this.label2.TabIndex = 34;
            this.label2.Text = "Оплатить";
            // 
            // pay
            // 
            this.pay.BackColor = System.Drawing.Color.Transparent;
            this.pay.FlatAppearance.BorderSize = 0;
            this.pay.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.pay.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.pay.ForeColor = System.Drawing.Color.Red;
            this.pay.Location = new System.Drawing.Point(149, 497);
            this.pay.Name = "pay";
            this.pay.Size = new System.Drawing.Size(106, 52);
            this.pay.TabIndex = 35;
            this.pay.Text = "Оплатить";
            this.pay.UseVisualStyleBackColor = false;
            this.pay.Click += new System.EventHandler(this.pay_Click);
            // 
            // Utilites
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(404, 561);
            this.Controls.Add(this.pay);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.all);
            this.Controls.Add(this.flatPayCheck);
            this.Controls.Add(this.trashCheck);
            this.Controls.Add(this.electicityCheck);
            this.Controls.Add(this.waterCheck);
            this.Controls.Add(this.gazCheck);
            this.Controls.Add(this.flatPayText);
            this.Controls.Add(this.trashText);
            this.Controls.Add(this.electricityText);
            this.Controls.Add(this.waterText);
            this.Controls.Add(this.gazText);
            this.Controls.Add(this.pictureBox5);
            this.Controls.Add(this.pictureBox4);
            this.Controls.Add(this.pictureBox3);
            this.Controls.Add(this.pictureBox2);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.addressLbl);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.backBut);
            this.Controls.Add(this.exitBut);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "Utilites";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Оплата ЖКХ услуг";
            this.Load += new System.EventHandler(this.Utilites_Load);
            ((System.ComponentModel.ISupportInitialize)(this.exitBut)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.backBut)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox5)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.PictureBox exitBut;
        private System.Windows.Forms.PictureBox backBut;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label addressLbl;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.PictureBox pictureBox2;
        private System.Windows.Forms.PictureBox pictureBox3;
        private System.Windows.Forms.PictureBox pictureBox4;
        private System.Windows.Forms.PictureBox pictureBox5;
        private System.Windows.Forms.Label gazText;
        private System.Windows.Forms.Label waterText;
        private System.Windows.Forms.Label electricityText;
        private System.Windows.Forms.Label trashText;
        private System.Windows.Forms.Label flatPayText;
        private System.Windows.Forms.CheckBox gazCheck;
        private System.Windows.Forms.CheckBox waterCheck;
        private System.Windows.Forms.CheckBox electicityCheck;
        private System.Windows.Forms.CheckBox trashCheck;
        private System.Windows.Forms.CheckBox flatPayCheck;
        private System.Windows.Forms.CheckBox all;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button pay;
    }
}