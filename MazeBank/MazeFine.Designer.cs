﻿namespace MazeBank
{
    partial class MazeFine
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MazeFine));
            this.label1 = new System.Windows.Forms.Label();
            this.exitBut = new System.Windows.Forms.PictureBox();
            this.backBut = new System.Windows.Forms.PictureBox();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.amountText = new System.Windows.Forms.TextBox();
            this.pictureBox3 = new System.Windows.Forms.PictureBox();
            this.fineIdText = new System.Windows.Forms.TextBox();
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            this.intruderText = new System.Windows.Forms.TextBox();
            this.pictureBox4 = new System.Windows.Forms.PictureBox();
            this.emailText = new System.Windows.Forms.TextBox();
            this.pay = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.exitBut)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.backBut)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox4)).BeginInit();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Verdana", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label1.Location = new System.Drawing.Point(76, 60);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(245, 29);
            this.label1.TabIndex = 20;
            this.label1.Text = "Оплата штрафов";
            // 
            // exitBut
            // 
            this.exitBut.Cursor = System.Windows.Forms.Cursors.Hand;
            this.exitBut.Image = ((System.Drawing.Image)(resources.GetObject("exitBut.Image")));
            this.exitBut.Location = new System.Drawing.Point(342, 12);
            this.exitBut.Name = "exitBut";
            this.exitBut.Size = new System.Drawing.Size(34, 34);
            this.exitBut.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.exitBut.TabIndex = 19;
            this.exitBut.TabStop = false;
            this.exitBut.Click += new System.EventHandler(this.exitBut_Click);
            // 
            // backBut
            // 
            this.backBut.Cursor = System.Windows.Forms.Cursors.Hand;
            this.backBut.Image = ((System.Drawing.Image)(resources.GetObject("backBut.Image")));
            this.backBut.Location = new System.Drawing.Point(12, 12);
            this.backBut.Name = "backBut";
            this.backBut.Size = new System.Drawing.Size(34, 34);
            this.backBut.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.backBut.TabIndex = 18;
            this.backBut.TabStop = false;
            this.backBut.Click += new System.EventHandler(this.backBut_Click);
            // 
            // pictureBox1
            // 
            this.pictureBox1.BackColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.pictureBox1.Location = new System.Drawing.Point(121, 302);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(148, 2);
            this.pictureBox1.TabIndex = 43;
            this.pictureBox1.TabStop = false;
            // 
            // amountText
            // 
            this.amountText.BackColor = System.Drawing.SystemColors.Control;
            this.amountText.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.amountText.Font = new System.Drawing.Font("Microsoft Sans Serif", 20.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.amountText.ForeColor = System.Drawing.Color.Gray;
            this.amountText.Location = new System.Drawing.Point(122, 267);
            this.amountText.Name = "amountText";
            this.amountText.Size = new System.Drawing.Size(146, 31);
            this.amountText.TabIndex = 42;
            this.amountText.TabStop = false;
            this.amountText.Text = "Сумма";
            this.amountText.Click += new System.EventHandler(this.amountText_Click);
            this.amountText.TextChanged += new System.EventHandler(this.amountText_TextChanged);
            this.amountText.Leave += new System.EventHandler(this.amountText_Leave);
            // 
            // pictureBox3
            // 
            this.pictureBox3.BackColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.pictureBox3.Location = new System.Drawing.Point(88, 162);
            this.pictureBox3.Name = "pictureBox3";
            this.pictureBox3.Size = new System.Drawing.Size(222, 2);
            this.pictureBox3.TabIndex = 41;
            this.pictureBox3.TabStop = false;
            // 
            // fineIdText
            // 
            this.fineIdText.BackColor = System.Drawing.SystemColors.Control;
            this.fineIdText.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.fineIdText.Font = new System.Drawing.Font("Microsoft Sans Serif", 20.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.fineIdText.ForeColor = System.Drawing.Color.Gray;
            this.fineIdText.Location = new System.Drawing.Point(92, 129);
            this.fineIdText.Name = "fineIdText";
            this.fineIdText.Size = new System.Drawing.Size(218, 31);
            this.fineIdText.TabIndex = 40;
            this.fineIdText.TabStop = false;
            this.fineIdText.Text = "Постановление";
            this.fineIdText.Click += new System.EventHandler(this.fineIdText_Click);
            this.fineIdText.TextChanged += new System.EventHandler(this.fineIdText_TextChanged);
            this.fineIdText.Leave += new System.EventHandler(this.fineIdText_Leave);
            // 
            // pictureBox2
            // 
            this.pictureBox2.BackColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.pictureBox2.Location = new System.Drawing.Point(88, 230);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new System.Drawing.Size(222, 2);
            this.pictureBox2.TabIndex = 45;
            this.pictureBox2.TabStop = false;
            // 
            // intruderText
            // 
            this.intruderText.BackColor = System.Drawing.SystemColors.Control;
            this.intruderText.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.intruderText.Font = new System.Drawing.Font("Microsoft Sans Serif", 20.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.intruderText.ForeColor = System.Drawing.Color.Gray;
            this.intruderText.Location = new System.Drawing.Point(92, 197);
            this.intruderText.Name = "intruderText";
            this.intruderText.Size = new System.Drawing.Size(218, 31);
            this.intruderText.TabIndex = 44;
            this.intruderText.TabStop = false;
            this.intruderText.Text = "ФИО";
            this.intruderText.Click += new System.EventHandler(this.intruderText_Click);
            this.intruderText.Leave += new System.EventHandler(this.intruderText_Leave);
            // 
            // pictureBox4
            // 
            this.pictureBox4.BackColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.pictureBox4.Location = new System.Drawing.Point(84, 374);
            this.pictureBox4.Name = "pictureBox4";
            this.pictureBox4.Size = new System.Drawing.Size(222, 2);
            this.pictureBox4.TabIndex = 47;
            this.pictureBox4.TabStop = false;
            // 
            // emailText
            // 
            this.emailText.BackColor = System.Drawing.SystemColors.Control;
            this.emailText.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.emailText.Font = new System.Drawing.Font("Microsoft Sans Serif", 20.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.emailText.ForeColor = System.Drawing.Color.Gray;
            this.emailText.Location = new System.Drawing.Point(88, 341);
            this.emailText.Name = "emailText";
            this.emailText.Size = new System.Drawing.Size(218, 31);
            this.emailText.TabIndex = 46;
            this.emailText.TabStop = false;
            this.emailText.Text = "Email";
            this.emailText.Click += new System.EventHandler(this.emailText_Click);
            this.emailText.Leave += new System.EventHandler(this.emailText_Leave);
            // 
            // pay
            // 
            this.pay.FlatAppearance.BorderSize = 0;
            this.pay.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.pay.Font = new System.Drawing.Font("Verdana", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.pay.ForeColor = System.Drawing.Color.Red;
            this.pay.Location = new System.Drawing.Point(106, 419);
            this.pay.Name = "pay";
            this.pay.Size = new System.Drawing.Size(191, 72);
            this.pay.TabIndex = 48;
            this.pay.Text = "Оплатить";
            this.pay.UseVisualStyleBackColor = true;
            this.pay.Click += new System.EventHandler(this.pay_Click);
            this.pay.KeyDown += new System.Windows.Forms.KeyEventHandler(this.pay_KeyDown);
            // 
            // MazeFine
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(388, 522);
            this.Controls.Add(this.pay);
            this.Controls.Add(this.pictureBox4);
            this.Controls.Add(this.emailText);
            this.Controls.Add(this.pictureBox2);
            this.Controls.Add(this.intruderText);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.amountText);
            this.Controls.Add(this.pictureBox3);
            this.Controls.Add(this.fineIdText);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.exitBut);
            this.Controls.Add(this.backBut);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.KeyPreview = true;
            this.Name = "MazeFine";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Оплата штрафов";
            this.Load += new System.EventHandler(this.MazeFine_Load);
            ((System.ComponentModel.ISupportInitialize)(this.exitBut)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.backBut)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox4)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.PictureBox exitBut;
        private System.Windows.Forms.PictureBox backBut;
        private System.Windows.Forms.PictureBox pictureBox1;
        public System.Windows.Forms.TextBox amountText;
        private System.Windows.Forms.PictureBox pictureBox3;
        private System.Windows.Forms.TextBox fineIdText;
        private System.Windows.Forms.PictureBox pictureBox2;
        private System.Windows.Forms.TextBox intruderText;
        private System.Windows.Forms.PictureBox pictureBox4;
        private System.Windows.Forms.TextBox emailText;
        public System.Windows.Forms.Button pay;
    }
}