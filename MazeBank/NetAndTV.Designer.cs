﻿namespace MazeBank
{
    partial class NetAndTV
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(NetAndTV));
            this.exitBut = new System.Windows.Forms.PictureBox();
            this.backBut = new System.Windows.Forms.PictureBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.pictureBox3 = new System.Windows.Forms.PictureBox();
            this.idText = new System.Windows.Forms.TextBox();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.amountText = new System.Windows.Forms.TextBox();
            this.pay = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.exitBut)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.backBut)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // exitBut
            // 
            this.exitBut.Cursor = System.Windows.Forms.Cursors.Hand;
            this.exitBut.Image = ((System.Drawing.Image)(resources.GetObject("exitBut.Image")));
            this.exitBut.Location = new System.Drawing.Point(358, 12);
            this.exitBut.Name = "exitBut";
            this.exitBut.Size = new System.Drawing.Size(34, 34);
            this.exitBut.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.exitBut.TabIndex = 13;
            this.exitBut.TabStop = false;
            this.exitBut.Click += new System.EventHandler(this.exitBut_Click);
            // 
            // backBut
            // 
            this.backBut.Cursor = System.Windows.Forms.Cursors.Hand;
            this.backBut.Image = ((System.Drawing.Image)(resources.GetObject("backBut.Image")));
            this.backBut.Location = new System.Drawing.Point(12, 12);
            this.backBut.Name = "backBut";
            this.backBut.Size = new System.Drawing.Size(34, 34);
            this.backBut.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.backBut.TabIndex = 16;
            this.backBut.TabStop = false;
            this.backBut.Click += new System.EventHandler(this.backBut_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Verdana", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label1.Location = new System.Drawing.Point(68, 57);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(267, 23);
            this.label1.TabIndex = 17;
            this.label1.Text = "Оплата Интернета и ТВ";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Verdana", 11.25F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label2.Location = new System.Drawing.Point(137, 114);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(130, 18);
            this.label2.TabIndex = 41;
            this.label2.Text = "Введите Ваш id ";
            // 
            // pictureBox3
            // 
            this.pictureBox3.BackColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.pictureBox3.Location = new System.Drawing.Point(108, 176);
            this.pictureBox3.Name = "pictureBox3";
            this.pictureBox3.Size = new System.Drawing.Size(187, 2);
            this.pictureBox3.TabIndex = 40;
            this.pictureBox3.TabStop = false;
            // 
            // idText
            // 
            this.idText.BackColor = System.Drawing.SystemColors.Control;
            this.idText.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.idText.Font = new System.Drawing.Font("Microsoft Sans Serif", 20.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.idText.ForeColor = System.Drawing.Color.Gray;
            this.idText.Location = new System.Drawing.Point(110, 144);
            this.idText.Name = "idText";
            this.idText.Size = new System.Drawing.Size(183, 31);
            this.idText.TabIndex = 39;
            this.idText.TabStop = false;
            this.idText.Text = "Id";
            this.idText.Click += new System.EventHandler(this.idText_Click);
            this.idText.TextChanged += new System.EventHandler(this.idText_TextChanged);
            this.idText.Leave += new System.EventHandler(this.idText_Leave);
            // 
            // pictureBox1
            // 
            this.pictureBox1.BackColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.pictureBox1.Location = new System.Drawing.Point(129, 251);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(148, 2);
            this.pictureBox1.TabIndex = 44;
            this.pictureBox1.TabStop = false;
            // 
            // amountText
            // 
            this.amountText.BackColor = System.Drawing.SystemColors.Control;
            this.amountText.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.amountText.Font = new System.Drawing.Font("Microsoft Sans Serif", 20.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.amountText.ForeColor = System.Drawing.Color.Gray;
            this.amountText.Location = new System.Drawing.Point(130, 216);
            this.amountText.Name = "amountText";
            this.amountText.Size = new System.Drawing.Size(146, 31);
            this.amountText.TabIndex = 43;
            this.amountText.TabStop = false;
            this.amountText.Text = "Сумма";
            this.amountText.Click += new System.EventHandler(this.amountText_Click);
            this.amountText.TextChanged += new System.EventHandler(this.amountText_TextChanged);
            this.amountText.Leave += new System.EventHandler(this.amountText_Leave);
            // 
            // pay
            // 
            this.pay.FlatAppearance.BorderSize = 0;
            this.pay.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.pay.Font = new System.Drawing.Font("Verdana", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.pay.ForeColor = System.Drawing.Color.Red;
            this.pay.Location = new System.Drawing.Point(107, 292);
            this.pay.Name = "pay";
            this.pay.Size = new System.Drawing.Size(191, 72);
            this.pay.TabIndex = 42;
            this.pay.Text = "Пополнить";
            this.pay.UseVisualStyleBackColor = true;
            this.pay.Click += new System.EventHandler(this.pay_Click);
            // 
            // NetAndTV
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(404, 561);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.amountText);
            this.Controls.Add(this.pay);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.pictureBox3);
            this.Controls.Add(this.idText);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.backBut);
            this.Controls.Add(this.exitBut);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.KeyPreview = true;
            this.Name = "NetAndTV";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Оплата Интернета и Телевиденья";
            this.Load += new System.EventHandler(this.NetAndTV_Load);
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.NetAndTV_KeyDown);
            ((System.ComponentModel.ISupportInitialize)(this.exitBut)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.backBut)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.PictureBox exitBut;
        private System.Windows.Forms.PictureBox backBut;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.PictureBox pictureBox3;
        private System.Windows.Forms.TextBox idText;
        private System.Windows.Forms.PictureBox pictureBox1;
        public System.Windows.Forms.TextBox amountText;
        public System.Windows.Forms.Button pay;
    }
}