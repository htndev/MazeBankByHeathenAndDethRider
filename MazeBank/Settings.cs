﻿using System;
using System.Drawing;
using System.Text.RegularExpressions;
using System.Windows.Forms;
using AuthClass;
using WinNotification;

namespace MazeBank
{
    public partial class Settings : Form
    {
        public Settings()
        {
            InitializeComponent();
        }

        #region Переменные 

        public long login;

        int changer;

        MainMenu mainMenu = new MainMenu();

        UserEntering user = new UserEntering();

        LoadingForm loading = new LoadingForm();

        Auth auth = new Auth();

        int newVisCount = 1;

        int oldPassVisCount = 1;

        int oldPassConfVisCount = 1;

        #endregion Переменные

        private void Settings_Load(object sender, EventArgs e)
        {
            changePinPanel.Hide();
            changePhonePanel.Hide();
            changePassPanel.Hide();
            changeEmailPanel.Hide();
            user.UserConnection(login);
            newPass.UseSystemPasswordChar = true;
            oldPass.UseSystemPasswordChar = true;
            oldPassConf.UseSystemPasswordChar = true;
            passEmail.UseSystemPasswordChar = true;
            passConfEmail.UseSystemPasswordChar = true;
            passPhone.UseSystemPasswordChar = true;
            passPhoneConf.UseSystemPasswordChar = true;
            passConfPin.UseSystemPasswordChar = true;
            passPin.UseSystemPasswordChar = true;
            newPin.UseSystemPasswordChar = true;
            if (user.theme)
            {
                changer = 1;
                QuitProfile.Load(@"C:\Users\aleks\source\repos\MazeBank\Pics\exitProfileWhite.png");
                exitBut.Load(@"C:\Users\aleks\source\repos\MazeBank\ExitPics\exitBlackTheme.png");
                backBut.Load(@"C:\Users\aleks\source\repos\MazeBank\Pics\backWhite.png");
                exitPass.Load(@"C:\Users\aleks\source\repos\MazeBank\ExitPics\exitBlackTheme.png");
                backToSettingsPass.Load(@"C:\Users\aleks\source\repos\MazeBank\Pics\backWhite.png");
                emailExit.Load(@"C:\Users\aleks\source\repos\MazeBank\ExitPics\exitBlackTheme.png");
                backToSettingEmail.Load(@"C:\Users\aleks\source\repos\MazeBank\Pics\backWhite.png");
                exitPhone.Load(@"C:\Users\aleks\source\repos\MazeBank\ExitPics\exitBlackTheme.png");
                backToSettingsPhone.Load(@"C:\Users\aleks\source\repos\MazeBank\Pics\backWhite.png");
                exitPin.Load(@"C:\Users\aleks\source\repos\MazeBank\ExitPics\exitBlackTheme.png");
                backToSettingsPin.Load(@"C:\Users\aleks\source\repos\MazeBank\Pics\backWhite.png");
                toggleTheme.Load(@"C:\Users\aleks\source\repos\MazeBank\Pics\toggleON.png");
                this.BackColor = Color.FromArgb(56, 56, 56);
                pictureBox1.BackColor = Color.White;
                pictureBox2.BackColor = Color.White;
                pictureBox3.BackColor = Color.White;
                pictureBox10.BackColor = Color.White;
                pictureBox11.BackColor = Color.White;
                pictureBox12.BackColor = Color.White;
                pictureBox9.BackColor = Color.White;
                pictureBox13.BackColor = Color.White;
                pictureBox14.BackColor = Color.White;
                pictureBox17.BackColor = Color.White;
                pictureBox16.BackColor = Color.White;
                pictureBox15.BackColor = Color.White;
                foreach (Control c in this.Controls)
                {
                    if(c is Label)
                    {
                        ((Label)c).ForeColor = Color.White;
                    }
                }
                foreach (Control c in changePassPanel.Controls)
                {
                    if (c is Label)
                    {
                        ((Label)c).ForeColor = Color.White;
                    }
                    if (c is TextBox)
                    {
                        ((TextBox)c).BackColor = Color.FromArgb(56, 56, 56);
                        ((TextBox)c).ForeColor = Color.White;
                    }
                }
                foreach (Control c in changePinPanel.Controls)
                {
                    if (c is Label)
                    {
                        ((Label)c).ForeColor = Color.White;
                    }
                    if (c is TextBox)
                    {
                        ((TextBox)c).BackColor = Color.FromArgb(56, 56, 56);
                        ((TextBox)c).ForeColor = Color.White;
                    }
                }
                foreach (Control c in changeEmailPanel.Controls)
                {
                    if (c is Label)
                    {
                        ((Label)c).ForeColor = Color.White;
                    }
                    if (c is TextBox)
                    {
                        ((TextBox)c).BackColor = Color.FromArgb(56, 56, 56);
                        ((TextBox)c).ForeColor = Color.White;
                    }
                }
                foreach (Control c in changePhonePanel.Controls)
                {
                    if (c is Label)
                    {
                        ((Label)c).ForeColor = Color.White;
                    }
                    if (c is TextBox)
                    {
                        ((TextBox)c).BackColor = Color.FromArgb(56, 56, 56);
                        ((TextBox)c).ForeColor = Color.White;
                    }
                }
                foreach (Control c in changePhonePanel.Controls)
                {
                    if (c is Label)
                    {
                        ((Label)c).ForeColor = Color.White;
                    }
                    if (c is TextBox)
                    {
                        ((TextBox)c).BackColor = Color.FromArgb(56, 56, 56);
                        ((TextBox)c).ForeColor = Color.White;
                    }
                }
            }
        }

        private void backBut_Click(object sender, EventArgs e)
        {
            Close();
            MainMenu mainMenu = new MainMenu();
            mainMenu.login = login;
            mainMenu.Show();
        }

        private void exitBut_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void toggleTheme_Click(object sender, EventArgs e)
        {
            changer++;
            if (changer % 2 == 0)
            {
                user.theme = false;
                user.UpdateUserInfo(user.theme, login);
                QuitProfile.Load(@"C:\Users\aleks\source\repos\MazeBank\Pics\exitProfile.png");
                toggleTheme.Load(@"C:\Users\aleks\source\repos\MazeBank\Pics\toggleOFF.png");
                exitBut.Load(@"C:\Users\aleks\source\repos\MazeBank\ExitPics\exit.png");
                backBut.Load(@"C:\Users\aleks\source\repos\MazeBank\Pics\back.png");
                exitPass.Load(@"C:\Users\aleks\source\repos\MazeBank\ExitPics\exit.png");
                backToSettingsPass.Load(@"C:\Users\aleks\source\repos\MazeBank\Pics\back.png");
                emailExit.Load(@"C:\Users\aleks\source\repos\MazeBank\ExitPics\exit.png");
                backToSettingsPhone.Load(@"C:\Users\aleks\source\repos\MazeBank\Pics\back.png");
                exitPhone.Load(@"C:\Users\aleks\source\repos\MazeBank\ExitPics\exit.png");
                backToSettingEmail.Load(@"C:\Users\aleks\source\repos\MazeBank\Pics\back.png");
                exitPin.Load(@"C:\Users\aleks\source\repos\MazeBank\ExitPics\exit.png");
                backToSettingsPin.Load(@"C:\Users\aleks\source\repos\MazeBank\Pics\back.png");
                this.BackColor = Color.FromArgb(240, 240, 240);
                foreach (Control c in this.Controls)
                {
                    if (c is Label)
                    {
                        ((Label)c).ForeColor = Color.Black;
                    }
                }
                foreach (Control c in changePassPanel.Controls)
                {
                    if (c is Label)
                    {
                        ((Label)c).ForeColor = Color.Black;
                    }
                    if (c is TextBox)
                    {
                        ((TextBox)c).BackColor = Color.FromArgb(240, 240, 240);
                        ((TextBox)c).ForeColor = Color.Black;
                    }
                }
                foreach (Control c in changeEmailPanel.Controls)
                {
                    if (c is Label)
                    {
                        ((Label)c).ForeColor = Color.Black;
                    }
                    if (c is TextBox)
                    {
                        ((TextBox)c).BackColor = Color.FromArgb(240, 240, 240);
                        ((TextBox)c).ForeColor = Color.Black;
                    }
                }
                foreach (Control c in changePhonePanel.Controls)
                {
                    if (c is Label)
                    {
                        ((Label)c).ForeColor = Color.Black;
                    }
                    if (c is TextBox)
                    {
                        ((TextBox)c).BackColor = Color.FromArgb(240, 240, 240);
                        ((TextBox)c).ForeColor = Color.Black;
                    }
                }
                foreach (Control c in changePinPanel.Controls)
                {
                    if (c is Label)
                    {
                        ((Label)c).ForeColor = Color.Black;
                    }
                    if (c is TextBox)
                    {
                        ((TextBox)c).BackColor = Color.FromArgb(240, 240, 240);
                        ((TextBox)c).ForeColor = Color.Black;
                    }
                }
                pictureBox1.BackColor = Color.Black;
                pictureBox2.BackColor = Color.Black;
                pictureBox3.BackColor = Color.Black;
                pictureBox10.BackColor = Color.Black;
                pictureBox11.BackColor = Color.Black;
                pictureBox12.BackColor = Color.Black;
                pictureBox9.BackColor = Color.Black;
                pictureBox13.BackColor = Color.Black;
                pictureBox14.BackColor = Color.Black;
                pictureBox17.BackColor = Color.Black;
                pictureBox16.BackColor = Color.Black;
                pictureBox15.BackColor = Color.Black;
            }
            else if(changer % 2 == 1)
            {
                user.theme = true;
                user.UpdateUserInfo(user.theme, login);
                QuitProfile.Load(@"C:\Users\aleks\source\repos\MazeBank\Pics\exitProfileWhite.png");
                exitBut.Load(@"C:\Users\aleks\source\repos\MazeBank\ExitPics\exitBlackTheme.png");
                backBut.Load(@"C:\Users\aleks\source\repos\MazeBank\Pics\backWhite.png");
                exitPass.Load(@"C:\Users\aleks\source\repos\MazeBank\ExitPics\exitBlackTheme.png");
                backToSettingsPass.Load(@"C:\Users\aleks\source\repos\MazeBank\Pics\backWhite.png");
                emailExit.Load(@"C:\Users\aleks\source\repos\MazeBank\ExitPics\exitBlackTheme.png");
                backToSettingEmail.Load(@"C:\Users\aleks\source\repos\MazeBank\Pics\backWhite.png");
                exitPhone.Load(@"C:\Users\aleks\source\repos\MazeBank\ExitPics\exitBlackTheme.png");
                backToSettingsPhone.Load(@"C:\Users\aleks\source\repos\MazeBank\Pics\backWhite.png");
                exitPin.Load(@"C:\Users\aleks\source\repos\MazeBank\ExitPics\exitBlackTheme.png");
                backToSettingsPin.Load(@"C:\Users\aleks\source\repos\MazeBank\Pics\backWhite.png");
                toggleTheme.Load(@"C:\Users\aleks\source\repos\MazeBank\Pics\toggleON.png");
                this.BackColor = Color.FromArgb(56, 56, 56);
                foreach (Control c in this.Controls)
                {
                    if (c is Label)
                    {
                        ((Label)c).ForeColor = Color.White;
                    }
                }
                foreach(Control c in changePassPanel.Controls)
                {
                    if (c is Label)
                    {
                        ((Label)c).ForeColor = Color.White;
                    }
                    if (c is TextBox)
                    {
                        ((TextBox)c).BackColor = Color.FromArgb(56, 56, 56);
                        ((TextBox)c).ForeColor = Color.White;
                    }
                }
                foreach (Control c in changeEmailPanel.Controls)
                {
                    if (c is Label)
                    {
                        ((Label)c).ForeColor = Color.White;
                    }
                    if (c is TextBox)
                    {
                        ((TextBox)c).BackColor = Color.FromArgb(56, 56, 56);
                        ((TextBox)c).ForeColor = Color.White;
                    }
                }
                foreach (Control c in changePhonePanel.Controls)
                {
                    if (c is Label)
                    {
                        ((Label)c).ForeColor = Color.White;
                    }
                    if (c is TextBox)
                    {
                        ((TextBox)c).BackColor = Color.FromArgb(56, 56, 56);
                        ((TextBox)c).ForeColor = Color.White;
                    }
                }
                foreach (Control c in changePinPanel.Controls)
                {
                    if (c is Label)
                    {
                        ((Label)c).ForeColor = Color.White;
                    }
                    if (c is TextBox)
                    {
                        ((TextBox)c).BackColor = Color.FromArgb(56, 56, 56);
                        ((TextBox)c).ForeColor = Color.White;
                    }
                }
                pictureBox1.BackColor = Color.White;
                pictureBox2.BackColor = Color.White;
                pictureBox3.BackColor = Color.White;
                pictureBox10.BackColor = Color.White;
                pictureBox11.BackColor = Color.White;
                pictureBox12.BackColor = Color.White;
                pictureBox9.BackColor = Color.White;
                pictureBox13.BackColor = Color.White;
                pictureBox14.BackColor = Color.White;
                pictureBox17.BackColor = Color.White;
                pictureBox16.BackColor = Color.White;
                pictureBox15.BackColor = Color.White;
            }
        }

        private void exitPass_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void backToSettingsPass_Click(object sender, EventArgs e)
        {
            changePassPanel.Hide();
            newVisCount = 1;
            oldPassVisCount = 1;
            oldPassConfVisCount = 1;
        }

        private void changePass_Click(object sender, EventArgs e)
        {
            changePassPanel.Show();
        }

        private void ConfBtn_Click(object sender, EventArgs e)
        {
            if(newPass.Text != "" && Regex.IsMatch(newPass.Text, @"^(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{5,20}$", RegexOptions.Compiled) && oldPass.Text != "" && Regex.IsMatch(oldPass.Text, @"^(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{5,20}$", RegexOptions.Compiled) && oldPassConf.Text != "" && Regex.IsMatch(oldPassConf.Text, @"^(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{5,20}$", RegexOptions.Compiled) && oldPass.Text == oldPassConf.Text)
            {
                user.UpdateUserInfo(newPass.Text, login);
                changePassPanel.Hide();
                this.Hide();
                LoadingForm loading = new LoadingForm();
                loading.ShowDialog();
                newVisCount = 1;
                oldPassVisCount = 1;
                oldPassConfVisCount = 1;
                WinNotify.ShowWinNotify("Изменение пароля", $"{user.userName}, Вы успешно изменили пароль!", 5000);
                this.Show();
            }
            else
            {
                MessageBox.Show("Введены некорректные данные. Проверьте их и попробуйте снова.", "Ошибка ввода данных", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
        }

        private void newPassVis_Click(object sender, EventArgs e)
        {
            if (newVisCount % 2 == 1)
            {
                newPassVis.Load(@"C:\Users\aleks\source\repos\MazeBank\Eyes\openedEye.png");
                newPass.UseSystemPasswordChar = false;
                newVisCount++;
            }

            else if (newVisCount % 2 == 0)
            {
                newPassVis.Load(@"C:\Users\aleks\source\repos\MazeBank\Eyes\closedEye.png");
                newPass.UseSystemPasswordChar = true;
                newVisCount--;
            }
        }

        private void oldPassConfVis_Click(object sender, EventArgs e)
        {
            if (oldPassConfVisCount % 2 == 1)
            {
                oldPassConfVis.Load(@"C:\Users\aleks\source\repos\MazeBank\Eyes\openedEye.png");
                oldPassConf.UseSystemPasswordChar = false;
                oldPassConfVisCount++;
            }

            else if (oldPassConfVisCount % 2 == 0)
            {
                oldPassConfVis.Load(@"C:\Users\aleks\source\repos\MazeBank\Eyes\closedEye.png");
                oldPassConf.UseSystemPasswordChar = true;
                oldPassConfVisCount--;
            }
        }

        private void oldPassVis_Click(object sender, EventArgs e)
        {
            if (oldPassVisCount % 2 == 1)
            {
                oldPassVis.Load(@"C:\Users\aleks\source\repos\MazeBank\Eyes\openedEye.png");
                oldPass.UseSystemPasswordChar = false;
                oldPassVisCount++;
            }

            else if (oldPassVisCount % 2 == 0)
            {
                oldPassVis.Load(@"C:\Users\aleks\source\repos\MazeBank\Eyes\closedEye.png");
                oldPass.UseSystemPasswordChar = true;
                oldPassVisCount--;
            }
        }

        private void newPass_TextChanged(object sender, EventArgs e)
        {
            if (newPass.Text == "" || !Regex.IsMatch(newPass.Text, @"^(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{5,20}$", RegexOptions.Compiled))
            {
                newPassValid.Load(@"C:\Users\aleks\source\repos\MazeBank\Pics\wrong.png");
            }
            else if (newPass.Text != "" && Regex.IsMatch(newPass.Text, @"^(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{5,20}$", RegexOptions.Compiled))
            {
                newPassValid.Load(@"C:\Users\aleks\source\repos\MazeBank\Pics\correct.png");
            }
        }

        private void oldPass_TextChanged(object sender, EventArgs e)
        {
            if (oldPass.Text == "" || !Regex.IsMatch(oldPass.Text, @"^(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{5,20}$", RegexOptions.Compiled) || oldPass.Text != user.userPassword)
            {
                oldPassValid.Load(@"C:\Users\aleks\source\repos\MazeBank\Pics\wrong.png");
            }
            else if (oldPass.Text != "" && Regex.IsMatch(oldPass.Text, @"^(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{5,20}$", RegexOptions.Compiled) && oldPass.Text == user.userPassword)
            {
                oldPassValid.Load(@"C:\Users\aleks\source\repos\MazeBank\Pics\correct.png");
            }
        }

        private void oldPassConf_TextChanged(object sender, EventArgs e)
        {
            if (oldPassConf.Text == "" || !Regex.IsMatch(oldPassConf.Text, @"^(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{5,20}$", RegexOptions.Compiled) || oldPassConf.Text != oldPass.Text || oldPass.Text != user.userPassword)
            {
                oldPassConfValid.Load(@"C:\Users\aleks\source\repos\MazeBank\Pics\wrong.png");
            }
            else if (oldPassConf.Text != "" && Regex.IsMatch(oldPassConf.Text, @"^(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{5,20}$", RegexOptions.Compiled) && oldPassConf.Text == oldPass.Text && oldPass.Text == user.userPassword)
            {
                oldPassConfValid.Load(@"C:\Users\aleks\source\repos\MazeBank\Pics\correct.png");
            }
        }

        private void newEmail_TextChanged(object sender, EventArgs e)
        {
            if(newEmail.Text == "" || !Regex.IsMatch(newEmail.Text, @"^[A-Za-z]+[\.A-Za-z\d_-]*[A-Za-z\d]+@[A-Za-z]+\.[A-Za-z]{2,6}$", RegexOptions.Compiled))
            {
                newEmailCheck.Load(@"C:\Users\aleks\source\repos\MazeBank\Pics\wrong.png");
            }
            else if(newEmail.Text != "" && Regex.IsMatch(newEmail.Text, @"^[A-Za-z]+[\.A-Za-z\d_-]*[A-Za-z\d]+@[A-Za-z]+\.[A-Za-z]{2,6}$", RegexOptions.Compiled))
            {
                newEmailCheck.Load(@"C:\Users\aleks\source\repos\MazeBank\Pics\correct.png");
            }
        }

        private void passEmail_TextChanged(object sender, EventArgs e)
        {
            if (passEmail.Text == "" || !Regex.IsMatch(passEmail.Text, @"^(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{5,20}$", RegexOptions.Compiled) || passEmail.Text != user.userPassword)
            {
                emailPassCheck.Load(@"C:\Users\aleks\source\repos\MazeBank\Pics\wrong.png");
            }
            else if (passEmail.Text != "" && Regex.IsMatch(passEmail.Text, @"^(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{5,20}$", RegexOptions.Compiled) && passEmail.Text == user.userPassword)
            {
                emailPassCheck.Load(@"C:\Users\aleks\source\repos\MazeBank\Pics\correct.png");
            }
        }

        private void passConfEmail_TextChanged(object sender, EventArgs e)
        {
            if (passConfEmail.Text == "" || !Regex.IsMatch(passConfEmail.Text, @"^(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{5,20}$", RegexOptions.Compiled) || passConfEmail.Text != user.userPassword || passConfEmail.Text != passEmail.Text)
            {
                emailPassConfCheck.Load(@"C:\Users\aleks\source\repos\MazeBank\Pics\wrong.png");
            }
            else if (passConfEmail.Text != "" && Regex.IsMatch(passConfEmail.Text, @"^(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{5,20}$", RegexOptions.Compiled) && passConfEmail.Text == user.userPassword && passConfEmail.Text == passEmail.Text)
            {
                emailPassConfCheck.Load(@"C:\Users\aleks\source\repos\MazeBank\Pics\correct.png");
            }
        }

        private void passVisEmail_Click(object sender, EventArgs e)
        {
            if (oldPassConfVisCount % 2 == 1)
            {
                passVisEmail.Load(@"C:\Users\aleks\source\repos\MazeBank\Eyes\openedEye.png");
                passEmail.UseSystemPasswordChar = false;
                oldPassVisCount++;
            }

            else if (oldPassConfVisCount % 2 == 0)
            {
                passVisEmail.Load(@"C:\Users\aleks\source\repos\MazeBank\Eyes\closedEye.png");
                passEmail.UseSystemPasswordChar = true;
                oldPassVisCount--;
            }
        }

        private void passConfVisEmail_Click(object sender, EventArgs e)
        {
            if (oldPassConfVisCount % 2 == 1)
            {
                passConfVisEmail.Load(@"C:\Users\aleks\source\repos\MazeBank\Eyes\openedEye.png");
                passConfEmail.UseSystemPasswordChar = false;
                oldPassConfVisCount++;
            }

            else if (oldPassConfVisCount % 2 == 0)
            {
                passConfVisEmail.Load(@"C:\Users\aleks\source\repos\MazeBank\Eyes\closedEye.png");
                passConfEmail.UseSystemPasswordChar = true;
                oldPassConfVisCount--;
            }
        }

        private void emailExit_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void backToSettingEmail_Click(object sender, EventArgs e)
        {
            changeEmailPanel.Hide();
            oldPassVisCount = 1;
            oldPassConfVisCount = 1;
        }

        private void saveEmail_Click(object sender, EventArgs e)
        {
            if(newEmail.Text != "" && Regex.IsMatch(newEmail.Text, @"^[A-Za-z]+[\.A-Za-z\d_-]*[A-Za-z\d]+@[A-Za-z]+\.[A-Za-z]{2,6}$", RegexOptions.Compiled) && passEmail.Text != "" && Regex.IsMatch(passEmail.Text, @"^(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{5,20}$", RegexOptions.Compiled) && passEmail.Text == user.userPassword && passConfEmail.Text != "" && Regex.IsMatch(passConfEmail.Text, @"^(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{5,20}$", RegexOptions.Compiled) && passConfEmail.Text == user.userPassword && passConfEmail.Text == passEmail.Text)
            {
                user.UpdateUserInfo(newEmail.Text, login.ToString());
                changeEmailPanel.Hide();
                this.Hide();
                LoadingForm loading = new LoadingForm();
                loading.ShowDialog();
                oldPassVisCount = 1;
                oldPassConfVisCount = 1;
                WinNotify.ShowWinNotify("Изменение почты", $"{user.userName}, Вы успешно изменили почту!", 5000);
                this.Show();
            }
            else
            {
                MessageBox.Show("Введены некорректные данные. Проверьте их и попробуйте снова.", "Ошибка ввода данных", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
        }

        private void changeEmail_Click(object sender, EventArgs e)
        {
            changeEmailPanel.Show();
        }

        private void exitPhone_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void backToSettingsPhone_Click(object sender, EventArgs e)
        {
            changePhonePanel.Hide();
            oldPassVisCount = 1;
            oldPassConfVisCount = 1;
        }

        private void changeLogin_Click(object sender, EventArgs e)
        {
            changePhonePanel.Show();
        }

        private void newPhone_TextChanged(object sender, EventArgs e)
        {
            if (newPhone.Text == "" || !Regex.IsMatch(newPhone.Text, @"^380(3|[5-6]|9)[0-9]\d{3}\d{2}\d{2}$", RegexOptions.Compiled))
            {
                phoneCheck.Load(@"C:\Users\aleks\source\repos\MazeBank\Pics\wrong.png");
            }
            else if (newPhone.Text != "" || Regex.IsMatch(newPhone.Text, @"^380(3|[5-6]|9)[0-9]\d{3}\d{2}\d{2}$", RegexOptions.Compiled))
            {
                phoneCheck.Load(@"C:\Users\aleks\source\repos\MazeBank\Pics\correct.png");
            }
        }

        private void passPhone_TextChanged(object sender, EventArgs e)
        {
            if (passPhone.Text == "" || !Regex.IsMatch(passPhone.Text, @"^(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{5,20}$", RegexOptions.Compiled) || passPhone.Text != user.userPassword)
            {
                passPhoneCheck.Load(@"C:\Users\aleks\source\repos\MazeBank\Pics\wrong.png");
            }
            else if (passPhone.Text != "" && Regex.IsMatch(passPhone.Text, @"^(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{5,20}$", RegexOptions.Compiled) && passPhone.Text == user.userPassword)
            {
                passPhoneCheck.Load(@"C:\Users\aleks\source\repos\MazeBank\Pics\correct.png");
            }
        }

        private void passPhoneConf_TextChanged(object sender, EventArgs e)
        {
            if (passPhoneConf.Text == "" || !Regex.IsMatch(passPhoneConf.Text, @"^(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{5,20}$", RegexOptions.Compiled) || passPhoneConf.Text != user.userPassword)
            {
                passPhoneConfCheck.Load(@"C:\Users\aleks\source\repos\MazeBank\Pics\wrong.png");
            }
            else if (passPhoneConf.Text != "" && Regex.IsMatch(passPhoneConf.Text, @"^(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{5,20}$", RegexOptions.Compiled) && passPhoneConf.Text == user.userPassword)
            {
                passPhoneConfCheck.Load(@"C:\Users\aleks\source\repos\MazeBank\Pics\correct.png");
            }
        }

        private void passPhoneVis_Click(object sender, EventArgs e)
        {
            if (oldPassVisCount % 2 == 1)
            {
                passPhoneVis.Load(@"C:\Users\aleks\source\repos\MazeBank\Eyes\openedEye.png");
                passPhone.UseSystemPasswordChar = false;
                oldPassVisCount++;
            }

            else if (oldPassVisCount % 2 == 0)
            {
                passPhoneVis.Load(@"C:\Users\aleks\source\repos\MazeBank\Eyes\closedEye.png");
                passPhone.UseSystemPasswordChar = true;
                oldPassVisCount--;
            }
        }

        private void passPhoneConfVis_Click(object sender, EventArgs e)
        {
            if (oldPassConfVisCount % 2 == 1)
            {
                passPhoneConfVis.Load(@"C:\Users\aleks\source\repos\MazeBank\Eyes\openedEye.png");
                passPhoneConf.UseSystemPasswordChar = false;
                oldPassConfVisCount++;
            }

            else if (oldPassConfVisCount % 2 == 0)
            {
                passPhoneConfVis.Load(@"C:\Users\aleks\source\repos\MazeBank\Eyes\closedEye.png");
                passPhoneConf.UseSystemPasswordChar = true;
                oldPassConfVisCount--;
            }
        }

        private void savePhone_Click(object sender, EventArgs e)
        {
            if (newPhone.Text != "" && Regex.IsMatch(newPhone.Text, @"^380(3|[5-6]|9)[0-9]\d{3}\d{2}\d{2}$", RegexOptions.Compiled) && passPhone.Text != "" && Regex.IsMatch(passPhone.Text, @"^(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{5,20}$", RegexOptions.Compiled) && passPhone.Text == user.userPassword && passPhoneConf.Text != "" && Regex.IsMatch(passPhoneConf.Text, @"^(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{5,20}$", RegexOptions.Compiled) && passPhoneConf.Text == user.userPassword)
            {
                user.UpdateUserInfo(long.Parse(newPhone.Text), login);
                changePhonePanel.Hide();
                this.Hide();
                LoadingForm loading = new LoadingForm();
                loading.ShowDialog();
                oldPassVisCount = 1;
                oldPassConfVisCount = 1;
                WinNotify.ShowWinNotify("Изменение номера телефона", $"{user.userName}, Вы успешно изменили номер телефона!", 5000);
                this.Show();
                login = long.Parse(newPhone.Text);
            }
            else
            {
                MessageBox.Show("Введены некорректные данные. Проверьте их и попробуйте снова.", "Ошибка ввода данных", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
        }

        private void QuitProfile_Click(object sender, EventArgs e)
        {
            Close();
            auth.Show();
        }

        private void exitPin_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void changePin_Click(object sender, EventArgs e)
        {
            changePinPanel.Show();
        }

        private void backToSettingsPin_Click(object sender, EventArgs e)
        {
            changePinPanel.Hide();
            newVisCount = 1;
            oldPassVisCount = 1;
            oldPassConfVisCount = 1;

        }

        private void newPin_TextChanged(object sender, EventArgs e)
        {
            if (newPin.Text == "" || !Regex.IsMatch(newPin.Text, @"^\d{4}$", RegexOptions.Compiled))
            {
                pinCheck.Load(@"C:\Users\aleks\source\repos\MazeBank\Pics\wrong.png");
            }
            else if (newPin.Text != "" && Regex.IsMatch(newPin.Text, @"^\d{4}$", RegexOptions.Compiled))
            {
                pinCheck.Load(@"C:\Users\aleks\source\repos\MazeBank\Pics\correct.png");
            }
        }

        private void passPin_TextChanged(object sender, EventArgs e)
        {
            if (passPin.Text == "" || !Regex.IsMatch(passPin.Text, @"^(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{5,20}$", RegexOptions.Compiled) || passPin.Text != user.userPassword)
            {
                passCheckPin.Load(@"C:\Users\aleks\source\repos\MazeBank\Pics\wrong.png");
            }
            else if (passPin.Text != "" && Regex.IsMatch(passPin.Text, @"^(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{5,20}$", RegexOptions.Compiled) && passPin.Text == user.userPassword)
            {
                passCheckPin.Load(@"C:\Users\aleks\source\repos\MazeBank\Pics\correct.png");
            }
        }

        private void passConfPin_TextChanged(object sender, EventArgs e)
        {
            if (passConfPin.Text == "" || !Regex.IsMatch(passConfPin.Text, @"^(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{5,20}$", RegexOptions.Compiled) || passConfPin.Text != passPin.Text)
            {
                passCheckConfPin.Load(@"C:\Users\aleks\source\repos\MazeBank\Pics\wrong.png");
            }
            else if (passConfPin.Text != "" && Regex.IsMatch(passConfPin.Text, @"^(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{5,20}$", RegexOptions.Compiled) && passConfPin.Text == passPin.Text)
            {
                passCheckConfPin.Load(@"C:\Users\aleks\source\repos\MazeBank\Pics\correct.png");
            }
        }

        private void savePin_Click(object sender, EventArgs e)
        {
            if(newPin.Text != "" && Regex.IsMatch(newPin.Text, @"^\d{4}$", RegexOptions.Compiled) && passConfPin.Text != "" && Regex.IsMatch(passConfPin.Text, @"^(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{5,20}$", RegexOptions.Compiled) && passConfPin.Text == passPin.Text && passPin.Text != "" && Regex.IsMatch(passPin.Text, @"^(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{5,20}$", RegexOptions.Compiled) && passPin.Text == user.userPassword)
            {
                user.UpdateUserInfo(short.Parse(newPin.Text), login);
                changePinPanel.Hide();
                this.Hide();
                LoadingForm loading = new LoadingForm();
                loading.ShowDialog();
                newVisCount = 1;
                oldPassVisCount = 1;
                oldPassConfVisCount = 1;
                WinNotify.ShowWinNotify("Изменение ПИН'а", $"{user.userName}, Вы успешно изменили ПИН!", 5000);
                this.Show();
            }
            else
            {
                MessageBox.Show("Введены некорректные данные. Проверьте их и попробуйте снова.", "Ошибка ввода данных", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
        }

        private void pinVis_Click(object sender, EventArgs e)
        {
            if (newVisCount % 2 == 1)
            {
                pinVis.Load(@"C:\Users\aleks\source\repos\MazeBank\Eyes\openedEye.png");
                newPin.UseSystemPasswordChar = false;
                newVisCount++;
            }

            else if (newVisCount % 2 == 0)
            {
                pinVis.Load(@"C:\Users\aleks\source\repos\MazeBank\Eyes\closedEye.png");
                newPin.UseSystemPasswordChar = true;
                newVisCount--;
            }
        }

        private void passVisPin_Click(object sender, EventArgs e)
        {
            if (oldPassVisCount % 2 == 1)
            {
                passVisPin.Load(@"C:\Users\aleks\source\repos\MazeBank\Eyes\openedEye.png");
                passPin.UseSystemPasswordChar = false;
                oldPassVisCount++;
            }

            else if (oldPassVisCount % 2 == 0)
            {
                passVisPin.Load(@"C:\Users\aleks\source\repos\MazeBank\Eyes\closedEye.png");
                passPin.UseSystemPasswordChar = true;
                oldPassVisCount--;
            }
        }

        private void passConfVisPin_Click(object sender, EventArgs e)
        {
            if (oldPassConfVisCount % 2 == 1)
            {
                passConfVisPin.Load(@"C:\Users\aleks\source\repos\MazeBank\Eyes\openedEye.png");
                passConfPin.UseSystemPasswordChar = false;
                oldPassConfVisCount++;
            }

            else if (oldPassConfVisCount % 2 == 0)
            {
                passConfVisPin.Load(@"C:\Users\aleks\source\repos\MazeBank\Eyes\closedEye.png");
                passConfPin.UseSystemPasswordChar = true;
                oldPassConfVisCount--;
            }
        }
    }
}