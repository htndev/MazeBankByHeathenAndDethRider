﻿using System;
using System.Drawing;
using System.Windows.Forms;
using AuthClass;
using BillPrinting;
using MailNotify;
using WinNotification;
using HistoryClass;
using System.Text.RegularExpressions;
using System.Media;

namespace MazeBank
{
    public partial class MazeFine : Form
    {
        public MazeFine()
        {
            InitializeComponent();
        }
        #region Переменные

        public long login;

        MainMenu mainMenu = new MainMenu();

        MoneyTransactions user = new MoneyTransactions();

        ConfirmationForm confirmation = new ConfirmationForm();

        LoadingForm loading = new LoadingForm();

        SoundPlayer sound = new SoundPlayer(@"C:\Users\aleks\source\repos\MazeBank\sound.wav");

        int fineIdClick = 0;

        int fineIdChanged = 0;

        int fineIdErr = 0;

        int intruderClick = 0;

        int intruderChanged = 0;

        int intruderErr = 0;

        int amountClick = 0;

        int amountChanged = 0;

        int amountErr = 0;

        int emailClick = 0;

        int emailChanged = 0;

        int emailErr = 0;

        decimal amount;

        double percent = 0.3;

        #endregion Перемемнные

        private void exitBut_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void backBut_Click(object sender, EventArgs e)
        {
            Close();
            mainMenu.login = login;
            mainMenu.Show();
        }

        private void MazeFine_Load(object sender, EventArgs e)
        {
            user.UserConnection(login);
            if (user.theme)
            {
                exitBut.Load(@"C:\Users\aleks\source\repos\MazeBank\ExitPics\exitBlackTheme.png");
                backBut.Load(@"C:\Users\aleks\source\repos\MazeBank\Pics\backWhite.png");
                this.BackColor = Color.FromArgb(56, 56, 56);
                foreach (Control c in this.Controls)
                {
                    if (c is Label)
                    {
                        ((Label)c).ForeColor = Color.White;
                    }
                    if (c is TextBox)
                    {
                        ((TextBox)c).BackColor = Color.FromArgb(56, 56, 56);
                    }
                }
                pictureBox1.BackColor = Color.White;
                pictureBox2.BackColor = Color.White;
                pictureBox3.BackColor = Color.White;
                pictureBox4.BackColor = Color.White;
            }
        }

        private void fineIdText_Click(object sender, EventArgs e)
        {
            if (user.theme)
            {
                if (fineIdText.Text != null && fineIdClick == 0)
                {
                    fineIdText.Clear();
                    fineIdText.ForeColor = Color.White;
                    fineIdClick++;
                }
                else if (fineIdText.Text != null && fineIdChanged != 0)
                {
                    fineIdText.Clear();
                    fineIdText.ForeColor = Color.White;
                }
            }
            else if (!user.theme)
            {
                if (fineIdText.Text != null && fineIdClick == 0)
                {
                    fineIdText.Clear();
                    fineIdText.ForeColor = Color.Black;
                    fineIdClick++;
                }
                else if (fineIdText.Text != null && fineIdChanged != 0)
                {
                    fineIdText.Clear();
                    fineIdText.ForeColor = Color.Black;
                }
            }
        }

        private void fineIdText_Leave(object sender, EventArgs e)
        {
            fineIdChanged = 0;
            if (fineIdText.Text == "")
            {
                fineIdChanged++;
                fineIdText.Text = "Постановление";
                fineIdText.ForeColor = Color.Gray;
            }
            if (!Regex.IsMatch(fineIdText.Text, @"^([А-Я]{2}\d|[А-Я]\d{2})\s\d{6}$", RegexOptions.Compiled) && fineIdErr <= 4)
            {
                fineIdErr++;
                fineIdText.Clear();
                fineIdText.Focus();
                MessageBox.Show("Введенное постановление не соответствует формату! Проверьте или введите еще раз.");
                if(!user.theme)
                {
                    fineIdText.ForeColor = Color.Black;
                }
                if(user.theme)
                {
                    fineIdText.ForeColor = Color.White;
                }
                return;
            }
            if (fineIdErr > 4)
            {
                fineIdText.Clear();
                fineIdText.Focus();
                fineIdText.ForeColor = Color.Black;
                WinNotify.ShowWinNotify("Ошибка ввода постановления", $"{user.userName}, Вы вводите неправильный формат постановления.\nФормат должен быть таким: АБ3 123456 или А12 345678", 10000);
            }
        }

        private void fineIdText_TextChanged(object sender, EventArgs e)
        {
            fineIdText.MaxLength = 10;
        }

        private void intruderText_Leave(object sender, EventArgs e)
        {
            intruderChanged = 0;
            if (intruderText.Text == "")
            {
                intruderChanged++;
                intruderText.Text = "ФИО";
                intruderText.ForeColor = Color.Gray;
            }
            if (!Regex.IsMatch(intruderText.Text, @"^[А-Я][а-я]+(\-[А-Я][а-я]+)?\s[А-Я][а-я]+(\-[А-Я][а-я]+)?\s[А-Я][а-я]+$", RegexOptions.Compiled) && fineIdErr <= 4)
            {
                intruderErr++;
                intruderText.Clear();
                intruderText.Focus();
                MessageBox.Show("Введенное ФИО не соответствует формату! Проверьте или введите еще раз.");
                if (!user.theme)
                {
                    intruderText.ForeColor = Color.Black;
                }
                if (user.theme)
                {
                    intruderText.ForeColor = Color.White;
                }
                return;
            }
            if (intruderErr > 4)
            {
                intruderText.Clear();
                intruderText.Focus();
                intruderText.ForeColor = Color.Black;
                WinNotify.ShowWinNotify("Ошибка ввода ФИО", $"{user.userName}, Вы вводите неправильный формат ФИО.\nФормат должен быть таким: Иванов Иван Иванович или Василенко Василий Васильевич", 10000);
            }
        }

        private void intruderText_Click(object sender, EventArgs e)
        {
            if (user.theme)
            {
                if (intruderText.Text != null && intruderClick == 0)
                {
                    intruderText.Clear();
                    intruderText.ForeColor = Color.White;
                    intruderClick++;
                }
                else if (intruderText.Text != null && intruderChanged != 0)
                {
                    intruderText.Clear();
                    intruderText.ForeColor = Color.White;
                }
            }
            else if (!user.theme)
            {
                if (intruderText.Text != null && intruderClick == 0)
                {
                    intruderText.Clear();
                    intruderText.ForeColor = Color.Black;
                    intruderClick++;
                }
                else if (intruderText.Text != null && intruderChanged != 0)
                {
                    intruderText.Clear();
                    intruderText.ForeColor = Color.Black;
                }
            }
        }

        private void amountText_Click(object sender, EventArgs e)
        {
            if (user.theme)
            {
                if (amountText.Text != null && amountClick == 0)
                {
                    amountText.Clear();
                    amountText.ForeColor = Color.White;
                    amountClick++;
                }
                else if (amountText.Text != null && amountChanged != 0)
                {
                    amountText.Clear();
                    amountText.ForeColor = Color.White;
                }
            }
            else if (!user.theme)
            {
                if (amountText.Text != null && amountClick == 0)
                {
                    amountText.Clear();
                    amountText.ForeColor = Color.Black;
                    amountClick++;
                }
                else if (amountText.Text != null && amountChanged != 0)
                {
                    amountText.Clear();
                    amountText.ForeColor = Color.Black;
                }
            }
        }

        private void amountText_Leave(object sender, EventArgs e)
        {
            amountChanged = 0;
            if (amountText.Text == "")
            {
                amountChanged++;
                amountText.Text = "Сумма";
                amountText.ForeColor = Color.Gray;
            }
            if (!Regex.IsMatch(amountText.Text, @"^\d{0,5}(?:(?:(\.)|(\,))\d{0,2})?$", RegexOptions.Compiled) && fineIdErr <= 4)
            {
                amountErr++;
                amountText.Clear();
                amountText.Focus();
                MessageBox.Show("Введенаня сумма не соответствует формату! Проверьте или введите еще раз.");
                if (!user.theme)
                {
                    amountText.ForeColor = Color.Black;
                }
                if (user.theme)
                {
                    amountText.ForeColor = Color.White;
                }
                return;
            }
            if (amountErr > 4)
            {
                amountText.Clear();
                amountText.Focus();
                amountText.ForeColor = Color.Black;
                WinNotify.ShowWinNotify("Ошибка ввода суммы", $"{user.userName}, Вы вводите неправильный формат суммы.\nФормат должен быть таким: 12.34 или 15", 10000);
            }
        }

        private void emailText_Click(object sender, EventArgs e)
        {
            if (user.theme)
            {
                if (emailText.Text != null && emailClick == 0)
                {
                    emailText.Clear();
                    emailText.ForeColor = Color.White;
                    emailClick++;
                }
                else if (emailText.Text != null && emailChanged != 0)
                {
                    emailText.Clear();
                    emailText.ForeColor = Color.White;
                }
            }
            else if (!user.theme)
            {
                if (emailText.Text != null && emailClick == 0)
                {
                    emailText.Clear();
                    emailText.ForeColor = Color.Black;
                    emailClick++;
                }
                else if (emailText.Text != null && emailChanged != 0)
                {
                    emailText.Clear();
                    emailText.ForeColor = Color.Black;
                }
            }
        }

        private void emailText_Leave(object sender, EventArgs e)
        {
            emailChanged = 0;
            if (emailText.Text == "")
            {
                emailChanged++;
                emailText.Text = "Email";
                emailText.ForeColor = Color.Gray;
            }
            if (!Regex.IsMatch(emailText.Text, @"^[A-Za-z]+[\.A-Za-z\d_-]*[A-Za-z\d]+@[A-Za-z]+\.[A-Za-z]{2,6}$", RegexOptions.Compiled) && emailErr <= 4)
            {
                emailErr++;
                emailText.Clear();
                emailText.Focus();
                MessageBox.Show("Введенная почта не соответствует формату! Проверьте или введите еще раз.");
                if (!user.theme)
                {
                    emailText.ForeColor = Color.Black;
                }
                if (user.theme)
                {
                    emailText.ForeColor = Color.White;
                }
                return;
            }
            if (emailErr > 4)
            {
                emailText.Clear();
                emailText.Focus();
                emailText.ForeColor = Color.Black;
                WinNotify.ShowWinNotify("Ошибка ввода почты", $"{user.userName}, Вы вводите неправильный формат почты.\nФормат должен быть таким: t3st.mail@mail.com или try_th1s.mail@mail.ua", 10000);
            }
        }

        private void pay_Click(object sender, EventArgs e)
        {
            if (Regex.IsMatch(amountText.Text, @"^\d{0,5}(?:(?:(\.)|(\,))\d{0,2})?$", RegexOptions.Compiled) && Regex.IsMatch(intruderText.Text, @"^[А-Я][а-я]+(\-[А-Я][а-я]+)?\s[А-Я][а-я]+(\-[А-Я][а-я]+)?\s[А-Я][а-я]+$", RegexOptions.Compiled) && Regex.IsMatch(emailText.Text, @"^[A-Za-z]+[\.A-Za-z\d_-]*[A-Za-z\d]+@[A-Za-z]+\.[A-Za-z]{2,6}$", RegexOptions.Compiled) && Regex.IsMatch(emailText.Text, @"^[A-Za-z]+[\.A-Za-z\d_-]*[A-Za-z\d]+@[A-Za-z]+\.[A-Za-z]{2,6}$", RegexOptions.Compiled) && Regex.IsMatch(fineIdText.Text, @"^([А-Я]{2}\d|[А-Я]\d{2})\s\d{6}$", RegexOptions.Compiled))
            {
                try
                {
                    string fineId = fineIdText.Text;
                    string intruder = intruderText.Text;
                    string email = emailText.Text;
                    amount = decimal.Parse(amountText.Text.Replace(".", ","));
                    decimal committee = MoneyTransactions.CalculateCommittee(amount, percent);
                    amount += committee;
                    if (user.userBalance >= amount)
                    {
                        confirmation.userName = user.userName;
                        confirmation.userPassword = user.userPassword;
                        confirmation.amount = amount;
                        this.Opacity = 0.2;
                        confirmation.ShowDialog();
                        if (!confirmation.ok)
                        {
                            user.userBalance -= amount;
                            this.Opacity = 1;
                            user.UserTransfer(login, user.userBalance);
                            Close();
                            loading.ShowDialog();
                            DialogResult askPrintBill = MessageBox.Show("Вы желаете распечатать чек?", "Печать чека", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                            if (askPrintBill == DialogResult.Yes)
                            {
                                History.AddToHistory(user.userCard, $"-{amount} – Оплата штрафа");
                                MailDelivery.NotifyByMail(user.userEmail, user.userName, user.userSurname, $"оплата штрафа {fineId}", amount, user.userBalance);
                                PrintBill.PrintingBill(intruder, amount, committee, fineId, user.userCard, email);
                                WinNotify.ShowWinNotify("Оплата штрафа", $"{user.userName}, Вы успешно оплатили штраф {fineId} в размере {amount - committee}₴\nВаш поточный баланс: {user.userBalance:0.##}₴", 5000);
                                sound.Play();
                                mainMenu.login = login;
                                mainMenu.Show();
                            }
                            else
                            {
                                History.AddToHistory(user.userCard, $"-{amount} – Оплата штрафа");
                                MailDelivery.NotifyByMail(user.userEmail, user.userName, user.userSurname, $"оплата штрафа {fineId}", amount, user.userBalance);
                                PrintBill.CreatingBill(intruder, amount, committee, fineId, user.userCard, email);
                                WinNotify.ShowWinNotify("Оплата штрафа", $"{user.userName}, Вы успешно оплатили штраф {fineId} в размере {amount - committee}₴\nВаш поточный баланс: {user.userBalance:0.##}₴", 5000);
                                sound.Play();
                                mainMenu.login = login;
                                mainMenu.Show();
                            }
                        }
                        else if (confirmation.ok)
                        {
                            MessageBox.Show($"Извините, {user.userName}, Вы не можете произвести операцию без подтверждения.", "Ошибка перевода", MessageBoxButtons.OK, MessageBoxIcon.Error);
                            pay.Focus();
                            this.Opacity = 1;
                            return;
                        }
                        else
                        {
                            this.Opacity = 1;
                        }
                    }
                    else
                    {
                        MessageBox.Show($"У вас недостаточно средств для осуществления пополнения. Введите сумму меньше или равную {amount - (MoneyTransactions.CalculateCommittee(amount, percent))}.", "Ошибка размера суммы", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        amountText.Focus();
                        this.Opacity = 1;
                        return;
                    }
                }
                catch (FormatException fe)
                {
                    MessageBox.Show(fe.Message);
                }
            }
            else
            {
                MessageBox.Show("Извините, Вы ввели некорректные данные.", "Ошибка формата", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
        }

        private void amountText_TextChanged(object sender, EventArgs e)
        {
            amountText.MaxLength = 8;
        }

        private void pay_KeyDown(object sender, KeyEventArgs e)
        {
            if(e.KeyCode == Keys.Enter)
            {
                pay.PerformClick();
            }
        }
    }
}