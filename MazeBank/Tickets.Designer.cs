﻿namespace MazeBank
{
    partial class Tickets
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Tickets));
            this.exitBut = new System.Windows.Forms.PictureBox();
            this.backBut = new System.Windows.Forms.PictureBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.airplanePic = new System.Windows.Forms.PictureBox();
            this.trainPic = new System.Windows.Forms.PictureBox();
            this.busPic = new System.Windows.Forms.PictureBox();
            this.cinemaPic = new System.Windows.Forms.PictureBox();
            this.airplane = new System.Windows.Forms.Panel();
            this.issueAP = new System.Windows.Forms.Panel();
            this.arrivalTimeAP = new System.Windows.Forms.Label();
            this.backToAP = new System.Windows.Forms.Label();
            this.placesAP = new System.Windows.Forms.ListBox();
            this.placeAP = new System.Windows.Forms.Label();
            this.arrivalPlaceAP = new System.Windows.Forms.Label();
            this.departTimeAP = new System.Windows.Forms.Label();
            this.departPlaceAP = new System.Windows.Forms.Label();
            this.apPay = new System.Windows.Forms.Button();
            this.amountAP = new System.Windows.Forms.Label();
            this.apIssueBack = new System.Windows.Forms.PictureBox();
            this.apIssueExit = new System.Windows.Forms.PictureBox();
            this.issueBtnAP = new System.Windows.Forms.Button();
            this.amountOfTicketsAP = new System.Windows.Forms.NumericUpDown();
            this.dtPanelAP = new System.Windows.Forms.Panel();
            this.backAP = new System.Windows.Forms.Label();
            this.arrivalDateAP = new System.Windows.Forms.DateTimePicker();
            this.arrivalHourAP = new System.Windows.Forms.NumericUpDown();
            this.ArrivalMinAP = new System.Windows.Forms.NumericUpDown();
            this.label6 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.departHourAI = new System.Windows.Forms.NumericUpDown();
            this.departMinAP = new System.Windows.Forms.NumericUpDown();
            this.label4 = new System.Windows.Forms.Label();
            this.departDateAP = new System.Windows.Forms.DateTimePicker();
            this.twoWays = new System.Windows.Forms.CheckBox();
            this.oneWay = new System.Windows.Forms.CheckBox();
            this.arrivalAP = new System.Windows.Forms.ComboBox();
            this.departAP = new System.Windows.Forms.ComboBox();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.label3 = new System.Windows.Forms.Label();
            this.apBack = new System.Windows.Forms.PictureBox();
            this.apExit = new System.Windows.Forms.PictureBox();
            this.trExit = new System.Windows.Forms.PictureBox();
            this.trBack = new System.Windows.Forms.PictureBox();
            this.label11 = new System.Windows.Forms.Label();
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            this.departTR = new System.Windows.Forms.ComboBox();
            this.arrivalTR = new System.Windows.Forms.ComboBox();
            this.oneWayTr = new System.Windows.Forms.CheckBox();
            this.twoWaysTr = new System.Windows.Forms.CheckBox();
            this.departDateTR = new System.Windows.Forms.DateTimePicker();
            this.label10 = new System.Windows.Forms.Label();
            this.departTimeMinTR = new System.Windows.Forms.NumericUpDown();
            this.departTimeHourTR = new System.Windows.Forms.NumericUpDown();
            this.label9 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.issueBtnTR = new System.Windows.Forms.Button();
            this.dtTimeTR = new System.Windows.Forms.Panel();
            this.label7 = new System.Windows.Forms.Label();
            this.arrivalDateTR = new System.Windows.Forms.DateTimePicker();
            this.arrivalTimeHourTR = new System.Windows.Forms.NumericUpDown();
            this.arrivalTimeMinTR = new System.Windows.Forms.NumericUpDown();
            this.amountOfTicketsTR = new System.Windows.Forms.NumericUpDown();
            this.issueTR = new System.Windows.Forms.Panel();
            this.toPayTR = new System.Windows.Forms.Label();
            this.payTR = new System.Windows.Forms.Button();
            this.placesTR = new System.Windows.Forms.ListBox();
            this.label12 = new System.Windows.Forms.Label();
            this.backToTR = new System.Windows.Forms.Label();
            this.arrivalDateLabelTR = new System.Windows.Forms.Label();
            this.arrivalToTR = new System.Windows.Forms.Label();
            this.departDateLabelTR = new System.Windows.Forms.Label();
            this.departFromTR = new System.Windows.Forms.Label();
            this.issueTRBack = new System.Windows.Forms.PictureBox();
            this.issueTRExit = new System.Windows.Forms.PictureBox();
            this.train = new System.Windows.Forms.Panel();
            this.bus = new System.Windows.Forms.Panel();
            this.issueBS = new System.Windows.Forms.Panel();
            this.arrivalTimeLblBS = new System.Windows.Forms.Label();
            this.backToBS = new System.Windows.Forms.Label();
            this.placesBS = new System.Windows.Forms.ListBox();
            this.label20 = new System.Windows.Forms.Label();
            this.arrivalToLblBS = new System.Windows.Forms.Label();
            this.departTimeLblBS = new System.Windows.Forms.Label();
            this.departFromBS = new System.Windows.Forms.Label();
            this.payBS = new System.Windows.Forms.Button();
            this.toPayBS = new System.Windows.Forms.Label();
            this.issueBSBack = new System.Windows.Forms.PictureBox();
            this.issueBsExit = new System.Windows.Forms.PictureBox();
            this.amountOfTicketsBS = new System.Windows.Forms.NumericUpDown();
            this.arrivalPanelBS = new System.Windows.Forms.Panel();
            this.label13 = new System.Windows.Forms.Label();
            this.arrivalDateBS = new System.Windows.Forms.DateTimePicker();
            this.arrivalTimeHourBS = new System.Windows.Forms.NumericUpDown();
            this.arrivalTimeMinBS = new System.Windows.Forms.NumericUpDown();
            this.issueBtnBS = new System.Windows.Forms.Button();
            this.label14 = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.departTimeHourBS = new System.Windows.Forms.NumericUpDown();
            this.departTimeMinBS = new System.Windows.Forms.NumericUpDown();
            this.label16 = new System.Windows.Forms.Label();
            this.departDateBS = new System.Windows.Forms.DateTimePicker();
            this.twoWaysBS = new System.Windows.Forms.CheckBox();
            this.oneWayBS = new System.Windows.Forms.CheckBox();
            this.arrivalBS = new System.Windows.Forms.ComboBox();
            this.departBS = new System.Windows.Forms.ComboBox();
            this.pictureBox3 = new System.Windows.Forms.PictureBox();
            this.label17 = new System.Windows.Forms.Label();
            this.backBS = new System.Windows.Forms.PictureBox();
            this.exitBS = new System.Windows.Forms.PictureBox();
            this.cinema = new System.Windows.Forms.Panel();
            this.issueCinema = new System.Windows.Forms.Panel();
            this.cinemaTickets = new System.Windows.Forms.ListBox();
            this.label21 = new System.Windows.Forms.Label();
            this.movieDate = new System.Windows.Forms.Label();
            this.movieTitle = new System.Windows.Forms.Label();
            this.payCinema = new System.Windows.Forms.Button();
            this.toPayCinema = new System.Windows.Forms.Label();
            this.cinemaIssueBack = new System.Windows.Forms.PictureBox();
            this.cinemaIssueExit = new System.Windows.Forms.PictureBox();
            this.cinemaMin = new System.Windows.Forms.NumericUpDown();
            this.cinemaHour = new System.Windows.Forms.NumericUpDown();
            this.issueMovie = new System.Windows.Forms.Button();
            this.amountOfTickets = new System.Windows.Forms.NumericUpDown();
            this.moviesSchedule = new System.Windows.Forms.DateTimePicker();
            this.movies = new System.Windows.Forms.ComboBox();
            this.pictureBox4 = new System.Windows.Forms.PictureBox();
            this.label23 = new System.Windows.Forms.Label();
            this.cinemaBack = new System.Windows.Forms.PictureBox();
            this.cinemaExit = new System.Windows.Forms.PictureBox();
            ((System.ComponentModel.ISupportInitialize)(this.exitBut)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.backBut)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.airplanePic)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.trainPic)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.busPic)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cinemaPic)).BeginInit();
            this.airplane.SuspendLayout();
            this.issueAP.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.apIssueBack)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.apIssueExit)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.amountOfTicketsAP)).BeginInit();
            this.dtPanelAP.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.arrivalHourAP)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ArrivalMinAP)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.departHourAI)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.departMinAP)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.apBack)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.apExit)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.trExit)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.trBack)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.departTimeMinTR)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.departTimeHourTR)).BeginInit();
            this.dtTimeTR.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.arrivalTimeHourTR)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.arrivalTimeMinTR)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.amountOfTicketsTR)).BeginInit();
            this.issueTR.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.issueTRBack)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.issueTRExit)).BeginInit();
            this.train.SuspendLayout();
            this.bus.SuspendLayout();
            this.issueBS.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.issueBSBack)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.issueBsExit)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.amountOfTicketsBS)).BeginInit();
            this.arrivalPanelBS.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.arrivalTimeHourBS)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.arrivalTimeMinBS)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.departTimeHourBS)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.departTimeMinBS)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.backBS)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.exitBS)).BeginInit();
            this.cinema.SuspendLayout();
            this.issueCinema.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cinemaIssueBack)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cinemaIssueExit)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cinemaMin)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cinemaHour)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.amountOfTickets)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cinemaBack)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cinemaExit)).BeginInit();
            this.SuspendLayout();
            // 
            // exitBut
            // 
            this.exitBut.Cursor = System.Windows.Forms.Cursors.Hand;
            this.exitBut.Image = ((System.Drawing.Image)(resources.GetObject("exitBut.Image")));
            this.exitBut.Location = new System.Drawing.Point(358, 12);
            this.exitBut.Name = "exitBut";
            this.exitBut.Size = new System.Drawing.Size(34, 34);
            this.exitBut.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.exitBut.TabIndex = 19;
            this.exitBut.TabStop = false;
            this.exitBut.Click += new System.EventHandler(this.exitBut_Click);
            // 
            // backBut
            // 
            this.backBut.Cursor = System.Windows.Forms.Cursors.Hand;
            this.backBut.Image = ((System.Drawing.Image)(resources.GetObject("backBut.Image")));
            this.backBut.Location = new System.Drawing.Point(12, 12);
            this.backBut.Name = "backBut";
            this.backBut.Size = new System.Drawing.Size(34, 34);
            this.backBut.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.backBut.TabIndex = 20;
            this.backBut.TabStop = false;
            this.backBut.Click += new System.EventHandler(this.backBut_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Verdana", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label1.ForeColor = System.Drawing.Color.Black;
            this.label1.Location = new System.Drawing.Point(92, 46);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(220, 25);
            this.label1.TabIndex = 21;
            this.label1.Text = "Покупка билетов";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label2.ForeColor = System.Drawing.Color.Black;
            this.label2.Location = new System.Drawing.Point(24, 95);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(356, 18);
            this.label2.TabIndex = 22;
            this.label2.Text = "Выберете какой билет хотите преобрести";
            // 
            // airplanePic
            // 
            this.airplanePic.Cursor = System.Windows.Forms.Cursors.Hand;
            this.airplanePic.Image = ((System.Drawing.Image)(resources.GetObject("airplanePic.Image")));
            this.airplanePic.Location = new System.Drawing.Point(162, 136);
            this.airplanePic.Name = "airplanePic";
            this.airplanePic.Size = new System.Drawing.Size(80, 80);
            this.airplanePic.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.airplanePic.TabIndex = 23;
            this.airplanePic.TabStop = false;
            this.airplanePic.Click += new System.EventHandler(this.airplanePic_Click);
            // 
            // trainPic
            // 
            this.trainPic.Cursor = System.Windows.Forms.Cursors.Hand;
            this.trainPic.Image = ((System.Drawing.Image)(resources.GetObject("trainPic.Image")));
            this.trainPic.Location = new System.Drawing.Point(162, 234);
            this.trainPic.Name = "trainPic";
            this.trainPic.Size = new System.Drawing.Size(80, 80);
            this.trainPic.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.trainPic.TabIndex = 24;
            this.trainPic.TabStop = false;
            this.trainPic.Click += new System.EventHandler(this.trainPic_Click);
            // 
            // busPic
            // 
            this.busPic.Cursor = System.Windows.Forms.Cursors.Hand;
            this.busPic.Image = ((System.Drawing.Image)(resources.GetObject("busPic.Image")));
            this.busPic.Location = new System.Drawing.Point(162, 331);
            this.busPic.Name = "busPic";
            this.busPic.Size = new System.Drawing.Size(80, 80);
            this.busPic.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.busPic.TabIndex = 25;
            this.busPic.TabStop = false;
            this.busPic.Click += new System.EventHandler(this.busPic_Click);
            // 
            // cinemaPic
            // 
            this.cinemaPic.Cursor = System.Windows.Forms.Cursors.Hand;
            this.cinemaPic.Image = ((System.Drawing.Image)(resources.GetObject("cinemaPic.Image")));
            this.cinemaPic.Location = new System.Drawing.Point(162, 432);
            this.cinemaPic.Name = "cinemaPic";
            this.cinemaPic.Size = new System.Drawing.Size(80, 80);
            this.cinemaPic.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.cinemaPic.TabIndex = 26;
            this.cinemaPic.TabStop = false;
            this.cinemaPic.Click += new System.EventHandler(this.cinemaPic_Click);
            // 
            // airplane
            // 
            this.airplane.Controls.Add(this.issueAP);
            this.airplane.Controls.Add(this.issueBtnAP);
            this.airplane.Controls.Add(this.amountOfTicketsAP);
            this.airplane.Controls.Add(this.dtPanelAP);
            this.airplane.Controls.Add(this.label6);
            this.airplane.Controls.Add(this.label5);
            this.airplane.Controls.Add(this.departHourAI);
            this.airplane.Controls.Add(this.departMinAP);
            this.airplane.Controls.Add(this.label4);
            this.airplane.Controls.Add(this.departDateAP);
            this.airplane.Controls.Add(this.twoWays);
            this.airplane.Controls.Add(this.oneWay);
            this.airplane.Controls.Add(this.arrivalAP);
            this.airplane.Controls.Add(this.departAP);
            this.airplane.Controls.Add(this.pictureBox1);
            this.airplane.Controls.Add(this.label3);
            this.airplane.Controls.Add(this.apBack);
            this.airplane.Controls.Add(this.apExit);
            this.airplane.Dock = System.Windows.Forms.DockStyle.Fill;
            this.airplane.Location = new System.Drawing.Point(0, 0);
            this.airplane.Name = "airplane";
            this.airplane.Size = new System.Drawing.Size(404, 561);
            this.airplane.TabIndex = 27;
            // 
            // issueAP
            // 
            this.issueAP.Controls.Add(this.arrivalTimeAP);
            this.issueAP.Controls.Add(this.backToAP);
            this.issueAP.Controls.Add(this.placesAP);
            this.issueAP.Controls.Add(this.placeAP);
            this.issueAP.Controls.Add(this.arrivalPlaceAP);
            this.issueAP.Controls.Add(this.departTimeAP);
            this.issueAP.Controls.Add(this.departPlaceAP);
            this.issueAP.Controls.Add(this.apPay);
            this.issueAP.Controls.Add(this.amountAP);
            this.issueAP.Controls.Add(this.apIssueBack);
            this.issueAP.Controls.Add(this.apIssueExit);
            this.issueAP.Dock = System.Windows.Forms.DockStyle.Fill;
            this.issueAP.Location = new System.Drawing.Point(0, 0);
            this.issueAP.Name = "issueAP";
            this.issueAP.Size = new System.Drawing.Size(404, 561);
            this.issueAP.TabIndex = 94;
            // 
            // arrivalTimeAP
            // 
            this.arrivalTimeAP.AutoSize = true;
            this.arrivalTimeAP.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.arrivalTimeAP.Location = new System.Drawing.Point(163, 192);
            this.arrivalTimeAP.Name = "arrivalTimeAP";
            this.arrivalTimeAP.Size = new System.Drawing.Size(0, 18);
            this.arrivalTimeAP.TabIndex = 123;
            // 
            // backToAP
            // 
            this.backToAP.AutoSize = true;
            this.backToAP.Font = new System.Drawing.Font("Verdana", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.backToAP.Location = new System.Drawing.Point(158, 157);
            this.backToAP.Name = "backToAP";
            this.backToAP.Size = new System.Drawing.Size(102, 23);
            this.backToAP.TabIndex = 122;
            this.backToAP.Text = "Обратно";
            // 
            // placesAP
            // 
            this.placesAP.FormattingEnabled = true;
            this.placesAP.Location = new System.Drawing.Point(67, 268);
            this.placesAP.Name = "placesAP";
            this.placesAP.Size = new System.Drawing.Size(274, 147);
            this.placesAP.TabIndex = 121;
            // 
            // placeAP
            // 
            this.placeAP.AutoSize = true;
            this.placeAP.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.placeAP.Location = new System.Drawing.Point(171, 237);
            this.placeAP.Name = "placeAP";
            this.placeAP.Size = new System.Drawing.Size(62, 20);
            this.placeAP.TabIndex = 120;
            this.placeAP.Text = "Места";
            // 
            // arrivalPlaceAP
            // 
            this.arrivalPlaceAP.AutoSize = true;
            this.arrivalPlaceAP.Font = new System.Drawing.Font("Verdana", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.arrivalPlaceAP.Location = new System.Drawing.Point(128, 126);
            this.arrivalPlaceAP.Name = "arrivalPlaceAP";
            this.arrivalPlaceAP.Size = new System.Drawing.Size(74, 16);
            this.arrivalPlaceAP.TabIndex = 118;
            this.arrivalPlaceAP.Text = "Вылет в:";
            // 
            // departTimeAP
            // 
            this.departTimeAP.AutoSize = true;
            this.departTimeAP.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.departTimeAP.Location = new System.Drawing.Point(128, 92);
            this.departTimeAP.Name = "departTimeAP";
            this.departTimeAP.Size = new System.Drawing.Size(153, 18);
            this.departTimeAP.TabIndex = 117;
            this.departTimeAP.Text = "Время отправки";
            // 
            // departPlaceAP
            // 
            this.departPlaceAP.AutoSize = true;
            this.departPlaceAP.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.departPlaceAP.Location = new System.Drawing.Point(128, 55);
            this.departPlaceAP.Name = "departPlaceAP";
            this.departPlaceAP.Size = new System.Drawing.Size(126, 18);
            this.departPlaceAP.TabIndex = 116;
            this.departPlaceAP.Text = "Отправка из:";
            // 
            // apPay
            // 
            this.apPay.FlatAppearance.BorderSize = 0;
            this.apPay.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.apPay.Font = new System.Drawing.Font("Verdana", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.apPay.ForeColor = System.Drawing.Color.Red;
            this.apPay.Location = new System.Drawing.Point(106, 459);
            this.apPay.Name = "apPay";
            this.apPay.Size = new System.Drawing.Size(190, 72);
            this.apPay.TabIndex = 115;
            this.apPay.Text = "Оформить";
            this.apPay.UseVisualStyleBackColor = true;
            this.apPay.Click += new System.EventHandler(this.apPay_Click);
            // 
            // amountAP
            // 
            this.amountAP.AutoSize = true;
            this.amountAP.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.amountAP.Location = new System.Drawing.Point(111, 426);
            this.amountAP.Name = "amountAP";
            this.amountAP.Size = new System.Drawing.Size(193, 20);
            this.amountAP.TabIndex = 114;
            this.amountAP.Text = "Сумма к оплате: 5234";
            // 
            // apIssueBack
            // 
            this.apIssueBack.Cursor = System.Windows.Forms.Cursors.Hand;
            this.apIssueBack.Image = ((System.Drawing.Image)(resources.GetObject("apIssueBack.Image")));
            this.apIssueBack.Location = new System.Drawing.Point(11, 12);
            this.apIssueBack.Name = "apIssueBack";
            this.apIssueBack.Size = new System.Drawing.Size(34, 34);
            this.apIssueBack.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.apIssueBack.TabIndex = 113;
            this.apIssueBack.TabStop = false;
            this.apIssueBack.Click += new System.EventHandler(this.apIssueBack_Click);
            // 
            // apIssueExit
            // 
            this.apIssueExit.Cursor = System.Windows.Forms.Cursors.Hand;
            this.apIssueExit.Image = ((System.Drawing.Image)(resources.GetObject("apIssueExit.Image")));
            this.apIssueExit.Location = new System.Drawing.Point(357, 12);
            this.apIssueExit.Name = "apIssueExit";
            this.apIssueExit.Size = new System.Drawing.Size(34, 34);
            this.apIssueExit.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.apIssueExit.TabIndex = 112;
            this.apIssueExit.TabStop = false;
            this.apIssueExit.Click += new System.EventHandler(this.apIssueExit_Click);
            // 
            // issueBtnAP
            // 
            this.issueBtnAP.FlatAppearance.BorderSize = 0;
            this.issueBtnAP.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.issueBtnAP.Font = new System.Drawing.Font("Verdana", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.issueBtnAP.ForeColor = System.Drawing.Color.Red;
            this.issueBtnAP.Location = new System.Drawing.Point(107, 475);
            this.issueBtnAP.Name = "issueBtnAP";
            this.issueBtnAP.Size = new System.Drawing.Size(190, 72);
            this.issueBtnAP.TabIndex = 89;
            this.issueBtnAP.Text = "Оформить";
            this.issueBtnAP.UseVisualStyleBackColor = true;
            this.issueBtnAP.Click += new System.EventHandler(this.issueBtnAP_Click);
            // 
            // amountOfTicketsAP
            // 
            this.amountOfTicketsAP.Location = new System.Drawing.Point(168, 444);
            this.amountOfTicketsAP.Maximum = new decimal(new int[] {
            130,
            0,
            0,
            0});
            this.amountOfTicketsAP.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.amountOfTicketsAP.Name = "amountOfTicketsAP";
            this.amountOfTicketsAP.ReadOnly = true;
            this.amountOfTicketsAP.Size = new System.Drawing.Size(68, 20);
            this.amountOfTicketsAP.TabIndex = 93;
            this.amountOfTicketsAP.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // dtPanelAP
            // 
            this.dtPanelAP.Controls.Add(this.backAP);
            this.dtPanelAP.Controls.Add(this.arrivalDateAP);
            this.dtPanelAP.Controls.Add(this.arrivalHourAP);
            this.dtPanelAP.Controls.Add(this.ArrivalMinAP);
            this.dtPanelAP.Location = new System.Drawing.Point(248, 329);
            this.dtPanelAP.Name = "dtPanelAP";
            this.dtPanelAP.Size = new System.Drawing.Size(118, 82);
            this.dtPanelAP.TabIndex = 90;
            // 
            // backAP
            // 
            this.backAP.AutoSize = true;
            this.backAP.Font = new System.Drawing.Font("Verdana", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.backAP.Location = new System.Drawing.Point(31, 0);
            this.backAP.Name = "backAP";
            this.backAP.Size = new System.Drawing.Size(65, 16);
            this.backAP.TabIndex = 35;
            this.backAP.Text = "Обратно";
            // 
            // arrivalDateAP
            // 
            this.arrivalDateAP.CalendarFont = new System.Drawing.Font("Verdana", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.arrivalDateAP.Cursor = System.Windows.Forms.Cursors.Help;
            this.arrivalDateAP.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.arrivalDateAP.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.arrivalDateAP.Location = new System.Drawing.Point(15, 19);
            this.arrivalDateAP.MaxDate = new System.DateTime(2018, 12, 31, 0, 0, 0, 0);
            this.arrivalDateAP.Name = "arrivalDateAP";
            this.arrivalDateAP.Size = new System.Drawing.Size(91, 21);
            this.arrivalDateAP.TabIndex = 32;
            // 
            // arrivalHourAP
            // 
            this.arrivalHourAP.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.arrivalHourAP.Location = new System.Drawing.Point(15, 46);
            this.arrivalHourAP.Maximum = new decimal(new int[] {
            23,
            0,
            0,
            0});
            this.arrivalHourAP.Name = "arrivalHourAP";
            this.arrivalHourAP.ReadOnly = true;
            this.arrivalHourAP.Size = new System.Drawing.Size(44, 21);
            this.arrivalHourAP.TabIndex = 40;
            // 
            // ArrivalMinAP
            // 
            this.ArrivalMinAP.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.ArrivalMinAP.Location = new System.Drawing.Point(62, 46);
            this.ArrivalMinAP.Maximum = new decimal(new int[] {
            59,
            0,
            0,
            0});
            this.ArrivalMinAP.Name = "ArrivalMinAP";
            this.ArrivalMinAP.ReadOnly = true;
            this.ArrivalMinAP.Size = new System.Drawing.Size(44, 21);
            this.ArrivalMinAP.TabIndex = 39;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label6.Location = new System.Drawing.Point(94, 272);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(106, 18);
            this.label6.TabIndex = 88;
            this.label6.Text = "Прибытие В";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label5.Location = new System.Drawing.Point(94, 201);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(83, 18);
            this.label5.TabIndex = 87;
            this.label5.Text = "Вылет из";
            // 
            // departHourAI
            // 
            this.departHourAI.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.departHourAI.Location = new System.Drawing.Point(66, 375);
            this.departHourAI.Maximum = new decimal(new int[] {
            23,
            0,
            0,
            0});
            this.departHourAI.Name = "departHourAI";
            this.departHourAI.ReadOnly = true;
            this.departHourAI.Size = new System.Drawing.Size(44, 21);
            this.departHourAI.TabIndex = 38;
            // 
            // departMinAP
            // 
            this.departMinAP.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.departMinAP.Location = new System.Drawing.Point(115, 375);
            this.departMinAP.Maximum = new decimal(new int[] {
            59,
            0,
            0,
            0});
            this.departMinAP.Name = "departMinAP";
            this.departMinAP.ReadOnly = true;
            this.departMinAP.Size = new System.Drawing.Size(44, 21);
            this.departMinAP.TabIndex = 37;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Verdana", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label4.Location = new System.Drawing.Point(94, 329);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(41, 16);
            this.label4.TabIndex = 34;
            this.label4.Text = "Туда";
            // 
            // departDateAP
            // 
            this.departDateAP.CalendarTitleBackColor = System.Drawing.Color.Firebrick;
            this.departDateAP.Cursor = System.Windows.Forms.Cursors.Help;
            this.departDateAP.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.departDateAP.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.departDateAP.Location = new System.Drawing.Point(67, 348);
            this.departDateAP.MaxDate = new System.DateTime(2018, 12, 31, 0, 0, 0, 0);
            this.departDateAP.MinDate = new System.DateTime(2018, 6, 3, 0, 0, 0, 0);
            this.departDateAP.Name = "departDateAP";
            this.departDateAP.Size = new System.Drawing.Size(91, 21);
            this.departDateAP.TabIndex = 33;
            this.departDateAP.TabStop = false;
            this.departDateAP.Value = new System.DateTime(2018, 12, 31, 0, 0, 0, 0);
            // 
            // twoWays
            // 
            this.twoWays.AutoSize = true;
            this.twoWays.Checked = true;
            this.twoWays.CheckState = System.Windows.Forms.CheckState.Checked;
            this.twoWays.Font = new System.Drawing.Font("Verdana", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.twoWays.Location = new System.Drawing.Point(235, 293);
            this.twoWays.Name = "twoWays";
            this.twoWays.Size = new System.Drawing.Size(145, 22);
            this.twoWays.TabIndex = 31;
            this.twoWays.Text = "Туда и обратно";
            this.twoWays.UseVisualStyleBackColor = true;
            this.twoWays.CheckedChanged += new System.EventHandler(this.twoWays_CheckedChanged);
            // 
            // oneWay
            // 
            this.oneWay.AutoSize = true;
            this.oneWay.Font = new System.Drawing.Font("Verdana", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.oneWay.Location = new System.Drawing.Point(235, 223);
            this.oneWay.Name = "oneWay";
            this.oneWay.Size = new System.Drawing.Size(131, 22);
            this.oneWay.TabIndex = 30;
            this.oneWay.Text = "В один конец";
            this.oneWay.UseVisualStyleBackColor = true;
            this.oneWay.CheckedChanged += new System.EventHandler(this.oneWay_CheckedChanged);
            // 
            // arrivalAP
            // 
            this.arrivalAP.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.arrivalAP.FormattingEnabled = true;
            this.arrivalAP.Items.AddRange(new object[] {
            "Берлин, а-п. Берлин-Тэгель",
            "Париж, а-п. Шарль-де-Голь",
            "Нью-Йорк, а-п.  Джона Кеннеди",
            "Торонто, а-п. им. Лестера Пирсона",
            "Рим, а-п. Рим-Фьюмичино",
            "Барселона, а-п. Эль-Прат"});
            this.arrivalAP.Location = new System.Drawing.Point(53, 293);
            this.arrivalAP.Name = "arrivalAP";
            this.arrivalAP.Size = new System.Drawing.Size(164, 21);
            this.arrivalAP.TabIndex = 29;
            // 
            // departAP
            // 
            this.departAP.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.departAP.FormattingEnabled = true;
            this.departAP.Items.AddRange(new object[] {
            "Киев, Борисполь",
            "Киев, Жуляны",
            "Житомир, Смоковка",
            "Львов, Львов",
            "Одесса, Одесса",
            "Харьков, Основа"});
            this.departAP.Location = new System.Drawing.Point(53, 222);
            this.departAP.Name = "departAP";
            this.departAP.Size = new System.Drawing.Size(164, 21);
            this.departAP.TabIndex = 28;
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox1.Image")));
            this.pictureBox1.Location = new System.Drawing.Point(162, 50);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(80, 80);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox1.TabIndex = 27;
            this.pictureBox1.TabStop = false;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label3.ForeColor = System.Drawing.Color.Black;
            this.label3.Location = new System.Drawing.Point(62, 161);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(279, 18);
            this.label3.TabIndex = 26;
            this.label3.Text = "Выбирете нужный Вам авибилет";
            // 
            // apBack
            // 
            this.apBack.Cursor = System.Windows.Forms.Cursors.Hand;
            this.apBack.Image = ((System.Drawing.Image)(resources.GetObject("apBack.Image")));
            this.apBack.Location = new System.Drawing.Point(12, 12);
            this.apBack.Name = "apBack";
            this.apBack.Size = new System.Drawing.Size(34, 34);
            this.apBack.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.apBack.TabIndex = 24;
            this.apBack.TabStop = false;
            this.apBack.Click += new System.EventHandler(this.apBack_Click);
            // 
            // apExit
            // 
            this.apExit.Cursor = System.Windows.Forms.Cursors.Hand;
            this.apExit.Image = ((System.Drawing.Image)(resources.GetObject("apExit.Image")));
            this.apExit.Location = new System.Drawing.Point(358, 12);
            this.apExit.Name = "apExit";
            this.apExit.Size = new System.Drawing.Size(34, 34);
            this.apExit.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.apExit.TabIndex = 23;
            this.apExit.TabStop = false;
            this.apExit.Click += new System.EventHandler(this.apExit_Click);
            // 
            // trExit
            // 
            this.trExit.Cursor = System.Windows.Forms.Cursors.Hand;
            this.trExit.Image = ((System.Drawing.Image)(resources.GetObject("trExit.Image")));
            this.trExit.Location = new System.Drawing.Point(358, 12);
            this.trExit.Name = "trExit";
            this.trExit.Size = new System.Drawing.Size(34, 34);
            this.trExit.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.trExit.TabIndex = 107;
            this.trExit.TabStop = false;
            this.trExit.Click += new System.EventHandler(this.trExit_Click);
            // 
            // trBack
            // 
            this.trBack.Cursor = System.Windows.Forms.Cursors.Hand;
            this.trBack.Image = ((System.Drawing.Image)(resources.GetObject("trBack.Image")));
            this.trBack.Location = new System.Drawing.Point(12, 12);
            this.trBack.Name = "trBack";
            this.trBack.Size = new System.Drawing.Size(34, 34);
            this.trBack.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.trBack.TabIndex = 108;
            this.trBack.TabStop = false;
            this.trBack.Click += new System.EventHandler(this.trBack_Click);
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label11.ForeColor = System.Drawing.Color.Black;
            this.label11.Location = new System.Drawing.Point(50, 162);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(330, 18);
            this.label11.TabIndex = 109;
            this.label11.Text = "Выбирете нужный Вам билет на поезд";
            // 
            // pictureBox2
            // 
            this.pictureBox2.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox2.Image")));
            this.pictureBox2.Location = new System.Drawing.Point(162, 50);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new System.Drawing.Size(80, 80);
            this.pictureBox2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox2.TabIndex = 110;
            this.pictureBox2.TabStop = false;
            // 
            // departTR
            // 
            this.departTR.FormattingEnabled = true;
            this.departTR.Items.AddRange(new object[] {
            "Киев",
            "Житомир",
            "Одесса",
            "Львов",
            "Харьков",
            "Херсон",
            "Суммы",
            "Полтава",
            "Ровно",
            "Хмельницкий",
            "Запорожье",
            "Тернополь",
            "Черновцы",
            "Черкасы"});
            this.departTR.Location = new System.Drawing.Point(53, 222);
            this.departTR.Name = "departTR";
            this.departTR.Size = new System.Drawing.Size(164, 21);
            this.departTR.TabIndex = 111;
            // 
            // arrivalTR
            // 
            this.arrivalTR.FormattingEnabled = true;
            this.arrivalTR.Items.AddRange(new object[] {
            "Киев",
            "Житомир",
            "Одесса",
            "Львов",
            "Харьков",
            "Херсон",
            "Суммы",
            "Полтава",
            "Ровно",
            "Хмельницкий",
            "Запорожье",
            "Тернополь",
            "Черновцы",
            "Черкасы"});
            this.arrivalTR.Location = new System.Drawing.Point(53, 293);
            this.arrivalTR.Name = "arrivalTR";
            this.arrivalTR.Size = new System.Drawing.Size(164, 21);
            this.arrivalTR.TabIndex = 112;
            // 
            // oneWayTr
            // 
            this.oneWayTr.AutoSize = true;
            this.oneWayTr.Checked = true;
            this.oneWayTr.CheckState = System.Windows.Forms.CheckState.Checked;
            this.oneWayTr.Font = new System.Drawing.Font("Verdana", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.oneWayTr.Location = new System.Drawing.Point(235, 223);
            this.oneWayTr.Name = "oneWayTr";
            this.oneWayTr.Size = new System.Drawing.Size(131, 22);
            this.oneWayTr.TabIndex = 113;
            this.oneWayTr.Text = "В один конец";
            this.oneWayTr.UseVisualStyleBackColor = true;
            this.oneWayTr.CheckedChanged += new System.EventHandler(this.oneWayTr_CheckedChanged);
            // 
            // twoWaysTr
            // 
            this.twoWaysTr.AutoSize = true;
            this.twoWaysTr.Font = new System.Drawing.Font("Verdana", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.twoWaysTr.Location = new System.Drawing.Point(235, 293);
            this.twoWaysTr.Name = "twoWaysTr";
            this.twoWaysTr.Size = new System.Drawing.Size(145, 22);
            this.twoWaysTr.TabIndex = 114;
            this.twoWaysTr.Text = "Туда и обратно";
            this.twoWaysTr.UseVisualStyleBackColor = true;
            this.twoWaysTr.CheckedChanged += new System.EventHandler(this.twoWaysTr_CheckedChanged);
            // 
            // departDateTR
            // 
            this.departDateTR.CalendarTitleBackColor = System.Drawing.Color.Firebrick;
            this.departDateTR.Cursor = System.Windows.Forms.Cursors.Help;
            this.departDateTR.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.departDateTR.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.departDateTR.Location = new System.Drawing.Point(67, 348);
            this.departDateTR.MaxDate = new System.DateTime(2018, 12, 31, 0, 0, 0, 0);
            this.departDateTR.MinDate = new System.DateTime(2018, 6, 3, 0, 0, 0, 0);
            this.departDateTR.Name = "departDateTR";
            this.departDateTR.Size = new System.Drawing.Size(91, 21);
            this.departDateTR.TabIndex = 115;
            this.departDateTR.TabStop = false;
            this.departDateTR.Value = new System.DateTime(2018, 12, 31, 0, 0, 0, 0);
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Verdana", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label10.Location = new System.Drawing.Point(94, 329);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(41, 16);
            this.label10.TabIndex = 116;
            this.label10.Text = "Туда";
            // 
            // departTimeMinTR
            // 
            this.departTimeMinTR.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.departTimeMinTR.Location = new System.Drawing.Point(115, 375);
            this.departTimeMinTR.Maximum = new decimal(new int[] {
            59,
            0,
            0,
            0});
            this.departTimeMinTR.Name = "departTimeMinTR";
            this.departTimeMinTR.ReadOnly = true;
            this.departTimeMinTR.Size = new System.Drawing.Size(44, 21);
            this.departTimeMinTR.TabIndex = 117;
            // 
            // departTimeHourTR
            // 
            this.departTimeHourTR.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.departTimeHourTR.Location = new System.Drawing.Point(66, 375);
            this.departTimeHourTR.Maximum = new decimal(new int[] {
            23,
            0,
            0,
            0});
            this.departTimeHourTR.Name = "departTimeHourTR";
            this.departTimeHourTR.ReadOnly = true;
            this.departTimeHourTR.Size = new System.Drawing.Size(44, 21);
            this.departTimeHourTR.TabIndex = 118;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label9.Location = new System.Drawing.Point(94, 201);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(85, 18);
            this.label9.TabIndex = 119;
            this.label9.Text = "Выезд из";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label8.Location = new System.Drawing.Point(79, 272);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(112, 18);
            this.label8.TabIndex = 120;
            this.label8.Text = "Прибытие в:";
            // 
            // issueBtnTR
            // 
            this.issueBtnTR.FlatAppearance.BorderSize = 0;
            this.issueBtnTR.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.issueBtnTR.Font = new System.Drawing.Font("Verdana", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.issueBtnTR.ForeColor = System.Drawing.Color.Red;
            this.issueBtnTR.Location = new System.Drawing.Point(107, 462);
            this.issueBtnTR.Name = "issueBtnTR";
            this.issueBtnTR.Size = new System.Drawing.Size(190, 72);
            this.issueBtnTR.TabIndex = 121;
            this.issueBtnTR.Text = "Оформить";
            this.issueBtnTR.UseVisualStyleBackColor = true;
            this.issueBtnTR.Click += new System.EventHandler(this.issueBtnTR_Click);
            // 
            // dtTimeTR
            // 
            this.dtTimeTR.Controls.Add(this.label7);
            this.dtTimeTR.Controls.Add(this.arrivalDateTR);
            this.dtTimeTR.Controls.Add(this.arrivalTimeHourTR);
            this.dtTimeTR.Controls.Add(this.arrivalTimeMinTR);
            this.dtTimeTR.Location = new System.Drawing.Point(248, 329);
            this.dtTimeTR.Name = "dtTimeTR";
            this.dtTimeTR.Size = new System.Drawing.Size(118, 82);
            this.dtTimeTR.TabIndex = 122;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Verdana", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label7.Location = new System.Drawing.Point(31, 0);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(65, 16);
            this.label7.TabIndex = 35;
            this.label7.Text = "Обратно";
            // 
            // arrivalDateTR
            // 
            this.arrivalDateTR.CalendarFont = new System.Drawing.Font("Verdana", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.arrivalDateTR.Cursor = System.Windows.Forms.Cursors.Help;
            this.arrivalDateTR.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.arrivalDateTR.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.arrivalDateTR.Location = new System.Drawing.Point(15, 19);
            this.arrivalDateTR.MaxDate = new System.DateTime(2018, 12, 31, 0, 0, 0, 0);
            this.arrivalDateTR.Name = "arrivalDateTR";
            this.arrivalDateTR.Size = new System.Drawing.Size(91, 21);
            this.arrivalDateTR.TabIndex = 32;
            // 
            // arrivalTimeHourTR
            // 
            this.arrivalTimeHourTR.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.arrivalTimeHourTR.Location = new System.Drawing.Point(15, 46);
            this.arrivalTimeHourTR.Maximum = new decimal(new int[] {
            23,
            0,
            0,
            0});
            this.arrivalTimeHourTR.Name = "arrivalTimeHourTR";
            this.arrivalTimeHourTR.ReadOnly = true;
            this.arrivalTimeHourTR.Size = new System.Drawing.Size(44, 21);
            this.arrivalTimeHourTR.TabIndex = 40;
            // 
            // arrivalTimeMinTR
            // 
            this.arrivalTimeMinTR.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.arrivalTimeMinTR.Location = new System.Drawing.Point(62, 46);
            this.arrivalTimeMinTR.Maximum = new decimal(new int[] {
            59,
            0,
            0,
            0});
            this.arrivalTimeMinTR.Name = "arrivalTimeMinTR";
            this.arrivalTimeMinTR.ReadOnly = true;
            this.arrivalTimeMinTR.Size = new System.Drawing.Size(44, 21);
            this.arrivalTimeMinTR.TabIndex = 39;
            // 
            // amountOfTicketsTR
            // 
            this.amountOfTicketsTR.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.amountOfTicketsTR.Location = new System.Drawing.Point(175, 428);
            this.amountOfTicketsTR.Maximum = new decimal(new int[] {
            450,
            0,
            0,
            0});
            this.amountOfTicketsTR.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.amountOfTicketsTR.Name = "amountOfTicketsTR";
            this.amountOfTicketsTR.ReadOnly = true;
            this.amountOfTicketsTR.Size = new System.Drawing.Size(53, 21);
            this.amountOfTicketsTR.TabIndex = 123;
            this.amountOfTicketsTR.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // issueTR
            // 
            this.issueTR.Controls.Add(this.toPayTR);
            this.issueTR.Controls.Add(this.payTR);
            this.issueTR.Controls.Add(this.placesTR);
            this.issueTR.Controls.Add(this.label12);
            this.issueTR.Controls.Add(this.backToTR);
            this.issueTR.Controls.Add(this.arrivalDateLabelTR);
            this.issueTR.Controls.Add(this.arrivalToTR);
            this.issueTR.Controls.Add(this.departDateLabelTR);
            this.issueTR.Controls.Add(this.departFromTR);
            this.issueTR.Controls.Add(this.issueTRBack);
            this.issueTR.Controls.Add(this.issueTRExit);
            this.issueTR.Dock = System.Windows.Forms.DockStyle.Fill;
            this.issueTR.Location = new System.Drawing.Point(0, 0);
            this.issueTR.Name = "issueTR";
            this.issueTR.Size = new System.Drawing.Size(404, 561);
            this.issueTR.TabIndex = 124;
            // 
            // toPayTR
            // 
            this.toPayTR.AutoSize = true;
            this.toPayTR.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.toPayTR.Location = new System.Drawing.Point(128, 434);
            this.toPayTR.Name = "toPayTR";
            this.toPayTR.Size = new System.Drawing.Size(154, 18);
            this.toPayTR.TabIndex = 119;
            this.toPayTR.Text = "Сумма к оплате:";
            // 
            // payTR
            // 
            this.payTR.FlatAppearance.BorderSize = 0;
            this.payTR.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.payTR.Font = new System.Drawing.Font("Verdana", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.payTR.ForeColor = System.Drawing.Color.Red;
            this.payTR.Location = new System.Drawing.Point(114, 468);
            this.payTR.Name = "payTR";
            this.payTR.Size = new System.Drawing.Size(190, 72);
            this.payTR.TabIndex = 118;
            this.payTR.Text = "Оплатить";
            this.payTR.UseVisualStyleBackColor = true;
            this.payTR.Click += new System.EventHandler(this.payTR_Click);
            // 
            // placesTR
            // 
            this.placesTR.FormattingEnabled = true;
            this.placesTR.Location = new System.Drawing.Point(66, 272);
            this.placesTR.Name = "placesTR";
            this.placesTR.Size = new System.Drawing.Size(275, 147);
            this.placesTR.TabIndex = 117;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label12.Location = new System.Drawing.Point(167, 239);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(69, 18);
            this.label12.TabIndex = 116;
            this.label12.Text = "Места:";
            // 
            // backToTR
            // 
            this.backToTR.AutoSize = true;
            this.backToTR.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.backToTR.Location = new System.Drawing.Point(159, 162);
            this.backToTR.Name = "backToTR";
            this.backToTR.Size = new System.Drawing.Size(85, 18);
            this.backToTR.TabIndex = 115;
            this.backToTR.Text = "Обратно";
            // 
            // arrivalDateLabelTR
            // 
            this.arrivalDateLabelTR.AutoSize = true;
            this.arrivalDateLabelTR.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.arrivalDateLabelTR.Location = new System.Drawing.Point(146, 194);
            this.arrivalDateLabelTR.Name = "arrivalDateLabelTR";
            this.arrivalDateLabelTR.Size = new System.Drawing.Size(0, 18);
            this.arrivalDateLabelTR.TabIndex = 114;
            // 
            // arrivalToTR
            // 
            this.arrivalToTR.AutoSize = true;
            this.arrivalToTR.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.arrivalToTR.Location = new System.Drawing.Point(143, 130);
            this.arrivalToTR.Name = "arrivalToTR";
            this.arrivalToTR.Size = new System.Drawing.Size(130, 18);
            this.arrivalToTR.TabIndex = 113;
            this.arrivalToTR.Text = "Прибывает в:";
            // 
            // departDateLabelTR
            // 
            this.departDateLabelTR.AutoSize = true;
            this.departDateLabelTR.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.departDateLabelTR.Location = new System.Drawing.Point(146, 94);
            this.departDateLabelTR.Name = "departDateLabelTR";
            this.departDateLabelTR.Size = new System.Drawing.Size(153, 18);
            this.departDateLabelTR.TabIndex = 112;
            this.departDateLabelTR.Text = "Время отправки";
            // 
            // departFromTR
            // 
            this.departFromTR.AutoSize = true;
            this.departFromTR.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.departFromTR.Location = new System.Drawing.Point(146, 57);
            this.departFromTR.Name = "departFromTR";
            this.departFromTR.Size = new System.Drawing.Size(126, 18);
            this.departFromTR.TabIndex = 111;
            this.departFromTR.Text = "Отправка из:";
            // 
            // issueTRBack
            // 
            this.issueTRBack.Cursor = System.Windows.Forms.Cursors.Hand;
            this.issueTRBack.Image = ((System.Drawing.Image)(resources.GetObject("issueTRBack.Image")));
            this.issueTRBack.Location = new System.Drawing.Point(11, 12);
            this.issueTRBack.Name = "issueTRBack";
            this.issueTRBack.Size = new System.Drawing.Size(34, 34);
            this.issueTRBack.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.issueTRBack.TabIndex = 110;
            this.issueTRBack.TabStop = false;
            this.issueTRBack.Click += new System.EventHandler(this.issueTRBack_Click);
            // 
            // issueTRExit
            // 
            this.issueTRExit.Cursor = System.Windows.Forms.Cursors.Hand;
            this.issueTRExit.Image = ((System.Drawing.Image)(resources.GetObject("issueTRExit.Image")));
            this.issueTRExit.Location = new System.Drawing.Point(357, 12);
            this.issueTRExit.Name = "issueTRExit";
            this.issueTRExit.Size = new System.Drawing.Size(34, 34);
            this.issueTRExit.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.issueTRExit.TabIndex = 109;
            this.issueTRExit.TabStop = false;
            this.issueTRExit.Click += new System.EventHandler(this.issueTRExit_Click);
            // 
            // train
            // 
            this.train.Controls.Add(this.issueTR);
            this.train.Controls.Add(this.amountOfTicketsTR);
            this.train.Controls.Add(this.dtTimeTR);
            this.train.Controls.Add(this.issueBtnTR);
            this.train.Controls.Add(this.label8);
            this.train.Controls.Add(this.label9);
            this.train.Controls.Add(this.departTimeHourTR);
            this.train.Controls.Add(this.departTimeMinTR);
            this.train.Controls.Add(this.label10);
            this.train.Controls.Add(this.departDateTR);
            this.train.Controls.Add(this.twoWaysTr);
            this.train.Controls.Add(this.oneWayTr);
            this.train.Controls.Add(this.arrivalTR);
            this.train.Controls.Add(this.departTR);
            this.train.Controls.Add(this.pictureBox2);
            this.train.Controls.Add(this.label11);
            this.train.Controls.Add(this.trBack);
            this.train.Controls.Add(this.trExit);
            this.train.Dock = System.Windows.Forms.DockStyle.Fill;
            this.train.Location = new System.Drawing.Point(0, 0);
            this.train.Name = "train";
            this.train.Size = new System.Drawing.Size(404, 561);
            this.train.TabIndex = 124;
            // 
            // bus
            // 
            this.bus.Controls.Add(this.issueBS);
            this.bus.Controls.Add(this.amountOfTicketsBS);
            this.bus.Controls.Add(this.arrivalPanelBS);
            this.bus.Controls.Add(this.issueBtnBS);
            this.bus.Controls.Add(this.label14);
            this.bus.Controls.Add(this.label15);
            this.bus.Controls.Add(this.departTimeHourBS);
            this.bus.Controls.Add(this.departTimeMinBS);
            this.bus.Controls.Add(this.label16);
            this.bus.Controls.Add(this.departDateBS);
            this.bus.Controls.Add(this.twoWaysBS);
            this.bus.Controls.Add(this.oneWayBS);
            this.bus.Controls.Add(this.arrivalBS);
            this.bus.Controls.Add(this.departBS);
            this.bus.Controls.Add(this.pictureBox3);
            this.bus.Controls.Add(this.label17);
            this.bus.Controls.Add(this.backBS);
            this.bus.Controls.Add(this.exitBS);
            this.bus.Dock = System.Windows.Forms.DockStyle.Fill;
            this.bus.Location = new System.Drawing.Point(0, 0);
            this.bus.Name = "bus";
            this.bus.Size = new System.Drawing.Size(404, 561);
            this.bus.TabIndex = 124;
            // 
            // issueBS
            // 
            this.issueBS.Controls.Add(this.arrivalTimeLblBS);
            this.issueBS.Controls.Add(this.backToBS);
            this.issueBS.Controls.Add(this.placesBS);
            this.issueBS.Controls.Add(this.label20);
            this.issueBS.Controls.Add(this.arrivalToLblBS);
            this.issueBS.Controls.Add(this.departTimeLblBS);
            this.issueBS.Controls.Add(this.departFromBS);
            this.issueBS.Controls.Add(this.payBS);
            this.issueBS.Controls.Add(this.toPayBS);
            this.issueBS.Controls.Add(this.issueBSBack);
            this.issueBS.Controls.Add(this.issueBsExit);
            this.issueBS.Dock = System.Windows.Forms.DockStyle.Fill;
            this.issueBS.Location = new System.Drawing.Point(0, 0);
            this.issueBS.Name = "issueBS";
            this.issueBS.Size = new System.Drawing.Size(404, 561);
            this.issueBS.TabIndex = 123;
            // 
            // arrivalTimeLblBS
            // 
            this.arrivalTimeLblBS.AutoSize = true;
            this.arrivalTimeLblBS.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.arrivalTimeLblBS.Location = new System.Drawing.Point(164, 192);
            this.arrivalTimeLblBS.Name = "arrivalTimeLblBS";
            this.arrivalTimeLblBS.Size = new System.Drawing.Size(0, 18);
            this.arrivalTimeLblBS.TabIndex = 145;
            // 
            // backToBS
            // 
            this.backToBS.AutoSize = true;
            this.backToBS.Font = new System.Drawing.Font("Verdana", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.backToBS.Location = new System.Drawing.Point(159, 157);
            this.backToBS.Name = "backToBS";
            this.backToBS.Size = new System.Drawing.Size(102, 23);
            this.backToBS.TabIndex = 144;
            this.backToBS.Text = "Обратно";
            // 
            // placesBS
            // 
            this.placesBS.FormattingEnabled = true;
            this.placesBS.Location = new System.Drawing.Point(68, 268);
            this.placesBS.Name = "placesBS";
            this.placesBS.Size = new System.Drawing.Size(274, 147);
            this.placesBS.TabIndex = 143;
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label20.Location = new System.Drawing.Point(172, 237);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(62, 20);
            this.label20.TabIndex = 142;
            this.label20.Text = "Места";
            // 
            // arrivalToLblBS
            // 
            this.arrivalToLblBS.AutoSize = true;
            this.arrivalToLblBS.Font = new System.Drawing.Font("Verdana", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.arrivalToLblBS.Location = new System.Drawing.Point(129, 126);
            this.arrivalToLblBS.Name = "arrivalToLblBS";
            this.arrivalToLblBS.Size = new System.Drawing.Size(74, 16);
            this.arrivalToLblBS.TabIndex = 141;
            this.arrivalToLblBS.Text = "Вылет в:";
            // 
            // departTimeLblBS
            // 
            this.departTimeLblBS.AutoSize = true;
            this.departTimeLblBS.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.departTimeLblBS.Location = new System.Drawing.Point(129, 92);
            this.departTimeLblBS.Name = "departTimeLblBS";
            this.departTimeLblBS.Size = new System.Drawing.Size(153, 18);
            this.departTimeLblBS.TabIndex = 140;
            this.departTimeLblBS.Text = "Время отправки";
            // 
            // departFromBS
            // 
            this.departFromBS.AutoSize = true;
            this.departFromBS.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.departFromBS.Location = new System.Drawing.Point(129, 55);
            this.departFromBS.Name = "departFromBS";
            this.departFromBS.Size = new System.Drawing.Size(126, 18);
            this.departFromBS.TabIndex = 139;
            this.departFromBS.Text = "Отправка из:";
            // 
            // payBS
            // 
            this.payBS.FlatAppearance.BorderSize = 0;
            this.payBS.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.payBS.Font = new System.Drawing.Font("Verdana", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.payBS.ForeColor = System.Drawing.Color.Red;
            this.payBS.Location = new System.Drawing.Point(107, 459);
            this.payBS.Name = "payBS";
            this.payBS.Size = new System.Drawing.Size(190, 72);
            this.payBS.TabIndex = 138;
            this.payBS.Text = "Оформить";
            this.payBS.UseVisualStyleBackColor = true;
            this.payBS.Click += new System.EventHandler(this.payBS_Click);
            // 
            // toPayBS
            // 
            this.toPayBS.AutoSize = true;
            this.toPayBS.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.toPayBS.Location = new System.Drawing.Point(112, 426);
            this.toPayBS.Name = "toPayBS";
            this.toPayBS.Size = new System.Drawing.Size(193, 20);
            this.toPayBS.TabIndex = 137;
            this.toPayBS.Text = "Сумма к оплате: 5234";
            // 
            // issueBSBack
            // 
            this.issueBSBack.Cursor = System.Windows.Forms.Cursors.Hand;
            this.issueBSBack.Image = ((System.Drawing.Image)(resources.GetObject("issueBSBack.Image")));
            this.issueBSBack.Location = new System.Drawing.Point(12, 12);
            this.issueBSBack.Name = "issueBSBack";
            this.issueBSBack.Size = new System.Drawing.Size(34, 34);
            this.issueBSBack.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.issueBSBack.TabIndex = 136;
            this.issueBSBack.TabStop = false;
            this.issueBSBack.Click += new System.EventHandler(this.issueBSBack_Click);
            // 
            // issueBsExit
            // 
            this.issueBsExit.Cursor = System.Windows.Forms.Cursors.Hand;
            this.issueBsExit.Image = ((System.Drawing.Image)(resources.GetObject("issueBsExit.Image")));
            this.issueBsExit.Location = new System.Drawing.Point(358, 12);
            this.issueBsExit.Name = "issueBsExit";
            this.issueBsExit.Size = new System.Drawing.Size(34, 34);
            this.issueBsExit.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.issueBsExit.TabIndex = 135;
            this.issueBsExit.TabStop = false;
            this.issueBsExit.Click += new System.EventHandler(this.issueBsExit_Click);
            // 
            // amountOfTicketsBS
            // 
            this.amountOfTicketsBS.Location = new System.Drawing.Point(175, 444);
            this.amountOfTicketsBS.Maximum = new decimal(new int[] {
            130,
            0,
            0,
            0});
            this.amountOfTicketsBS.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.amountOfTicketsBS.Name = "amountOfTicketsBS";
            this.amountOfTicketsBS.ReadOnly = true;
            this.amountOfTicketsBS.Size = new System.Drawing.Size(51, 20);
            this.amountOfTicketsBS.TabIndex = 124;
            this.amountOfTicketsBS.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // arrivalPanelBS
            // 
            this.arrivalPanelBS.Controls.Add(this.label13);
            this.arrivalPanelBS.Controls.Add(this.arrivalDateBS);
            this.arrivalPanelBS.Controls.Add(this.arrivalTimeHourBS);
            this.arrivalPanelBS.Controls.Add(this.arrivalTimeMinBS);
            this.arrivalPanelBS.Location = new System.Drawing.Point(248, 329);
            this.arrivalPanelBS.Name = "arrivalPanelBS";
            this.arrivalPanelBS.Size = new System.Drawing.Size(118, 82);
            this.arrivalPanelBS.TabIndex = 122;
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Font = new System.Drawing.Font("Verdana", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label13.Location = new System.Drawing.Point(31, 0);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(65, 16);
            this.label13.TabIndex = 35;
            this.label13.Text = "Обратно";
            // 
            // arrivalDateBS
            // 
            this.arrivalDateBS.CalendarFont = new System.Drawing.Font("Verdana", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.arrivalDateBS.Cursor = System.Windows.Forms.Cursors.Help;
            this.arrivalDateBS.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.arrivalDateBS.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.arrivalDateBS.Location = new System.Drawing.Point(15, 19);
            this.arrivalDateBS.MaxDate = new System.DateTime(2018, 12, 31, 0, 0, 0, 0);
            this.arrivalDateBS.Name = "arrivalDateBS";
            this.arrivalDateBS.Size = new System.Drawing.Size(91, 21);
            this.arrivalDateBS.TabIndex = 32;
            // 
            // arrivalTimeHourBS
            // 
            this.arrivalTimeHourBS.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.arrivalTimeHourBS.Location = new System.Drawing.Point(15, 46);
            this.arrivalTimeHourBS.Maximum = new decimal(new int[] {
            23,
            0,
            0,
            0});
            this.arrivalTimeHourBS.Name = "arrivalTimeHourBS";
            this.arrivalTimeHourBS.ReadOnly = true;
            this.arrivalTimeHourBS.Size = new System.Drawing.Size(44, 21);
            this.arrivalTimeHourBS.TabIndex = 40;
            // 
            // arrivalTimeMinBS
            // 
            this.arrivalTimeMinBS.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.arrivalTimeMinBS.Location = new System.Drawing.Point(62, 46);
            this.arrivalTimeMinBS.Maximum = new decimal(new int[] {
            59,
            0,
            0,
            0});
            this.arrivalTimeMinBS.Name = "arrivalTimeMinBS";
            this.arrivalTimeMinBS.ReadOnly = true;
            this.arrivalTimeMinBS.Size = new System.Drawing.Size(44, 21);
            this.arrivalTimeMinBS.TabIndex = 39;
            // 
            // issueBtnBS
            // 
            this.issueBtnBS.FlatAppearance.BorderSize = 0;
            this.issueBtnBS.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.issueBtnBS.Font = new System.Drawing.Font("Verdana", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.issueBtnBS.ForeColor = System.Drawing.Color.Red;
            this.issueBtnBS.Location = new System.Drawing.Point(107, 466);
            this.issueBtnBS.Name = "issueBtnBS";
            this.issueBtnBS.Size = new System.Drawing.Size(190, 72);
            this.issueBtnBS.TabIndex = 121;
            this.issueBtnBS.Text = "Оформить";
            this.issueBtnBS.UseVisualStyleBackColor = true;
            this.issueBtnBS.Click += new System.EventHandler(this.issueBtnBS_Click);
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label14.Location = new System.Drawing.Point(94, 272);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(85, 18);
            this.label14.TabIndex = 120;
            this.label14.Text = "Приезд в";
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label15.Location = new System.Drawing.Point(94, 201);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(85, 18);
            this.label15.TabIndex = 119;
            this.label15.Text = "Выезд из";
            // 
            // departTimeHourBS
            // 
            this.departTimeHourBS.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.departTimeHourBS.Location = new System.Drawing.Point(66, 375);
            this.departTimeHourBS.Maximum = new decimal(new int[] {
            23,
            0,
            0,
            0});
            this.departTimeHourBS.Name = "departTimeHourBS";
            this.departTimeHourBS.ReadOnly = true;
            this.departTimeHourBS.Size = new System.Drawing.Size(44, 21);
            this.departTimeHourBS.TabIndex = 118;
            // 
            // departTimeMinBS
            // 
            this.departTimeMinBS.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.departTimeMinBS.Location = new System.Drawing.Point(115, 375);
            this.departTimeMinBS.Maximum = new decimal(new int[] {
            59,
            0,
            0,
            0});
            this.departTimeMinBS.Name = "departTimeMinBS";
            this.departTimeMinBS.ReadOnly = true;
            this.departTimeMinBS.Size = new System.Drawing.Size(44, 21);
            this.departTimeMinBS.TabIndex = 117;
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Font = new System.Drawing.Font("Verdana", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label16.Location = new System.Drawing.Point(94, 329);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(41, 16);
            this.label16.TabIndex = 116;
            this.label16.Text = "Туда";
            // 
            // departDateBS
            // 
            this.departDateBS.CalendarTitleBackColor = System.Drawing.Color.Firebrick;
            this.departDateBS.Cursor = System.Windows.Forms.Cursors.Help;
            this.departDateBS.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.departDateBS.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.departDateBS.Location = new System.Drawing.Point(67, 348);
            this.departDateBS.MaxDate = new System.DateTime(2018, 12, 31, 0, 0, 0, 0);
            this.departDateBS.MinDate = new System.DateTime(2018, 6, 3, 0, 0, 0, 0);
            this.departDateBS.Name = "departDateBS";
            this.departDateBS.Size = new System.Drawing.Size(91, 21);
            this.departDateBS.TabIndex = 115;
            this.departDateBS.TabStop = false;
            this.departDateBS.Value = new System.DateTime(2018, 12, 31, 0, 0, 0, 0);
            // 
            // twoWaysBS
            // 
            this.twoWaysBS.AutoSize = true;
            this.twoWaysBS.Font = new System.Drawing.Font("Verdana", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.twoWaysBS.Location = new System.Drawing.Point(235, 293);
            this.twoWaysBS.Name = "twoWaysBS";
            this.twoWaysBS.Size = new System.Drawing.Size(145, 22);
            this.twoWaysBS.TabIndex = 114;
            this.twoWaysBS.Text = "Туда и обратно";
            this.twoWaysBS.UseVisualStyleBackColor = true;
            this.twoWaysBS.CheckedChanged += new System.EventHandler(this.twoWaysBS_CheckedChanged);
            // 
            // oneWayBS
            // 
            this.oneWayBS.AutoSize = true;
            this.oneWayBS.Checked = true;
            this.oneWayBS.CheckState = System.Windows.Forms.CheckState.Checked;
            this.oneWayBS.Font = new System.Drawing.Font("Verdana", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.oneWayBS.Location = new System.Drawing.Point(235, 223);
            this.oneWayBS.Name = "oneWayBS";
            this.oneWayBS.Size = new System.Drawing.Size(131, 22);
            this.oneWayBS.TabIndex = 113;
            this.oneWayBS.Text = "В один конец";
            this.oneWayBS.UseVisualStyleBackColor = true;
            this.oneWayBS.CheckedChanged += new System.EventHandler(this.oneWayBS_CheckedChanged);
            // 
            // arrivalBS
            // 
            this.arrivalBS.FormattingEnabled = true;
            this.arrivalBS.Items.AddRange(new object[] {
            "Киев",
            "Житомир",
            "Одесса",
            "Львов",
            "Харьков",
            "Херсон",
            "Суммы",
            "Полтава",
            "Ровно",
            "Хмельницкий",
            "Запорожье",
            "Тернополь",
            "Черновцы",
            "Черкасы"});
            this.arrivalBS.Location = new System.Drawing.Point(53, 293);
            this.arrivalBS.Name = "arrivalBS";
            this.arrivalBS.Size = new System.Drawing.Size(164, 21);
            this.arrivalBS.TabIndex = 112;
            // 
            // departBS
            // 
            this.departBS.FormattingEnabled = true;
            this.departBS.Items.AddRange(new object[] {
            "Киев",
            "Житомир",
            "Одесса",
            "Львов",
            "Харьков",
            "Херсон",
            "Суммы",
            "Полтава",
            "Ровно",
            "Хмельницкий",
            "Запорожье",
            "Тернополь",
            "Черновцы",
            "Черкасы"});
            this.departBS.Location = new System.Drawing.Point(53, 222);
            this.departBS.Name = "departBS";
            this.departBS.Size = new System.Drawing.Size(164, 21);
            this.departBS.TabIndex = 111;
            // 
            // pictureBox3
            // 
            this.pictureBox3.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox3.Image")));
            this.pictureBox3.Location = new System.Drawing.Point(162, 50);
            this.pictureBox3.Name = "pictureBox3";
            this.pictureBox3.Size = new System.Drawing.Size(80, 80);
            this.pictureBox3.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox3.TabIndex = 110;
            this.pictureBox3.TabStop = false;
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label17.ForeColor = System.Drawing.Color.Black;
            this.label17.Location = new System.Drawing.Point(81, 161);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(241, 18);
            this.label17.TabIndex = 109;
            this.label17.Text = "Выбирете нужный Вам рейс";
            // 
            // backBS
            // 
            this.backBS.Cursor = System.Windows.Forms.Cursors.Hand;
            this.backBS.Image = ((System.Drawing.Image)(resources.GetObject("backBS.Image")));
            this.backBS.Location = new System.Drawing.Point(12, 12);
            this.backBS.Name = "backBS";
            this.backBS.Size = new System.Drawing.Size(34, 34);
            this.backBS.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.backBS.TabIndex = 108;
            this.backBS.TabStop = false;
            this.backBS.Click += new System.EventHandler(this.backBS_Click);
            // 
            // exitBS
            // 
            this.exitBS.Cursor = System.Windows.Forms.Cursors.Hand;
            this.exitBS.Image = ((System.Drawing.Image)(resources.GetObject("exitBS.Image")));
            this.exitBS.Location = new System.Drawing.Point(358, 12);
            this.exitBS.Name = "exitBS";
            this.exitBS.Size = new System.Drawing.Size(34, 34);
            this.exitBS.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.exitBS.TabIndex = 107;
            this.exitBS.TabStop = false;
            this.exitBS.Click += new System.EventHandler(this.exitBS_Click);
            // 
            // cinema
            // 
            this.cinema.Controls.Add(this.issueCinema);
            this.cinema.Controls.Add(this.cinemaMin);
            this.cinema.Controls.Add(this.cinemaHour);
            this.cinema.Controls.Add(this.issueMovie);
            this.cinema.Controls.Add(this.amountOfTickets);
            this.cinema.Controls.Add(this.moviesSchedule);
            this.cinema.Controls.Add(this.movies);
            this.cinema.Controls.Add(this.pictureBox4);
            this.cinema.Controls.Add(this.label23);
            this.cinema.Controls.Add(this.cinemaBack);
            this.cinema.Controls.Add(this.cinemaExit);
            this.cinema.Dock = System.Windows.Forms.DockStyle.Fill;
            this.cinema.Location = new System.Drawing.Point(0, 0);
            this.cinema.Name = "cinema";
            this.cinema.Size = new System.Drawing.Size(404, 561);
            this.cinema.TabIndex = 146;
            // 
            // issueCinema
            // 
            this.issueCinema.Controls.Add(this.cinemaTickets);
            this.issueCinema.Controls.Add(this.label21);
            this.issueCinema.Controls.Add(this.movieDate);
            this.issueCinema.Controls.Add(this.movieTitle);
            this.issueCinema.Controls.Add(this.payCinema);
            this.issueCinema.Controls.Add(this.toPayCinema);
            this.issueCinema.Controls.Add(this.cinemaIssueBack);
            this.issueCinema.Controls.Add(this.cinemaIssueExit);
            this.issueCinema.Dock = System.Windows.Forms.DockStyle.Fill;
            this.issueCinema.Location = new System.Drawing.Point(0, 0);
            this.issueCinema.Name = "issueCinema";
            this.issueCinema.Size = new System.Drawing.Size(404, 561);
            this.issueCinema.TabIndex = 124;
            // 
            // cinemaTickets
            // 
            this.cinemaTickets.FormattingEnabled = true;
            this.cinemaTickets.Location = new System.Drawing.Point(67, 162);
            this.cinemaTickets.Name = "cinemaTickets";
            this.cinemaTickets.Size = new System.Drawing.Size(274, 147);
            this.cinemaTickets.TabIndex = 165;
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label21.Location = new System.Drawing.Point(171, 131);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(62, 20);
            this.label21.TabIndex = 164;
            this.label21.Text = "Места";
            // 
            // movieDate
            // 
            this.movieDate.AutoSize = true;
            this.movieDate.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.movieDate.Location = new System.Drawing.Point(128, 92);
            this.movieDate.Name = "movieDate";
            this.movieDate.Size = new System.Drawing.Size(140, 18);
            this.movieDate.TabIndex = 162;
            this.movieDate.Text = "Время фильма";
            // 
            // movieTitle
            // 
            this.movieTitle.AutoSize = true;
            this.movieTitle.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.movieTitle.Location = new System.Drawing.Point(128, 55);
            this.movieTitle.Name = "movieTitle";
            this.movieTitle.Size = new System.Drawing.Size(65, 18);
            this.movieTitle.TabIndex = 161;
            this.movieTitle.Text = "Фильм";
            // 
            // payCinema
            // 
            this.payCinema.FlatAppearance.BorderSize = 0;
            this.payCinema.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.payCinema.Font = new System.Drawing.Font("Verdana", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.payCinema.ForeColor = System.Drawing.Color.Red;
            this.payCinema.Location = new System.Drawing.Point(106, 459);
            this.payCinema.Name = "payCinema";
            this.payCinema.Size = new System.Drawing.Size(190, 72);
            this.payCinema.TabIndex = 160;
            this.payCinema.Text = "Оформить";
            this.payCinema.UseVisualStyleBackColor = true;
            this.payCinema.Click += new System.EventHandler(this.payCinema_Click);
            // 
            // toPayCinema
            // 
            this.toPayCinema.AutoSize = true;
            this.toPayCinema.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.toPayCinema.Location = new System.Drawing.Point(111, 320);
            this.toPayCinema.Name = "toPayCinema";
            this.toPayCinema.Size = new System.Drawing.Size(193, 20);
            this.toPayCinema.TabIndex = 159;
            this.toPayCinema.Text = "Сумма к оплате: 5234";
            // 
            // cinemaIssueBack
            // 
            this.cinemaIssueBack.Cursor = System.Windows.Forms.Cursors.Hand;
            this.cinemaIssueBack.Image = ((System.Drawing.Image)(resources.GetObject("cinemaIssueBack.Image")));
            this.cinemaIssueBack.Location = new System.Drawing.Point(11, 12);
            this.cinemaIssueBack.Name = "cinemaIssueBack";
            this.cinemaIssueBack.Size = new System.Drawing.Size(34, 34);
            this.cinemaIssueBack.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.cinemaIssueBack.TabIndex = 158;
            this.cinemaIssueBack.TabStop = false;
            this.cinemaIssueBack.Click += new System.EventHandler(this.cinemaIssueBack_Click);
            // 
            // cinemaIssueExit
            // 
            this.cinemaIssueExit.Cursor = System.Windows.Forms.Cursors.Hand;
            this.cinemaIssueExit.Image = ((System.Drawing.Image)(resources.GetObject("cinemaIssueExit.Image")));
            this.cinemaIssueExit.Location = new System.Drawing.Point(357, 12);
            this.cinemaIssueExit.Name = "cinemaIssueExit";
            this.cinemaIssueExit.Size = new System.Drawing.Size(34, 34);
            this.cinemaIssueExit.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.cinemaIssueExit.TabIndex = 157;
            this.cinemaIssueExit.TabStop = false;
            this.cinemaIssueExit.Click += new System.EventHandler(this.cinemaIssueExit_Click);
            // 
            // cinemaMin
            // 
            this.cinemaMin.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.cinemaMin.Location = new System.Drawing.Point(212, 272);
            this.cinemaMin.Maximum = new decimal(new int[] {
            59,
            0,
            0,
            0});
            this.cinemaMin.Name = "cinemaMin";
            this.cinemaMin.ReadOnly = true;
            this.cinemaMin.Size = new System.Drawing.Size(49, 21);
            this.cinemaMin.TabIndex = 123;
            // 
            // cinemaHour
            // 
            this.cinemaHour.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.cinemaHour.Location = new System.Drawing.Point(142, 272);
            this.cinemaHour.Maximum = new decimal(new int[] {
            23,
            0,
            0,
            0});
            this.cinemaHour.Name = "cinemaHour";
            this.cinemaHour.ReadOnly = true;
            this.cinemaHour.Size = new System.Drawing.Size(49, 21);
            this.cinemaHour.TabIndex = 122;
            // 
            // issueMovie
            // 
            this.issueMovie.FlatAppearance.BorderSize = 0;
            this.issueMovie.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.issueMovie.Font = new System.Drawing.Font("Verdana", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.issueMovie.ForeColor = System.Drawing.Color.Red;
            this.issueMovie.Location = new System.Drawing.Point(107, 432);
            this.issueMovie.Name = "issueMovie";
            this.issueMovie.Size = new System.Drawing.Size(190, 72);
            this.issueMovie.TabIndex = 121;
            this.issueMovie.Text = "Оформить";
            this.issueMovie.UseVisualStyleBackColor = true;
            this.issueMovie.Click += new System.EventHandler(this.issueMovie_Click);
            // 
            // amountOfTickets
            // 
            this.amountOfTickets.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.amountOfTickets.Location = new System.Drawing.Point(165, 331);
            this.amountOfTickets.Maximum = new decimal(new int[] {
            50,
            0,
            0,
            0});
            this.amountOfTickets.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.amountOfTickets.Name = "amountOfTickets";
            this.amountOfTickets.ReadOnly = true;
            this.amountOfTickets.Size = new System.Drawing.Size(69, 21);
            this.amountOfTickets.TabIndex = 118;
            this.amountOfTickets.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // moviesSchedule
            // 
            this.moviesSchedule.CalendarTitleBackColor = System.Drawing.Color.Firebrick;
            this.moviesSchedule.Cursor = System.Windows.Forms.Cursors.Help;
            this.moviesSchedule.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.moviesSchedule.Location = new System.Drawing.Point(133, 245);
            this.moviesSchedule.MaxDate = new System.DateTime(2018, 12, 31, 0, 0, 0, 0);
            this.moviesSchedule.MinDate = new System.DateTime(2018, 6, 3, 0, 0, 0, 0);
            this.moviesSchedule.Name = "moviesSchedule";
            this.moviesSchedule.Size = new System.Drawing.Size(138, 21);
            this.moviesSchedule.TabIndex = 115;
            this.moviesSchedule.TabStop = false;
            this.moviesSchedule.Value = new System.DateTime(2018, 12, 31, 0, 0, 0, 0);
            // 
            // movies
            // 
            this.movies.FormattingEnabled = true;
            this.movies.Items.AddRange(new object[] {
            "Мстители: Война бесконечности",
            "Джон Уик 2",
            "Человек-Муравей",
            "Агент 007: Казино Рояль",
            "Волк с Уолстрит",
            "Криминальное Чтиво",
            "Маска",
            "Драйв",
            "Пираты карибского моря 3",
            "Гарри Поттер 4",
            "Шоу Трумана"});
            this.movies.Location = new System.Drawing.Point(73, 200);
            this.movies.Name = "movies";
            this.movies.Size = new System.Drawing.Size(257, 21);
            this.movies.TabIndex = 111;
            // 
            // pictureBox4
            // 
            this.pictureBox4.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox4.Image")));
            this.pictureBox4.Location = new System.Drawing.Point(162, 50);
            this.pictureBox4.Name = "pictureBox4";
            this.pictureBox4.Size = new System.Drawing.Size(80, 80);
            this.pictureBox4.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox4.TabIndex = 110;
            this.pictureBox4.TabStop = false;
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label23.ForeColor = System.Drawing.Color.Black;
            this.label23.Location = new System.Drawing.Point(62, 161);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(290, 18);
            this.label23.TabIndex = 109;
            this.label23.Text = "Выбирете удобный для Вас билет";
            // 
            // cinemaBack
            // 
            this.cinemaBack.Cursor = System.Windows.Forms.Cursors.Hand;
            this.cinemaBack.Image = ((System.Drawing.Image)(resources.GetObject("cinemaBack.Image")));
            this.cinemaBack.Location = new System.Drawing.Point(12, 12);
            this.cinemaBack.Name = "cinemaBack";
            this.cinemaBack.Size = new System.Drawing.Size(34, 34);
            this.cinemaBack.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.cinemaBack.TabIndex = 108;
            this.cinemaBack.TabStop = false;
            this.cinemaBack.Click += new System.EventHandler(this.cinemaBack_Click);
            // 
            // cinemaExit
            // 
            this.cinemaExit.Cursor = System.Windows.Forms.Cursors.Hand;
            this.cinemaExit.Image = ((System.Drawing.Image)(resources.GetObject("cinemaExit.Image")));
            this.cinemaExit.Location = new System.Drawing.Point(358, 12);
            this.cinemaExit.Name = "cinemaExit";
            this.cinemaExit.Size = new System.Drawing.Size(34, 34);
            this.cinemaExit.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.cinemaExit.TabIndex = 107;
            this.cinemaExit.TabStop = false;
            this.cinemaExit.Click += new System.EventHandler(this.cinemaExit_Click);
            // 
            // Tickets
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(404, 561);
            this.Controls.Add(this.cinema);
            this.Controls.Add(this.bus);
            this.Controls.Add(this.airplane);
            this.Controls.Add(this.train);
            this.Controls.Add(this.cinemaPic);
            this.Controls.Add(this.busPic);
            this.Controls.Add(this.trainPic);
            this.Controls.Add(this.airplanePic);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.backBut);
            this.Controls.Add(this.exitBut);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "Tickets";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Maze Tickets";
            this.Load += new System.EventHandler(this.Tickets_Load);
            ((System.ComponentModel.ISupportInitialize)(this.exitBut)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.backBut)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.airplanePic)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.trainPic)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.busPic)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cinemaPic)).EndInit();
            this.airplane.ResumeLayout(false);
            this.airplane.PerformLayout();
            this.issueAP.ResumeLayout(false);
            this.issueAP.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.apIssueBack)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.apIssueExit)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.amountOfTicketsAP)).EndInit();
            this.dtPanelAP.ResumeLayout(false);
            this.dtPanelAP.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.arrivalHourAP)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ArrivalMinAP)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.departHourAI)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.departMinAP)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.apBack)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.apExit)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.trExit)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.trBack)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.departTimeMinTR)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.departTimeHourTR)).EndInit();
            this.dtTimeTR.ResumeLayout(false);
            this.dtTimeTR.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.arrivalTimeHourTR)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.arrivalTimeMinTR)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.amountOfTicketsTR)).EndInit();
            this.issueTR.ResumeLayout(false);
            this.issueTR.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.issueTRBack)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.issueTRExit)).EndInit();
            this.train.ResumeLayout(false);
            this.train.PerformLayout();
            this.bus.ResumeLayout(false);
            this.bus.PerformLayout();
            this.issueBS.ResumeLayout(false);
            this.issueBS.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.issueBSBack)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.issueBsExit)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.amountOfTicketsBS)).EndInit();
            this.arrivalPanelBS.ResumeLayout(false);
            this.arrivalPanelBS.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.arrivalTimeHourBS)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.arrivalTimeMinBS)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.departTimeHourBS)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.departTimeMinBS)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.backBS)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.exitBS)).EndInit();
            this.cinema.ResumeLayout(false);
            this.cinema.PerformLayout();
            this.issueCinema.ResumeLayout(false);
            this.issueCinema.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cinemaIssueBack)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cinemaIssueExit)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cinemaMin)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cinemaHour)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.amountOfTickets)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cinemaBack)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cinemaExit)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.PictureBox exitBut;
        private System.Windows.Forms.PictureBox backBut;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.PictureBox airplanePic;
        private System.Windows.Forms.PictureBox trainPic;
        private System.Windows.Forms.PictureBox busPic;
        private System.Windows.Forms.PictureBox cinemaPic;
        private System.Windows.Forms.Panel airplane;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.PictureBox apBack;
        private System.Windows.Forms.PictureBox apExit;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.CheckBox twoWays;
        private System.Windows.Forms.CheckBox oneWay;
        private System.Windows.Forms.ComboBox arrivalAP;
        private System.Windows.Forms.ComboBox departAP;
        private System.Windows.Forms.DateTimePicker departDateAP;
        private System.Windows.Forms.DateTimePicker arrivalDateAP;
        private System.Windows.Forms.Label backAP;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.NumericUpDown departHourAI;
        private System.Windows.Forms.NumericUpDown departMinAP;
        private System.Windows.Forms.NumericUpDown arrivalHourAP;
        private System.Windows.Forms.NumericUpDown ArrivalMinAP;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label5;
        public System.Windows.Forms.Button issueBtnAP;
        private System.Windows.Forms.Panel dtPanelAP;
        private System.Windows.Forms.NumericUpDown amountOfTicketsAP;
        private System.Windows.Forms.Panel issueAP;
        private System.Windows.Forms.Label placeAP;
        private System.Windows.Forms.Label arrivalPlaceAP;
        private System.Windows.Forms.Label departTimeAP;
        private System.Windows.Forms.Label departPlaceAP;
        public System.Windows.Forms.Button apPay;
        private System.Windows.Forms.Label amountAP;
        private System.Windows.Forms.PictureBox apIssueBack;
        private System.Windows.Forms.PictureBox apIssueExit;
        private System.Windows.Forms.ListBox placesAP;
        private System.Windows.Forms.Label arrivalTimeAP;
        private System.Windows.Forms.Label backToAP;
        private System.Windows.Forms.Panel train;
        private System.Windows.Forms.Panel issueTR;
        private System.Windows.Forms.Label toPayTR;
        public System.Windows.Forms.Button payTR;
        private System.Windows.Forms.ListBox placesTR;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label backToTR;
        private System.Windows.Forms.Label arrivalDateLabelTR;
        private System.Windows.Forms.Label arrivalToTR;
        private System.Windows.Forms.Label departDateLabelTR;
        private System.Windows.Forms.Label departFromTR;
        private System.Windows.Forms.PictureBox issueTRBack;
        private System.Windows.Forms.PictureBox issueTRExit;
        private System.Windows.Forms.NumericUpDown amountOfTicketsTR;
        private System.Windows.Forms.Panel dtTimeTR;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.DateTimePicker arrivalDateTR;
        private System.Windows.Forms.NumericUpDown arrivalTimeHourTR;
        private System.Windows.Forms.NumericUpDown arrivalTimeMinTR;
        public System.Windows.Forms.Button issueBtnTR;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.NumericUpDown departTimeHourTR;
        private System.Windows.Forms.NumericUpDown departTimeMinTR;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.DateTimePicker departDateTR;
        private System.Windows.Forms.CheckBox twoWaysTr;
        private System.Windows.Forms.CheckBox oneWayTr;
        private System.Windows.Forms.ComboBox arrivalTR;
        private System.Windows.Forms.ComboBox departTR;
        private System.Windows.Forms.PictureBox pictureBox2;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.PictureBox trBack;
        private System.Windows.Forms.PictureBox trExit;
        private System.Windows.Forms.Panel bus;
        private System.Windows.Forms.Panel arrivalPanelBS;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.DateTimePicker arrivalDateBS;
        private System.Windows.Forms.NumericUpDown arrivalTimeHourBS;
        private System.Windows.Forms.NumericUpDown arrivalTimeMinBS;
        public System.Windows.Forms.Button issueBtnBS;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.NumericUpDown departTimeHourBS;
        private System.Windows.Forms.NumericUpDown departTimeMinBS;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.DateTimePicker departDateBS;
        private System.Windows.Forms.CheckBox twoWaysBS;
        private System.Windows.Forms.CheckBox oneWayBS;
        private System.Windows.Forms.ComboBox arrivalBS;
        private System.Windows.Forms.ComboBox departBS;
        private System.Windows.Forms.PictureBox pictureBox3;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.PictureBox backBS;
        private System.Windows.Forms.PictureBox exitBS;
        private System.Windows.Forms.Panel issueBS;
        private System.Windows.Forms.Label arrivalTimeLblBS;
        private System.Windows.Forms.Label backToBS;
        private System.Windows.Forms.ListBox placesBS;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.Label arrivalToLblBS;
        private System.Windows.Forms.Label departTimeLblBS;
        private System.Windows.Forms.Label departFromBS;
        public System.Windows.Forms.Button payBS;
        private System.Windows.Forms.Label toPayBS;
        private System.Windows.Forms.PictureBox issueBSBack;
        private System.Windows.Forms.PictureBox issueBsExit;
        private System.Windows.Forms.NumericUpDown amountOfTicketsBS;
        private System.Windows.Forms.Panel cinema;
        public System.Windows.Forms.Button issueMovie;
        private System.Windows.Forms.NumericUpDown amountOfTickets;
        private System.Windows.Forms.DateTimePicker moviesSchedule;
        private System.Windows.Forms.ComboBox movies;
        private System.Windows.Forms.PictureBox pictureBox4;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.PictureBox cinemaBack;
        private System.Windows.Forms.PictureBox cinemaExit;
        private System.Windows.Forms.Panel issueCinema;
        private System.Windows.Forms.NumericUpDown cinemaMin;
        private System.Windows.Forms.NumericUpDown cinemaHour;
        private System.Windows.Forms.ListBox cinemaTickets;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.Label movieDate;
        private System.Windows.Forms.Label movieTitle;
        public System.Windows.Forms.Button payCinema;
        private System.Windows.Forms.Label toPayCinema;
        private System.Windows.Forms.PictureBox cinemaIssueBack;
        private System.Windows.Forms.PictureBox cinemaIssueExit;
    }
}