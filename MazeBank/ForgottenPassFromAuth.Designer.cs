﻿namespace MazeBank
{
    partial class ForgottenPassFromAuth
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ForgottenPassFromAuth));
            this.backBut = new System.Windows.Forms.PictureBox();
            this.exitBut = new System.Windows.Forms.PictureBox();
            this.enterCode = new System.Windows.Forms.TextBox();
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            this.verifyCodeString = new System.Windows.Forms.Label();
            this.submitCode = new System.Windows.Forms.Button();
            this.hidingPhoneVerification = new System.Windows.Forms.Panel();
            this.verifyPhn = new System.Windows.Forms.Button();
            this.underPhn = new System.Windows.Forms.PictureBox();
            this.fgtPhoneNum = new System.Windows.Forms.TextBox();
            this.phnNumString = new System.Windows.Forms.Label();
            this.confirmationPanel = new System.Windows.Forms.Panel();
            this.confirmNewPassString = new System.Windows.Forms.Label();
            this.newPassString = new System.Windows.Forms.Label();
            this.underConfirm = new System.Windows.Forms.PictureBox();
            this.newPasswordConfirm = new System.Windows.Forms.TextBox();
            this.underNewPass = new System.Windows.Forms.PictureBox();
            this.newPassword = new System.Windows.Forms.TextBox();
            this.submitChanges = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.backBut)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.exitBut)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            this.hidingPhoneVerification.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.underPhn)).BeginInit();
            this.confirmationPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.underConfirm)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.underNewPass)).BeginInit();
            this.SuspendLayout();
            // 
            // backBut
            // 
            this.backBut.Cursor = System.Windows.Forms.Cursors.Hand;
            this.backBut.Image = ((System.Drawing.Image)(resources.GetObject("backBut.Image")));
            this.backBut.Location = new System.Drawing.Point(12, 12);
            this.backBut.Name = "backBut";
            this.backBut.Size = new System.Drawing.Size(34, 34);
            this.backBut.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.backBut.TabIndex = 22;
            this.backBut.TabStop = false;
            this.backBut.Click += new System.EventHandler(this.backBut_Click);
            // 
            // exitBut
            // 
            this.exitBut.Cursor = System.Windows.Forms.Cursors.Hand;
            this.exitBut.Image = ((System.Drawing.Image)(resources.GetObject("exitBut.Image")));
            this.exitBut.Location = new System.Drawing.Point(358, 12);
            this.exitBut.Name = "exitBut";
            this.exitBut.Size = new System.Drawing.Size(34, 34);
            this.exitBut.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.exitBut.TabIndex = 23;
            this.exitBut.TabStop = false;
            this.exitBut.Click += new System.EventHandler(this.exitBut_Click);
            // 
            // enterCode
            // 
            this.enterCode.BackColor = System.Drawing.SystemColors.Control;
            this.enterCode.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.enterCode.Font = new System.Drawing.Font("Verdana", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.enterCode.Location = new System.Drawing.Point(117, 127);
            this.enterCode.Name = "enterCode";
            this.enterCode.Size = new System.Drawing.Size(186, 24);
            this.enterCode.TabIndex = 24;
            this.enterCode.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // pictureBox2
            // 
            this.pictureBox2.BackColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.pictureBox2.Location = new System.Drawing.Point(115, 155);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new System.Drawing.Size(190, 2);
            this.pictureBox2.TabIndex = 25;
            this.pictureBox2.TabStop = false;
            // 
            // verifyCodeString
            // 
            this.verifyCodeString.AutoSize = true;
            this.verifyCodeString.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.verifyCodeString.Location = new System.Drawing.Point(114, 74);
            this.verifyCodeString.Name = "verifyCodeString";
            this.verifyCodeString.Size = new System.Drawing.Size(192, 18);
            this.verifyCodeString.TabIndex = 30;
            this.verifyCodeString.Text = "Введите код-пароль";
            // 
            // submitCode
            // 
            this.submitCode.FlatAppearance.BorderSize = 0;
            this.submitCode.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.submitCode.Font = new System.Drawing.Font("Verdana", 9.75F, System.Drawing.FontStyle.Bold);
            this.submitCode.ForeColor = System.Drawing.Color.Red;
            this.submitCode.Location = new System.Drawing.Point(154, 179);
            this.submitCode.Name = "submitCode";
            this.submitCode.Size = new System.Drawing.Size(117, 40);
            this.submitCode.TabIndex = 31;
            this.submitCode.TabStop = false;
            this.submitCode.Text = "Подтвердить";
            this.submitCode.UseVisualStyleBackColor = true;
            this.submitCode.Click += new System.EventHandler(this.submitCode_Click);
            this.submitCode.KeyDown += new System.Windows.Forms.KeyEventHandler(this.submitCode_KeyDown);
            // 
            // hidingPhoneVerification
            // 
            this.hidingPhoneVerification.Controls.Add(this.verifyPhn);
            this.hidingPhoneVerification.Controls.Add(this.underPhn);
            this.hidingPhoneVerification.Controls.Add(this.fgtPhoneNum);
            this.hidingPhoneVerification.Controls.Add(this.phnNumString);
            this.hidingPhoneVerification.Location = new System.Drawing.Point(0, 49);
            this.hidingPhoneVerification.Name = "hidingPhoneVerification";
            this.hidingPhoneVerification.Size = new System.Drawing.Size(420, 187);
            this.hidingPhoneVerification.TabIndex = 32;
            // 
            // verifyPhn
            // 
            this.verifyPhn.FlatAppearance.BorderSize = 0;
            this.verifyPhn.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.verifyPhn.Font = new System.Drawing.Font("Verdana", 9.75F, System.Drawing.FontStyle.Bold);
            this.verifyPhn.ForeColor = System.Drawing.Color.Red;
            this.verifyPhn.Location = new System.Drawing.Point(162, 144);
            this.verifyPhn.Name = "verifyPhn";
            this.verifyPhn.Size = new System.Drawing.Size(95, 37);
            this.verifyPhn.TabIndex = 38;
            this.verifyPhn.TabStop = false;
            this.verifyPhn.Text = "Получить код";
            this.verifyPhn.UseVisualStyleBackColor = true;
            this.verifyPhn.Click += new System.EventHandler(this.verifyPhn_Click);
            this.verifyPhn.KeyDown += new System.Windows.Forms.KeyEventHandler(this.verifyPhn_KeyDown);
            // 
            // underPhn
            // 
            this.underPhn.BackColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.underPhn.Location = new System.Drawing.Point(115, 129);
            this.underPhn.Name = "underPhn";
            this.underPhn.Size = new System.Drawing.Size(190, 2);
            this.underPhn.TabIndex = 37;
            this.underPhn.TabStop = false;
            // 
            // fgtPhoneNum
            // 
            this.fgtPhoneNum.BackColor = System.Drawing.SystemColors.Control;
            this.fgtPhoneNum.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.fgtPhoneNum.Font = new System.Drawing.Font("Verdana", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.fgtPhoneNum.Location = new System.Drawing.Point(117, 101);
            this.fgtPhoneNum.Name = "fgtPhoneNum";
            this.fgtPhoneNum.Size = new System.Drawing.Size(186, 24);
            this.fgtPhoneNum.TabIndex = 36;
            this.fgtPhoneNum.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // phnNumString
            // 
            this.phnNumString.AutoSize = true;
            this.phnNumString.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.phnNumString.Location = new System.Drawing.Point(90, 59);
            this.phnNumString.Name = "phnNumString";
            this.phnNumString.Size = new System.Drawing.Size(238, 18);
            this.phnNumString.TabIndex = 35;
            this.phnNumString.Text = "Введите номер телефона";
            // 
            // confirmationPanel
            // 
            this.confirmationPanel.Controls.Add(this.confirmNewPassString);
            this.confirmationPanel.Controls.Add(this.newPassString);
            this.confirmationPanel.Controls.Add(this.underConfirm);
            this.confirmationPanel.Controls.Add(this.newPasswordConfirm);
            this.confirmationPanel.Controls.Add(this.underNewPass);
            this.confirmationPanel.Controls.Add(this.newPassword);
            this.confirmationPanel.Controls.Add(this.submitChanges);
            this.confirmationPanel.Location = new System.Drawing.Point(0, 49);
            this.confirmationPanel.Name = "confirmationPanel";
            this.confirmationPanel.Size = new System.Drawing.Size(420, 329);
            this.confirmationPanel.TabIndex = 33;
            // 
            // confirmNewPassString
            // 
            this.confirmNewPassString.AutoSize = true;
            this.confirmNewPassString.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.confirmNewPassString.Location = new System.Drawing.Point(82, 138);
            this.confirmNewPassString.Name = "confirmNewPassString";
            this.confirmNewPassString.Size = new System.Drawing.Size(256, 18);
            this.confirmNewPassString.TabIndex = 46;
            this.confirmNewPassString.Text = "Подтвердите новый пароль";
            // 
            // newPassString
            // 
            this.newPassString.AutoSize = true;
            this.newPassString.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.newPassString.Location = new System.Drawing.Point(103, 21);
            this.newPassString.Name = "newPassString";
            this.newPassString.Size = new System.Drawing.Size(214, 18);
            this.newPassString.TabIndex = 45;
            this.newPassString.Text = "Введите новый пароль";
            // 
            // underConfirm
            // 
            this.underConfirm.BackColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.underConfirm.Location = new System.Drawing.Point(117, 200);
            this.underConfirm.Name = "underConfirm";
            this.underConfirm.Size = new System.Drawing.Size(190, 2);
            this.underConfirm.TabIndex = 44;
            this.underConfirm.TabStop = false;
            // 
            // newPasswordConfirm
            // 
            this.newPasswordConfirm.BackColor = System.Drawing.SystemColors.Control;
            this.newPasswordConfirm.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.newPasswordConfirm.Font = new System.Drawing.Font("Verdana", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.newPasswordConfirm.Location = new System.Drawing.Point(119, 173);
            this.newPasswordConfirm.Name = "newPasswordConfirm";
            this.newPasswordConfirm.Size = new System.Drawing.Size(186, 24);
            this.newPasswordConfirm.TabIndex = 43;
            this.newPasswordConfirm.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.newPasswordConfirm.UseSystemPasswordChar = true;
            // 
            // underNewPass
            // 
            this.underNewPass.BackColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.underNewPass.Location = new System.Drawing.Point(117, 88);
            this.underNewPass.Name = "underNewPass";
            this.underNewPass.Size = new System.Drawing.Size(190, 2);
            this.underNewPass.TabIndex = 42;
            this.underNewPass.TabStop = false;
            // 
            // newPassword
            // 
            this.newPassword.BackColor = System.Drawing.SystemColors.Control;
            this.newPassword.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.newPassword.Font = new System.Drawing.Font("Verdana", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.newPassword.Location = new System.Drawing.Point(119, 60);
            this.newPassword.Name = "newPassword";
            this.newPassword.Size = new System.Drawing.Size(186, 24);
            this.newPassword.TabIndex = 41;
            this.newPassword.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.newPassword.UseSystemPasswordChar = true;
            // 
            // submitChanges
            // 
            this.submitChanges.FlatAppearance.BorderSize = 0;
            this.submitChanges.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.submitChanges.Font = new System.Drawing.Font("Verdana", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.submitChanges.ForeColor = System.Drawing.Color.Red;
            this.submitChanges.Location = new System.Drawing.Point(154, 219);
            this.submitChanges.Name = "submitChanges";
            this.submitChanges.Size = new System.Drawing.Size(106, 45);
            this.submitChanges.TabIndex = 0;
            this.submitChanges.TabStop = false;
            this.submitChanges.Text = "Сохранить";
            this.submitChanges.UseVisualStyleBackColor = true;
            this.submitChanges.Click += new System.EventHandler(this.submitChanges_Click);
            this.submitChanges.KeyDown += new System.Windows.Forms.KeyEventHandler(this.submitChanges_KeyDown);
            // 
            // ForgottenPassFromAuth
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(404, 561);
            this.Controls.Add(this.confirmationPanel);
            this.Controls.Add(this.hidingPhoneVerification);
            this.Controls.Add(this.submitCode);
            this.Controls.Add(this.verifyCodeString);
            this.Controls.Add(this.pictureBox2);
            this.Controls.Add(this.enterCode);
            this.Controls.Add(this.exitBut);
            this.Controls.Add(this.backBut);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.KeyPreview = true;
            this.Name = "ForgottenPassFromAuth";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Восстановление пароля";
            this.Load += new System.EventHandler(this.ForgottenPassFromAuth_Load);
            ((System.ComponentModel.ISupportInitialize)(this.backBut)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.exitBut)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            this.hidingPhoneVerification.ResumeLayout(false);
            this.hidingPhoneVerification.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.underPhn)).EndInit();
            this.confirmationPanel.ResumeLayout(false);
            this.confirmationPanel.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.underConfirm)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.underNewPass)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.PictureBox backBut;
        private System.Windows.Forms.PictureBox exitBut;
        private System.Windows.Forms.TextBox enterCode;
        private System.Windows.Forms.PictureBox pictureBox2;
        private System.Windows.Forms.Label verifyCodeString;
        private System.Windows.Forms.Button submitCode;
        private System.Windows.Forms.Panel hidingPhoneVerification;
        private System.Windows.Forms.PictureBox underPhn;
        private System.Windows.Forms.TextBox fgtPhoneNum;
        private System.Windows.Forms.Label phnNumString;
        private System.Windows.Forms.Button verifyPhn;
        private System.Windows.Forms.Panel confirmationPanel;
        private System.Windows.Forms.Button submitChanges;
        private System.Windows.Forms.Label confirmNewPassString;
        private System.Windows.Forms.Label newPassString;
        private System.Windows.Forms.PictureBox underConfirm;
        private System.Windows.Forms.TextBox newPasswordConfirm;
        private System.Windows.Forms.PictureBox underNewPass;
        private System.Windows.Forms.TextBox newPassword;
    }
}