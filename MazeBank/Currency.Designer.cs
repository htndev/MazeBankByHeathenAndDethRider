﻿namespace MazeBank
{
    partial class Currency
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Currency));
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.label1 = new System.Windows.Forms.Label();
            this.today = new System.Windows.Forms.Label();
            this.exitBut = new System.Windows.Forms.PictureBox();
            this.backBut = new System.Windows.Forms.PictureBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.buyUSD = new System.Windows.Forms.Label();
            this.sellUsd = new System.Windows.Forms.Label();
            this.buyEur = new System.Windows.Forms.Label();
            this.sellEur = new System.Windows.Forms.Label();
            this.buyGbp = new System.Windows.Forms.Label();
            this.sellGbp = new System.Windows.Forms.Label();
            this.currencyToConvert = new System.Windows.Forms.ComboBox();
            this.convertorBox = new System.Windows.Forms.TextBox();
            this.exgStripes = new System.Windows.Forms.PictureBox();
            this.convertationResult = new System.Windows.Forms.TextBox();
            this.text = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.exitBut)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.backBut)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.exgStripes)).BeginInit();
            this.SuspendLayout();
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox1.Image")));
            this.pictureBox1.Location = new System.Drawing.Point(143, 12);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(120, 120);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox1.TabIndex = 0;
            this.pictureBox1.TabStop = false;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Verdana", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label1.Location = new System.Drawing.Point(60, 155);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(300, 29);
            this.label1.TabIndex = 1;
            this.label1.Text = "Текущий курс валют";
            // 
            // today
            // 
            this.today.AutoSize = true;
            this.today.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.today.Location = new System.Drawing.Point(156, 194);
            this.today.Name = "today";
            this.today.Size = new System.Drawing.Size(0, 18);
            this.today.TabIndex = 2;
            // 
            // exitBut
            // 
            this.exitBut.Cursor = System.Windows.Forms.Cursors.Hand;
            this.exitBut.Image = ((System.Drawing.Image)(resources.GetObject("exitBut.Image")));
            this.exitBut.Location = new System.Drawing.Point(358, 12);
            this.exitBut.Name = "exitBut";
            this.exitBut.Size = new System.Drawing.Size(34, 34);
            this.exitBut.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.exitBut.TabIndex = 11;
            this.exitBut.TabStop = false;
            this.exitBut.Click += new System.EventHandler(this.exitBut_Click);
            // 
            // backBut
            // 
            this.backBut.Cursor = System.Windows.Forms.Cursors.Hand;
            this.backBut.Image = ((System.Drawing.Image)(resources.GetObject("backBut.Image")));
            this.backBut.Location = new System.Drawing.Point(12, 12);
            this.backBut.Name = "backBut";
            this.backBut.Size = new System.Drawing.Size(34, 34);
            this.backBut.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.backBut.TabIndex = 12;
            this.backBut.TabStop = false;
            this.backBut.Click += new System.EventHandler(this.backBut_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Verdana", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label2.Location = new System.Drawing.Point(155, 237);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(87, 23);
            this.label2.TabIndex = 13;
            this.label2.Text = "Купить";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Verdana", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label3.Location = new System.Drawing.Point(283, 237);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(100, 23);
            this.label3.TabIndex = 14;
            this.label3.Text = "Продать";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Verdana", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label4.Location = new System.Drawing.Point(30, 237);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(92, 23);
            this.label4.TabIndex = 15;
            this.label4.Text = "Валюта";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label5.Location = new System.Drawing.Point(40, 278);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(70, 18);
            this.label5.TabIndex = 16;
            this.label5.Text = "Доллар";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label6.Location = new System.Drawing.Point(40, 318);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(48, 18);
            this.label6.TabIndex = 17;
            this.label6.Text = "Евро";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label7.Location = new System.Drawing.Point(40, 356);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(47, 18);
            this.label7.TabIndex = 18;
            this.label7.Text = "Фунт";
            // 
            // buyUSD
            // 
            this.buyUSD.AutoSize = true;
            this.buyUSD.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.buyUSD.Location = new System.Drawing.Point(165, 278);
            this.buyUSD.Name = "buyUSD";
            this.buyUSD.Size = new System.Drawing.Size(18, 18);
            this.buyUSD.TabIndex = 19;
            this.buyUSD.Text = "1";
            // 
            // sellUsd
            // 
            this.sellUsd.AutoSize = true;
            this.sellUsd.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.sellUsd.Location = new System.Drawing.Point(293, 278);
            this.sellUsd.Name = "sellUsd";
            this.sellUsd.Size = new System.Drawing.Size(18, 18);
            this.sellUsd.TabIndex = 20;
            this.sellUsd.Text = "2";
            // 
            // buyEur
            // 
            this.buyEur.AutoSize = true;
            this.buyEur.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.buyEur.Location = new System.Drawing.Point(165, 318);
            this.buyEur.Name = "buyEur";
            this.buyEur.Size = new System.Drawing.Size(18, 18);
            this.buyEur.TabIndex = 21;
            this.buyEur.Text = "1";
            // 
            // sellEur
            // 
            this.sellEur.AutoSize = true;
            this.sellEur.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.sellEur.Location = new System.Drawing.Point(293, 318);
            this.sellEur.Name = "sellEur";
            this.sellEur.Size = new System.Drawing.Size(18, 18);
            this.sellEur.TabIndex = 22;
            this.sellEur.Text = "2";
            // 
            // buyGbp
            // 
            this.buyGbp.AutoSize = true;
            this.buyGbp.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.buyGbp.Location = new System.Drawing.Point(165, 356);
            this.buyGbp.Name = "buyGbp";
            this.buyGbp.Size = new System.Drawing.Size(18, 18);
            this.buyGbp.TabIndex = 23;
            this.buyGbp.Text = "1";
            // 
            // sellGbp
            // 
            this.sellGbp.AutoSize = true;
            this.sellGbp.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.sellGbp.Location = new System.Drawing.Point(293, 356);
            this.sellGbp.Name = "sellGbp";
            this.sellGbp.Size = new System.Drawing.Size(18, 18);
            this.sellGbp.TabIndex = 24;
            this.sellGbp.Text = "2";
            // 
            // currencyToConvert
            // 
            this.currencyToConvert.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.currencyToConvert.Font = new System.Drawing.Font("Verdana", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.currencyToConvert.FormattingEnabled = true;
            this.currencyToConvert.Items.AddRange(new object[] {
            "USD",
            "EUR",
            "GBP"});
            this.currencyToConvert.Location = new System.Drawing.Point(22, 473);
            this.currencyToConvert.Name = "currencyToConvert";
            this.currencyToConvert.Size = new System.Drawing.Size(63, 26);
            this.currencyToConvert.TabIndex = 25;
            this.currencyToConvert.TabStop = false;
            // 
            // convertorBox
            // 
            this.convertorBox.BackColor = System.Drawing.SystemColors.Control;
            this.convertorBox.Font = new System.Drawing.Font("Verdana", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.convertorBox.Location = new System.Drawing.Point(95, 473);
            this.convertorBox.Name = "convertorBox";
            this.convertorBox.Size = new System.Drawing.Size(75, 26);
            this.convertorBox.TabIndex = 26;
            this.convertorBox.TabStop = false;
            this.convertorBox.TextChanged += new System.EventHandler(this.convertorBox_TextChanged);
            // 
            // exgStripes
            // 
            this.exgStripes.Image = ((System.Drawing.Image)(resources.GetObject("exgStripes.Image")));
            this.exgStripes.Location = new System.Drawing.Point(188, 465);
            this.exgStripes.Name = "exgStripes";
            this.exgStripes.Size = new System.Drawing.Size(40, 40);
            this.exgStripes.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.exgStripes.TabIndex = 27;
            this.exgStripes.TabStop = false;
            // 
            // convertationResult
            // 
            this.convertationResult.Font = new System.Drawing.Font("Verdana", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.convertationResult.Location = new System.Drawing.Point(251, 473);
            this.convertationResult.Name = "convertationResult";
            this.convertationResult.ReadOnly = true;
            this.convertationResult.Size = new System.Drawing.Size(132, 26);
            this.convertationResult.TabIndex = 29;
            this.convertationResult.TabStop = false;
            // 
            // text
            // 
            this.text.AutoSize = true;
            this.text.Font = new System.Drawing.Font("Verdana", 14.25F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.text.Location = new System.Drawing.Point(80, 422);
            this.text.Name = "text";
            this.text.Size = new System.Drawing.Size(244, 23);
            this.text.TabIndex = 30;
            this.text.Text = "Конвертор валют в ₴";
            // 
            // Currency
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(404, 561);
            this.ControlBox = false;
            this.Controls.Add(this.text);
            this.Controls.Add(this.convertationResult);
            this.Controls.Add(this.exgStripes);
            this.Controls.Add(this.convertorBox);
            this.Controls.Add(this.currencyToConvert);
            this.Controls.Add(this.sellGbp);
            this.Controls.Add(this.buyGbp);
            this.Controls.Add(this.sellEur);
            this.Controls.Add(this.buyEur);
            this.Controls.Add(this.sellUsd);
            this.Controls.Add(this.buyUSD);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.backBut);
            this.Controls.Add(this.exitBut);
            this.Controls.Add(this.today);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.pictureBox1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.KeyPreview = true;
            this.Name = "Currency";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Currency";
            this.Load += new System.EventHandler(this.Currency_Load);
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.Currency_KeyDown);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.exitBut)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.backBut)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.exgStripes)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label today;
        private System.Windows.Forms.PictureBox exitBut;
        private System.Windows.Forms.PictureBox backBut;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label buyUSD;
        private System.Windows.Forms.Label sellUsd;
        private System.Windows.Forms.Label buyEur;
        private System.Windows.Forms.Label sellEur;
        private System.Windows.Forms.Label buyGbp;
        private System.Windows.Forms.Label sellGbp;
        private System.Windows.Forms.ComboBox currencyToConvert;
        private System.Windows.Forms.TextBox convertorBox;
        private System.Windows.Forms.PictureBox exgStripes;
        private System.Windows.Forms.TextBox convertationResult;
        private System.Windows.Forms.Label text;
    }
}