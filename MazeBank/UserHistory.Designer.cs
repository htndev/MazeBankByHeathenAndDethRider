﻿namespace MazeBank
{
    partial class UserHistory
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(UserHistory));
            this.printHistory = new System.Windows.Forms.Button();
            this.historyOperation = new System.Windows.Forms.ListBox();
            this.closeHistory = new System.Windows.Forms.PictureBox();
            this.label = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.closeHistory)).BeginInit();
            this.SuspendLayout();
            // 
            // printHistory
            // 
            this.printHistory.FlatAppearance.BorderSize = 0;
            this.printHistory.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.printHistory.Font = new System.Drawing.Font("Verdana", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.printHistory.ForeColor = System.Drawing.Color.Red;
            this.printHistory.Location = new System.Drawing.Point(93, 427);
            this.printHistory.Name = "printHistory";
            this.printHistory.Size = new System.Drawing.Size(181, 70);
            this.printHistory.TabIndex = 7;
            this.printHistory.Text = "Распечатать";
            this.printHistory.UseVisualStyleBackColor = true;
            this.printHistory.Click += new System.EventHandler(this.printHistory_Click);
            // 
            // historyOperation
            // 
            this.historyOperation.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.historyOperation.FormattingEnabled = true;
            this.historyOperation.HorizontalScrollbar = true;
            this.historyOperation.Location = new System.Drawing.Point(25, 99);
            this.historyOperation.Name = "historyOperation";
            this.historyOperation.Size = new System.Drawing.Size(314, 303);
            this.historyOperation.TabIndex = 6;
            // 
            // closeHistory
            // 
            this.closeHistory.Cursor = System.Windows.Forms.Cursors.Hand;
            this.closeHistory.Image = ((System.Drawing.Image)(resources.GetObject("closeHistory.Image")));
            this.closeHistory.Location = new System.Drawing.Point(326, 12);
            this.closeHistory.Name = "closeHistory";
            this.closeHistory.Size = new System.Drawing.Size(26, 26);
            this.closeHistory.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.closeHistory.TabIndex = 5;
            this.closeHistory.TabStop = false;
            this.closeHistory.Click += new System.EventHandler(this.closeHistory_Click);
            // 
            // label
            // 
            this.label.AutoSize = true;
            this.label.Font = new System.Drawing.Font("Verdana", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label.Location = new System.Drawing.Point(34, 50);
            this.label.Name = "label";
            this.label.Size = new System.Drawing.Size(295, 23);
            this.label.TabIndex = 4;
            this.label.Text = "История Ваших операций";
            // 
            // UserHistory
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(364, 530);
            this.Controls.Add(this.printHistory);
            this.Controls.Add(this.historyOperation);
            this.Controls.Add(this.closeHistory);
            this.Controls.Add(this.label);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "UserHistory";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "История пользователя";
            this.Load += new System.EventHandler(this.UserHistory_Load);
            ((System.ComponentModel.ISupportInitialize)(this.closeHistory)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button printHistory;
        private System.Windows.Forms.ListBox historyOperation;
        private System.Windows.Forms.PictureBox closeHistory;
        private System.Windows.Forms.Label label;
    }
}