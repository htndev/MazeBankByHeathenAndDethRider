﻿namespace MazeBank
{
    partial class OnlinePayments
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(OnlinePayments));
            this.backBut = new System.Windows.Forms.PictureBox();
            this.exitBut = new System.Windows.Forms.PictureBox();
            this.label2 = new System.Windows.Forms.Label();
            this.yandexmoneyPic = new System.Windows.Forms.PictureBox();
            this.skrillPic = new System.Windows.Forms.PictureBox();
            this.qiwiPic = new System.Windows.Forms.PictureBox();
            this.webmoneyPic = new System.Windows.Forms.PictureBox();
            this.label1 = new System.Windows.Forms.Label();
            this.webmoney = new System.Windows.Forms.Panel();
            this.wmExit = new System.Windows.Forms.PictureBox();
            this.wmBack = new System.Windows.Forms.PictureBox();
            this.wmPay = new System.Windows.Forms.Button();
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            this.wmAmount = new System.Windows.Forms.TextBox();
            this.pictureBox3 = new System.Windows.Forms.PictureBox();
            this.wmWallet = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.qiwi = new System.Windows.Forms.Panel();
            this.qiwiExit = new System.Windows.Forms.PictureBox();
            this.qiwiBack = new System.Windows.Forms.PictureBox();
            this.qiwiPay = new System.Windows.Forms.Button();
            this.pictureBox6 = new System.Windows.Forms.PictureBox();
            this.qiwiAmount = new System.Windows.Forms.TextBox();
            this.pictureBox7 = new System.Windows.Forms.PictureBox();
            this.qiwiWallet = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.pictureBox8 = new System.Windows.Forms.PictureBox();
            this.yandexMoney = new System.Windows.Forms.Panel();
            this.ydExit = new System.Windows.Forms.PictureBox();
            this.ydBack = new System.Windows.Forms.PictureBox();
            this.payYD = new System.Windows.Forms.Button();
            this.pictureBox9 = new System.Windows.Forms.PictureBox();
            this.ydAmount = new System.Windows.Forms.TextBox();
            this.pictureBox10 = new System.Windows.Forms.PictureBox();
            this.ydWallet = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.pictureBox11 = new System.Windows.Forms.PictureBox();
            this.skrill = new System.Windows.Forms.Panel();
            this.skExit = new System.Windows.Forms.PictureBox();
            this.skBack = new System.Windows.Forms.PictureBox();
            this.skPay = new System.Windows.Forms.Button();
            this.pictureBox12 = new System.Windows.Forms.PictureBox();
            this.skAmount = new System.Windows.Forms.TextBox();
            this.pictureBox13 = new System.Windows.Forms.PictureBox();
            this.skWallet = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.pictureBox14 = new System.Windows.Forms.PictureBox();
            ((System.ComponentModel.ISupportInitialize)(this.backBut)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.exitBut)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.yandexmoneyPic)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.skrillPic)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.qiwiPic)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.webmoneyPic)).BeginInit();
            this.webmoney.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.wmExit)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.wmBack)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.qiwi.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.qiwiExit)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.qiwiBack)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox8)).BeginInit();
            this.yandexMoney.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ydExit)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ydBack)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox11)).BeginInit();
            this.skrill.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.skExit)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.skBack)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox12)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox13)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox14)).BeginInit();
            this.SuspendLayout();
            // 
            // backBut
            // 
            this.backBut.Cursor = System.Windows.Forms.Cursors.Hand;
            this.backBut.Image = ((System.Drawing.Image)(resources.GetObject("backBut.Image")));
            this.backBut.Location = new System.Drawing.Point(12, 12);
            this.backBut.Name = "backBut";
            this.backBut.Size = new System.Drawing.Size(34, 34);
            this.backBut.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.backBut.TabIndex = 17;
            this.backBut.TabStop = false;
            this.backBut.Click += new System.EventHandler(this.backBut_Click);
            // 
            // exitBut
            // 
            this.exitBut.Cursor = System.Windows.Forms.Cursors.Hand;
            this.exitBut.Image = ((System.Drawing.Image)(resources.GetObject("exitBut.Image")));
            this.exitBut.Location = new System.Drawing.Point(358, 12);
            this.exitBut.Name = "exitBut";
            this.exitBut.Size = new System.Drawing.Size(34, 34);
            this.exitBut.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.exitBut.TabIndex = 18;
            this.exitBut.TabStop = false;
            this.exitBut.Click += new System.EventHandler(this.exitBut_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Verdana", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label2.Location = new System.Drawing.Point(103, 44);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(197, 23);
            this.label2.TabIndex = 40;
            this.label2.Text = "Онлайн платежи";
            // 
            // yandexmoneyPic
            // 
            this.yandexmoneyPic.Cursor = System.Windows.Forms.Cursors.Hand;
            this.yandexmoneyPic.Image = ((System.Drawing.Image)(resources.GetObject("yandexmoneyPic.Image")));
            this.yandexmoneyPic.Location = new System.Drawing.Point(156, 330);
            this.yandexmoneyPic.Name = "yandexmoneyPic";
            this.yandexmoneyPic.Size = new System.Drawing.Size(80, 80);
            this.yandexmoneyPic.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.yandexmoneyPic.TabIndex = 44;
            this.yandexmoneyPic.TabStop = false;
            this.yandexmoneyPic.Click += new System.EventHandler(this.yandexmoneyPic_Click);
            // 
            // skrillPic
            // 
            this.skrillPic.Cursor = System.Windows.Forms.Cursors.Hand;
            this.skrillPic.Image = ((System.Drawing.Image)(resources.GetObject("skrillPic.Image")));
            this.skrillPic.Location = new System.Drawing.Point(156, 430);
            this.skrillPic.Name = "skrillPic";
            this.skrillPic.Size = new System.Drawing.Size(80, 80);
            this.skrillPic.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.skrillPic.TabIndex = 43;
            this.skrillPic.TabStop = false;
            this.skrillPic.Click += new System.EventHandler(this.skrillPic_Click);
            // 
            // qiwiPic
            // 
            this.qiwiPic.Cursor = System.Windows.Forms.Cursors.Hand;
            this.qiwiPic.Image = ((System.Drawing.Image)(resources.GetObject("qiwiPic.Image")));
            this.qiwiPic.Location = new System.Drawing.Point(156, 230);
            this.qiwiPic.Name = "qiwiPic";
            this.qiwiPic.Size = new System.Drawing.Size(80, 80);
            this.qiwiPic.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.qiwiPic.TabIndex = 42;
            this.qiwiPic.TabStop = false;
            this.qiwiPic.Click += new System.EventHandler(this.qiwiPic_Click);
            // 
            // webmoneyPic
            // 
            this.webmoneyPic.Cursor = System.Windows.Forms.Cursors.Hand;
            this.webmoneyPic.Image = ((System.Drawing.Image)(resources.GetObject("webmoneyPic.Image")));
            this.webmoneyPic.Location = new System.Drawing.Point(156, 128);
            this.webmoneyPic.Name = "webmoneyPic";
            this.webmoneyPic.Size = new System.Drawing.Size(80, 80);
            this.webmoneyPic.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.webmoneyPic.TabIndex = 41;
            this.webmoneyPic.TabStop = false;
            this.webmoneyPic.Click += new System.EventHandler(this.webmoneyPic_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Verdana", 14.25F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label1.Location = new System.Drawing.Point(25, 83);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(354, 23);
            this.label1.TabIndex = 38;
            this.label1.Text = "Выберите систему онлайн платежа";
            // 
            // webmoney
            // 
            this.webmoney.Controls.Add(this.wmExit);
            this.webmoney.Controls.Add(this.wmBack);
            this.webmoney.Controls.Add(this.wmPay);
            this.webmoney.Controls.Add(this.pictureBox2);
            this.webmoney.Controls.Add(this.wmAmount);
            this.webmoney.Controls.Add(this.pictureBox3);
            this.webmoney.Controls.Add(this.wmWallet);
            this.webmoney.Controls.Add(this.label3);
            this.webmoney.Controls.Add(this.pictureBox1);
            this.webmoney.Dock = System.Windows.Forms.DockStyle.Fill;
            this.webmoney.Location = new System.Drawing.Point(0, 0);
            this.webmoney.Name = "webmoney";
            this.webmoney.Size = new System.Drawing.Size(404, 561);
            this.webmoney.TabIndex = 45;
            // 
            // wmExit
            // 
            this.wmExit.Cursor = System.Windows.Forms.Cursors.Hand;
            this.wmExit.Image = ((System.Drawing.Image)(resources.GetObject("wmExit.Image")));
            this.wmExit.Location = new System.Drawing.Point(358, 12);
            this.wmExit.Name = "wmExit";
            this.wmExit.Size = new System.Drawing.Size(34, 34);
            this.wmExit.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.wmExit.TabIndex = 60;
            this.wmExit.TabStop = false;
            this.wmExit.Click += new System.EventHandler(this.wmExit_Click);
            // 
            // wmBack
            // 
            this.wmBack.Cursor = System.Windows.Forms.Cursors.Hand;
            this.wmBack.Image = ((System.Drawing.Image)(resources.GetObject("wmBack.Image")));
            this.wmBack.Location = new System.Drawing.Point(12, 12);
            this.wmBack.Name = "wmBack";
            this.wmBack.Size = new System.Drawing.Size(34, 34);
            this.wmBack.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.wmBack.TabIndex = 59;
            this.wmBack.TabStop = false;
            this.wmBack.Click += new System.EventHandler(this.wmBack_Click);
            // 
            // wmPay
            // 
            this.wmPay.FlatAppearance.BorderSize = 0;
            this.wmPay.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.wmPay.Font = new System.Drawing.Font("Verdana", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.wmPay.ForeColor = System.Drawing.Color.Red;
            this.wmPay.Location = new System.Drawing.Point(106, 374);
            this.wmPay.Name = "wmPay";
            this.wmPay.Size = new System.Drawing.Size(191, 72);
            this.wmPay.TabIndex = 58;
            this.wmPay.Text = "Пополнить";
            this.wmPay.UseVisualStyleBackColor = true;
            this.wmPay.Click += new System.EventHandler(this.wmPay_Click);
            this.wmPay.KeyDown += new System.Windows.Forms.KeyEventHandler(this.wmPay_KeyDown);
            // 
            // pictureBox2
            // 
            this.pictureBox2.BackColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.pictureBox2.Location = new System.Drawing.Point(128, 326);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new System.Drawing.Size(148, 2);
            this.pictureBox2.TabIndex = 57;
            this.pictureBox2.TabStop = false;
            // 
            // wmAmount
            // 
            this.wmAmount.BackColor = System.Drawing.SystemColors.Control;
            this.wmAmount.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.wmAmount.Font = new System.Drawing.Font("Microsoft Sans Serif", 20.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.wmAmount.ForeColor = System.Drawing.Color.Gray;
            this.wmAmount.Location = new System.Drawing.Point(129, 293);
            this.wmAmount.Name = "wmAmount";
            this.wmAmount.Size = new System.Drawing.Size(146, 31);
            this.wmAmount.TabIndex = 56;
            this.wmAmount.TabStop = false;
            this.wmAmount.Text = "Сумма";
            this.wmAmount.Click += new System.EventHandler(this.wmAmount_Click);
            this.wmAmount.TextChanged += new System.EventHandler(this.wmAmount_TextChanged);
            this.wmAmount.Leave += new System.EventHandler(this.wmAmount_Leave);
            // 
            // pictureBox3
            // 
            this.pictureBox3.BackColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.pictureBox3.Location = new System.Drawing.Point(99, 253);
            this.pictureBox3.Name = "pictureBox3";
            this.pictureBox3.Size = new System.Drawing.Size(206, 2);
            this.pictureBox3.TabIndex = 55;
            this.pictureBox3.TabStop = false;
            // 
            // wmWallet
            // 
            this.wmWallet.BackColor = System.Drawing.SystemColors.Control;
            this.wmWallet.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.wmWallet.Font = new System.Drawing.Font("Microsoft Sans Serif", 20.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.wmWallet.ForeColor = System.Drawing.Color.Gray;
            this.wmWallet.Location = new System.Drawing.Point(100, 221);
            this.wmWallet.Name = "wmWallet";
            this.wmWallet.Size = new System.Drawing.Size(204, 31);
            this.wmWallet.TabIndex = 54;
            this.wmWallet.TabStop = false;
            this.wmWallet.Text = "U";
            this.wmWallet.Click += new System.EventHandler(this.wmWallet_Click);
            this.wmWallet.TextChanged += new System.EventHandler(this.wmWallet_TextChanged);
            this.wmWallet.Leave += new System.EventHandler(this.wmWallet_Leave);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Verdana", 14.25F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label3.Location = new System.Drawing.Point(111, 172);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(181, 23);
            this.label3.TabIndex = 53;
            this.label3.Text = "Введите кошелек";
            // 
            // pictureBox1
            // 
            this.pictureBox1.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pictureBox1.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox1.Image")));
            this.pictureBox1.Location = new System.Drawing.Point(162, 66);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(80, 80);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox1.TabIndex = 52;
            this.pictureBox1.TabStop = false;
            // 
            // qiwi
            // 
            this.qiwi.Controls.Add(this.qiwiExit);
            this.qiwi.Controls.Add(this.qiwiBack);
            this.qiwi.Controls.Add(this.qiwiPay);
            this.qiwi.Controls.Add(this.pictureBox6);
            this.qiwi.Controls.Add(this.qiwiAmount);
            this.qiwi.Controls.Add(this.pictureBox7);
            this.qiwi.Controls.Add(this.qiwiWallet);
            this.qiwi.Controls.Add(this.label4);
            this.qiwi.Controls.Add(this.pictureBox8);
            this.qiwi.Dock = System.Windows.Forms.DockStyle.Fill;
            this.qiwi.Location = new System.Drawing.Point(0, 0);
            this.qiwi.Name = "qiwi";
            this.qiwi.Size = new System.Drawing.Size(404, 561);
            this.qiwi.TabIndex = 62;
            // 
            // qiwiExit
            // 
            this.qiwiExit.Cursor = System.Windows.Forms.Cursors.Hand;
            this.qiwiExit.Image = ((System.Drawing.Image)(resources.GetObject("qiwiExit.Image")));
            this.qiwiExit.Location = new System.Drawing.Point(358, 16);
            this.qiwiExit.Name = "qiwiExit";
            this.qiwiExit.Size = new System.Drawing.Size(34, 34);
            this.qiwiExit.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.qiwiExit.TabIndex = 69;
            this.qiwiExit.TabStop = false;
            this.qiwiExit.Click += new System.EventHandler(this.qiwiExit_Click);
            // 
            // qiwiBack
            // 
            this.qiwiBack.Cursor = System.Windows.Forms.Cursors.Hand;
            this.qiwiBack.Image = ((System.Drawing.Image)(resources.GetObject("qiwiBack.Image")));
            this.qiwiBack.Location = new System.Drawing.Point(12, 16);
            this.qiwiBack.Name = "qiwiBack";
            this.qiwiBack.Size = new System.Drawing.Size(34, 34);
            this.qiwiBack.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.qiwiBack.TabIndex = 68;
            this.qiwiBack.TabStop = false;
            this.qiwiBack.Click += new System.EventHandler(this.qiwiBack_Click);
            // 
            // qiwiPay
            // 
            this.qiwiPay.FlatAppearance.BorderSize = 0;
            this.qiwiPay.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.qiwiPay.Font = new System.Drawing.Font("Verdana", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.qiwiPay.ForeColor = System.Drawing.Color.Red;
            this.qiwiPay.Location = new System.Drawing.Point(106, 378);
            this.qiwiPay.Name = "qiwiPay";
            this.qiwiPay.Size = new System.Drawing.Size(191, 72);
            this.qiwiPay.TabIndex = 67;
            this.qiwiPay.Text = "Пополнить";
            this.qiwiPay.UseVisualStyleBackColor = true;
            this.qiwiPay.Click += new System.EventHandler(this.qiwiPay_Click);
            // 
            // pictureBox6
            // 
            this.pictureBox6.BackColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.pictureBox6.Location = new System.Drawing.Point(128, 330);
            this.pictureBox6.Name = "pictureBox6";
            this.pictureBox6.Size = new System.Drawing.Size(148, 2);
            this.pictureBox6.TabIndex = 66;
            this.pictureBox6.TabStop = false;
            // 
            // qiwiAmount
            // 
            this.qiwiAmount.BackColor = System.Drawing.SystemColors.Control;
            this.qiwiAmount.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.qiwiAmount.Font = new System.Drawing.Font("Microsoft Sans Serif", 20.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.qiwiAmount.ForeColor = System.Drawing.Color.Gray;
            this.qiwiAmount.Location = new System.Drawing.Point(129, 297);
            this.qiwiAmount.Name = "qiwiAmount";
            this.qiwiAmount.Size = new System.Drawing.Size(146, 31);
            this.qiwiAmount.TabIndex = 65;
            this.qiwiAmount.TabStop = false;
            this.qiwiAmount.Text = "Сумма";
            this.qiwiAmount.Click += new System.EventHandler(this.qiwiAmount_Click);
            this.qiwiAmount.TextChanged += new System.EventHandler(this.qiwiAmount_TextChanged);
            this.qiwiAmount.Leave += new System.EventHandler(this.qiwiAmount_Leave);
            // 
            // pictureBox7
            // 
            this.pictureBox7.BackColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.pictureBox7.Location = new System.Drawing.Point(99, 257);
            this.pictureBox7.Name = "pictureBox7";
            this.pictureBox7.Size = new System.Drawing.Size(206, 2);
            this.pictureBox7.TabIndex = 64;
            this.pictureBox7.TabStop = false;
            // 
            // qiwiWallet
            // 
            this.qiwiWallet.BackColor = System.Drawing.SystemColors.Control;
            this.qiwiWallet.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.qiwiWallet.Font = new System.Drawing.Font("Microsoft Sans Serif", 20.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.qiwiWallet.ForeColor = System.Drawing.Color.Gray;
            this.qiwiWallet.Location = new System.Drawing.Point(100, 225);
            this.qiwiWallet.Name = "qiwiWallet";
            this.qiwiWallet.Size = new System.Drawing.Size(204, 31);
            this.qiwiWallet.TabIndex = 63;
            this.qiwiWallet.TabStop = false;
            this.qiwiWallet.Text = "ID";
            this.qiwiWallet.Click += new System.EventHandler(this.qiwiWallet_Click);
            this.qiwiWallet.TextChanged += new System.EventHandler(this.qiwiWallet_TextChanged);
            this.qiwiWallet.Leave += new System.EventHandler(this.qiwiWallet_Leave);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Verdana", 14.25F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label4.Location = new System.Drawing.Point(111, 176);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(181, 23);
            this.label4.TabIndex = 62;
            this.label4.Text = "Введите кошелек";
            // 
            // pictureBox8
            // 
            this.pictureBox8.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pictureBox8.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox8.Image")));
            this.pictureBox8.Location = new System.Drawing.Point(162, 70);
            this.pictureBox8.Name = "pictureBox8";
            this.pictureBox8.Size = new System.Drawing.Size(80, 80);
            this.pictureBox8.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox8.TabIndex = 61;
            this.pictureBox8.TabStop = false;
            // 
            // yandexMoney
            // 
            this.yandexMoney.Controls.Add(this.ydExit);
            this.yandexMoney.Controls.Add(this.ydBack);
            this.yandexMoney.Controls.Add(this.payYD);
            this.yandexMoney.Controls.Add(this.pictureBox9);
            this.yandexMoney.Controls.Add(this.ydAmount);
            this.yandexMoney.Controls.Add(this.pictureBox10);
            this.yandexMoney.Controls.Add(this.ydWallet);
            this.yandexMoney.Controls.Add(this.label5);
            this.yandexMoney.Controls.Add(this.pictureBox11);
            this.yandexMoney.Dock = System.Windows.Forms.DockStyle.Fill;
            this.yandexMoney.Location = new System.Drawing.Point(0, 0);
            this.yandexMoney.Name = "yandexMoney";
            this.yandexMoney.Size = new System.Drawing.Size(404, 561);
            this.yandexMoney.TabIndex = 63;
            // 
            // ydExit
            // 
            this.ydExit.Cursor = System.Windows.Forms.Cursors.Hand;
            this.ydExit.Image = ((System.Drawing.Image)(resources.GetObject("ydExit.Image")));
            this.ydExit.Location = new System.Drawing.Point(358, 12);
            this.ydExit.Name = "ydExit";
            this.ydExit.Size = new System.Drawing.Size(34, 34);
            this.ydExit.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.ydExit.TabIndex = 78;
            this.ydExit.TabStop = false;
            this.ydExit.Click += new System.EventHandler(this.ydExit_Click);
            // 
            // ydBack
            // 
            this.ydBack.Cursor = System.Windows.Forms.Cursors.Hand;
            this.ydBack.Image = ((System.Drawing.Image)(resources.GetObject("ydBack.Image")));
            this.ydBack.Location = new System.Drawing.Point(12, 12);
            this.ydBack.Name = "ydBack";
            this.ydBack.Size = new System.Drawing.Size(34, 34);
            this.ydBack.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.ydBack.TabIndex = 77;
            this.ydBack.TabStop = false;
            this.ydBack.Click += new System.EventHandler(this.ydBack_Click);
            // 
            // payYD
            // 
            this.payYD.FlatAppearance.BorderSize = 0;
            this.payYD.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.payYD.Font = new System.Drawing.Font("Verdana", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.payYD.ForeColor = System.Drawing.Color.Red;
            this.payYD.Location = new System.Drawing.Point(106, 374);
            this.payYD.Name = "payYD";
            this.payYD.Size = new System.Drawing.Size(191, 72);
            this.payYD.TabIndex = 76;
            this.payYD.Text = "Пополнить";
            this.payYD.UseVisualStyleBackColor = true;
            this.payYD.Click += new System.EventHandler(this.payYD_Click);
            // 
            // pictureBox9
            // 
            this.pictureBox9.BackColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.pictureBox9.Location = new System.Drawing.Point(128, 326);
            this.pictureBox9.Name = "pictureBox9";
            this.pictureBox9.Size = new System.Drawing.Size(148, 2);
            this.pictureBox9.TabIndex = 75;
            this.pictureBox9.TabStop = false;
            // 
            // ydAmount
            // 
            this.ydAmount.BackColor = System.Drawing.SystemColors.Control;
            this.ydAmount.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.ydAmount.Font = new System.Drawing.Font("Microsoft Sans Serif", 20.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.ydAmount.ForeColor = System.Drawing.Color.Gray;
            this.ydAmount.Location = new System.Drawing.Point(129, 293);
            this.ydAmount.Name = "ydAmount";
            this.ydAmount.Size = new System.Drawing.Size(146, 31);
            this.ydAmount.TabIndex = 74;
            this.ydAmount.TabStop = false;
            this.ydAmount.Text = "Сумма";
            this.ydAmount.Click += new System.EventHandler(this.ydAmount_Click);
            this.ydAmount.TextChanged += new System.EventHandler(this.ydAmount_TextChanged);
            this.ydAmount.Leave += new System.EventHandler(this.ydAmount_Leave);
            // 
            // pictureBox10
            // 
            this.pictureBox10.BackColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.pictureBox10.Location = new System.Drawing.Point(99, 253);
            this.pictureBox10.Name = "pictureBox10";
            this.pictureBox10.Size = new System.Drawing.Size(206, 2);
            this.pictureBox10.TabIndex = 73;
            this.pictureBox10.TabStop = false;
            // 
            // ydWallet
            // 
            this.ydWallet.BackColor = System.Drawing.SystemColors.Control;
            this.ydWallet.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.ydWallet.Font = new System.Drawing.Font("Microsoft Sans Serif", 20.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.ydWallet.ForeColor = System.Drawing.Color.Gray;
            this.ydWallet.Location = new System.Drawing.Point(100, 221);
            this.ydWallet.Name = "ydWallet";
            this.ydWallet.Size = new System.Drawing.Size(204, 31);
            this.ydWallet.TabIndex = 72;
            this.ydWallet.TabStop = false;
            this.ydWallet.Text = "ID";
            this.ydWallet.Click += new System.EventHandler(this.ydWallet_Click);
            this.ydWallet.TextChanged += new System.EventHandler(this.ydWallet_TextChanged);
            this.ydWallet.Leave += new System.EventHandler(this.ydWallet_Leave);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Verdana", 14.25F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label5.Location = new System.Drawing.Point(111, 172);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(181, 23);
            this.label5.TabIndex = 71;
            this.label5.Text = "Введите кошелек";
            // 
            // pictureBox11
            // 
            this.pictureBox11.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pictureBox11.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox11.Image")));
            this.pictureBox11.Location = new System.Drawing.Point(162, 66);
            this.pictureBox11.Name = "pictureBox11";
            this.pictureBox11.Size = new System.Drawing.Size(80, 80);
            this.pictureBox11.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox11.TabIndex = 70;
            this.pictureBox11.TabStop = false;
            // 
            // skrill
            // 
            this.skrill.Controls.Add(this.skExit);
            this.skrill.Controls.Add(this.skBack);
            this.skrill.Controls.Add(this.skPay);
            this.skrill.Controls.Add(this.pictureBox12);
            this.skrill.Controls.Add(this.skAmount);
            this.skrill.Controls.Add(this.pictureBox13);
            this.skrill.Controls.Add(this.skWallet);
            this.skrill.Controls.Add(this.label6);
            this.skrill.Controls.Add(this.pictureBox14);
            this.skrill.Dock = System.Windows.Forms.DockStyle.Fill;
            this.skrill.Location = new System.Drawing.Point(0, 0);
            this.skrill.Name = "skrill";
            this.skrill.Size = new System.Drawing.Size(404, 561);
            this.skrill.TabIndex = 64;
            // 
            // skExit
            // 
            this.skExit.Cursor = System.Windows.Forms.Cursors.Hand;
            this.skExit.Image = ((System.Drawing.Image)(resources.GetObject("skExit.Image")));
            this.skExit.Location = new System.Drawing.Point(358, 12);
            this.skExit.Name = "skExit";
            this.skExit.Size = new System.Drawing.Size(34, 34);
            this.skExit.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.skExit.TabIndex = 87;
            this.skExit.TabStop = false;
            this.skExit.Click += new System.EventHandler(this.skExit_Click);
            // 
            // skBack
            // 
            this.skBack.Cursor = System.Windows.Forms.Cursors.Hand;
            this.skBack.Image = ((System.Drawing.Image)(resources.GetObject("skBack.Image")));
            this.skBack.Location = new System.Drawing.Point(12, 12);
            this.skBack.Name = "skBack";
            this.skBack.Size = new System.Drawing.Size(34, 34);
            this.skBack.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.skBack.TabIndex = 86;
            this.skBack.TabStop = false;
            this.skBack.Click += new System.EventHandler(this.skBack_Click);
            // 
            // skPay
            // 
            this.skPay.FlatAppearance.BorderSize = 0;
            this.skPay.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.skPay.Font = new System.Drawing.Font("Verdana", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.skPay.ForeColor = System.Drawing.Color.Red;
            this.skPay.Location = new System.Drawing.Point(106, 374);
            this.skPay.Name = "skPay";
            this.skPay.Size = new System.Drawing.Size(191, 72);
            this.skPay.TabIndex = 85;
            this.skPay.Text = "Пополнить";
            this.skPay.UseVisualStyleBackColor = true;
            this.skPay.Click += new System.EventHandler(this.skPay_Click);
            // 
            // pictureBox12
            // 
            this.pictureBox12.BackColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.pictureBox12.Location = new System.Drawing.Point(128, 326);
            this.pictureBox12.Name = "pictureBox12";
            this.pictureBox12.Size = new System.Drawing.Size(148, 2);
            this.pictureBox12.TabIndex = 84;
            this.pictureBox12.TabStop = false;
            // 
            // skAmount
            // 
            this.skAmount.BackColor = System.Drawing.SystemColors.Control;
            this.skAmount.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.skAmount.Font = new System.Drawing.Font("Microsoft Sans Serif", 20.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.skAmount.ForeColor = System.Drawing.Color.Gray;
            this.skAmount.Location = new System.Drawing.Point(129, 293);
            this.skAmount.Name = "skAmount";
            this.skAmount.Size = new System.Drawing.Size(146, 31);
            this.skAmount.TabIndex = 83;
            this.skAmount.TabStop = false;
            this.skAmount.Text = "Сумма";
            this.skAmount.Click += new System.EventHandler(this.skAmount_Click);
            this.skAmount.TextChanged += new System.EventHandler(this.skAmount_TextChanged);
            this.skAmount.Leave += new System.EventHandler(this.skAmount_Leave);
            // 
            // pictureBox13
            // 
            this.pictureBox13.BackColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.pictureBox13.Location = new System.Drawing.Point(99, 253);
            this.pictureBox13.Name = "pictureBox13";
            this.pictureBox13.Size = new System.Drawing.Size(206, 2);
            this.pictureBox13.TabIndex = 82;
            this.pictureBox13.TabStop = false;
            // 
            // skWallet
            // 
            this.skWallet.BackColor = System.Drawing.SystemColors.Control;
            this.skWallet.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.skWallet.Font = new System.Drawing.Font("Microsoft Sans Serif", 20.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.skWallet.ForeColor = System.Drawing.Color.Gray;
            this.skWallet.Location = new System.Drawing.Point(100, 221);
            this.skWallet.Name = "skWallet";
            this.skWallet.Size = new System.Drawing.Size(204, 31);
            this.skWallet.TabIndex = 81;
            this.skWallet.TabStop = false;
            this.skWallet.Text = "ID";
            this.skWallet.Click += new System.EventHandler(this.skWallet_Click);
            this.skWallet.TextChanged += new System.EventHandler(this.skWallet_TextChanged);
            this.skWallet.Leave += new System.EventHandler(this.skWallet_Leave);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Verdana", 14.25F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label6.Location = new System.Drawing.Point(111, 172);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(181, 23);
            this.label6.TabIndex = 80;
            this.label6.Text = "Введите кошелек";
            // 
            // pictureBox14
            // 
            this.pictureBox14.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pictureBox14.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox14.Image")));
            this.pictureBox14.Location = new System.Drawing.Point(162, 66);
            this.pictureBox14.Name = "pictureBox14";
            this.pictureBox14.Size = new System.Drawing.Size(80, 80);
            this.pictureBox14.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox14.TabIndex = 79;
            this.pictureBox14.TabStop = false;
            // 
            // OnlinePayments
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(404, 561);
            this.Controls.Add(this.skrill);
            this.Controls.Add(this.yandexMoney);
            this.Controls.Add(this.qiwi);
            this.Controls.Add(this.webmoney);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.yandexmoneyPic);
            this.Controls.Add(this.skrillPic);
            this.Controls.Add(this.qiwiPic);
            this.Controls.Add(this.webmoneyPic);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.exitBut);
            this.Controls.Add(this.backBut);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "OnlinePayments";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Пополнение онлайн кошельков";
            this.TopMost = true;
            this.Load += new System.EventHandler(this.OnlinePayments_Load);
            ((System.ComponentModel.ISupportInitialize)(this.backBut)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.exitBut)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.yandexmoneyPic)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.skrillPic)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.qiwiPic)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.webmoneyPic)).EndInit();
            this.webmoney.ResumeLayout(false);
            this.webmoney.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.wmExit)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.wmBack)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.qiwi.ResumeLayout(false);
            this.qiwi.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.qiwiExit)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.qiwiBack)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox8)).EndInit();
            this.yandexMoney.ResumeLayout(false);
            this.yandexMoney.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ydExit)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ydBack)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox11)).EndInit();
            this.skrill.ResumeLayout(false);
            this.skrill.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.skExit)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.skBack)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox12)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox13)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox14)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.PictureBox backBut;
        private System.Windows.Forms.PictureBox exitBut;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.PictureBox yandexmoneyPic;
        private System.Windows.Forms.PictureBox skrillPic;
        private System.Windows.Forms.PictureBox qiwiPic;
        private System.Windows.Forms.PictureBox webmoneyPic;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Panel webmoney;
        private System.Windows.Forms.PictureBox wmExit;
        private System.Windows.Forms.PictureBox wmBack;
        public System.Windows.Forms.Button wmPay;
        private System.Windows.Forms.PictureBox pictureBox2;
        public System.Windows.Forms.TextBox wmAmount;
        private System.Windows.Forms.PictureBox pictureBox3;
        private System.Windows.Forms.TextBox wmWallet;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Panel qiwi;
        private System.Windows.Forms.PictureBox qiwiExit;
        private System.Windows.Forms.PictureBox qiwiBack;
        public System.Windows.Forms.Button qiwiPay;
        private System.Windows.Forms.PictureBox pictureBox6;
        public System.Windows.Forms.TextBox qiwiAmount;
        private System.Windows.Forms.PictureBox pictureBox7;
        private System.Windows.Forms.TextBox qiwiWallet;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.PictureBox pictureBox8;
        private System.Windows.Forms.Panel yandexMoney;
        private System.Windows.Forms.PictureBox ydExit;
        private System.Windows.Forms.PictureBox ydBack;
        public System.Windows.Forms.Button payYD;
        private System.Windows.Forms.PictureBox pictureBox9;
        public System.Windows.Forms.TextBox ydAmount;
        private System.Windows.Forms.PictureBox pictureBox10;
        private System.Windows.Forms.TextBox ydWallet;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.PictureBox pictureBox11;
        private System.Windows.Forms.Panel skrill;
        private System.Windows.Forms.PictureBox skExit;
        private System.Windows.Forms.PictureBox skBack;
        public System.Windows.Forms.Button skPay;
        private System.Windows.Forms.PictureBox pictureBox12;
        public System.Windows.Forms.TextBox skAmount;
        private System.Windows.Forms.PictureBox pictureBox13;
        private System.Windows.Forms.TextBox skWallet;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.PictureBox pictureBox14;
    }
}