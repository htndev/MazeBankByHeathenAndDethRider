﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Timers;
using System.Data.SqlClient;
using AuthClass;
using WinNotification;
using System.Text.RegularExpressions;

namespace MazeBank
{
    public partial class ForgottenPassFromAuth : Form
    {
        public ForgottenPassFromAuth()
        {
            InitializeComponent();
        }

        #region Переменные

        GetSafeCode getSafeCode = new GetSafeCode();

        Auth auth = new Auth();

        UserEntering userAuth = new UserEntering();

        public int safeCodeGetter;

        long phoneNum;
        #endregion Переменные

        private void ForgottenPassFromAuth_Load(object sender, EventArgs e)
        {
            confirmationPanel.Hide();
            hidingPhoneVerification.Show();
            this.KeyPreview = true;
        }

        private void exitBut_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void backBut_Click(object sender, EventArgs e)
        {
            ForgottenPassFromAuth forgotten = new ForgottenPassFromAuth();
            this.Hide();
            getSafeCode.Hide();
            auth.Show();
        }

        private void submitCode_Click(object sender, EventArgs e)
        {
            getSafeCode.Hide();
            this.Visible = false;
            LoadingForm loading = new LoadingForm();
            loading.ShowDialog();
            this.Visible = true;
            int code = Int32.Parse(enterCode.Text);
            if (code == safeCodeGetter)
            {
                hidingPhoneVerification.Hide();
                confirmationPanel.Show();
            }
            else
            {
                getSafeCode.Show();
                MessageBox.Show("Введенный код неверен!");
                return;
            }

        }

        private void verifyPhn_Click(object sender, EventArgs e)
        {
            phoneNum = long.Parse(fgtPhoneNum.Text);
            
            userAuth.UserAuthing(phoneNum);
            if (userAuth.counter == 1)
            {
                hidingPhoneVerification.Hide();
                this.Visible = false;
                LoadingForm loading = new LoadingForm();
                loading.ShowDialog();
                this.Visible = true;
                getSafeCode.Show();
                safeCodeGetter = int.Parse(getSafeCode.safeCode.Text);
                getSafeCode.SetDesktopLocation(this.Location.X + this.Size.Width, this.Location.Y - 30);
            }
        }

        private void submitChanges_Click(object sender, EventArgs e)
        {
            if (newPassword.Text == newPasswordConfirm.Text && Regex.IsMatch(newPassword.Text, @"^(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{5,20}$", RegexOptions.Compiled) && Regex.IsMatch(newPasswordConfirm.Text, @"^(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{5,20}$", RegexOptions.Compiled))
            {
                string newPass = newPassword.Text;
                phoneNum = long.Parse(fgtPhoneNum.Text);
                userAuth.UpdateUserInfo(newPass, phoneNum);
                this.Hide();
                LoadingForm loadingForm = new LoadingForm();
                loadingForm.Show();
                WinNotify.ShowWinNotify("Изменение пароля", "Ваш пароль успешно изменен!", 5000);
                auth.Show();
            }
            else
            {
                MessageBox.Show("Вы ввели неверный формат пароля! Попробуйте снова.");
                return;
            }
        }

        private void ForgottenPassFromAuth_KeyDown(object sender, KeyEventArgs e)
        {
            
        }

        private void submitChanges_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter && hidingPhoneVerification.Visible == false && confirmationPanel.Visible == true)
            {
                submitChanges.PerformClick();
            }
        }

        private void submitCode_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter && hidingPhoneVerification.Visible == false && confirmationPanel.Visible == false)
            {
                submitCode.PerformClick();
            }
        }

        private void verifyPhn_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter&& hidingPhoneVerification.Visible == true && confirmationPanel.Visible == false)
            {
                verifyPhn.PerformClick();
            }
        }
    }
}
