﻿namespace MazeBank
{
    partial class RegistrationForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(RegistrationForm));
            this.label14 = new System.Windows.Forms.Label();
            this.backToSettingsPhone = new System.Windows.Forms.PictureBox();
            this.exit = new System.Windows.Forms.PictureBox();
            this.label13 = new System.Windows.Forms.Label();
            this.pictureBox14 = new System.Windows.Forms.PictureBox();
            this.newPhone = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.newSurname = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            this.newCard = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.pictureBox3 = new System.Windows.Forms.PictureBox();
            this.newName = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.pictureBox4 = new System.Windows.Forms.PictureBox();
            this.newEmail = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.pictureBox5 = new System.Windows.Forms.PictureBox();
            this.newPatronymic = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.pictureBox6 = new System.Windows.Forms.PictureBox();
            this.newAdress = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.pictureBox7 = new System.Windows.Forms.PictureBox();
            this.newPassword = new System.Windows.Forms.TextBox();
            this.createAccount = new System.Windows.Forms.Button();
            this.newPhoneCheck = new System.Windows.Forms.PictureBox();
            this.newCardCheck = new System.Windows.Forms.PictureBox();
            this.newEmailCheck = new System.Windows.Forms.PictureBox();
            this.newPassCheck = new System.Windows.Forms.PictureBox();
            this.newSurnameCheck = new System.Windows.Forms.PictureBox();
            this.newNameCheck = new System.Windows.Forms.PictureBox();
            this.newPatronymicCheck = new System.Windows.Forms.PictureBox();
            this.newAddressCheck = new System.Windows.Forms.PictureBox();
            this.passVis = new System.Windows.Forms.PictureBox();
            this.correct = new System.Windows.Forms.ToolTip(this.components);
            ((System.ComponentModel.ISupportInitialize)(this.backToSettingsPhone)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.exit)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox14)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.newPhoneCheck)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.newCardCheck)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.newEmailCheck)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.newPassCheck)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.newSurnameCheck)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.newNameCheck)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.newPatronymicCheck)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.newAddressCheck)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.passVis)).BeginInit();
            this.SuspendLayout();
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Font = new System.Drawing.Font("Verdana", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label14.Location = new System.Drawing.Point(250, 12);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(281, 25);
            this.label14.TabIndex = 75;
            this.label14.Text = "Регистрация аккаунта";
            // 
            // backToSettingsPhone
            // 
            this.backToSettingsPhone.Cursor = System.Windows.Forms.Cursors.Hand;
            this.backToSettingsPhone.Image = ((System.Drawing.Image)(resources.GetObject("backToSettingsPhone.Image")));
            this.backToSettingsPhone.Location = new System.Drawing.Point(12, 12);
            this.backToSettingsPhone.Name = "backToSettingsPhone";
            this.backToSettingsPhone.Size = new System.Drawing.Size(34, 34);
            this.backToSettingsPhone.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.backToSettingsPhone.TabIndex = 74;
            this.backToSettingsPhone.TabStop = false;
            this.backToSettingsPhone.Click += new System.EventHandler(this.backToSettingsPhone_Click);
            // 
            // exit
            // 
            this.exit.Cursor = System.Windows.Forms.Cursors.Hand;
            this.exit.Image = ((System.Drawing.Image)(resources.GetObject("exit.Image")));
            this.exit.Location = new System.Drawing.Point(685, 12);
            this.exit.Name = "exit";
            this.exit.Size = new System.Drawing.Size(34, 34);
            this.exit.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.exit.TabIndex = 73;
            this.exit.TabStop = false;
            this.exit.Click += new System.EventHandler(this.exit_Click);
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Font = new System.Drawing.Font("Verdana", 14.25F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label13.Location = new System.Drawing.Point(82, 55);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(173, 23);
            this.label13.TabIndex = 83;
            this.label13.Text = "Номер телефона";
            // 
            // pictureBox14
            // 
            this.pictureBox14.BackColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.pictureBox14.Location = new System.Drawing.Point(47, 112);
            this.pictureBox14.Name = "pictureBox14";
            this.pictureBox14.Size = new System.Drawing.Size(231, 2);
            this.pictureBox14.TabIndex = 82;
            this.pictureBox14.TabStop = false;
            // 
            // newPhone
            // 
            this.newPhone.BackColor = System.Drawing.SystemColors.Control;
            this.newPhone.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.newPhone.Font = new System.Drawing.Font("Microsoft Sans Serif", 20.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.newPhone.ForeColor = System.Drawing.Color.Black;
            this.newPhone.Location = new System.Drawing.Point(48, 80);
            this.newPhone.Name = "newPhone";
            this.newPhone.Size = new System.Drawing.Size(229, 31);
            this.newPhone.TabIndex = 81;
            this.newPhone.TabStop = false;
            this.newPhone.Text = "380";
            this.newPhone.TextChanged += new System.EventHandler(this.newPhone_TextChanged);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Verdana", 14.25F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label1.Location = new System.Drawing.Point(532, 55);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(96, 23);
            this.label1.TabIndex = 86;
            this.label1.Text = "Фамилия";
            // 
            // pictureBox1
            // 
            this.pictureBox1.BackColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.pictureBox1.Location = new System.Drawing.Point(458, 113);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(231, 2);
            this.pictureBox1.TabIndex = 85;
            this.pictureBox1.TabStop = false;
            // 
            // newSurname
            // 
            this.newSurname.BackColor = System.Drawing.SystemColors.Control;
            this.newSurname.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.newSurname.Font = new System.Drawing.Font("Microsoft Sans Serif", 20.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.newSurname.ForeColor = System.Drawing.Color.Black;
            this.newSurname.Location = new System.Drawing.Point(459, 81);
            this.newSurname.Name = "newSurname";
            this.newSurname.Size = new System.Drawing.Size(229, 31);
            this.newSurname.TabIndex = 84;
            this.newSurname.TabStop = false;
            this.newSurname.TextChanged += new System.EventHandler(this.newSurname_TextChanged);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Verdana", 14.25F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label2.Location = new System.Drawing.Point(100, 157);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(137, 23);
            this.label2.TabIndex = 89;
            this.label2.Text = "Номер карты";
            // 
            // pictureBox2
            // 
            this.pictureBox2.BackColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.pictureBox2.Location = new System.Drawing.Point(46, 215);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new System.Drawing.Size(231, 2);
            this.pictureBox2.TabIndex = 88;
            this.pictureBox2.TabStop = false;
            // 
            // newCard
            // 
            this.newCard.BackColor = System.Drawing.SystemColors.Control;
            this.newCard.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.newCard.Font = new System.Drawing.Font("Microsoft Sans Serif", 20.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.newCard.ForeColor = System.Drawing.Color.Black;
            this.newCard.Location = new System.Drawing.Point(47, 183);
            this.newCard.Name = "newCard";
            this.newCard.Size = new System.Drawing.Size(229, 31);
            this.newCard.TabIndex = 87;
            this.newCard.TabStop = false;
            this.newCard.TextChanged += new System.EventHandler(this.newCard_TextChanged);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Verdana", 14.25F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label3.Location = new System.Drawing.Point(557, 157);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(48, 23);
            this.label3.TabIndex = 92;
            this.label3.Text = "Имя";
            // 
            // pictureBox3
            // 
            this.pictureBox3.BackColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.pictureBox3.Location = new System.Drawing.Point(458, 215);
            this.pictureBox3.Name = "pictureBox3";
            this.pictureBox3.Size = new System.Drawing.Size(231, 2);
            this.pictureBox3.TabIndex = 91;
            this.pictureBox3.TabStop = false;
            // 
            // newName
            // 
            this.newName.BackColor = System.Drawing.SystemColors.Control;
            this.newName.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.newName.Font = new System.Drawing.Font("Microsoft Sans Serif", 20.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.newName.ForeColor = System.Drawing.Color.Black;
            this.newName.Location = new System.Drawing.Point(459, 183);
            this.newName.Name = "newName";
            this.newName.Size = new System.Drawing.Size(229, 31);
            this.newName.TabIndex = 90;
            this.newName.TabStop = false;
            this.newName.TextChanged += new System.EventHandler(this.newName_TextChanged);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Verdana", 14.25F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label4.Location = new System.Drawing.Point(64, 254);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(198, 23);
            this.label4.TabIndex = 95;
            this.label4.Text = "Електронная почта";
            // 
            // pictureBox4
            // 
            this.pictureBox4.BackColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.pictureBox4.Location = new System.Drawing.Point(46, 312);
            this.pictureBox4.Name = "pictureBox4";
            this.pictureBox4.Size = new System.Drawing.Size(231, 2);
            this.pictureBox4.TabIndex = 94;
            this.pictureBox4.TabStop = false;
            // 
            // newEmail
            // 
            this.newEmail.BackColor = System.Drawing.SystemColors.Control;
            this.newEmail.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.newEmail.Font = new System.Drawing.Font("Microsoft Sans Serif", 20.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.newEmail.ForeColor = System.Drawing.Color.Black;
            this.newEmail.Location = new System.Drawing.Point(47, 280);
            this.newEmail.Name = "newEmail";
            this.newEmail.Size = new System.Drawing.Size(229, 31);
            this.newEmail.TabIndex = 93;
            this.newEmail.TabStop = false;
            this.newEmail.TextChanged += new System.EventHandler(this.newEmail_TextChanged);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Verdana", 14.25F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label5.Location = new System.Drawing.Point(532, 254);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(99, 23);
            this.label5.TabIndex = 98;
            this.label5.Text = "Отчество";
            // 
            // pictureBox5
            // 
            this.pictureBox5.BackColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.pictureBox5.Location = new System.Drawing.Point(458, 312);
            this.pictureBox5.Name = "pictureBox5";
            this.pictureBox5.Size = new System.Drawing.Size(231, 2);
            this.pictureBox5.TabIndex = 97;
            this.pictureBox5.TabStop = false;
            // 
            // newPatronymic
            // 
            this.newPatronymic.BackColor = System.Drawing.SystemColors.Control;
            this.newPatronymic.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.newPatronymic.Font = new System.Drawing.Font("Microsoft Sans Serif", 20.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.newPatronymic.ForeColor = System.Drawing.Color.Black;
            this.newPatronymic.Location = new System.Drawing.Point(459, 280);
            this.newPatronymic.Name = "newPatronymic";
            this.newPatronymic.Size = new System.Drawing.Size(229, 31);
            this.newPatronymic.TabIndex = 96;
            this.newPatronymic.TabStop = false;
            this.newPatronymic.TextChanged += new System.EventHandler(this.newPatronymic_TextChanged);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Verdana", 14.25F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label6.Location = new System.Drawing.Point(483, 349);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(190, 23);
            this.label6.TabIndex = 104;
            this.label6.Text = "Физический адрес";
            // 
            // pictureBox6
            // 
            this.pictureBox6.BackColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.pictureBox6.Location = new System.Drawing.Point(458, 407);
            this.pictureBox6.Name = "pictureBox6";
            this.pictureBox6.Size = new System.Drawing.Size(231, 2);
            this.pictureBox6.TabIndex = 103;
            this.pictureBox6.TabStop = false;
            // 
            // newAdress
            // 
            this.newAdress.BackColor = System.Drawing.SystemColors.Control;
            this.newAdress.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.newAdress.Font = new System.Drawing.Font("Microsoft Sans Serif", 20.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.newAdress.ForeColor = System.Drawing.Color.Black;
            this.newAdress.Location = new System.Drawing.Point(459, 375);
            this.newAdress.Name = "newAdress";
            this.newAdress.Size = new System.Drawing.Size(229, 31);
            this.newAdress.TabIndex = 102;
            this.newAdress.TabStop = false;
            this.newAdress.TextChanged += new System.EventHandler(this.newAdress_TextChanged);
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Verdana", 14.25F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label7.Location = new System.Drawing.Point(127, 349);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(82, 23);
            this.label7.TabIndex = 101;
            this.label7.Text = "Пароль";
            // 
            // pictureBox7
            // 
            this.pictureBox7.BackColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.pictureBox7.Location = new System.Drawing.Point(46, 407);
            this.pictureBox7.Name = "pictureBox7";
            this.pictureBox7.Size = new System.Drawing.Size(231, 2);
            this.pictureBox7.TabIndex = 100;
            this.pictureBox7.TabStop = false;
            // 
            // newPassword
            // 
            this.newPassword.BackColor = System.Drawing.SystemColors.Control;
            this.newPassword.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.newPassword.Font = new System.Drawing.Font("Microsoft Sans Serif", 20.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.newPassword.ForeColor = System.Drawing.Color.Black;
            this.newPassword.Location = new System.Drawing.Point(47, 375);
            this.newPassword.Name = "newPassword";
            this.newPassword.Size = new System.Drawing.Size(229, 31);
            this.newPassword.TabIndex = 99;
            this.newPassword.TabStop = false;
            this.correct.SetToolTip(this.newPassword, "Пароль должен иметь хотя бы одну заглавную букву, строчную букву, цифру , минимум" +
        " 5 символов");
            this.newPassword.TextChanged += new System.EventHandler(this.newPassword_TextChanged);
            // 
            // createAccount
            // 
            this.createAccount.FlatAppearance.BorderSize = 0;
            this.createAccount.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.createAccount.Font = new System.Drawing.Font("Verdana", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.createAccount.ForeColor = System.Drawing.Color.Red;
            this.createAccount.Location = new System.Drawing.Point(288, 419);
            this.createAccount.Name = "createAccount";
            this.createAccount.Size = new System.Drawing.Size(158, 77);
            this.createAccount.TabIndex = 105;
            this.createAccount.Text = "Создать";
            this.createAccount.UseVisualStyleBackColor = true;
            this.createAccount.Click += new System.EventHandler(this.createAccount_Click);
            // 
            // newPhoneCheck
            // 
            this.newPhoneCheck.Location = new System.Drawing.Point(288, 80);
            this.newPhoneCheck.Name = "newPhoneCheck";
            this.newPhoneCheck.Size = new System.Drawing.Size(30, 30);
            this.newPhoneCheck.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.newPhoneCheck.TabIndex = 106;
            this.newPhoneCheck.TabStop = false;
            // 
            // newCardCheck
            // 
            this.newCardCheck.Location = new System.Drawing.Point(288, 183);
            this.newCardCheck.Name = "newCardCheck";
            this.newCardCheck.Size = new System.Drawing.Size(30, 30);
            this.newCardCheck.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.newCardCheck.TabIndex = 107;
            this.newCardCheck.TabStop = false;
            // 
            // newEmailCheck
            // 
            this.newEmailCheck.Location = new System.Drawing.Point(288, 280);
            this.newEmailCheck.Name = "newEmailCheck";
            this.newEmailCheck.Size = new System.Drawing.Size(30, 30);
            this.newEmailCheck.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.newEmailCheck.TabIndex = 108;
            this.newEmailCheck.TabStop = false;
            // 
            // newPassCheck
            // 
            this.newPassCheck.Location = new System.Drawing.Point(288, 375);
            this.newPassCheck.Name = "newPassCheck";
            this.newPassCheck.Size = new System.Drawing.Size(30, 30);
            this.newPassCheck.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.newPassCheck.TabIndex = 109;
            this.newPassCheck.TabStop = false;
            // 
            // newSurnameCheck
            // 
            this.newSurnameCheck.Location = new System.Drawing.Point(694, 80);
            this.newSurnameCheck.Name = "newSurnameCheck";
            this.newSurnameCheck.Size = new System.Drawing.Size(30, 30);
            this.newSurnameCheck.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.newSurnameCheck.TabIndex = 110;
            this.newSurnameCheck.TabStop = false;
            // 
            // newNameCheck
            // 
            this.newNameCheck.Location = new System.Drawing.Point(694, 183);
            this.newNameCheck.Name = "newNameCheck";
            this.newNameCheck.Size = new System.Drawing.Size(30, 30);
            this.newNameCheck.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.newNameCheck.TabIndex = 111;
            this.newNameCheck.TabStop = false;
            // 
            // newPatronymicCheck
            // 
            this.newPatronymicCheck.Location = new System.Drawing.Point(694, 280);
            this.newPatronymicCheck.Name = "newPatronymicCheck";
            this.newPatronymicCheck.Size = new System.Drawing.Size(30, 30);
            this.newPatronymicCheck.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.newPatronymicCheck.TabIndex = 112;
            this.newPatronymicCheck.TabStop = false;
            // 
            // newAddressCheck
            // 
            this.newAddressCheck.Location = new System.Drawing.Point(694, 375);
            this.newAddressCheck.Name = "newAddressCheck";
            this.newAddressCheck.Size = new System.Drawing.Size(30, 30);
            this.newAddressCheck.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.newAddressCheck.TabIndex = 113;
            this.newAddressCheck.TabStop = false;
            // 
            // passVis
            // 
            this.passVis.Cursor = System.Windows.Forms.Cursors.Hand;
            this.passVis.Image = ((System.Drawing.Image)(resources.GetObject("passVis.Image")));
            this.passVis.Location = new System.Drawing.Point(1, 375);
            this.passVis.Name = "passVis";
            this.passVis.Size = new System.Drawing.Size(41, 41);
            this.passVis.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.passVis.TabIndex = 114;
            this.passVis.TabStop = false;
            this.passVis.Click += new System.EventHandler(this.passVis_Click);
            // 
            // correct
            // 
            this.correct.IsBalloon = true;
            this.correct.ToolTipTitle = "Подсказка";
            // 
            // RegistrationForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(734, 508);
            this.Controls.Add(this.passVis);
            this.Controls.Add(this.newAddressCheck);
            this.Controls.Add(this.newPatronymicCheck);
            this.Controls.Add(this.newNameCheck);
            this.Controls.Add(this.newSurnameCheck);
            this.Controls.Add(this.newPassCheck);
            this.Controls.Add(this.newEmailCheck);
            this.Controls.Add(this.newCardCheck);
            this.Controls.Add(this.newPhoneCheck);
            this.Controls.Add(this.createAccount);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.pictureBox6);
            this.Controls.Add(this.newAdress);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.pictureBox7);
            this.Controls.Add(this.newPassword);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.pictureBox5);
            this.Controls.Add(this.newPatronymic);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.pictureBox4);
            this.Controls.Add(this.newEmail);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.pictureBox3);
            this.Controls.Add(this.newName);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.pictureBox2);
            this.Controls.Add(this.newCard);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.newSurname);
            this.Controls.Add(this.label13);
            this.Controls.Add(this.pictureBox14);
            this.Controls.Add(this.newPhone);
            this.Controls.Add(this.label14);
            this.Controls.Add(this.backToSettingsPhone);
            this.Controls.Add(this.exit);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "RegistrationForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Форма регистрации";
            ((System.ComponentModel.ISupportInitialize)(this.backToSettingsPhone)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.exit)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox14)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.newPhoneCheck)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.newCardCheck)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.newEmailCheck)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.newPassCheck)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.newSurnameCheck)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.newNameCheck)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.newPatronymicCheck)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.newAddressCheck)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.passVis)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.PictureBox backToSettingsPhone;
        private System.Windows.Forms.PictureBox exit;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.PictureBox pictureBox14;
        private System.Windows.Forms.TextBox newPhone;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.TextBox newSurname;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.PictureBox pictureBox2;
        private System.Windows.Forms.TextBox newCard;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.PictureBox pictureBox3;
        private System.Windows.Forms.TextBox newName;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.PictureBox pictureBox4;
        private System.Windows.Forms.TextBox newEmail;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.PictureBox pictureBox5;
        private System.Windows.Forms.TextBox newPatronymic;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.PictureBox pictureBox6;
        private System.Windows.Forms.TextBox newAdress;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.PictureBox pictureBox7;
        private System.Windows.Forms.TextBox newPassword;
        private System.Windows.Forms.Button createAccount;
        private System.Windows.Forms.PictureBox newPhoneCheck;
        private System.Windows.Forms.PictureBox newCardCheck;
        private System.Windows.Forms.PictureBox newEmailCheck;
        private System.Windows.Forms.PictureBox newPassCheck;
        private System.Windows.Forms.PictureBox newSurnameCheck;
        private System.Windows.Forms.PictureBox newNameCheck;
        private System.Windows.Forms.PictureBox newPatronymicCheck;
        private System.Windows.Forms.PictureBox newAddressCheck;
        private System.Windows.Forms.PictureBox passVis;
        private System.Windows.Forms.ToolTip correct;
    }
}