﻿using System;
using System.Drawing;
using System.Windows.Forms;
using System.Data.SqlClient;
using System.Text.RegularExpressions;
using AuthClass;
using WinNotification;

namespace MazeBank
{
    public partial class Auth : Form
    {
        public Auth()
        {
            InitializeComponent();
        }

        #region Переменные

        int logCount = 0;
        
        public long tryLog;

        UserEntering user = new UserEntering();

        int logBoxClicked = 0;

        int logChanged = 0;

        int passBoxClicked = 0;

        int passChanged = 0;

        int visCounter = 1;

        #endregion Переменные

        private void Auth_Load(object sender, EventArgs e)
        {
            this.KeyPreview = true;
        }

        private void authLogin_TextChanged(object sender, EventArgs e)
        {
            authLogin.MaxLength = 12;
        }

        private void authLogin_Click(object sender, EventArgs e)
        {
            if (authLogin.Text != null && logBoxClicked == 0)
            {
                authLogin.Clear();
                authLogin.ForeColor = Color.Black;
                logBoxClicked++;
            }

            else if (authLogin.Text != null && logChanged != 0)
            {
                authLogin.Clear();
                authLogin.ForeColor = Color.Black;
            }
        }

        private void authPass_Click(object sender, EventArgs e)
        {
            if (authPass.Text != null && passBoxClicked == 0)
            {
                authPass.Clear();
                authPass.UseSystemPasswordChar = true;
                authPass.ForeColor = Color.Black;
                passBoxClicked++;
            }

            else if (authPass.Text != null && passChanged != 0)
            {
                authPass.Clear();
                authPass.UseSystemPasswordChar = true;
                authPass.ForeColor = Color.Black;
            }

        }

        private void exitBut_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void authLogin_Leave(object sender, EventArgs e)
        {
            logChanged = 0;
            if (authLogin.Text == "")
            {
                authLogin.Text = "Введите телефон";
                authLogin.ForeColor = Color.Gray;
                logChanged++;
            }
            string phonePattern = @"^380(3|[5-6]|9)[0-9]\d{3}\d{2}\d{2}$";
            string value = authLogin.Text;
            Regex phoneValid = new Regex(phonePattern);
            if (!Regex.IsMatch(value, phonePattern, RegexOptions.Compiled) && logCount <= 3)
            {
                logBoxClicked--;
                logCount++;
                authLogin.Clear();
                authLogin.ForeColor = Color.Gray;
                authLogin.Text = "Введите телефон";
                MessageBox.Show("Извините, вы ввели некорректный телефон!\nПопробуйте еще раз!", "Ошибка ауетентификации", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }

            else if(logCount > 3)
            {
                logBoxClicked--;
                logCount++;
                authLogin.Clear();
                authLogin.ForeColor = Color.Gray;
                authLogin.Text = "Введите телефон";
                WinNotify.ShowWinNotify("Ошибка ввода телефона", "Вы вводите неправильный формат номера телефона.\nФормат телефона содержит 12 символов: 380123456789", 10000);
            }
        }

        private void authPass_Leave(object sender, EventArgs e)
        {
            passChanged = 0;
            if (authPass.Text == "")
            {
                authPass.UseSystemPasswordChar = false;
                authPass.ForeColor = Color.Gray;
                authPass.Text = "Введите пароль";
                passChanged++;
            }
        }

        private void LogIn_Click(object sender, EventArgs e)
        {
            if(Regex.IsMatch(authLogin.Text, @"^380(3|[5-6]|9)[0-9]\d{3}\d{2}\d{2}$", RegexOptions.Compiled))
            {
                LogIn.ForeColor = Color.FromArgb(127, 10, 10);
                LogIn.BackColor = Color.FromArgb(240, 240, 240);
                tryLog = long.Parse(authLogin.Text);
                string tryPass = authPass.Text;
                this.Visible = false;
                LoadingForm loadingForm = new LoadingForm();
                loadingForm.Show();
                try
                {
                    user.UserAuthing(tryLog, tryPass);
                    if (user.counter != 0)
                    {
                        MainMenu mainMenu = new MainMenu();
                        this.Hide();
                        mainMenu.login = tryLog;
                        mainMenu.Show();
                    }

                    else if (user.counter == 0)
                    {
                        this.Visible = true;
                        MessageBox.Show("Вы ввели неправильные данные! Проверьте и введите еще раз!", "Ошибка аутентификации", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        return;
                    }
                }

                catch (FormatException)
                {
                    LogIn.ForeColor = Color.FromArgb(255, 0, 0);
                    MessageBox.Show("Вы ввели неправильные данные! Проверьте и введите еще раз!", "Ошибка аутентификации", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
            else
            {
                MessageBox.Show("Вы ввели некрректные значения!", "Ошибка формата", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
        }

        private void LogIn_KeyDown(object sender, KeyEventArgs e)
        {
            if(e.KeyCode == Keys.Enter)
            {
                LogIn.PerformClick();
            }
        }

        private void fgtPass_Click(object sender, EventArgs e)
        {
            ForgottenPassFromAuth forgottenPassFromAuth = new ForgottenPassFromAuth();
            forgottenPassFromAuth.Show();
            this.Hide();
        }

        private void LogIn_MouseHover(object sender, EventArgs e)
        {
            LogIn.ForeColor = Color.FromArgb(183, 20, 20);
        }

        private void authLogin_KeyDown(object sender, KeyEventArgs e)
        {
            if(e.KeyCode == Keys.Down)
            {
                authPass.Focus();
                if(authPass != null && passBoxClicked == 0)
                {
                    authPass.Clear();
                    authPass.ForeColor = Color.Black;
                    authPass.UseSystemPasswordChar = true;
                    passBoxClicked++;
                }

                else if(authPass != null && passChanged != 0)
                {
                    authPass.Clear();
                    authPass.ForeColor = Color.Black;
                    authPass.UseSystemPasswordChar = true;
                }
                
            }
        }

        private void authPass_KeyDown(object sender, KeyEventArgs e)
        {
            if(e.KeyCode == Keys.Up)
            {
                authLogin.Focus();
                if (authLogin.Text != null && logBoxClicked == 0)
                {
                    authLogin.Clear();
                    authLogin.ForeColor = Color.Black;
                    logBoxClicked++;
                }

                else if (authLogin.Text != null && logChanged != 0)
                {
                    authLogin.Clear();
                    authLogin.ForeColor = Color.Black;
                }
            }
        }

        private void Auth_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Escape)
            {
                DialogResult dialogResult = MessageBox.Show("Вы уверены, что хотите завершить работу в программе?", "Выход из программы", MessageBoxButtons.YesNo, MessageBoxIcon.Warning);
                if(dialogResult == DialogResult.Yes)
                {
                    Application.Exit();
                }
            }
        }

        private void passVisibility_Click(object sender, EventArgs e)
        {
            if (visCounter % 2 == 1)
            {
                passVisibility.Load(@"C:\Users\aleks\source\repos\MazeBank\Eyes\openedEye.png");
                authPass.UseSystemPasswordChar = false;
                visCounter++;
            }

            else if(visCounter % 2 == 0)
            {
                passVisibility.Load(@"C:\Users\aleks\source\repos\MazeBank\Eyes\closedEye.png");
                authPass.UseSystemPasswordChar = true;
                visCounter--;
            }
        }

        private void registration_Click(object sender, EventArgs e)
        {
            RegistrationForm registrationForm = new RegistrationForm();
           Hide();
            registrationForm.Show();
        }
    }
}
