﻿namespace MazeBank
{
    partial class Auth
    {
        /// <summary>
        /// Обязательная переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Требуемый метод для поддержки конструктора — не изменяйте 
        /// содержимое этого метода с помощью редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Auth));
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.label1 = new System.Windows.Forms.Label();
            this.LoginInfo = new System.Windows.Forms.ToolTip(this.components);
            this.authLogin = new System.Windows.Forms.TextBox();
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            this.label2 = new System.Windows.Forms.Label();
            this.pictureBox3 = new System.Windows.Forms.PictureBox();
            this.authPass = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.exitBut = new System.Windows.Forms.PictureBox();
            this.LogIn = new System.Windows.Forms.Button();
            this.fgtPass = new System.Windows.Forms.Label();
            this.passVisibility = new System.Windows.Forms.PictureBox();
            this.registration = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.exitBut)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.passVisibility)).BeginInit();
            this.SuspendLayout();
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox1.Image")));
            this.pictureBox1.InitialImage = null;
            this.pictureBox1.Location = new System.Drawing.Point(150, 36);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(120, 120);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox1.TabIndex = 0;
            this.pictureBox1.TabStop = false;
            this.LoginInfo.SetToolTip(this.pictureBox1, "MazeBank");
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Verdana", 15F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label1.Location = new System.Drawing.Point(107, 185);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(205, 25);
            this.label1.TabIndex = 1;
            this.label1.Text = "Номер телефона";
            // 
            // LoginInfo
            // 
            this.LoginInfo.ToolTipIcon = System.Windows.Forms.ToolTipIcon.Info;
            this.LoginInfo.ToolTipTitle = "Подсказка";
            // 
            // authLogin
            // 
            this.authLogin.BackColor = System.Drawing.SystemColors.Control;
            this.authLogin.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.authLogin.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.authLogin.ForeColor = System.Drawing.SystemColors.WindowFrame;
            this.authLogin.Location = new System.Drawing.Point(120, 228);
            this.authLogin.Name = "authLogin";
            this.authLogin.Size = new System.Drawing.Size(180, 22);
            this.authLogin.TabIndex = 2;
            this.authLogin.TabStop = false;
            this.authLogin.Text = "Введите телефон";
            this.authLogin.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.authLogin.Click += new System.EventHandler(this.authLogin_Click);
            this.authLogin.KeyDown += new System.Windows.Forms.KeyEventHandler(this.authLogin_KeyDown);
            this.authLogin.Leave += new System.EventHandler(this.authLogin_Leave);
            // 
            // pictureBox2
            // 
            this.pictureBox2.BackColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.pictureBox2.Location = new System.Drawing.Point(118, 252);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new System.Drawing.Size(184, 2);
            this.pictureBox2.TabIndex = 3;
            this.pictureBox2.TabStop = false;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Verdana", 15F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label2.Location = new System.Drawing.Point(161, 268);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(97, 25);
            this.label2.TabIndex = 4;
            this.label2.Text = "Пароль";
            // 
            // pictureBox3
            // 
            this.pictureBox3.BackColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.pictureBox3.Location = new System.Drawing.Point(117, 331);
            this.pictureBox3.Name = "pictureBox3";
            this.pictureBox3.Size = new System.Drawing.Size(184, 2);
            this.pictureBox3.TabIndex = 6;
            this.pictureBox3.TabStop = false;
            // 
            // authPass
            // 
            this.authPass.BackColor = System.Drawing.SystemColors.Control;
            this.authPass.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.authPass.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.authPass.ForeColor = System.Drawing.SystemColors.WindowFrame;
            this.authPass.Location = new System.Drawing.Point(119, 307);
            this.authPass.Name = "authPass";
            this.authPass.Size = new System.Drawing.Size(180, 22);
            this.authPass.TabIndex = 5;
            this.authPass.TabStop = false;
            this.authPass.Text = "Введите пароль";
            this.authPass.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.authPass.Click += new System.EventHandler(this.authPass_Click);
            this.authPass.KeyDown += new System.Windows.Forms.KeyEventHandler(this.authPass_KeyDown);
            this.authPass.Leave += new System.EventHandler(this.authPass_Leave);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Verdana", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label3.Location = new System.Drawing.Point(112, 529);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(193, 23);
            this.label3.TabIndex = 7;
            this.label3.Text = "Maze Bank © 2018";
            // 
            // exitBut
            // 
            this.exitBut.Cursor = System.Windows.Forms.Cursors.Hand;
            this.exitBut.Image = ((System.Drawing.Image)(resources.GetObject("exitBut.Image")));
            this.exitBut.Location = new System.Drawing.Point(358, 12);
            this.exitBut.Name = "exitBut";
            this.exitBut.Size = new System.Drawing.Size(34, 34);
            this.exitBut.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.exitBut.TabIndex = 10;
            this.exitBut.TabStop = false;
            this.exitBut.Click += new System.EventHandler(this.exitBut_Click);
            // 
            // LogIn
            // 
            this.LogIn.BackColor = System.Drawing.SystemColors.Control;
            this.LogIn.FlatAppearance.BorderColor = System.Drawing.SystemColors.Control;
            this.LogIn.FlatAppearance.BorderSize = 0;
            this.LogIn.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.LogIn.Font = new System.Drawing.Font("Verdana", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.LogIn.ForeColor = System.Drawing.Color.Red;
            this.LogIn.Location = new System.Drawing.Point(156, 369);
            this.LogIn.Name = "LogIn";
            this.LogIn.Size = new System.Drawing.Size(108, 53);
            this.LogIn.TabIndex = 12;
            this.LogIn.Text = "Войти";
            this.LogIn.UseVisualStyleBackColor = false;
            this.LogIn.Click += new System.EventHandler(this.LogIn_Click);
            this.LogIn.KeyDown += new System.Windows.Forms.KeyEventHandler(this.LogIn_KeyDown);
            this.LogIn.MouseHover += new System.EventHandler(this.LogIn_MouseHover);
            // 
            // fgtPass
            // 
            this.fgtPass.AutoSize = true;
            this.fgtPass.Cursor = System.Windows.Forms.Cursors.Hand;
            this.fgtPass.Font = new System.Drawing.Font("Verdana", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.fgtPass.ForeColor = System.Drawing.Color.Red;
            this.fgtPass.Location = new System.Drawing.Point(150, 340);
            this.fgtPass.Name = "fgtPass";
            this.fgtPass.Size = new System.Drawing.Size(119, 14);
            this.fgtPass.TabIndex = 13;
            this.fgtPass.Text = "Забыли пароль?";
            this.fgtPass.Click += new System.EventHandler(this.fgtPass_Click);
            // 
            // passVisibility
            // 
            this.passVisibility.Cursor = System.Windows.Forms.Cursors.Hand;
            this.passVisibility.Image = ((System.Drawing.Image)(resources.GetObject("passVisibility.Image")));
            this.passVisibility.Location = new System.Drawing.Point(308, 295);
            this.passVisibility.Name = "passVisibility";
            this.passVisibility.Size = new System.Drawing.Size(50, 50);
            this.passVisibility.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.passVisibility.TabIndex = 14;
            this.passVisibility.TabStop = false;
            this.passVisibility.Click += new System.EventHandler(this.passVisibility_Click);
            // 
            // registration
            // 
            this.registration.AutoSize = true;
            this.registration.Cursor = System.Windows.Forms.Cursors.Hand;
            this.registration.Font = new System.Drawing.Font("Verdana", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.registration.ForeColor = System.Drawing.Color.Red;
            this.registration.Location = new System.Drawing.Point(129, 511);
            this.registration.Name = "registration";
            this.registration.Size = new System.Drawing.Size(145, 14);
            this.registration.TabIndex = 15;
            this.registration.Text = "Зарегистрироваться";
            this.registration.Click += new System.EventHandler(this.registration_Click);
            // 
            // Auth
            // 
            this.AcceptButton = this.LogIn;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.Control;
            this.ClientSize = new System.Drawing.Size(404, 561);
            this.Controls.Add(this.registration);
            this.Controls.Add(this.passVisibility);
            this.Controls.Add(this.fgtPass);
            this.Controls.Add(this.LogIn);
            this.Controls.Add(this.exitBut);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.pictureBox3);
            this.Controls.Add(this.authPass);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.pictureBox2);
            this.Controls.Add(this.authLogin);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.pictureBox1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.KeyPreview = true;
            this.Name = "Auth";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Maze Bank";
            this.Load += new System.EventHandler(this.Auth_Load);
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.Auth_KeyDown);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.exitBut)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.passVisibility)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.ToolTip LoginInfo;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox authLogin;
        private System.Windows.Forms.PictureBox pictureBox2;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.PictureBox pictureBox3;
        private System.Windows.Forms.TextBox authPass;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.PictureBox exitBut;
        private System.Windows.Forms.Button LogIn;
        private System.Windows.Forms.Label fgtPass;
        private System.Windows.Forms.PictureBox passVisibility;
        private System.Windows.Forms.Label registration;
    }
}

