﻿using System;
using System.Drawing;
using System.Windows.Forms;
using System.Diagnostics;
using System.Text.RegularExpressions;
using AuthClass;
using System.Net;
using System.Collections.Generic;
using System.IO;

namespace MazeBank
{
    public partial class MainMenu : Form
    {
        public MainMenu()
        {
            InitializeComponent();
        }
        #region Переменные

        public long login;

        UserEntering logUser = new UserEntering();

        #endregion Переменные

        private void MainMenu_Load(object sender, EventArgs e)
        {
            try
            {
                yourCard.Left = (this.Width - yourCard.Width) / 2;
                CheckHistory.BackColor = Color.FromArgb(255, 196, 196);
                logUser.UserConnection(login);
                if (logUser.theme)
                {
                    balanceOfUser.ForeColor = Color.White;
                    exitBut.Load(@"C:\Users\aleks\source\repos\MazeBank\ExitPics\exitBlackTheme.png");
                    this.BackColor = Color.FromArgb(56, 56, 56);
                    moneyPanel.BackColor = Color.FromArgb(56, 56, 56);
                    user.ForeColor = Color.White;
                    settings.BackColor = Color.FromArgb(30, 30, 30);
                    settings.Load(@"C:\Users\aleks\source\repos\MazeBank\Pics\settingsWhite.png");
                    currency.BackColor = Color.FromArgb(30, 30, 30);
                    currency.Load(@"C:\Users\aleks\source\repos\MazeBank\Pics\currencyWhite.png");
                    callCenter.BackColor = Color.FromArgb(30, 30, 30);
                    callCenter.Load(@"C:\Users\aleks\source\repos\MazeBank\Pics\callCenterWhite.png");
                    panel.BackColor = Color.FromArgb(30, 30, 30);
                }
                user.Text = "Здраствуйте, " + logUser.userName + "!";
                user.Left = (this.Width - user.Width) / 2;
                balanceOfUser.Text = logUser.userBalance.ToString("F2").Replace(",", ".") + " ₴";
                string cardPattern = @"\d{12}";
                Regex regex = new Regex(cardPattern);
                string replaceText = "**** ";
                UserCard.Text = regex.Replace(logUser.userCard, replaceText);
            }
            catch(Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void exitBut_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void CheckHistory_Click(object sender, EventArgs e)
        {
            UserHistory history = new UserHistory();
            this.Opacity = .2;
            history.theme = logUser.theme;
            history.name = logUser.userName;
            history.surname = logUser.userSurname;
            history.patronymic = logUser.userPatronymic;
            history.card = logUser.userCard;
            history.ShowDialog();
            this.Opacity = 1;
        }

        private bool ConnectionAvailable(string strServer)
        {
            try
            {
                HttpWebRequest reqFP = (HttpWebRequest)HttpWebRequest.Create(strServer);

                HttpWebResponse rspFP = (HttpWebResponse)reqFP.GetResponse();
                if (HttpStatusCode.OK == rspFP.StatusCode)
                {
                    rspFP.Close();
                    return true;
                }
                else
                {
                    rspFP.Close();
                    return false;
                }
            }
            catch (WebException)
            {
                return false;
            }
        }

        private void currency_Click(object sender, EventArgs e)
        {
            this.Hide();
            LoadingForm loadingForm = new LoadingForm();
            loadingForm.Show();
            if (ConnectionAvailable("https://kurs.com.ua/"))
            {
                Currency currency = new Currency();
                currency.login = login;
                currency.theme = logUser.theme;
                currency.Show();
            }
            else
            {
                MessageBox.Show("Проблема с соединением. Проверьте наличие Интернет подключения.", "Ошибка подключения", MessageBoxButtons.OK, MessageBoxIcon.Error);
                this.Show();
                return;
            }
        }

        private void callCenter_Click(object sender, EventArgs e)
        {
            Process.Start("http://t.me/MazeBankBot");
        }

        private void settings_Click(object sender, EventArgs e)
        {
            this.Close();
            Settings settings = new Settings();
            settings.login = login;
            settings.Show();
        }

        private void sendMoney_Click(object sender, EventArgs e)
        {
            MainMenu mainMenu = new MainMenu();
            this.Close();
            MoneySending moneySending = new MoneySending();
            moneySending.login = login;
            moneySending.Show();
        }

        private void rechargePhone_Click(object sender, EventArgs e)
        {
            MainMenu mainMenu = new MainMenu();
            this.Close();
            RechargePhone rechargePhone = new RechargePhone();
            rechargePhone.login = login;
            rechargePhone.Show();
        }

        private void utilites_Click(object sender, EventArgs e)
        {
            this.Close();
            Utilites utilites = new Utilites();
            utilites.login = login;
            utilites.Show();
        }

        private void internetAndTV_Click(object sender, EventArgs e)
        {
            NetAndTV netAndTV = new NetAndTV();
            this.Close();
            netAndTV.login = login;
            netAndTV.Show();
        }

        private void onlinePayments_Click(object sender, EventArgs e)
        {
            this.Close();
            OnlinePayments onlinePayments = new OnlinePayments();
            onlinePayments.login = login;
            onlinePayments.Show();
        }

        private void tickets_Click(object sender, EventArgs e)
        {
            this.Close();
            Tickets tickets = new Tickets();
            tickets.login = login;
            tickets.Show();
        }

        private void gamesPayments_Click(object sender, EventArgs e)
        {
            this.Close();
            GamesPayments gamesPayments = new GamesPayments();
            gamesPayments.login = login;
            gamesPayments.Show();
        }

        private void mazeTaxi_Click(object sender, EventArgs e)
        {
            this.Close();
            MazeFine mazeFine = new MazeFine();
            mazeFine.login = login;
            mazeFine.Show();
        }

        private void MainMenu_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Escape)
            {
                DialogResult dialog = MessageBox.Show("Вы уверены, что хотите покинуть приложение?", "Выход", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                if(dialog == DialogResult.Yes)
                {
                    Application.Exit();
                }
            }
        }
    }
}
