﻿using System;
using System.Drawing;
using System.Media;
using System.Text.RegularExpressions;
using System.Windows.Forms;
using AuthClass;
using BillPrinting;
using HistoryClass;
using MailNotify;
using WinNotification;


namespace MazeBank
{
    public partial class NetAndTV : Form
    {
        public NetAndTV()
        {
            InitializeComponent();
        }

        #region Переменные

        MainMenu mainMenu = new MainMenu();

        MoneyTransactions user = new MoneyTransactions();

        ConfirmationForm confirmation = new ConfirmationForm();

        LoadingForm loading = new LoadingForm();

        decimal amount;

        const double percent = 1.5;

        public long login;

        string id;

        int idClick = 0;

        int idChanged = 0;

        int amountClick = 0;

        int amountChanged = 0;

        int errCountAmount = 0;

        int errCountId = 0;

        SoundPlayer sound = new SoundPlayer(@"C:\Users\aleks\source\repos\MazeBank\sound.wav");

        #endregion Переменные

        private void NetAndTV_Load(object sender, EventArgs e)
        {
            user.UserConnection(login);
            if(user.theme)
            {
                foreach(Control control in this.Controls)
                {
                    if (control is Label)
                    {
                        ((Label)control).ForeColor = Color.White;
                    }
                }
                pictureBox1.BackColor = Color.White;
                pictureBox3.BackColor = Color.White;
                amountText.BackColor = Color.FromArgb(56, 56, 56);
                idText.BackColor = Color.FromArgb(56, 56, 56);
                this.BackColor = Color.FromArgb(56, 56, 56);
                exitBut.Load(@"C:\Users\aleks\source\repos\MazeBank\ExitPics\exitBlackTheme.png");
                backBut.Load(@"C:\Users\aleks\source\repos\MazeBank\Pics\backWhite.png");
            }
        }

        private void exitBut_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void backBut_Click(object sender, EventArgs e)
        {
            this.Close();
            mainMenu.login = login;
            mainMenu.Show();
        }

        private void NetAndTV_KeyDown(object sender, KeyEventArgs e)
        {
            if(e.KeyCode == Keys.Enter)
            {
                pay.PerformClick();
            }
            if(e.KeyCode == Keys.Down)
            {
                amountText.Focus();
            }
            if(e.KeyCode == Keys.Up)
            {
                idText.Focus();
            }
        }

        private void amountText_Leave(object sender, EventArgs e)
        {
            amountChanged = 0;
            if (amountText.Text == "")
            {
                amountChanged++;
                amountText.Text = "Сумма";
                amountText.ForeColor = Color.Gray;
            }
            if (!Regex.IsMatch(amountText.Text, @"^\d{0,5}(?:(?:(\.)|(\,))\d{0,2})?$", RegexOptions.Compiled) && errCountAmount <= 4)
            {
                errCountAmount++;
                amountText.Clear();
                amountText.Focus();
                MessageBox.Show("Введенаня сумма не соответствует формату! Проверьте или введите еще раз.");
                amountText.ForeColor = Color.Black;
                return;
            }
            if (errCountAmount > 4)
            {
                amountText.Clear();
                amountText.Focus();
                WinNotify.ShowWinNotify("Ошибка ввода суммы", $"{user.userName}, Вы вводите неправильный формат суммы.\nФормат должен быть таким: 12.34 или 15", 10000);
            }
        }

        private void amountText_Click(object sender, EventArgs e)
        {
            if (user.theme)
            {
                if (amountText.Text != null && amountClick == 0)
                {
                    amountText.Clear();
                    amountText.ForeColor = Color.White;
                    amountClick++;
                }
                else if (amountText.Text != null && amountChanged != 0)
                {
                    amountText.Clear();
                    amountText.ForeColor = Color.White;
                }
            }
            else if (!user.theme)
            {
                if (amountText.Text != null && amountClick == 0)
                {
                    amountText.Clear();
                    amountText.ForeColor = Color.Black;
                    amountClick++;
                }
                else if (amountText.Text != null && amountChanged != 0)
                {
                    amountText.Clear();
                    amountText.ForeColor = Color.Black;
                }
            }
        }

        private void amountText_TextChanged(object sender, EventArgs e)
        {
            amountText.MaxLength = 8;
        }

        private void idText_TextChanged(object sender, EventArgs e)
        {
            idText.MaxLength = 8;
        }

        private void idText_Click(object sender, EventArgs e)
        {
            if (user.theme)
            {
                if (idText.Text != null && idClick == 0)
                {
                    idText.Clear();
                    idText.ForeColor = Color.White;
                    idClick++;
                }
                else if (idText.Text != null && idChanged != 0)
                {
                    idText.Clear();
                    idText.ForeColor = Color.White;
                }
            }
            else if (!user.theme)
            {
                if (idText.Text != null && idClick == 0)
                {
                    idText.Clear();
                    idText.ForeColor = Color.Black;
                    idClick++;
                }
                else if (idText.Text != null && idChanged != 0)
                {
                    idText.Clear();
                    idText.ForeColor = Color.Black;
                }
            }
        }

        private void idText_Leave(object sender, EventArgs e)
        {
            if (!Regex.IsMatch(idText.Text, @"^\d{6,8}$", RegexOptions.Compiled) && errCountId <= 3)
            {
                errCountId++;
                idText.Text = "Id";
                MessageBox.Show("Неверно введен номер телефона! Проверьте еще раз!");
                idText.ForeColor = Color.Gray;
                return;
            }
            if (errCountId > 3)
            {
                WinNotify.ShowWinNotify("Ошибка ввода id", $"{user.userName}, Вы вводите неверный формат id.\nПравильный формат: 123456 / 12345678 (От 6 до 8 цифр).", 10000);
            }
        }

        private void pay_Click(object sender, EventArgs e)
        {
            if(Regex.IsMatch(idText.Text, @"^\d{6,8}$", RegexOptions.Compiled) && Regex.IsMatch(amountText.Text, @"^\d{0,5}(?:(?:(\.)|(\,))\d{0,2})?$", RegexOptions.Compiled))
            {
                try
                {
                    id = idText.Text;
                    amount = decimal.Parse(amountText.Text);
                    amount += MoneyTransactions.CalculateCommittee(amount, percent);
                    if (user.userBalance >= amount)
                    {
                        this.Opacity = 0.2;
                        confirmation.userName = user.userName;
                        confirmation.userPassword = user.userPassword;
                        confirmation.amount = amount;
                        confirmation.ShowDialog();
                        if (!confirmation.ok)
                        {
                            user.userBalance -= amount;
                            this.Opacity = 1;
                            user.UserTransfer(login, user.userBalance);
                            Close();
                            loading.ShowDialog();
                            DialogResult askPrintBill = MessageBox.Show("Вы желаете распечатать чек?", "Печать чека", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                            if (askPrintBill == DialogResult.Yes)
                            {
                                History.AddToHistory(user.userCard, $"-{amount} – Оплата Интернета и ТВ (id#{id})");
                                MailDelivery.NotifyByMail(user.userEmail, user.userName, user.userSurname, $"оплаты Интернета и ТВ (id#{id})", amount, user.userBalance);
                                PrintBill.PrintingBill(user.userName, user.userSurname, user.userPatronymic, user.userCard, amount - MoneyTransactions.CalculateCommittee(amount, percent), MoneyTransactions.CalculateCommittee(amount, percent), id);
                                WinNotify.ShowWinNotify("Оплата Интернета и ТВ", $"Вы успешно оплатили Интернет и ТВ в размере {amount - (MoneyTransactions.CalculateCommittee(amount, percent))}₴\nВаш поточный баланс: {user.userBalance:0.##}₴", 5000);
                                sound.Play();
                                mainMenu.login = login;
                                mainMenu.Show();
                            }
                            else
                            {
                                History.AddToHistory(user.userCard, $"-{amount} – Оплата Интернета и ТВ (id#{id})");
                                MailDelivery.NotifyByMail(user.userEmail, user.userName, user.userSurname, $"оплаты Интернета и ТВ (id#{id})", amount, user.userBalance);
                                PrintBill.CreatingBill(user.userName, user.userSurname, user.userPatronymic, user.userCard, amount - MoneyTransactions.CalculateCommittee(amount, percent), MoneyTransactions.CalculateCommittee(amount, percent), id);
                                WinNotify.ShowWinNotify("Оплата Интернета и ТВ", $"Вы успешно оплатили Интернет и ТВ в размере {amount - (MoneyTransactions.CalculateCommittee(amount, percent))}₴\nВаш поточный баланс: {user.userBalance:0.##}₴", 5000);
                                sound.Play();
                                mainMenu.login = login;
                                mainMenu.Show();
                            }
                        }
                        else if (confirmation.ok)
                        {
                            this.Opacity = 1;
                            MessageBox.Show($"Извините, {user.userName}, Вы не можете произвести операцию без подтверждения.", "Ошибка перевода", MessageBoxButtons.OK, MessageBoxIcon.Error);
                            pay.Focus();
                            return;
                        }
                        else
                        {
                            this.Opacity = 1;
                        }
                    }
                    else
                    {
                        MessageBox.Show($"{user.userName}, у Вас не хватает средств оплатить эти услуги.", "Недостаточно средств", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        amount = 0;
                        return;
                    }
                }
                catch(Exception ex)
                {
                    MessageBox.Show(ex.Message);
                    return;
                }
            }
            else
            {
                MessageBox.Show("Введены неверные данные. Проверьте данные и попробуйте снова.", "Ошибка введеных данных", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
        }
    }
}
