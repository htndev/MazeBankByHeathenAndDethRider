﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MazeBank
{
    public partial class ConfirmationForm : Form
    {
        public ConfirmationForm()
        {
            InitializeComponent();
        }

        private void exitBut_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        MoneySending moneySending = new MoneySending();

        public string userName;

        public decimal amount;

        public string userPassword;

        int clicker = 0;

        int changer = 0;

        int passVis = 1;

        public bool ok = true;

        public bool PressedMethod(string userPass, string truePass)
        {
            if (userPass == truePass)
            {
                this.Close();
                return false;
            }
            else
            {
                MessageBox.Show("Вы ввели неправильный пароль подтверждения!\nПроверьте его и введите еще раз.", "Ошибка подтверждения", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return true;
            }
        }

        private void ConfirmationForm_Load(object sender, EventArgs e)
        {
            confirmStr.Text = $"{userName}, подтвердите перевод";
            confirmStr.Left = (this.Width - confirmStr.Width) / 2;
            toPay.Text = $"Сумма к оплате: {amount.ToString("F2")}";
            toPay.Left = (this.Width - toPay.Width) / 2;
        }

        private void confPass_Click(object sender, EventArgs e)
        {
            ok = PressedMethod(passwordBox.Text, userPassword);
        }

        private void passwordBox_Click(object sender, EventArgs e)
        {
            if (passwordBox.Text != null && clicker == 0)
            {
                passwordBox.Clear();
                passwordBox.UseSystemPasswordChar = true;
                passwordBox.ForeColor = Color.Black;
                clicker++;
            }

            else if (passwordBox.Text != null && changer != 0)
            {
                passwordBox.Clear();
                passwordBox.UseSystemPasswordChar = true;
                passwordBox.ForeColor = Color.Black;
            }
        }

        private void passwordBox_Leave(object sender, EventArgs e)
        {
            changer = 0;
            if (passwordBox.Text == "")
            {
                passwordBox.UseSystemPasswordChar = false;
                passwordBox.ForeColor = Color.Gray;
                passwordBox.Text = "Введите пароль";
                changer++;
            }
        }

        private void passVisibility_Click(object sender, EventArgs e)
        {
            if (passVis % 2 == 1)
            {
                passVisibility.Load(@"C:\Users\aleks\source\repos\MazeBank\Eyes\openedEye.png");
                passwordBox.UseSystemPasswordChar = false;
                passVis++;
            }

            else if (passVis % 2 == 0)
            {
                passVisibility.Load(@"C:\Users\aleks\source\repos\MazeBank\Eyes\closedEye.png");
                passwordBox.UseSystemPasswordChar = true;
                passVis--;
            }
        }

        private void ConfirmationForm_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                confPass.PerformClick();
            }
        }
    }
}
