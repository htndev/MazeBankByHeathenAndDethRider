﻿namespace MazeBank
{
    partial class GamesPayments
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(GamesPayments));
            this.backBtn = new System.Windows.Forms.PictureBox();
            this.exitBtn = new System.Windows.Forms.PictureBox();
            this.steamPic = new System.Windows.Forms.PictureBox();
            this.label = new System.Windows.Forms.Label();
            this.battlenetPic = new System.Windows.Forms.PictureBox();
            this.wargamingPic = new System.Windows.Forms.PictureBox();
            this.steam = new System.Windows.Forms.Panel();
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.amountText = new System.Windows.Forms.TextBox();
            this.paySteam = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.pictureBox3 = new System.Windows.Forms.PictureBox();
            this.idText = new System.Windows.Forms.TextBox();
            this.steamBack = new System.Windows.Forms.PictureBox();
            this.steamExit = new System.Windows.Forms.PictureBox();
            this.wargaming = new System.Windows.Forms.Panel();
            this.pictureBox4 = new System.Windows.Forms.PictureBox();
            this.pictureBox5 = new System.Windows.Forms.PictureBox();
            this.amountTextWG = new System.Windows.Forms.TextBox();
            this.payWG = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.pictureBox6 = new System.Windows.Forms.PictureBox();
            this.idTextWG = new System.Windows.Forms.TextBox();
            this.wgBack = new System.Windows.Forms.PictureBox();
            this.wgExit = new System.Windows.Forms.PictureBox();
            this.battlenet = new System.Windows.Forms.Panel();
            this.pictureBox7 = new System.Windows.Forms.PictureBox();
            this.pictureBox8 = new System.Windows.Forms.PictureBox();
            this.amountTextBN = new System.Windows.Forms.TextBox();
            this.payBN = new System.Windows.Forms.Button();
            this.label3 = new System.Windows.Forms.Label();
            this.pictureBox9 = new System.Windows.Forms.PictureBox();
            this.idTextBN = new System.Windows.Forms.TextBox();
            this.bnBack = new System.Windows.Forms.PictureBox();
            this.bnExit = new System.Windows.Forms.PictureBox();
            ((System.ComponentModel.ISupportInitialize)(this.backBtn)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.exitBtn)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.steamPic)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.battlenetPic)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.wargamingPic)).BeginInit();
            this.steam.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.steamBack)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.steamExit)).BeginInit();
            this.wargaming.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.wgBack)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.wgExit)).BeginInit();
            this.battlenet.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.bnBack)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.bnExit)).BeginInit();
            this.SuspendLayout();
            // 
            // backBtn
            // 
            this.backBtn.AccessibleRole = System.Windows.Forms.AccessibleRole.None;
            this.backBtn.Cursor = System.Windows.Forms.Cursors.Hand;
            this.backBtn.Image = ((System.Drawing.Image)(resources.GetObject("backBtn.Image")));
            this.backBtn.Location = new System.Drawing.Point(12, 12);
            this.backBtn.Name = "backBtn";
            this.backBtn.Size = new System.Drawing.Size(34, 34);
            this.backBtn.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.backBtn.TabIndex = 160;
            this.backBtn.TabStop = false;
            this.backBtn.Click += new System.EventHandler(this.backBtn_Click);
            // 
            // exitBtn
            // 
            this.exitBtn.AccessibleRole = System.Windows.Forms.AccessibleRole.None;
            this.exitBtn.Cursor = System.Windows.Forms.Cursors.Hand;
            this.exitBtn.Image = ((System.Drawing.Image)(resources.GetObject("exitBtn.Image")));
            this.exitBtn.Location = new System.Drawing.Point(358, 12);
            this.exitBtn.Name = "exitBtn";
            this.exitBtn.Size = new System.Drawing.Size(34, 34);
            this.exitBtn.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.exitBtn.TabIndex = 159;
            this.exitBtn.TabStop = false;
            this.exitBtn.Click += new System.EventHandler(this.exitBtn_Click);
            // 
            // steamPic
            // 
            this.steamPic.Cursor = System.Windows.Forms.Cursors.Hand;
            this.steamPic.Image = ((System.Drawing.Image)(resources.GetObject("steamPic.Image")));
            this.steamPic.Location = new System.Drawing.Point(152, 113);
            this.steamPic.Name = "steamPic";
            this.steamPic.Size = new System.Drawing.Size(100, 100);
            this.steamPic.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.steamPic.TabIndex = 161;
            this.steamPic.TabStop = false;
            this.steamPic.Click += new System.EventHandler(this.steamPic_Click);
            // 
            // label
            // 
            this.label.AutoSize = true;
            this.label.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label.Location = new System.Drawing.Point(81, 61);
            this.label.Name = "label";
            this.label.Size = new System.Drawing.Size(241, 18);
            this.label.TabIndex = 163;
            this.label.Text = "Выберите игровой сервис";
            // 
            // battlenetPic
            // 
            this.battlenetPic.Cursor = System.Windows.Forms.Cursors.Hand;
            this.battlenetPic.Image = ((System.Drawing.Image)(resources.GetObject("battlenetPic.Image")));
            this.battlenetPic.Location = new System.Drawing.Point(152, 255);
            this.battlenetPic.Name = "battlenetPic";
            this.battlenetPic.Size = new System.Drawing.Size(100, 100);
            this.battlenetPic.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.battlenetPic.TabIndex = 164;
            this.battlenetPic.TabStop = false;
            this.battlenetPic.Click += new System.EventHandler(this.battlenetPic_Click);
            // 
            // wargamingPic
            // 
            this.wargamingPic.Cursor = System.Windows.Forms.Cursors.Hand;
            this.wargamingPic.Image = ((System.Drawing.Image)(resources.GetObject("wargamingPic.Image")));
            this.wargamingPic.Location = new System.Drawing.Point(152, 397);
            this.wargamingPic.Name = "wargamingPic";
            this.wargamingPic.Size = new System.Drawing.Size(100, 100);
            this.wargamingPic.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.wargamingPic.TabIndex = 165;
            this.wargamingPic.TabStop = false;
            this.wargamingPic.Click += new System.EventHandler(this.wargamingPic_Click);
            // 
            // steam
            // 
            this.steam.Controls.Add(this.pictureBox2);
            this.steam.Controls.Add(this.pictureBox1);
            this.steam.Controls.Add(this.amountText);
            this.steam.Controls.Add(this.paySteam);
            this.steam.Controls.Add(this.label2);
            this.steam.Controls.Add(this.pictureBox3);
            this.steam.Controls.Add(this.idText);
            this.steam.Controls.Add(this.steamBack);
            this.steam.Controls.Add(this.steamExit);
            this.steam.Dock = System.Windows.Forms.DockStyle.Fill;
            this.steam.Location = new System.Drawing.Point(0, 0);
            this.steam.Name = "steam";
            this.steam.Size = new System.Drawing.Size(404, 561);
            this.steam.TabIndex = 166;
            // 
            // pictureBox2
            // 
            this.pictureBox2.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pictureBox2.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox2.Image")));
            this.pictureBox2.Location = new System.Drawing.Point(152, 61);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new System.Drawing.Size(100, 100);
            this.pictureBox2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox2.TabIndex = 170;
            this.pictureBox2.TabStop = false;
            // 
            // pictureBox1
            // 
            this.pictureBox1.BackColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.pictureBox1.Location = new System.Drawing.Point(129, 327);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(148, 2);
            this.pictureBox1.TabIndex = 169;
            this.pictureBox1.TabStop = false;
            // 
            // amountText
            // 
            this.amountText.BackColor = System.Drawing.SystemColors.Control;
            this.amountText.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.amountText.Font = new System.Drawing.Font("Microsoft Sans Serif", 20.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.amountText.ForeColor = System.Drawing.Color.Gray;
            this.amountText.Location = new System.Drawing.Point(130, 292);
            this.amountText.Name = "amountText";
            this.amountText.Size = new System.Drawing.Size(146, 31);
            this.amountText.TabIndex = 168;
            this.amountText.TabStop = false;
            this.amountText.Text = "Сумма";
            this.amountText.Click += new System.EventHandler(this.amountText_Click);
            this.amountText.TextChanged += new System.EventHandler(this.amountText_TextChanged);
            this.amountText.Leave += new System.EventHandler(this.amountText_Leave);
            // 
            // paySteam
            // 
            this.paySteam.FlatAppearance.BorderSize = 0;
            this.paySteam.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.paySteam.Font = new System.Drawing.Font("Verdana", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.paySteam.ForeColor = System.Drawing.Color.Red;
            this.paySteam.Location = new System.Drawing.Point(107, 391);
            this.paySteam.Name = "paySteam";
            this.paySteam.Size = new System.Drawing.Size(191, 72);
            this.paySteam.TabIndex = 167;
            this.paySteam.Text = "Пополнить";
            this.paySteam.UseVisualStyleBackColor = true;
            this.paySteam.Click += new System.EventHandler(this.paySteam_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Verdana", 11.25F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label2.Location = new System.Drawing.Point(137, 190);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(130, 18);
            this.label2.TabIndex = 166;
            this.label2.Text = "Введите Ваш id ";
            // 
            // pictureBox3
            // 
            this.pictureBox3.BackColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.pictureBox3.Location = new System.Drawing.Point(108, 252);
            this.pictureBox3.Name = "pictureBox3";
            this.pictureBox3.Size = new System.Drawing.Size(187, 2);
            this.pictureBox3.TabIndex = 165;
            this.pictureBox3.TabStop = false;
            // 
            // idText
            // 
            this.idText.BackColor = System.Drawing.SystemColors.Control;
            this.idText.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.idText.Font = new System.Drawing.Font("Microsoft Sans Serif", 20.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.idText.ForeColor = System.Drawing.Color.Gray;
            this.idText.Location = new System.Drawing.Point(110, 220);
            this.idText.Name = "idText";
            this.idText.Size = new System.Drawing.Size(183, 31);
            this.idText.TabIndex = 164;
            this.idText.TabStop = false;
            this.idText.Text = "Id";
            this.idText.Click += new System.EventHandler(this.idText_Click);
            this.idText.TextChanged += new System.EventHandler(this.idText_TextChanged);
            this.idText.Leave += new System.EventHandler(this.idText_Leave);
            // 
            // steamBack
            // 
            this.steamBack.AccessibleRole = System.Windows.Forms.AccessibleRole.None;
            this.steamBack.Cursor = System.Windows.Forms.Cursors.Hand;
            this.steamBack.Image = ((System.Drawing.Image)(resources.GetObject("steamBack.Image")));
            this.steamBack.Location = new System.Drawing.Point(12, 12);
            this.steamBack.Name = "steamBack";
            this.steamBack.Size = new System.Drawing.Size(34, 34);
            this.steamBack.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.steamBack.TabIndex = 162;
            this.steamBack.TabStop = false;
            this.steamBack.Click += new System.EventHandler(this.steamBack_Click);
            // 
            // steamExit
            // 
            this.steamExit.AccessibleRole = System.Windows.Forms.AccessibleRole.None;
            this.steamExit.Cursor = System.Windows.Forms.Cursors.Hand;
            this.steamExit.Image = ((System.Drawing.Image)(resources.GetObject("steamExit.Image")));
            this.steamExit.Location = new System.Drawing.Point(358, 12);
            this.steamExit.Name = "steamExit";
            this.steamExit.Size = new System.Drawing.Size(34, 34);
            this.steamExit.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.steamExit.TabIndex = 161;
            this.steamExit.TabStop = false;
            this.steamExit.Click += new System.EventHandler(this.steamExit_Click);
            // 
            // wargaming
            // 
            this.wargaming.Controls.Add(this.pictureBox4);
            this.wargaming.Controls.Add(this.pictureBox5);
            this.wargaming.Controls.Add(this.amountTextWG);
            this.wargaming.Controls.Add(this.payWG);
            this.wargaming.Controls.Add(this.label1);
            this.wargaming.Controls.Add(this.pictureBox6);
            this.wargaming.Controls.Add(this.idTextWG);
            this.wargaming.Controls.Add(this.wgBack);
            this.wargaming.Controls.Add(this.wgExit);
            this.wargaming.Dock = System.Windows.Forms.DockStyle.Fill;
            this.wargaming.Location = new System.Drawing.Point(0, 0);
            this.wargaming.Name = "wargaming";
            this.wargaming.Size = new System.Drawing.Size(404, 561);
            this.wargaming.TabIndex = 171;
            // 
            // pictureBox4
            // 
            this.pictureBox4.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pictureBox4.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox4.Image")));
            this.pictureBox4.Location = new System.Drawing.Point(152, 61);
            this.pictureBox4.Name = "pictureBox4";
            this.pictureBox4.Size = new System.Drawing.Size(100, 100);
            this.pictureBox4.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox4.TabIndex = 179;
            this.pictureBox4.TabStop = false;
            // 
            // pictureBox5
            // 
            this.pictureBox5.BackColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.pictureBox5.Location = new System.Drawing.Point(129, 327);
            this.pictureBox5.Name = "pictureBox5";
            this.pictureBox5.Size = new System.Drawing.Size(148, 2);
            this.pictureBox5.TabIndex = 178;
            this.pictureBox5.TabStop = false;
            // 
            // amountTextWG
            // 
            this.amountTextWG.BackColor = System.Drawing.SystemColors.Control;
            this.amountTextWG.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.amountTextWG.Font = new System.Drawing.Font("Microsoft Sans Serif", 20.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.amountTextWG.ForeColor = System.Drawing.Color.Gray;
            this.amountTextWG.Location = new System.Drawing.Point(130, 292);
            this.amountTextWG.Name = "amountTextWG";
            this.amountTextWG.Size = new System.Drawing.Size(146, 31);
            this.amountTextWG.TabIndex = 177;
            this.amountTextWG.TabStop = false;
            this.amountTextWG.Text = "Сумма";
            this.amountTextWG.Click += new System.EventHandler(this.amountTextWG_Click);
            this.amountTextWG.TextChanged += new System.EventHandler(this.amountTextWG_TextChanged);
            this.amountTextWG.Leave += new System.EventHandler(this.amountTextWG_Leave);
            // 
            // payWG
            // 
            this.payWG.FlatAppearance.BorderSize = 0;
            this.payWG.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.payWG.Font = new System.Drawing.Font("Verdana", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.payWG.ForeColor = System.Drawing.Color.Red;
            this.payWG.Location = new System.Drawing.Point(107, 391);
            this.payWG.Name = "payWG";
            this.payWG.Size = new System.Drawing.Size(191, 72);
            this.payWG.TabIndex = 176;
            this.payWG.Text = "Пополнить";
            this.payWG.UseVisualStyleBackColor = true;
            this.payWG.Click += new System.EventHandler(this.payWG_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Verdana", 11.25F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label1.Location = new System.Drawing.Point(137, 190);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(130, 18);
            this.label1.TabIndex = 175;
            this.label1.Text = "Введите Ваш id ";
            // 
            // pictureBox6
            // 
            this.pictureBox6.BackColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.pictureBox6.Location = new System.Drawing.Point(108, 252);
            this.pictureBox6.Name = "pictureBox6";
            this.pictureBox6.Size = new System.Drawing.Size(187, 2);
            this.pictureBox6.TabIndex = 174;
            this.pictureBox6.TabStop = false;
            // 
            // idTextWG
            // 
            this.idTextWG.BackColor = System.Drawing.SystemColors.Control;
            this.idTextWG.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.idTextWG.Font = new System.Drawing.Font("Microsoft Sans Serif", 20.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.idTextWG.ForeColor = System.Drawing.Color.Gray;
            this.idTextWG.Location = new System.Drawing.Point(110, 220);
            this.idTextWG.Name = "idTextWG";
            this.idTextWG.Size = new System.Drawing.Size(183, 31);
            this.idTextWG.TabIndex = 173;
            this.idTextWG.TabStop = false;
            this.idTextWG.Text = "Id";
            this.idTextWG.Click += new System.EventHandler(this.idTextWG_Click);
            this.idTextWG.TextChanged += new System.EventHandler(this.idTextWG_TextChanged);
            this.idTextWG.Leave += new System.EventHandler(this.idTextWG_Leave);
            // 
            // wgBack
            // 
            this.wgBack.AccessibleRole = System.Windows.Forms.AccessibleRole.None;
            this.wgBack.Cursor = System.Windows.Forms.Cursors.Hand;
            this.wgBack.Image = ((System.Drawing.Image)(resources.GetObject("wgBack.Image")));
            this.wgBack.Location = new System.Drawing.Point(12, 12);
            this.wgBack.Name = "wgBack";
            this.wgBack.Size = new System.Drawing.Size(34, 34);
            this.wgBack.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.wgBack.TabIndex = 172;
            this.wgBack.TabStop = false;
            this.wgBack.Click += new System.EventHandler(this.wgBack_Click);
            // 
            // wgExit
            // 
            this.wgExit.AccessibleRole = System.Windows.Forms.AccessibleRole.None;
            this.wgExit.Cursor = System.Windows.Forms.Cursors.Hand;
            this.wgExit.Image = ((System.Drawing.Image)(resources.GetObject("wgExit.Image")));
            this.wgExit.Location = new System.Drawing.Point(358, 12);
            this.wgExit.Name = "wgExit";
            this.wgExit.Size = new System.Drawing.Size(34, 34);
            this.wgExit.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.wgExit.TabIndex = 171;
            this.wgExit.TabStop = false;
            this.wgExit.Click += new System.EventHandler(this.wgExit_Click);
            // 
            // battlenet
            // 
            this.battlenet.Controls.Add(this.pictureBox7);
            this.battlenet.Controls.Add(this.pictureBox8);
            this.battlenet.Controls.Add(this.amountTextBN);
            this.battlenet.Controls.Add(this.payBN);
            this.battlenet.Controls.Add(this.label3);
            this.battlenet.Controls.Add(this.pictureBox9);
            this.battlenet.Controls.Add(this.idTextBN);
            this.battlenet.Controls.Add(this.bnBack);
            this.battlenet.Controls.Add(this.bnExit);
            this.battlenet.Dock = System.Windows.Forms.DockStyle.Fill;
            this.battlenet.Location = new System.Drawing.Point(0, 0);
            this.battlenet.Name = "battlenet";
            this.battlenet.Size = new System.Drawing.Size(404, 561);
            this.battlenet.TabIndex = 180;
            // 
            // pictureBox7
            // 
            this.pictureBox7.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pictureBox7.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox7.Image")));
            this.pictureBox7.Location = new System.Drawing.Point(152, 61);
            this.pictureBox7.Name = "pictureBox7";
            this.pictureBox7.Size = new System.Drawing.Size(100, 100);
            this.pictureBox7.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox7.TabIndex = 188;
            this.pictureBox7.TabStop = false;
            // 
            // pictureBox8
            // 
            this.pictureBox8.BackColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.pictureBox8.Location = new System.Drawing.Point(129, 327);
            this.pictureBox8.Name = "pictureBox8";
            this.pictureBox8.Size = new System.Drawing.Size(148, 2);
            this.pictureBox8.TabIndex = 187;
            this.pictureBox8.TabStop = false;
            // 
            // amountTextBN
            // 
            this.amountTextBN.BackColor = System.Drawing.SystemColors.Control;
            this.amountTextBN.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.amountTextBN.Font = new System.Drawing.Font("Microsoft Sans Serif", 20.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.amountTextBN.ForeColor = System.Drawing.Color.Gray;
            this.amountTextBN.Location = new System.Drawing.Point(130, 292);
            this.amountTextBN.Name = "amountTextBN";
            this.amountTextBN.Size = new System.Drawing.Size(146, 31);
            this.amountTextBN.TabIndex = 186;
            this.amountTextBN.TabStop = false;
            this.amountTextBN.Text = "Сумма";
            this.amountTextBN.Click += new System.EventHandler(this.amountTextBN_Click);
            this.amountTextBN.TextChanged += new System.EventHandler(this.amountTextBN_TextChanged);
            this.amountTextBN.Leave += new System.EventHandler(this.amountTextBN_Leave);
            // 
            // payBN
            // 
            this.payBN.FlatAppearance.BorderSize = 0;
            this.payBN.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.payBN.Font = new System.Drawing.Font("Verdana", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.payBN.ForeColor = System.Drawing.Color.Red;
            this.payBN.Location = new System.Drawing.Point(107, 391);
            this.payBN.Name = "payBN";
            this.payBN.Size = new System.Drawing.Size(191, 72);
            this.payBN.TabIndex = 185;
            this.payBN.Text = "Пополнить";
            this.payBN.UseVisualStyleBackColor = true;
            this.payBN.Click += new System.EventHandler(this.payBN_Click);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Verdana", 11.25F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label3.Location = new System.Drawing.Point(137, 190);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(130, 18);
            this.label3.TabIndex = 184;
            this.label3.Text = "Введите Ваш id ";
            // 
            // pictureBox9
            // 
            this.pictureBox9.BackColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.pictureBox9.Location = new System.Drawing.Point(108, 252);
            this.pictureBox9.Name = "pictureBox9";
            this.pictureBox9.Size = new System.Drawing.Size(187, 2);
            this.pictureBox9.TabIndex = 183;
            this.pictureBox9.TabStop = false;
            // 
            // idTextBN
            // 
            this.idTextBN.BackColor = System.Drawing.SystemColors.Control;
            this.idTextBN.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.idTextBN.Font = new System.Drawing.Font("Microsoft Sans Serif", 20.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.idTextBN.ForeColor = System.Drawing.Color.Gray;
            this.idTextBN.Location = new System.Drawing.Point(110, 220);
            this.idTextBN.Name = "idTextBN";
            this.idTextBN.Size = new System.Drawing.Size(183, 31);
            this.idTextBN.TabIndex = 182;
            this.idTextBN.TabStop = false;
            this.idTextBN.Text = "Id";
            this.idTextBN.Click += new System.EventHandler(this.idTextBN_Click);
            this.idTextBN.TextChanged += new System.EventHandler(this.idTextBN_TextChanged);
            this.idTextBN.Leave += new System.EventHandler(this.idTextBN_Leave);
            // 
            // bnBack
            // 
            this.bnBack.AccessibleRole = System.Windows.Forms.AccessibleRole.None;
            this.bnBack.Cursor = System.Windows.Forms.Cursors.Hand;
            this.bnBack.Image = ((System.Drawing.Image)(resources.GetObject("bnBack.Image")));
            this.bnBack.Location = new System.Drawing.Point(12, 12);
            this.bnBack.Name = "bnBack";
            this.bnBack.Size = new System.Drawing.Size(34, 34);
            this.bnBack.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.bnBack.TabIndex = 181;
            this.bnBack.TabStop = false;
            this.bnBack.Click += new System.EventHandler(this.bnBack_Click);
            // 
            // bnExit
            // 
            this.bnExit.AccessibleRole = System.Windows.Forms.AccessibleRole.None;
            this.bnExit.Cursor = System.Windows.Forms.Cursors.Hand;
            this.bnExit.Image = ((System.Drawing.Image)(resources.GetObject("bnExit.Image")));
            this.bnExit.Location = new System.Drawing.Point(358, 12);
            this.bnExit.Name = "bnExit";
            this.bnExit.Size = new System.Drawing.Size(34, 34);
            this.bnExit.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.bnExit.TabIndex = 180;
            this.bnExit.TabStop = false;
            this.bnExit.Click += new System.EventHandler(this.bnExit_Click);
            // 
            // GamesPayments
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(404, 561);
            this.Controls.Add(this.battlenet);
            this.Controls.Add(this.wargaming);
            this.Controls.Add(this.steam);
            this.Controls.Add(this.wargamingPic);
            this.Controls.Add(this.battlenetPic);
            this.Controls.Add(this.label);
            this.Controls.Add(this.steamPic);
            this.Controls.Add(this.backBtn);
            this.Controls.Add(this.exitBtn);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "GamesPayments";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Пополнение игровых кошельков";
            this.Load += new System.EventHandler(this.GamesPayments_Load);
            ((System.ComponentModel.ISupportInitialize)(this.backBtn)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.exitBtn)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.steamPic)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.battlenetPic)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.wargamingPic)).EndInit();
            this.steam.ResumeLayout(false);
            this.steam.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.steamBack)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.steamExit)).EndInit();
            this.wargaming.ResumeLayout(false);
            this.wargaming.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.wgBack)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.wgExit)).EndInit();
            this.battlenet.ResumeLayout(false);
            this.battlenet.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.bnBack)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.bnExit)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.PictureBox backBtn;
        private System.Windows.Forms.PictureBox exitBtn;
        private System.Windows.Forms.PictureBox steamPic;
        private System.Windows.Forms.Label label;
        private System.Windows.Forms.PictureBox battlenetPic;
        private System.Windows.Forms.PictureBox wargamingPic;
        private System.Windows.Forms.Panel steam;
        private System.Windows.Forms.PictureBox steamBack;
        private System.Windows.Forms.PictureBox steamExit;
        private System.Windows.Forms.PictureBox pictureBox2;
        private System.Windows.Forms.PictureBox pictureBox1;
        public System.Windows.Forms.TextBox amountText;
        public System.Windows.Forms.Button paySteam;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.PictureBox pictureBox3;
        private System.Windows.Forms.TextBox idText;
        private System.Windows.Forms.Panel wargaming;
        private System.Windows.Forms.PictureBox pictureBox4;
        private System.Windows.Forms.PictureBox pictureBox5;
        public System.Windows.Forms.TextBox amountTextWG;
        public System.Windows.Forms.Button payWG;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.PictureBox pictureBox6;
        private System.Windows.Forms.TextBox idTextWG;
        private System.Windows.Forms.PictureBox wgBack;
        private System.Windows.Forms.PictureBox wgExit;
        private System.Windows.Forms.Panel battlenet;
        private System.Windows.Forms.PictureBox pictureBox7;
        private System.Windows.Forms.PictureBox pictureBox8;
        public System.Windows.Forms.TextBox amountTextBN;
        public System.Windows.Forms.Button payBN;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.PictureBox pictureBox9;
        private System.Windows.Forms.TextBox idTextBN;
        private System.Windows.Forms.PictureBox bnBack;
        private System.Windows.Forms.PictureBox bnExit;
    }
}