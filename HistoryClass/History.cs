﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Windows.Forms;

namespace HistoryClass
{
    public class History
    {
        const string file = @"C:\Users\aleks\source\repos\MazeBank\Histories\history_of_user#";

        public static void AddToHistory(string userCard, string text)
        {
            string path = file + userCard + ".txt";
            string[] history = File.ReadAllLines(path, System.Text.Encoding.Default);
            List<string> usersOperations = new List<string>();
            foreach(string element in history)
            {
                usersOperations.Add(element);
            }
            File.WriteAllText(path, string.Empty);
            usersOperations.Insert(0, text);
            foreach (string element in usersOperations)
            {
                File.AppendAllText(path, element + Environment.NewLine, System.Text.Encoding.Default);
            }
        }
    }
}
