﻿CREATE TABLE [dbo].[UsersInfo] (
    [Id]             INT           NOT NULL,
    [Phone]          VARCHAR (12)  NULL,
    [Password]       VARCHAR (50)  NULL,
    [CardNumber]     VARCHAR (16)  NULL,
    [PIN]            VARCHAR (4)   NULL,
    [UserName]       NVARCHAR (20) NULL,
    [UserSurname]    NVARCHAR (20) NULL,
    [UserPatronymic] NVARCHAR (20) NULL,
    [Balance]        FLOAT (53)    NULL,
    [Email]          NVARCHAR (50) NULL,
	[Address]        NVARCHAR (50) NULL,
    [Theme]          INT           NULL,
    PRIMARY KEY CLUSTERED ([Id] ASC)
);

