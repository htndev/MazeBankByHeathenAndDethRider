﻿using System;
using System.Data.SqlClient;
using System.Data;

namespace AuthClass
{
    public class MoneyTransactions :  UserEntering
    {
        const string connectionString = @"Data Source=(LocalDB)\MSSQLLocalDB;AttachDbFilename=C:\Users\aleks\source\repos\MazeBank\MazeBank\MazeBankCLientBase.mdf;Integrated Security=True";

        /*---------------------------------------------*/
        /*------------------ОТПРАВИТЕЛЬ----------------*/
        /*---------------------------------------------*/

        public int operationSuccsessful = 0;

        /*---------------------------------------------*/
        /*------------------ПОЛУЧАТЕЛЬ-----------------*/
        /*---------------------------------------------*/

        protected decimal RecipientBalance;

        protected string RecipientName;

        protected string RecipientSurname;

        protected string RecipientPatronymic;

        protected string RecipientCard;

        protected string RecipientEmail;

        public string recipientCard
        {
            set
            {
                RecipientCard = value;
            }

            get
            {
                return RecipientCard;
            }
        }

        public string recipientName
        {
            set
            {
                RecipientName = value;
            }

            get
            {
                return RecipientName;
            }
        }

        public string recipientSurname
        {
            set
            {
                RecipientSurname = value;
            }

            get
            {
                return RecipientSurname;
            }
        }

        public string recipientPatronymic
        {
            set
            {
                RecipientPatronymic = value;
            }

            get
            {
                return RecipientPatronymic;
            }
        }

        public decimal recipientBalance
        {
            set
            {
                RecipientBalance = value;
            }

            get
            {
                return RecipientBalance;
            }
        }

        public string recipientEmail
        {
            set
            {
                RecipientEmail = value;
            }

            get
            {
                return RecipientEmail;
            }
        }

        /*------------------------------------------------*/
        /*---------------------МЕТОДЫ---------------------*/
        /*------------------------------------------------*/

        public void UserTransfer(long login, decimal amount)
        {
            string query = "Update UsersInfo set Balance = @userBalance where Phone = @login";
            using (SqlConnection connection = new SqlConnection(connectionString))
            {
                SqlCommand userCommand = new SqlCommand(query, connection);
                userCommand.Parameters.Add("@userBalance", SqlDbType.Decimal).Value = amount;
                userCommand.Parameters.Add("@login", SqlDbType.BigInt).Value = login;
                connection.Open();
                userCommand.ExecuteNonQuery();
            }
        }

        public void RecipientTransfer(string recipientCard, decimal amount)
        {
            string query = "Update UsersInfo set Balance = @recipientBalance where CardNumber = '" + recipientCard + "'";
            using (SqlConnection connection = new SqlConnection(connectionString))
            {
                SqlCommand recipientCommand = new SqlCommand(query, connection);
                recipientCommand.Parameters.Add("@recipientBalance", SqlDbType.Decimal).Value = amount;
                connection.Open();
                recipientCommand.ExecuteNonQuery();
            }
        }

        public void SenderLoading(long login)
        {
            string query = "Select [UserName], [UserSurname], [UserPatronymic], [Balance], [CardNumber], [Password], [Theme], [Address], [Email] from UsersInfo where Phone = '" + login + "'";
            using (SqlConnection Connection = new SqlConnection(connectionString))
            {
                SqlCommand Command = new SqlCommand(query, Connection);
                SqlDataReader Reader;
                Connection.Open();
                Reader = Command.ExecuteReader();
                while (Reader.Read())
                {
                    userPassword = Reader["Password"].ToString();
                    userName = Reader["UserName"].ToString();
                    userSurname = Reader["UserSurname"].ToString();
                    userPatronymic = Reader["UserPatronymic"].ToString();
                    userCard = Reader["CardNumber"].ToString();
                    userBalance = decimal.Parse(Reader["Balance"].ToString());
                    theme = Boolean.Parse(Reader["Theme"].ToString());
                    userAddress = Reader["Address"].ToString();
                    userEmail = Reader["Email"].ToString();
                }
            }
        }

        public void RecipientLoading(string recipientCard)
        {
            string query = "Select [UserName], [UserSurname], [UserPatronymic], [Balance], [Email] from UsersInfo where CardNumber = '" + recipientCard + "'";
            using (SqlConnection Connection = new SqlConnection(connectionString))
            {
                SqlCommand Command = new SqlCommand(query, Connection);
                SqlDataReader Reader;
                Connection.Open();
                Reader = Command.ExecuteReader();
                while (Reader.Read())
                {
                    operationSuccsessful++;
                    recipientName = Reader["UserName"].ToString();
                    recipientSurname = Reader["UserSurname"].ToString();
                    recipientPatronymic = Reader["UserPatronymic"].ToString();
                    recipientBalance = decimal.Parse(Reader["Balance"].ToString());
                    recipientEmail = Reader["Email"].ToString();
                }
            }
        }

        static public decimal CalculateCommittee(decimal user, int percent)
        {
            return Math.Round((user * percent / 100), 2);
        }

        static public decimal CalculateCommittee(decimal user, double percent)
        {
            return Math.Round((user * (decimal)percent / 100), 2);
        }
    }
}
