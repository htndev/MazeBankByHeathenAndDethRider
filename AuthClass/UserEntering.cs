﻿using System;
using System.Data.SqlClient;
using System.Windows.Forms;

namespace AuthClass
{
    public class UserEntering
    {
        const string connectionString = @"Data Source=(LocalDB)\MSSQLLocalDB;AttachDbFilename=C:\Users\aleks\source\repos\MazeBank\MazeBank\MazeBankCLientBase.mdf;Integrated Security=True";

        protected decimal UserBalance;

        protected string UserName;

        protected string UserSurname;

        protected string UserPatronymic;

        protected string UserPassword;

        protected string UserCard;

        protected bool Theme;

        protected string UserAddress;

        protected long UserPhone;

        protected string UserEmail;

        public decimal userBalance
        {
            set
            {
                UserBalance = value;
            }

            get
            {
                return UserBalance;
            }
        }

        public string userName
        {
            set
            {
                UserName = value;
            }

            get
            {
                return UserName;
            }
        }

        public string userPatronymic
        {
            set
            {
                UserPatronymic = value;
            }

            get
            {
                return UserPatronymic;
            }
        }

        public string userSurname
        {
            set
            {
                UserSurname = value;
            }

            get
            {
                return UserSurname;
            }
        }

        public string userPassword
        {
            set
            {
                UserPassword = value;
            }

            get
            {
                return UserPassword;
            }
        }

        public string userCard
        {
            set
            {
                UserCard = value;
            }

            get
            {
                return UserCard;
            }
        }

        public bool theme
        {
            set
            {
                Theme = value;
            }

            get
            {
                return Theme;
            }
        }

        public string userAddress
        {
            set
            {
                UserAddress = value;
            }

            get
            {
                return UserAddress;
            }
        }

        public long userPhone
        {
            set
            {
                UserPhone = value;
            }

            get
            {
                return UserPhone;
            }
        }

        public string userEmail
        {
            set
            {
                UserEmail = value;
            }

            get
            {
                return UserEmail;
            }
        }

        protected int Id;

        public int id
        {
            set
            {
                Id = value;
            }

            get
            {
                return Id;
            }
        }

        public void UserConnection(long login)
        {
            string query = "Select [UserName], [UserSurname], [UserPatronymic], [Balance], [Password], [CardNumber], [Theme], [Address], [Email] from UsersInfo where Phone = '" + login + "'";
            using (SqlConnection Connection = new SqlConnection(connectionString))
            {
                SqlCommand Command = new SqlCommand(query, Connection);
                SqlDataReader Reader;
                Connection.Open();
                Reader = Command.ExecuteReader();
                while (Reader.Read())
                {
                    userPassword = Reader["Password"].ToString();
                    userName = Reader["UserName"].ToString();
                    userSurname = Reader["UserSurname"].ToString();
                    userCard = Reader["CardNumber"].ToString();
                    userPatronymic = Reader["UserPatronymic"].ToString();
                    userBalance = decimal.Parse(Reader["Balance"].ToString());
                    theme = Boolean.Parse(Reader["Theme"].ToString());
                    userAddress= Reader["Address"].ToString();
                    userEmail = Reader["Email"].ToString();
                }
            }
        }

        public int counter;

        public virtual void UserExist(long login)
        {
            counter = 1;
            string query = "Select [Phone] from UsersInfo where Phone = '" + login +"'";
            using (SqlConnection Connection = new SqlConnection(connectionString))
            {
                SqlCommand Command = new SqlCommand(query, Connection);
                SqlDataReader Reader;
                Connection.Open();
                Reader = Command.ExecuteReader();
                while (Reader.Read())
                {
                    
                    counter = 0;
                }
            }
        }

        public int cardCounter;

        public virtual void UserExist(string card)
        {
            cardCounter = 1;
            string query = "Select [CardNumber] from UsersInfo where CardNumber = '" + card + "'";
            using (SqlConnection Connection = new SqlConnection(connectionString))
            {
                SqlCommand Command = new SqlCommand(query, Connection);
                SqlDataReader Reader;
                Connection.Open();
                Reader = Command.ExecuteReader();
                while (Reader.Read())
                {

                    cardCounter = 0;
                }
            }
        }

        public virtual void UserAuthing(long login, string pass)
        {
            string query = "Select [Phone], [Password] from UsersInfo where Phone = '" + login + "' and Password = '" + pass + "'";
            using (SqlConnection Connection = new SqlConnection(connectionString))
            {
                SqlCommand Command = new SqlCommand(query, Connection);
                SqlDataReader Reader;
                Connection.Open();
                Reader = Command.ExecuteReader();
                while (Reader.Read())
                {
                    counter++;
                }
            }
        }

        public virtual void UserAuthing(long login)
        {
            string query = "Select [Phone] from UsersInfo where Phone = '" + login + "'";
            using (SqlConnection Connection = new SqlConnection(connectionString))
            {
                SqlCommand Command = new SqlCommand(query, Connection);
                SqlDataReader Reader;
                Connection.Open();
                Reader = Command.ExecuteReader();
                while (Reader.Read())
                {
                    counter++;
                }
            }
        }

        public virtual void UpdateUserInfo(string newPass, long login)
        {
            string query = "Update UsersInfo set Password = '" + newPass + "' where Phone = '" + login + "'";
            using (SqlConnection connection = new SqlConnection(connectionString))
            {
                connection.Open();
                SqlCommand Command = new SqlCommand(query, connection);
                Command.ExecuteNonQuery();
            }
        }

        public virtual void UpdateUserInfo(short newPin, long login)
        {
            string query = "Update UsersInfo set PIN = '" + newPin + "' where Phone = '" + login + "'";
            using (SqlConnection connection = new SqlConnection(connectionString))
            {
                connection.Open();
                SqlCommand Command = new SqlCommand(query, connection);
                Command.ExecuteNonQuery();
            }
        }

        public virtual void UpdateUserInfo(string newEmail, string login)
        {
            string query = "Update UsersInfo set Email = '" + newEmail + "' where Phone = '" + login + "'";
            using (SqlConnection connection = new SqlConnection(connectionString))
            {
                connection.Open();
                SqlCommand Command = new SqlCommand(query, connection);
                Command.ExecuteNonQuery();
            }
        }

        public virtual void UpdateUserInfo(long newPhone, long login)
        {
            string query = "Update UsersInfo set Phone = '" + newPhone + "' where Phone = '" + login + "'";
            using (SqlConnection connection = new SqlConnection(connectionString))
            {
                connection.Open();
                SqlCommand Command = new SqlCommand(query, connection);
                Command.ExecuteNonQuery();
            }
        }

        public virtual void UpdateUserInfo(bool theme, long login)
        {
            string query = "Update UsersInfo set Theme = '" + theme + "' where Phone = '" + login + "'";
            using (SqlConnection connection = new SqlConnection(connectionString))
            {
                connection.Open();
                SqlCommand Command = new SqlCommand(query, connection);
                Command.ExecuteNonQuery();
            }
        }

        public virtual void NewUser(string name, string surname, string patronymic, string card, string email, long phone, string address, string password, int pin)
        {
            int id;
            using (SqlConnection connection = new SqlConnection(connectionString))
            {
                SqlCommand cmdzs = new SqlCommand("SELECT MAX (Id) as max_Id FROM UsersInfo", connection);
                connection.Open();
                id = (int)cmdzs.ExecuteScalar();
            }
            string query = "INSERT into UsersInfo (Id, Phone, Password, CardNumber, PIN, UserName, UserSurname, UserPatronymic, Balance, Address, Theme, Email) VALUES (@id, @phone, @password, @card, @pin, @name, @surname, @patronymic, @balance, @address, @theme, @email); SELECT CAST(scope_identity() AS int)";
            using (SqlConnection connection = new SqlConnection(connectionString))
            {
                SqlCommand Command = new SqlCommand(query, connection);
                Command.Parameters.Add("@phone", System.Data.SqlDbType.VarChar).Value = phone;
                Command.Parameters.Add("@password", System.Data.SqlDbType.VarChar).Value = password;
                Command.Parameters.Add("@card", System.Data.SqlDbType.VarChar).Value = card;
                Command.Parameters.Add("@pin", System.Data.SqlDbType.VarChar).Value = pin;
                Command.Parameters.Add("@name", System.Data.SqlDbType.NVarChar).Value = name;
                Command.Parameters.Add("@surname", System.Data.SqlDbType.NVarChar).Value = surname;
                Command.Parameters.Add("@patronymic", System.Data.SqlDbType.NVarChar).Value = patronymic;
                Command.Parameters.Add("@address", System.Data.SqlDbType.NVarChar).Value = address;
                Command.Parameters.Add("@email", System.Data.SqlDbType.NVarChar).Value = email;
                Command.Parameters.Add("@theme", System.Data.SqlDbType.Bit).Value = false;
                Command.Parameters.Add("@balance", System.Data.SqlDbType.Decimal).Value = 0;
                Command.Parameters.Add("@id", System.Data.SqlDbType.Int).Value = id+1;
                connection.Open();
                Command.ExecuteNonQuery();
            }
        }
        
    }
}
