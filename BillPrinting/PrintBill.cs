﻿using System;
using System.Text.RegularExpressions;
using Microsoft.Office.Interop.Word;
using System.Windows.Forms;
using System.Collections.Generic;
using MailNotify;
using System.Drawing.Printing;
using System.IO;

namespace BillPrinting
{
    public class PrintBill
    {

        #region Шаблоны документов

        private static readonly string TemplateMoneyTransferSrc = @"C:\Users\aleks\source\repos\MazeBank\Templates\Transfer.docx";

        private static readonly string TemplatePhoneRechargeSrc = @"C:\Users\aleks\source\repos\MazeBank\Templates\PhoneRecharge.docx";

        private static readonly string TemplateUtilitiesSrc = @"C:\Users\aleks\source\repos\MazeBank\Templates\Utilities.docx";

        private static readonly string TemplateNetAndTVSrc = @"C:\Users\aleks\source\repos\MazeBank\Templates\TVnInternet.docx";

        private static readonly string TemplateInternetPaymentsSrc = @"C:\Users\aleks\source\repos\MazeBank\Templates\InternetPayments.docx";

        private static readonly string TemplateOneWaySrc = @"C:\Users\aleks\source\repos\MazeBank\Templates\TicketOneWay.docx";

        private static readonly string TemplateTwoWaysSrc = @"C:\Users\aleks\source\repos\MazeBank\Templates\TicketTwoWays.docx";

        private static readonly string TemplateCinemaSrc = @"C:\Users\aleks\source\repos\MazeBank\Templates\Movie.docx";

        private static readonly string TemplateGamesSrc = @"C:\Users\aleks\source\repos\MazeBank\Templates\Games.docx";

        private static readonly string TemplateHistorySrc = @"C:\Users\aleks\source\repos\MazeBank\Templates\History.docx";

        private static readonly string TemplateFineSrc = @"C:\Users\aleks\source\repos\MazeBank\Templates\Fine.docx";

        private static readonly string TemplateSave = @"C:\Users\aleks\source\repos\MazeBank\Bills\";

        #endregion Шаблоны документов

        static Microsoft.Office.Interop.Word.Application wordApp = new Microsoft.Office.Interop.Word.Application();

        static string[] providersInternetTV = { "Kyivstar", "DKS", "Freenet", "VIASAT", "Ozon" };

        static string[] providers = { "Kyivstar", "Vodafone", "lifecell" };

        private static StreamReader streamToPrint;

        public static void Printing(string filePath)
        {
            try
            {
                streamToPrint = new StreamReader(filePath);
                try
                {
                    PrintDocument pd = new PrintDocument();
                    pd.PrintPage += new PrintPageEventHandler(pd_PrintPage);
                    pd.Print();
                }
                catch (Exception)
                {
                    MessageBox.Show("К сожалению, принтер не обнаружен.", "Ошибка печати", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
                finally
                {
                    streamToPrint.Close();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private static void pd_PrintPage(object sender, PrintPageEventArgs e)
        {
            throw new NotImplementedException();
        }

        /*-------------ПЕЧАТЬ ЧЕКА ОПЛАТЫ ШТРАФОВ-------------*/
        public static void PrintingBill(string intruder, decimal amount, decimal committee, string fineId, string card, string intruderEmail)
        {
            wordApp.Visible = false;
            var bill = wordApp.Documents.Open(TemplateFineSrc);
            Random rndNum = new Random();
            Regex rgx = new Regex(@"\d{12}");
            BillPreparing("{date}", DateTime.Today.ToString("d"), bill);
            BillPreparing("{time}", DateTime.Now.ToString($"{DateTime.Now:T}"), bill);
            BillPreparing("{card}", rgx.Replace(card.ToString(), "**** "), bill);
            BillPreparing("{intruder}", intruder, bill);            
            BillPreparing("{amount}", amount.ToString(), bill);
            BillPreparing("{committee}", committee.ToString(), bill);
            BillPreparing("{fineId}", fineId, bill);
            BillPreparing("{email}", intruderEmail, bill);
            string title = TemplateSave + $"Fine payment for {intruder} for fine #{fineId} #{rndNum.Next(100000, 999999)}.pdf";
            bill.ExportAsFixedFormat(title, WdExportFormat.wdExportFormatPDF);
            bill.Close(WdSaveOptions.wdDoNotSaveChanges);
            Printing(title);
            MailDelivery.NotifyIntruder(intruder, intruderEmail, amount, fineId);
            System.Diagnostics.Process.Start(title);
        }

        /*-------------СОЗДАНИЕ ЧЕКА ОПЛАТЫ ШТРАФА-------------*/
        public static void CreatingBill(string intruder, decimal amount, decimal committee, string fineId, string card, string intruderEmail)
        {
            wordApp.Visible = false;
            var bill = wordApp.Documents.Open(TemplateFineSrc);
            Random rndNum = new Random();
            Regex rgx = new Regex(@"\d{12}");
            BillPreparing("{date}", DateTime.Today.ToString("d"), bill);
            BillPreparing("{time}", DateTime.Now.ToString($"{DateTime.Now:T}"), bill);
            BillPreparing("{card}", rgx.Replace(card.ToString(), "**** "), bill);
            BillPreparing("{intruder}", intruder, bill);
            BillPreparing("{amount}", amount.ToString(), bill);
            BillPreparing("{committee}", committee.ToString(), bill);
            BillPreparing("{fineId}", fineId, bill);
            BillPreparing("{email}", intruderEmail, bill);
            string title = TemplateSave + $"Fine payment for {intruder} for fine #{fineId} #{rndNum.Next(100000, 999999)}.pdf";
            bill.ExportAsFixedFormat(title, WdExportFormat.wdExportFormatPDF);
            bill.Close(WdSaveOptions.wdDoNotSaveChanges);
            MailDelivery.NotifyIntruder(intruder, intruderEmail, amount, fineId);
        }

        /*-------------ПЕЧАТЬ БИЛЕТОВ В КИНО-------------*/
        public static void PrintingBill(string path, string userName, string userSurname, string userPatronymic, decimal amount, decimal sale, ListBox listBox, string movieTitle, string date, string userEmail)
        {
            wordApp.Visible = false;
            List<string> attachments = new List<string>();
            Random rndNum = new Random();
            foreach (string sit in listBox.Items)
            {
                var bill = wordApp.Documents.Open(TemplateCinemaSrc);
                BillPreparing("{date}", DateTime.Today.ToString("d"), bill);
                BillPreparing("{time}", DateTime.Now.ToString($"{DateTime.Now:T}"), bill);
                BillPreparing("{amount}", amount.ToString(), bill);
                BillPreparing("{committee}", sale.ToString(), bill);
                BillPreparing("{name}", userName, bill);
                BillPreparing("{surname}", userSurname, bill);
                BillPreparing("{patronymic}", userPatronymic, bill);
                BillPreparing("{movie}", movieTitle, bill);
                BillPreparing("{movieDate}", date, bill);
                BillPreparing("{sit}", sit, bill);
                string title = $@"{path}\Cinema ticket {movieTitle.Replace(":", "")} sit {sit.Replace(":", "")} #{rndNum.Next(100000, 999999)}.pdf";
                string titleForList = title;
                bill.ExportAsFixedFormat(title, WdExportFormat.wdExportFormatPDF);
                bill.Close(WdSaveOptions.wdDoNotSaveChanges);
                attachments.Add(titleForList);
            }
            MailDelivery.SendTickets(userName, userSurname, userEmail, amount * listBox.Items.Count, attachments, movieTitle);
        }

        /*-------------ПЕЧАТЬ БИЛЕТОВ НА ТРАНСПОРТ В ОДИН КОНЕЦ-------------*/
        public static void PrintingBill(string savePath, string service, string userName, string userSurname, string userPatronymic, decimal amount, decimal committee, string departPlace, string arrivalPlace, string startDate, ListBox listBox, string serviceForMail, string smile, string userEmail)
        {
            wordApp.Visible = false;
            Random rndNum = new Random();
            List<string> attachments = new List<string>();
            foreach (string sit in listBox.Items)
            {
                var bill = wordApp.Documents.Open(TemplateOneWaySrc);
                BillPreparing("{date}", DateTime.Today.ToString("d"), bill);
                BillPreparing("{time}", DateTime.Now.ToString($"{DateTime.Now:T}"), bill);
                BillPreparing("{amount}", amount.ToString(), bill);
                BillPreparing("{committee}", committee.ToString(), bill);
                BillPreparing("{name}", userName, bill);
                BillPreparing("{surname}", userSurname, bill);
                BillPreparing("{patronymic}", userPatronymic, bill);
                BillPreparing("{departPlace}", departPlace, bill);
                BillPreparing("{arrivalPlace}", arrivalPlace, bill);
                BillPreparing("{startDate}", startDate, bill);
                BillPreparing("{sit}", sit, bill);
                BillPreparing("{service}", service, bill);
                string title = $@"{savePath}\Ticket from {departPlace} to {arrivalPlace} sit {sit} #{rndNum.Next(100000, 999999)}.pdf";
                string titleForList = title;
                bill.ExportAsFixedFormat(title, WdExportFormat.wdExportFormatPDF);
                bill.Close(WdSaveOptions.wdDoNotSaveChanges);
                attachments.Add(titleForList);
            }
            try
            {
                MailDelivery.SendTickets(userName, userSurname, userPatronymic, serviceForMail, userEmail, smile, amount * listBox.Items.Count, attachments);
            }
            catch(Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        /*-------------ПЕЧАТЬ БИЛЕТОВ НА ТРАНСПОРТ В ДВА КОНЦА-------------*/
        public static void PrintingBill(string path, string service, string serviceForMail, string userName, string userSurname, string userPatronymic, decimal amount, decimal committee, string departPlace, string arrivalPlace, string startDate, string backDate, ListBox listBox, string userEmail, string smile)
        {
            wordApp.Visible = false;
            Random rndNum = new Random();
            List<string> attachments = new List<string>();
            foreach (string sit in listBox.Items)
            {
                var bill = wordApp.Documents.Open(TemplateTwoWaysSrc);
                BillPreparing("{date}", DateTime.Today.ToString("d"), bill);
                BillPreparing("{time}", DateTime.Now.ToString($"{DateTime.Now:T}"), bill);
                BillPreparing("{amount}", amount.ToString(), bill);
                BillPreparing("{committee}", committee.ToString(), bill);
                BillPreparing("{name}", userName, bill);
                BillPreparing("{surname}", userSurname, bill);
                BillPreparing("{patronymic}", userPatronymic, bill);
                BillPreparing("{departPlace}", departPlace, bill);
                BillPreparing("{arrivalPlace}", arrivalPlace, bill);
                BillPreparing("{startDate}", startDate, bill);
                BillPreparing("{backDate}", backDate, bill);
                BillPreparing("{sit}", sit, bill);
                BillPreparing("{service}", service, bill);
                string title = $@"{path}\Ticket from {departPlace} to {arrivalPlace} sit {sit} #{rndNum.Next(100000, 999999)}.pdf";
                string titleForList = title;
                bill.ExportAsFixedFormat(title, WdExportFormat.wdExportFormatPDF);
                bill.Close();
                attachments.Add(titleForList);
            }
            MailDelivery.SendTickets(userName, userSurname, userPatronymic, serviceForMail, userEmail, smile, amount * listBox.Items.Count, attachments);
        }

        /*-------------ПЕЧАТЬ ЧЕКА ИГРОВЫХ СЕРВСИОВ-------------*/
        public static void PrintingBill(string service, string userName, string userSurname, string userPatronymic, string userCard, decimal amount, decimal committee, string id, string reciever)
        {
            wordApp.Visible = false;
            var bill = wordApp.Documents.Open(TemplateGamesSrc);
            Random rndNum = new Random();
            Regex rgx = new Regex(@"\d{12}");
            BillPreparing("{service}", service, bill);
            BillPreparing("{date}", DateTime.Today.ToString("d"), bill);
            BillPreparing("{time}", DateTime.Now.ToString($"{DateTime.Now:T}"), bill);
            BillPreparing("{card}", rgx.Replace(userCard.ToString(), "**** "), bill);
            BillPreparing("{name}", userName, bill);
            BillPreparing("{surname}", userSurname, bill);
            BillPreparing("{patronymic}", userPatronymic, bill);
            BillPreparing("{amount}", amount.ToString(), bill);
            BillPreparing("{committee}", committee.ToString(), bill);
            BillPreparing("{id}", id, bill);
            BillPreparing("{reciever}", reciever, bill);
            string title = TemplateSave + $"Transaction From {userName} for Gaming payment {service} account {id} #{rndNum.Next(100000, 999999)}.pdf";
            bill.ExportAsFixedFormat(title, WdExportFormat.wdExportFormatPDF);
            bill.Close(WdSaveOptions.wdDoNotSaveChanges);
            Printing(title);
            System.Diagnostics.Process.Start(title);
        }

        /*-------------СОЗДАНИЕ ЧЕКА ИГРОВЫХ СЕРВСИОВ-------------*/
        public static void CreatingBill(string service, string userName, string userSurname, string userPatronymic, string userCard, decimal amount, decimal committee, string id, string reciever)
        {
            wordApp.Visible = false;
            var bill = wordApp.Documents.Open(TemplateGamesSrc);
            Random rndNum = new Random();
            Regex rgx = new Regex(@"\d{12}");
            BillPreparing("{service}", service, bill);
            BillPreparing("{date}", DateTime.Today.ToString("d"), bill);
            BillPreparing("{time}", DateTime.Now.ToString($"{DateTime.Now:T}"), bill);
            BillPreparing("{card}", rgx.Replace(userCard.ToString(), "**** "), bill);
            BillPreparing("{name}", userName, bill);
            BillPreparing("{surname}", userSurname, bill);
            BillPreparing("{patronymic}", userPatronymic, bill);
            BillPreparing("{amount}", amount.ToString(), bill);
            BillPreparing("{committee}", committee.ToString(), bill);
            BillPreparing("{id}", id, bill);
            BillPreparing("{reciever}", reciever, bill);
            string title = TemplateSave + $"Transaction From {userName} for Gaming payment {service} account {id} #{rndNum.Next(100000, 999999)}.pdf";
            bill.ExportAsFixedFormat(title, WdExportFormat.wdExportFormatPDF);
            bill.Close(WdSaveOptions.wdDoNotSaveChanges);
        }

        /*-------------ПЕЧАТЬ ЧЕКА ОНЛАЙН ПЛАТЕЖНЫХ СЕРВСИОВ-------------*/
        public static void PrintingBill(string service, string userName, string userSurname, string userPatronymic, string userCard, decimal amount, decimal committee, string id)
        {
            wordApp.Visible = false;
            var bill = wordApp.Documents.Open(TemplateInternetPaymentsSrc);
            Random rndNum = new Random();
            Regex rgx = new Regex(@"\d{12}");
            BillPreparing("{service}", service, bill);
            BillPreparing("{date}", DateTime.Today.ToString("d"), bill);
            BillPreparing("{time}", DateTime.Now.ToString($"{DateTime.Now:T}"), bill);
            BillPreparing("{card}", rgx.Replace(userCard.ToString(), "**** "), bill);
            BillPreparing("{name}", userName, bill);
            BillPreparing("{surname}", userSurname, bill);
            BillPreparing("{patronymic}", userPatronymic, bill);
            BillPreparing("{amount}", amount.ToString(), bill);
            BillPreparing("{committee}", committee.ToString(), bill);
            BillPreparing("{id}", id, bill);
            string title = TemplateSave + $"Transaction From {userName} for Internet payment {service} id#{id} #{rndNum.Next(100000, 999999)}.pdf";
            bill.ExportAsFixedFormat(title, WdExportFormat.wdExportFormatPDF);
            bill.Close(WdSaveOptions.wdDoNotSaveChanges);
            Printing(title);
            System.Diagnostics.Process.Start(title);
        }

        /*-------------СОЗДАНИЕ ЧЕКА ОНЛАЙН ПЛАТЕЖНЫХ СЕРВСИОВ-------------*/
        public static void CreatingBill(string service, string userName, string userSurname, string userPatronymic, string userCard, decimal amount, decimal committee, string id)
        {
            wordApp.Visible = false;
            var bill = wordApp.Documents.Open(TemplateInternetPaymentsSrc);
            Random rndNum = new Random();
            Regex rgx = new Regex(@"\d{12}");
            BillPreparing("{service}", service, bill);
            BillPreparing("{date}", DateTime.Today.ToString("d"), bill);
            BillPreparing("{time}", DateTime.Now.ToString($"{DateTime.Now:T}"), bill);
            BillPreparing("{card}", rgx.Replace(userCard.ToString(), "**** "), bill);
            BillPreparing("{name}", userName, bill);
            BillPreparing("{surname}", userSurname, bill);
            BillPreparing("{patronymic}", userPatronymic, bill);
            BillPreparing("{amount}", amount.ToString(), bill);
            BillPreparing("{committee}", committee.ToString(), bill);
            BillPreparing("{id}", id, bill);
            string title = TemplateSave + $"Transaction From {userName} for Internet payment {service} id#{id} #{rndNum.Next(100000, 999999)}.pdf";
            bill.ExportAsFixedFormat(title, WdExportFormat.wdExportFormatPDF);
            bill.Close(WdSaveOptions.wdDoNotSaveChanges);
            Printing(title);
        }

        /*-------------ПЕЧАТЬ ЧЕКА ЗА ТВ И ИНТЕРНЕТ-------------*/
        public static void PrintingBill(string userName, string userSurname, string userPatronymic, string userCard, decimal amount, decimal committee, string id)
        {
            wordApp.Visible = false;
            var bill = wordApp.Documents.Open(TemplateNetAndTVSrc);
            Random rndNum = new Random();
            Regex rgx = new Regex(@"\d{12}");
            BillPreparing("{date}", DateTime.Today.ToString("d"), bill);
            BillPreparing("{time}", DateTime.Now.ToString($"{DateTime.Now:T}"), bill);
            BillPreparing("{card}", rgx.Replace(userCard.ToString(), "**** "), bill);
            BillPreparing("{name}", userName, bill);
            BillPreparing("{surname}", userSurname, bill);
            BillPreparing("{patronymic}", userPatronymic, bill);
            BillPreparing("{amount}", amount.ToString(), bill);
            BillPreparing("{committee}", committee.ToString(), bill);
            BillPreparing("{id}", id, bill);
            BillPreparing("{providersInternetTV}", providersInternetTV[rndNum.Next(0, 4)].ToString(), bill);
            string title = TemplateSave + $"Transaction From {userName} for Internet and TV id#{id} #{rndNum.Next(100000, 999999)}.pdf";
            bill.ExportAsFixedFormat(title, WdExportFormat.wdExportFormatPDF);
            bill.Close(WdSaveOptions.wdDoNotSaveChanges);
            Printing(title);
            System.Diagnostics.Process.Start(title);
        }

        /*-------------СОЗДАНИЕ ЧЕКА ЗА ТВ И ИНТЕРНЕТ-------------*/
        public static void CreatingBill(string userName, string userSurname, string userPatronymic, string userCard, decimal amount, decimal committee, string id)
        {
            wordApp.Visible = false;
            var bill = wordApp.Documents.Open(TemplateNetAndTVSrc);
            Random rndNum = new Random();
            Regex rgx = new Regex(@"\d{12}");
            BillPreparing("{date}", DateTime.Today.ToString("d"), bill);
            BillPreparing("{time}", DateTime.Now.ToString($"{DateTime.Now:T}"), bill);
            BillPreparing("{card}", rgx.Replace(userCard.ToString(), "**** "), bill);
            BillPreparing("{name}", userName, bill);
            BillPreparing("{surname}", userSurname, bill);
            BillPreparing("{patronymic}", userPatronymic, bill);
            BillPreparing("{amount}", amount.ToString(), bill);
            BillPreparing("{committee}", committee.ToString(), bill);
            BillPreparing("{id}", id, bill);
            BillPreparing("{providersInternetTV}", providersInternetTV[rndNum.Next(0, 4)].ToString(), bill);
            string title = TemplateSave + $"Transaction From {userName} for Internet and TV id#{id} #{rndNum.Next(100000, 999999)}.pdf";
            bill.ExportAsFixedFormat(title, WdExportFormat.wdExportFormatPDF);
            bill.Close(WdSaveOptions.wdDoNotSaveChanges);
        }

        /*-------------ПЕЧАТЬ ЧЕКА ОТПРАВКИ ДЕНЕГ-------------*/
        static public void PrintingBill(string userName, string userSurname, string userPatronymic, decimal amount, string userCard, string recipientCard, string recipientName, decimal committee)
        {
            wordApp.Visible = false;
            var bill = wordApp.Documents.Open(TemplateMoneyTransferSrc);
            Random rndNum = new Random();
            Regex rgx = new Regex(@"\d{12}");
            BillPreparing("{date}", DateTime.Today.ToString("d"), bill);
            BillPreparing("{time}", DateTime.Now.ToString($"{DateTime.Now:T}"), bill);
            BillPreparing("{userCard}", rgx.Replace(userCard.ToString(), "**** "), bill);
            BillPreparing("{recipientCard}", rgx.Replace(recipientCard.ToString(), "**** "), bill);
            BillPreparing("{amount}", amount.ToString(), bill);
            BillPreparing("{committee}", committee.ToString(), bill);
            BillPreparing("{name}", userName, bill);
            BillPreparing("{patronymic}", userPatronymic, bill);
            BillPreparing("{surname}", userSurname, bill);
            string title = TemplateSave + $"Transaction From {userName} To {recipientName} #{rndNum.Next(100000, 999999)}.pdf";
            bill.ExportAsFixedFormat(title, WdExportFormat.wdExportFormatPDF);
            bill.Close(WdSaveOptions.wdDoNotSaveChanges);
            Printing(title);
            System.Diagnostics.Process.Start(title);
        }

        /*-------------СОЗДАНИЕ ЧЕКА ОТПРАВКИ ДЕНЕГ-------------*/
        public static void CreatingBill(string userName, string userSurname, string userPatronymic, decimal amount, string userCard, string recipientCard, string recipientName, decimal committee)
        {
            wordApp.Visible = false;
            var bill = wordApp.Documents.Open(TemplateMoneyTransferSrc);
            Random rndNum = new Random();
            Regex rgx = new Regex(@"\d{12}");
            BillPreparing("{date}", DateTime.Today.ToString("d"), bill);
            BillPreparing("{time}", DateTime.Now.ToString($"{DateTime.Now:T}"), bill);
            BillPreparing("{userCard}", rgx.Replace(userCard.ToString(), "**** "), bill);
            BillPreparing("{recipientCard}", rgx.Replace(recipientCard.ToString(), "**** "), bill);
            BillPreparing("{amount}", amount.ToString(), bill);
            BillPreparing("{committee}", committee.ToString(), bill);
            BillPreparing("{name}", userName, bill);
            BillPreparing("{patronymic}", userPatronymic, bill);
            BillPreparing("{surname}", userSurname, bill);
            string title = TemplateSave + $"Transaction From {userName} To {recipientName} #{rndNum.Next(100000, 999999)}.pdf";
            bill.ExportAsFixedFormat(title, WdExportFormat.wdExportFormatPDF);
            bill.Close(WdSaveOptions.wdDoNotSaveChanges);
        }

        /*-------------ПЕЧАТЬ ЧЕКА ПОПОЛНЕНИЕ ТЕЛЕФОНА-------------*/
        public static void PrintingBill(string userName, string userSurname, string userPatronymic, decimal amount, decimal committee, string phone)
        {
            wordApp.Visible = false;
            var bill = wordApp.Documents.Open(TemplatePhoneRechargeSrc);
            Random rndNum = new Random();
            BillPreparing("{date}", DateTime.Today.ToString("d"), bill);
            BillPreparing("{time}", DateTime.Now.ToString($"{DateTime.Now:T}"), bill);
            BillPreparing("{phone}", phone.ToString(), bill);
            BillPreparing("{amount}", amount.ToString(), bill);
            BillPreparing("{committee}", committee.ToString(), bill);
            BillPreparing("{name}", userName, bill);
            BillPreparing("{patronymic}", userPatronymic, bill);
            BillPreparing("{surname}", userSurname, bill);
            BillPreparing("{phone}", phone.ToString(), bill);
            BillPreparing("{provider}", providers[rndNum.Next(0, 2)].ToString(), bill);
            string title = TemplateSave + $"Transaction From {userName} for phone recharging number {phone} #{rndNum.Next(100000, 999999)}.pdf";
            bill.ExportAsFixedFormat(title, WdExportFormat.wdExportFormatPDF);
            bill.Close(WdSaveOptions.wdDoNotSaveChanges);
            Printing(title);
            System.Diagnostics.Process.Start(title);
        }

        /*-------------СОЗДАНИЕ ЧЕКА ПОПОЛНЕНИЕ ТЕЛЕФОНА-------------*/
        public static void CreatingBill(string userName, string userSurname, string userPatronymic, decimal amount, decimal committee, string phone)
        {
            wordApp.Visible = false;
            var bill = wordApp.Documents.Open(TemplatePhoneRechargeSrc);
            Random rndNum = new Random();
            Regex rgx = new Regex(@"\d{12}");
            BillPreparing("{date}", DateTime.Today.ToString("d"), bill);
            BillPreparing("{time}", DateTime.Now.ToString($"{DateTime.Now:T}"), bill);
            BillPreparing("{phone}", phone.ToString(), bill);
            BillPreparing("{amount}", amount.ToString(), bill);
            BillPreparing("{committee}", committee.ToString(), bill);
            BillPreparing("{name}", userName, bill);
            BillPreparing("{patronymic}", userPatronymic, bill);
            BillPreparing("{surname}", userSurname, bill);
            BillPreparing("{phone}", phone, bill);
            BillPreparing("{provider}", providers[rndNum.Next(0, 2)], bill);
            string title = TemplateSave + $"Transaction From {userName} for phone recharging number {phone} #{rndNum.Next(100000, 999999)}.pdf";
            bill.ExportAsFixedFormat(title, WdExportFormat.wdExportFormatPDF);
            bill.Close(WdSaveOptions.wdDoNotSaveChanges);
        }

        /*-------------СОЗДАНИЕ ЧЕКА ОПЛАТЫ ЖКХ УСЛУГ-------------*/
        public static void PrintingBill(string userName, string userSurname, string userPatronymic, decimal amount, decimal committee, double gaz, double water, double electricity, double trash, double flatPay)
        {
            wordApp.Visible = false;
            var bill = wordApp.Documents.Open(TemplateUtilitiesSrc);
            Random rndNum = new Random();
            BillPreparing("{date}", DateTime.Today.ToString("d"), bill);
            BillPreparing("{time}", DateTime.Now.ToString($"{DateTime.Now:T}"), bill);
            BillPreparing("{amount}", amount.ToString(), bill);
            BillPreparing("{committee}", committee.ToString(), bill);
            BillPreparing("{name}", userName, bill);
            BillPreparing("{patronymic}", userPatronymic, bill);
            BillPreparing("{surname}", userSurname, bill);
            BillPreparing("{gaz}", gaz.ToString(), bill);
            BillPreparing("{water}", water.ToString(), bill);
            BillPreparing("{electricity}", electricity.ToString(), bill);
            BillPreparing("{trash}", trash.ToString(), bill);
            BillPreparing("{flatPay}", flatPay.ToString(), bill);
            string title = TemplateSave + $"Transaction From {userName} for utilities #{rndNum.Next(100000, 999999)}.pdf";
            bill.ExportAsFixedFormat(title, WdExportFormat.wdExportFormatPDF);
            bill.Close(WdSaveOptions.wdDoNotSaveChanges);
            Printing(title);
            System.Diagnostics.Process.Start(title);
        }

        /*-------------СОЗДАНИЕ ЧЕКА ОПЛАТЫ ЖКХ УСЛУГ-------------*/
        public static void CreatingBill(string userName, string userSurname, string userPatronymic, decimal amount, decimal committee, double gaz, double water, double electricity, double trash, double flatPay)
        {
            wordApp.Visible = false;
            var bill = wordApp.Documents.Open(TemplateUtilitiesSrc);
            Random rndNum = new Random();
            BillPreparing("{date}", DateTime.Today.ToString("d"), bill);
            BillPreparing("{time}", DateTime.Now.ToString($"{DateTime.Now:T}"), bill);
            BillPreparing("{amount}", amount.ToString(), bill);
            BillPreparing("{committee}", committee.ToString(), bill);
            BillPreparing("{name}", userName, bill);
            BillPreparing("{patronymic}", userPatronymic, bill);
            BillPreparing("{surname}", userSurname, bill);
            BillPreparing("{gaz}", gaz.ToString(), bill);
            BillPreparing("{water}", water.ToString(), bill);
            BillPreparing("{electricity}", electricity.ToString(), bill);
            BillPreparing("{trash}", trash.ToString(), bill);
            BillPreparing("{flatPay}", flatPay.ToString(), bill);
            string title = TemplateSave + $"Transaction From {userName} for utilities #{rndNum.Next(100000, 999999)}.pdf";
            bill.ExportAsFixedFormat(title, WdExportFormat.wdExportFormatPDF);
            bill.Close(WdSaveOptions.wdDoNotSaveChanges);
        }

        /*-------------ПЕЧАТЬ ИСТОРИИ-------------*/
        public static void PrintingBill(string name, string surname, string patronymic, string card, ListBox list)
        {
            wordApp.Visible = false;
            var bill = wordApp.Documents.Open(TemplateHistorySrc);
            Random rndNum = new Random();
            BillPreparing("{date}", DateTime.Today.ToString("d") + " " + DateTime.Now.ToString($"{DateTime.Now:T}"), bill);
            BillPreparing("{ФИО}", name + " " + surname + " " + patronymic, bill);
            BillPreparing("{card}", card, bill);
            foreach(string elem in list.Items)
            {
                object rStart = 0;
                object rEnd = bill.Content.End;
                bill.Range(ref rStart, ref rEnd).InsertAfter(elem + Environment.NewLine);
            }
            string title = TemplateSave + $"History of banking operation {name} {patronymic} {surname} #{rndNum.Next(100000, 999999)}.pdf";
            bill.ExportAsFixedFormat(title, WdExportFormat.wdExportFormatPDF);
            bill.Close(WdSaveOptions.wdDoNotSaveChanges);
            Printing(title);
            System.Diagnostics.Process.Start(title);
        }

        /*-------------ЗАМЕНА ТЕКСТА-------------*/
        private static void BillPreparing(string stub, string text, Microsoft.Office.Interop.Word.Document wordDoc)
        {
            var range = wordDoc.Content;
            range.Find.ClearFormatting();
            range.Find.Execute(FindText: stub, ReplaceWith: text);
        }

    }
}
