﻿using System.Drawing;
using System.Windows.Forms;

namespace WinNotification
{
    public class WinNotify
    {
        static private readonly string iconLocation = @"C:\Users\aleks\source\repos\MazeBank\Pics\Logo.ico";

        public static void ShowWinNotify(string Title, string Text, int Duration)
        {
            Icon icon = new Icon(iconLocation);
            NotifyIcon notifyIcon = new NotifyIcon();
            notifyIcon.Icon = icon;
            notifyIcon.Visible = true;
            notifyIcon.BalloonTipTitle = Title;
            notifyIcon.BalloonTipText = Text;
            notifyIcon.ShowBalloonTip(Duration);
        }
    }
}
