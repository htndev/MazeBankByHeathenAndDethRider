﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Text;
using System.Threading.Tasks;

namespace MailNotify
{
    public class MailDelivery
    {
        static MailAddress mazeBankMail = new MailAddress("mazebank.mail@gmail.com", "Maze Bank");

        public static void NotifyByMail(string userEmail, string userName, string userSurname, string service, decimal amount, decimal userBalance)
        {
            MailAddress recieverMail = new MailAddress(userEmail, userName + " " + userSurname);

            using (MailMessage message = new MailMessage(mazeBankMail, recieverMail))
            using (SmtpClient smptClient = new SmtpClient())
            {
                message.Subject = "💳 Операция по счету";
                message.Body = $"Уважаемый {userName} {userSurname}!\nНа Вашем счету произошла операция {service}, которая обошлась Вам в {amount}₴.\nВаш баланс: {userBalance}₴.\nХорошего дня! 😃👍🏻";

                smptClient.Host = "smtp.gmail.com";
                smptClient.Port = 587;
                smptClient.EnableSsl = true;
                smptClient.DeliveryMethod = SmtpDeliveryMethod.Network;
                smptClient.UseDefaultCredentials = false;
                smptClient.Credentials = new NetworkCredential(mazeBankMail.Address, "sashatolik");

                smptClient.Send(message);
            }
        }

        public static void WelcomeMail(string userEmail, string userName, string userSurname, string card, string pin)
        {
            MailAddress recieverMail = new MailAddress(userEmail, userName + " " + userSurname);

            using (MailMessage message = new MailMessage(mazeBankMail, recieverMail))
            using (SmtpClient smptClient = new SmtpClient())
            {
                message.Subject = "🏦 Добро пожаловать в MazeBank";
                message.Body = $"Уважаемый {userName} {userSurname}!\nВы зарегистрировались в MazeBank.\nСпасибо, что выбрали нас!\nВаша карта: {card};\nВаш ПИН: {pin};\nНикому не сообщайте свои данные. Работники банка никогда не спрашивают Ваши данные.\nХорошего дня! 😃👍\nMazeBank Llc.";

                smptClient.Host = "smtp.gmail.com";
                smptClient.Port = 587;
                smptClient.EnableSsl = true;
                smptClient.DeliveryMethod = SmtpDeliveryMethod.Network;
                smptClient.UseDefaultCredentials = false;
                smptClient.Credentials = new NetworkCredential(mazeBankMail.Address, "sashatolik");

                smptClient.Send(message);
            }
        }

        public static void NotifyByMail(string userEmail, string userName, string userSurname, string userPatronymic, string recipientName, string recipientSurname, string recipientPatronymic, decimal amount, decimal userBalance)
        {
            MailAddress recieverMail = new MailAddress(userEmail, userName + " " + userSurname);

            using (MailMessage message = new MailMessage(mazeBankMail, recieverMail))
            using (SmtpClient smptClient = new SmtpClient())
            {
                message.Subject = "💳 Операция по счету";
                message.Body = $"Уважаемый {userName} {userSurname}!\nВы произвели операцию перевода средств для {recipientName} {recipientPatronymic} {recipientSurname} в размере {amount}₴.\nВаш баланс: {userBalance}₴.\nХорошего дня! 😃👍🏻";
                smptClient.Host = "smtp.gmail.com";
                smptClient.Port = 587;
                smptClient.EnableSsl = true;
                smptClient.DeliveryMethod = SmtpDeliveryMethod.Network;
                smptClient.UseDefaultCredentials = false;
                smptClient.Credentials = new NetworkCredential(mazeBankMail.Address, "sashatolik");

                smptClient.Send(message);
            }
        }

        public static void NotifyByMailRecep(decimal amount, decimal recipientBalance, string recipientName, string recipientSurname, string recipientEmail, string userSurname, string userPatronymic, string userName)
        {
            MailAddress recieverMail = new MailAddress(recipientEmail, userName + " " + userSurname);
            using (MailMessage message = new MailMessage(mazeBankMail, recieverMail))
            using (SmtpClient smptClient = new SmtpClient())
            {
                message.Subject = "💳 Операция по счету";
                message.Body = $"Уважаемый {recipientName} {recipientSurname}!\nНа Ваш счет поступил перевод сресдтв от {userName} {userPatronymic} {userSurname} в размере {amount}₴.\nВаш баланс: {recipientBalance}₴.\nХорошего дня! 😃👍🏻\n";
                smptClient.Host = "smtp.gmail.com";
                smptClient.Port = 587;
                smptClient.EnableSsl = true;
                smptClient.DeliveryMethod = SmtpDeliveryMethod.Network;
                smptClient.UseDefaultCredentials = false;
                smptClient.Credentials = new NetworkCredential(mazeBankMail.Address, "sashatolik");

                smptClient.Send(message);
            }
        }

        public static void SendTickets(string name, string surname, string patronymic, string typeOfTickets, string email, string smile, decimal amount, List<string> files)
        {
            MailAddress recieverMail = new MailAddress(email, name + " " + surname);

            using (MailMessage message = new MailMessage(mazeBankMail, recieverMail))
            using (SmtpClient smptClient = new SmtpClient())
            {
                message.Subject = "🎫 Покупка билетов";
                message.Body = $"Уважаемый {name} {surname}!\nВы преобрели {typeOfTickets} через приложение MazeBank в размере, покупка обошлась Вам в {amount}₴!\nСкачайте Ваши билеты и отправляйтесь в путешествие!{smile}\nЖелаем Вам приятной поездки. 😊👍🏻\nПокупайте с MazeBank и экономьте!";
                List<Attachment> attachments = new List<Attachment>();
                foreach(string elem in files)
                {
                    attachments.Add(new Attachment(elem));
                }
                foreach (Attachment att in attachments)
                {
                    message.Attachments.Add(att);
                }
                smptClient.Host = "smtp.gmail.com";
                smptClient.Port = 587;
                smptClient.EnableSsl = true;
                smptClient.DeliveryMethod = SmtpDeliveryMethod.Network;
                smptClient.UseDefaultCredentials = false;
                smptClient.Credentials = new NetworkCredential(mazeBankMail.Address, "sashatolik");
                
                smptClient.Send(message);
            }
        }

        public static void SendTickets(string name, string surname, string email, decimal amount, List<string> files, string movie)
        {
            MailAddress recieverMail = new MailAddress(email, name + " " + surname);

            using (MailMessage message = new MailMessage(mazeBankMail, recieverMail))
            using (SmtpClient smptClient = new SmtpClient())
            {
                message.Subject = "🎥 Покупка билетов";
                message.Body = $"Уважаемый {name} {surname}!\nВы преобрели билеты в кино на фильм «{movie}» через приложение MazeBank в размере, покупка обошлась Вам в {amount}₴!\nСкачайте Ваши билеты и отправляйтесь в кинотеар! 🎥\nЖелаем Вам приятного просмотра!. 😊👍🏻\nПокупайте с MazeBank и экономьте!";
                List<Attachment> attachments = new List<Attachment>();
                foreach (string elem in files)
                {
                    attachments.Add(new Attachment(elem));
                }
                foreach (Attachment att in attachments)
                {
                    message.Attachments.Add(att);
                }
                smptClient.Host = "smtp.gmail.com";
                smptClient.Port = 587;
                smptClient.EnableSsl = true;
                smptClient.DeliveryMethod = SmtpDeliveryMethod.Network;
                smptClient.UseDefaultCredentials = false;
                smptClient.Credentials = new NetworkCredential(mazeBankMail.Address, "sashatolik");

                smptClient.Send(message);
            }
        }

        public static void NotifyIntruder(string intruder, string email, decimal amount, string fineId)
        {
            MailAddress recieverMail = new MailAddress(email, intruder);

            using (MailMessage message = new MailMessage(mazeBankMail, recieverMail))
            using (SmtpClient smptClient = new SmtpClient())
            {
                message.Subject = "🚨 Оплата штрафов";
                message.Body = $"Уважаемый {intruder}!\nОплата штрафа {fineId} в размере {amount}₴ прошла успешно!\nБольше не нарушайте! 👮‍\nОплачивайте Ваши штрафы при помощи MazeBank.";
                smptClient.Host = "smtp.gmail.com";
                smptClient.Port = 587;
                smptClient.EnableSsl = true;
                smptClient.DeliveryMethod = SmtpDeliveryMethod.Network;
                smptClient.UseDefaultCredentials = false;
                smptClient.Credentials = new NetworkCredential(mazeBankMail.Address, "sashatolik");

                smptClient.Send(message);
            }
        }
    }
}
