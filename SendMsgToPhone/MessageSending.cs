﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Twilio.Rest.Api.V2010.Account;
using Twilio;
using Twilio.Types;

namespace SendMsgToPhone
{
    public class MessageSending
    {
        const string accountSid = "ACecac26a9296f0dc76c4e154e0272f8f9";

        const string authToken = "0c89989bb1f8b2963e0ec34b19eeb8de";
        public static void SendMessage(long phone, string operation, string name, decimal amount, decimal balance)
        {
            try
            {
                TwilioClient.Init(accountSid, authToken);

                MessageResource.Create(
                    to: new PhoneNumber("+" + phone.ToString()),
                    from: new PhoneNumber("+14092150852"),
                    body: $"Здраствуйте, {name}! На Ваш счету произведена операция {operation} в размере {amount}₴. Ваш баланс: {balance}₴.\nХорошего дня! 😊👍🏻\nMazeBank Inc.");
            }
            catch (Exception)
            {

            }
        }

        public static void SendMessage(long phone, string operation, string name, string recieverName, string recieverSurname, string recieverPatronymic, decimal amount, decimal balance)
        {
            try
            {
                TwilioClient.Init(accountSid, authToken);

                MessageResource.Create(
                    to: new PhoneNumber("+" + phone.ToString()),
                    from: new PhoneNumber("+14092150852"),
                    body: $"Здраствуйте, {name}! Вы совершили перевод для {recieverName} {recieverPatronymic} {recieverSurname} в размере {amount}₴. Ваш баланс: {balance}₴.\nХорошего дня! 😊👍🏻\nMazeBank Inc.");
            }

            catch (Exception)
            {

            }
        }
    }
}
