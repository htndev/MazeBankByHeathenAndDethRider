﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WhatsAppApi;

namespace MessagingClass
{
    public class WAMessaging
    {
        public static void SendMessage(long phone, string text, string name, decimal amount, decimal balance)
        {
            string from = "79821178920";
            string to = phone.ToString();
            string msg = "Здраствуйте, " + name + "!\nНа Вашем счету была проиведена операция " + text + "в размере " + amount + "₴.\nВаш баланс: " + balance + "₴.\nХорошего дня! 😊\n© MazeBank 2018";
            WhatsApp wa = new WhatsApp(from, "354390066603108", "Maze Bank", false, false);
            wa.OnConnectSuccess += () =>
            {
                wa.OnLoginSuccess += (phoneNumber, data) =>
                {
                    wa.SendMessage(to, msg);
                };

                wa.Login();
            };
        }
    }
}
