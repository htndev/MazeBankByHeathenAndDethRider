﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.IO;
using System.Net;


namespace CurrencyLibrary
{
    public class CurrencyExg
    {
        private readonly string usdLink = "https://kurs.com.ua/valyuta/usd/";

        private readonly string eurLink = "https://kurs.com.ua/valyuta/eur/";

        private readonly string gbpLink = "https://kurs.com.ua/valyuta/gbp/";

        public string buyUsd { private set; get; }
        public string sellUsd { private set; get; }
        public string buyEur { private set; get; }
        public string sellEur { private set; get; }
        public string buyGbp { private set; get; }
        public string sellGbp { private set; get; }

        protected double BuyUsd;

        protected double SellUsd;

        protected double BuyEur;

        protected double SellEur;

        protected double BuyGbp;

        protected double SellGbp;

        public double toBuyUsd
        {
            private set
            {
                BuyUsd = value;
            }

            get
            {
                return BuyUsd;
            }
        }

        public double toSellUsd
        {
            private set
            {
                SellUsd = value;
            }

            get
            {
                return SellUsd;
            }
        }

        public double toBuyEur
        {
            private set
            {
                BuyEur = value;
            }

            get
            {
                return BuyEur;
            }
        }

        public double toSellEur
        {
            private set
            {
                SellEur = value;
            }
            get
            {
                return SellEur;
            }
        }

        public double toBuyGbp
        {
            private set
            {
                BuyGbp = value;
            }
            get
            {
                return BuyGbp;
            }
        }

        public double toSellGbp
        {
            private set
            {
                SellGbp = value;
            }
            get
            {
                return SellGbp;
            }
        }

        public CurrencyExg()
        {
            if (System.Net.NetworkInformation.NetworkInterface.GetIsNetworkAvailable())
            {
                WebClient src = new WebClient();
                src.Encoding = Encoding.UTF8;
                string linkUsd = src.DownloadString(usdLink);
                string linkEur = src.DownloadString(eurLink);
                string linkGbp = src.DownloadString(gbpLink);
                File.WriteAllText("currency.txt", linkUsd);
                buyUsd = Regex.Match(linkUsd, @"<span class='ipsKurs_rate'>(\d+\,\d+)</span>").Groups[1].Value;
                toBuyUsd = Math.Round(double.Parse(buyUsd), 2);
                sellUsd = Regex.Match(linkUsd, @"<td class='ipsKursTable_rate ipsType_right' data-rate-type='ask' data-rate='(\d+\.\d+)").Groups[1].Value.Replace(".", ",");
                toSellUsd = Math.Round(double.Parse(sellUsd), 2);
                buyEur = Regex.Match(linkEur, @"<span class='ipsKurs_rate'>(\d+\,\d+)</span>").Groups[1].Value;
                toBuyEur = Math.Round(double.Parse(buyEur), 2);
                sellEur = Regex.Match(linkEur, @"<td class='ipsKursTable_rate ipsType_right' data-rate-type='ask' data-rate='(\d+\.\d+)").Groups[1].Value.Replace(".", ",");
                toSellEur = Math.Round(double.Parse(sellEur), 2);
                buyGbp = Regex.Match(linkGbp, @"<span class='ipsKurs_rate'>(\d+\,\d+)</span>").Groups[1].Value;
                toBuyGbp = Math.Round(double.Parse(buyGbp), 2);
                sellGbp = Regex.Match(linkGbp, @"<td class='ipsKursTable_rate ipsType_right' data-rate-type='ask' data-rate='(\d+\.\d+)").Groups[1].Value.Replace(".", ",");
                toSellGbp = Math.Round(double.Parse(sellGbp), 2);
            }
        }
    }
}